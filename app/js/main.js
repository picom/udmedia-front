'use strict';

var directionUp = false,
    scrolled_pixels = 0;

var pageXstate,
    is_stacked = true;

var filterHeight = 0,
    sidebarWidth,
    $activeNewsTopic,
    $progressbar = $('.flash__progressbar'),
    sidebar = $('.side-content'),
    sidebarSticky = $('.content__side .side-content'),
    $header = $('.head'),
    sidebarContainer,
    $headerMenuContainer = document.querySelector('.main-menu'),
    sidebarLeft;

var mfpTitle,
    mfpTotal;

$.extend(true, $.magnificPopup.defaults, {
    tClose: 'Закрыть (Esc)',
    tLoading: 'Загрузка...',
    gallery: {
        tPrev: 'Пред',
        tNext: 'След',
        tCounter: '%curr% из %total%'
    },
    image: {
        tError: 'Извините, <a href="%url%">изображение</a> не может быть загружено.'
    },
    ajax: {
        tError: 'Извините, <a href="%url%">содержимое</a> не может быть загружено.'
    }
});

document.addEventListener('touchstart', function () {
}, true);

function setDefaults() {
    directionUp = false;
    scrolled_pixels = 0;
    is_stacked = true;
    filterHeight = 0;
    $progressbar = $('.flash__progressbar');
    sidebar = $('.side-content');
    sidebarSticky = $('.content__side .side-content');
}

var $button = $('.functional-button-wrap');

function deviceType() {
    return window.getComputedStyle(document.querySelector('body'), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");
}

var MQ = deviceType(),
    bindToggle = false;

function checkScrollDirection() {
    $(window).on('scroll', function (e) {
        var st = $(this).scrollTop();
        if (st > scrolled_pixels) {
            directionUp = false;
        } else {
            directionUp = true;
        }
        scrolled_pixels = st;
    });
}

function setProjectsHeaderPosition() {
    var projectsHeader = document.querySelector('.sp-hero__inner');
    if (projectsHeader) {
        projectsHeader.classList.add('sp-hero__inner--animate');
    }
}

function toTop() {
    var to_topWidth;
    var current_top;
    var $to_top = $('.to-top-area');
    var $to_down = $('.to-down-area');
    $('body').on('click', '[data-js="scrollup"]', function () {
        current_top = $(window).scrollTop();
        $('html, body').animate({
            scrollTop: 0
        }, 0);
        $to_down.show();
    });
    $('body').on('click', '[data-js="scrolldown"]', function () {
        $('html, body').animate({
            scrollTop: current_top
        }, 0);
    });
    $(window).on('resize', function () {
        to_topWidth = $('.head__inner').offset().left;
        $to_top.css('width', to_topWidth);
        $to_down.css('width', to_topWidth);
    }).resize();
    $(window).on('scroll', function () {
        if (scrolled_pixels > $('.sp-hero').outerHeight()) {
            $to_down.hide();
            $to_top.show();
        }
        else if (($('.sp-hero').length == 0) && (scrolled_pixels > $header.outerHeight())) {
            $to_down.hide();
            $to_top.show();
        } else {
            $to_top.hide();
        }
    });
}

function initMobileMenu() {
    $('body').on('click', '[data-js="mobile-menu-toggler"]', function (e) {
        e.preventDefault();
        $(this).closest('.head__inner').find('.head__menu').toggleClass('_shown');
        $(this).closest('.site-flasher').find('.head__menu').toggleClass('_shown');
    });
}

var pinInited = false;
function pinHeader() {
    if ($header.length) {

        var setScrollAreaTop = function setScrollAreaTop() {
            if ($menu.hasClass("_visible") && $flash.hasClass("_visible")) {
                top = $flash.height() + $menu.height();
            } else if ($menu.hasClass("_visible") && $tabs.hasClass("_fixed")) {
                top = $tabs.height() + $menu.height();
            } else if ($menu.hasClass("_visible")) {
                top = $menu.height();
            } else if ($flash.hasClass("_visible")) {
                top = $flash.height();
            } else if ($tabs.hasClass("_fixed")) {
                top = $tabs.height();
            } else {
                top = 0;
            }
            // console.log($menu.height());
            if ($menu.hasClass("_visible") || $flash.hasClass("_visible")) {
                $scrollTopArea.addClass("menu_is_visible");
            } else {
                $scrollTopArea.removeClass("menu_is_visible");
            }
            $scrollTopArea.css("top", top);
        };

        var $menu = $('.head__menu_flasher');
        var $scrollTopArea = $('.to-top-area');
        var $tabs = $('.tabs-list');
        var scrolledConentPx, translateY;
        var $flash = $('.site-flasher').find('.flash');
        var top = 0;
        if(!pinInited){
            $(window).on('resize scroll', function () {

                // console.log('directionUp = ' + directionUp);

                if (directionUp && scrolled_pixels > $header.outerHeight() + $(window).height() / 3 && !$menu.hasClass('_visible')) {
                    $menu.addClass('_visible');
                    $tabs.addClass('_nav_is_visible');
                    setScrollAreaTop();
                } else if (!directionUp) {
                    if ($menu.hasClass('_visible')) {
                        $menu.removeClass('_visible');
                        $tabs.removeClass('_nav_is_visible');
                    }
                    setScrollAreaTop();
                }

                if (scrolled_pixels < $header.outerHeight() + $(window).height() / 3 && $menu.hasClass('_visible')) {
                    console.log('hide');
                    $menu.removeClass('_visible');
                    $tabs.removeClass('_nav_is_visible');
                    setScrollAreaTop();
                }

                if (!$('.content__inner').length) return;

                if (scrolled_pixels > $('.content__inner').first().offset().top) {
                    $flash.addClass('_visible');
                    setScrollAreaTop();
                } else {
                    $flash.removeClass('_visible');
                    setScrollAreaTop();
                }

                $activeNewsTopic = $('.content__inner').filter('._active');
                var offset;

                if ($activeNewsTopic.length) {
                    offset = scrolled_pixels - $activeNewsTopic.offset().top + $header.outerHeight() + $(window).height();
                    if (offset >= 0) {
                        scrolledConentPx = offset / $activeNewsTopic.outerHeight(true) * 100;
                        $progressbar.addClass('_visible');
                    }
                } else {
                    $progressbar.removeClass('_visible');
                }
            });
            pinInited = true;
        }
    }
}

function photogallery() {
    $('body').on('click', '[data-js="photogallery"]', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        var title = $(this).attr('title');
        console.log(link);
        var $scrollTop = $(window).scrollTop();
        $.magnificPopup.open({
            type: 'ajax',
            items: {
                src: link
            },
            removalDelay: 300,
            mainClass: 'mfp-fadein',
            preloader: true,
            callbacks: {
                elementParse: function elementParse(item) {
                    mfpTitle = title;
                },
                ajaxContentAdded: function ajaxContentAdded(item) {
                    mfpTotal = $('.gallery-scope__item').length;
                    $('.mfp-content__header-title').text(mfpTitle);
                    $('.mfp-content__header-total span').text(mfpTotal);
                    slideShow();
                },
                close: function close() {
                    mfpTitle = null;
                    mfpTotal = null;
                },
                afterClose: function () {
                    $(window).scrollTop($scrollTop);
                }
            }
        });
    });
}

function showVideoModal() {
    $('body').on('click', '[data-js="video"]', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        var title = $(this).attr('title');
        console.log(link);
        var $scrollTop = $(window).scrollTop();
        $.magnificPopup.open({
            callbacks: {
                afterClose: function () {
                    $(window).scrollTop($scrollTop);
                }
            },
            type: 'ajax',
            items: {
                src: link,
                type: 'iframe'
            },
            removalDelay: 300,
            mainClass: 'mfp-fadein',
            preloader: true,
            iframe: {
                markup:
                '<div class="video-scope__item">' +
                '<div class="mfp-content__header _video">' +
                '<h3 class="mfp-content__header-title">' + title + '</h3>' +
                '</div>' +
                '<button class="mfp-close"></button>' +
                '<div class="mfp-iframe-scaler">' +
                '<iframe class="mfp-iframe view-video__iframe" frameborder="0" allowfullscreen  style="border-style: none;width: 100%; height: 100%"></iframe>' +
                '</div>' +
                '</div>'
            }
        });
    });
}

function slideShow() {
    if (!$('[data-js="swipe-picture"]').length) return;
    $('[data-js="swipe-picture"]')
        .photoSwipe('[data-js="swipe-picture-item"]', {
            bgOpacity: 0.8,
            shareEl: false,
            history: false,
            closeOnScroll: false,
            clickToCloseNonZoomable: false
        }, {});
}

function onresizeWindow() {
    if ($('.content__side_view_list').length || $('.content__side_view_feed').length) {
        sidebarContainer = $('.side-content').parent();
        sidebarLeft = sidebarContainer.offset().left;
        sidebarWidth = sidebarContainer.innerWidth();
        if ($(window).width() > 992) {
            sidebar.css({
                'left': sidebarLeft,
                'width': sidebarWidth
            });
        } else {
            sidebar.css('width', 'auto');
        }
    }
}

function onscrollWindow() {
    var deltaHeight,
        calcSidebarHeight;
    if ($('.content__tabs').length) {
        deltaHeight = 54;
    } else {
        deltaHeight = 0;
    }
    if ($(window).width() > 992) {

        // Условие при отсутствии новостей, чтобы не ломалась верстка
        if ($('.news').length && $('.news').height() == 0) {
            console.log('height = 0');

            if (scrolled_pixels > $('.content__main').offset().top - deltaHeight) {
                if ($('.content__tabs').length) {
                    $('.content__tabs').addClass('_fixed');
                }
            } else {
                if ($('.content__tabs').length) {
                    $('.content__tabs').removeClass('_fixed');
                }
            }
        }
        // END Условие при отсутствии новостей, чтобы не ломалась верстка
        else if (scrolled_pixels > (sidebarContainer.outerHeight() + sidebarContainer.offset().top - $(window).height())) {
            calcSidebarHeight = $(window).height();
            sidebar.css({
                'position': 'absolute',
                'top': 'auto',
                'bottom': '0',
                'left': '0',
                'height': calcSidebarHeight
            });
        } else if (scrolled_pixels > $('.content__main').offset().top - deltaHeight) {
            calcSidebarHeight = $(window).height();
            sidebar.css({
                'position': 'fixed',
                'top': '0',
                'left': sidebarLeft,
                'width': sidebarWidth,
                'height': calcSidebarHeight,
                'padding-top': (filterHeight - 15)
            });
            if ($('.content__tabs').length) {
                $('.content__tabs').addClass('_fixed');
            }
        } else {
            sidebar.css({
                'position': 'static',
                'width': 'auto',
                'height': '100%',
                'padding': '0'
            });
            if ($('.content__tabs').length) {
                $('.content__tabs').removeClass('_fixed');
            }
        }
    }
}

function setCardTitleMargin() {
    if ($('.card-item__body-wrapper .card-item__title').length) {
        var cardTitle = $('.card-item__body-wrapper .card-item__title');
        if ($(window).width() > 800) {
            cardTitle.each(function () {
                $(this).css({
                    "margin-top": $(this).parent().height() - $(this).height(),
                    "align-self": "flex-start"
                });
                $(this).siblings().css("display", "flex");
            })
            cardTitle.css("transition", "margin-top .3s cubic-bezier(1,.01,.52,.47)");
        }
    }
}

function fixNewsActivate() {
    if ($('.content__side_view_list').length || $('.content__side_view_feed').length) {
        sidebarContainer = $('.side-content').parent();
        sidebarLeft = sidebarContainer.offset().left;
        // $('.site-flasher').hide();
        $('body, html').scrollTop($(window).scrollTop() + 1).scrollTop($(window).scrollTop() - 1);
        if ($('.content__tabs').length) {
            filterHeight = 54; //$('.content__tabs').outerHeight(true);
        }
        $(window).on('resize', function () {
            $(window).scroll();
            onresizeWindow();
        }).on('scroll', function () {
            onscrollWindow();
        });
    }
}

function startAgregator() {
    onresizeWindow();
    fixNewsActivate();
}

function toggleNewsView() {
    $('body').on('click', '[data-js="toggle-news"]', function (e) {
        e.preventDefault();
        $(this).toggleClass('_active');
    })
}

function toggleShareList() {
    $('body').on('click', '[data-js="share-toggle"]', function () {
        $(this).closest('.share-widget').find('.share-list-container').toggleClass('_active');
    });
}

function ajaxPopup() {
    var $scrollTop;
    $('[data-js="popup"]').magnificPopup({
        fixedContentPos: true,
        callbacks: {
            open: function () {
                $scrollTop = $(window).scrollTop();
            },
            afterClose: function () {
                $(window).scrollTop($scrollTop);
            }
        },
        type: 'ajax',
        closeOnContentClick: false,
        removalDelay: 300,
        mainClass: 'mfp-fadein',
        preloader: true
    });
}

function ajaxComments() {
    var $scrollTop = $(window).scrollTop();
    $('[data-js="comments"]').magnificPopup({
        fixedContentPos: true,
        callbacks: {
            afterClose: function () {
                $(window).scrollTop($scrollTop);
            }
        },
        type: 'ajax',
        closeOnContentClick: false,
        removalDelay: 400,
        mainClass: 'mfp-slidein',
        preloader: true
    });
}

$('body').on('click', '[data-js="comments"]', function (e) {
    e.preventDefault();
    var href = $(this).attr('href');
    var $scrollTop = $(window).scrollTop();
    $.magnificPopup.open({
        fixedContentPos: true,
        callbacks: {
            afterClose: function () {
                $(window).scrollTop($scrollTop);
            }
        },
        items: {
            src: href,
            type: 'ajax',
            closeOnContentClick: false,
            removalDelay: 400,
            mainClass: 'mfp-slidein',
            preloader: true
        }
    });
});


function checkButtonSide() {
    if ($(window).width() / 2 > $button.offset().left) {
        is_stacked = false;
        $button.addClass('_drop_right');
    } else {
        is_stacked = true;
        $button.removeClass('_drop_right');
    }
}

function toggleRubricator() {
    $('body').on('click', '[data-js="rubrics-menu"]', function (e) {
        e.preventDefault();
        $('.content__side').toggleClass('_shown');
    })
}

function bindEvents(MQ, bool) {
    if (MQ == 'desktop' && bool) {
        $('html').addClass('desktop').removeClass('mobile');
    } else if (MQ == 'mobile') {
        $('html').addClass('mobile').removeClass('desktop');
    }
}

function scrollNews() {
    $('.content__inner').each(function () {
        var that = $(this);
        var $next = that.next('.content__inner._active');
        if (that.checkActiveNews()) {
            that.addClass('_active');
            that.next('.content__inner').addClass('_last_active');
        } else {
            that.removeClass('_active');
            that.next('.content__inner').removeClass('_last_active');
        }
        if ($next.length) {
            var delta = (($(window).scrollTop() - $next.offset().top) / $(window).height()).toFixed(2);
            // if (delta < 0) {
            //     $('.content__curtain').css({
            //         // 'z-index': '0',
            //         'opacity': 1 - delta * (-1)
            //     });
            // } else {
            //     $('.content__curtain').css({
            //         // 'z-index': '8',
            //         'opacity': '0'
            //     });
            // }
        }
    });
}

function stickySideMenu() {
  if (!$('[data-sticky]').length) return;

  $('[data-sticky]').each(function () {
      var that = $(this),
          parentArticle = $(this).closest('.content__side'),
          stickyLimit = parentArticle.find('[data-sticky-limit]').outerHeight();
      if ($(window).height() - 110 - that.height() < 0) {
          that.stick_in_parent({
              offset_top: 140,
              parent: parentArticle
          });
      } else {
          that.css({
              'margin-bottom': stickyLimit,
              'top': '12rem'
          });
      }
  });
}

function stickyTrigger() {
    $('body').on('click', '[data-sticky] .menu-trigger', function () {
        $(this).parent().toggleClass('_swiped');
    });
}

function preloader(canvas, anim_container, dom_overlay_container) {
    if (!$('.seq-preloader').length) return;

    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

    canvas = document.getElementById(canvas);
    anim_container = document.getElementById(anim_container);
    dom_overlay_container = document.getElementById(dom_overlay_container);
    handleComplete();

    function handleComplete() {
        exportRoot = new lib.preloaderUM();
        stage = new createjs.Stage(canvas);
        stage.addChild(exportRoot);

        fnStartAnimation = function() {
          createjs.Ticker.setFPS(lib.properties.fps);
          createjs.Ticker.addEventListener("tick", stage);
        }

        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();

            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                        }
                        else if(scaleType==1) {
                            sRatio = Math.min(xRatio, yRatio);
                        }
                        else if(scaleType==2) {
                            sRatio = Math.max(xRatio, yRatio);
                        }
                    }
                    if (canvas) {
                        canvas.width = w*pRatio*sRatio;
                        canvas.height = h*pRatio*sRatio;
                        canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                        stage.scaleX = pRatio*sRatio;
                        stage.scaleY = pRatio*sRatio;
                        lastW = iw; lastH = ih; lastS = sRatio;
                    }
                }
            }
        makeResponsive(false,'both',false,1);
        fnStartAnimation();
    }
}

//Remove icon from wrapped menu item
if ($headerMenuContainer) {
    function removeMenuItemIcon() {
        let wrappedItems = [];

        Array.prototype.forEach.call($headerMenuContainer.children, function(item) {
            item.offsetTop > 11 && wrappedItems.push(item);
        });

        if (wrappedItems.length) {
            wrappedItems[0].style.marginLeft = 0;
            wrappedItems[0].classList.add('no-before');
        } else {
            Array.prototype.forEach.call($headerMenuContainer.children, function(item) {
                if (item.classList.contains('no-before')) {
                    item.style.marginLeft = '23px';
                    item.classList.remove('no-before');
                }
            });
        }
    }

    removeMenuItemIcon();

    window.addEventListener('resize', function() {
        removeMenuItemIcon();
    }, true);
}


// Функция открытия попапа редактора
window.openEditor = function (editorPopup, editorElement, type) {
    if (!$(editorPopup).length) return;

    var topPopup = $(window).scrollTop();

    $(editorPopup).addClass('_active').css({ top: topPopup });
    $('body').addClass('_freezed _freezed_ui_cal');

    if (type === 2) {
        tinyMce2(editorElement);
    } else {
        tinyMce(editorElement);
    }
};
//END Функция открытия попапа редактора

// Функция закрытия попапа редактора
window.closeEditor = function (editorPopup, editorElement) {
    if (!$(editorPopup).length) return;
    $(editorPopup).removeClass('_active');
    $('body').removeClass('_freezed _freezed_ui_cal');
    tinyMceDestroy2(editorElement);
};

//END Функция закрытия попапа редактора

// tinyMce2('.js-tiny-init');

window.tinyMce2 = function (editorElement) {
    if (!$(editorElement).length) return;

    tinyInit();

    function tinyInit() {
        tinymce.init({
            selector: editorElement,
            theme: 'inlite',
            inline: true,
            height: 150,
            // menubar: false,
            // toolbar: false,
            statusbar: false,
            branding: false,
            contextmenu: false,
            language: 'ru',
            event_root: '#editor-text',
            mobile: {
                theme: 'mobile',
                plugins: 'lists link image media table link paste textpattern autolink codesample image',
                selection_toolbar: 'bold bullist link indentText indentFact header1 header2 addBlockquote',
                insert_toolbar: 'quickimage gallery media infographic quicktable quiz addNews addNewsList addOneEditingNews'
            },
            plugins: 'lists link image media table link paste textpattern autolink codesample image',
            selection_toolbar: 'bold bullist link indentText indentFact header1 header2 addBlockquote',
            insert_toolbar: 'quickimage gallery media infographic quicktable quiz addNews addNewsList addOneEditingNews',
            setup: function setup(editor) {

                // Учет скролла для правильног позиционирования редактора внутри попапа theme
                // var scrollTopPopup = document.getElementById('article-editor') ? document.getElementById('article-editor').scrollTop : 0;
                // y = y + scrollTopPopup;
                // //END Учет скролла для редактора внутри попапа

                editor.on('init', function (e) {
                    // Изменить контейнер для позиционирования всплывающиx toolbars c <body> на нужный
                    tinymce.ui.Control.prototype.getContainerElm = function () {
                        return document.getElementById('article-editor');
                    };
                });

                // сбрасываем текст по-умолчанию при клике в редакторе
                editor.on('mouseup', function (e) {
                    clearDefaultTextEditor('.editor__text-article p', 'Текст вашей статьи...');
                });

                editor.addButton('indentText', {
                    text: false,
                    tooltip: 'Отступ',
                    icon: 'um-icon-vrezka',
                    onclick: showIndent
                });

                editor.addButton('indentFact', {
                    text: false,
                    tooltip: 'Факт',
                    icon: 'um-icon-fact',
                    onclick: showIndentFact
                });

                editor.addButton('header1', {
                    text: false,
                    tooltip: 'Заголовок1',
                    icon: 'um-icon-h1',
                    onclick: addHeader1
                });

                editor.addButton('header2', {
                    text: false,
                    tooltip: 'Заголовок2',
                    icon: 'um-icon-h2',
                    onclick: addHeader2
                });

                editor.addButton('quiz', {
                    text: false,
                    tooltip: 'Опрос',
                    icon: 'um-icon-quiz'
                });

                editor.addButton('gallery', {
                    text: false,
                    tooltip: 'Галлерея',
                    icon: 'um-icon-add_many_photo'
                });

                editor.addButton('infographic', {
                    text: false,
                    tooltip: 'Инфографика',
                    icon: 'um-icon-add_infographic'
                });

                editor.addButton('addNews', {
                    text: false,
                    tooltip: 'Добавить новость',
                    icon: 'um-icon-add_news'
                });

                editor.addButton('addNewsList', {
                    text: false,
                    tooltip: 'Добавить новость списком',
                    icon: 'um-icon-add_news_edit'
                });

                editor.addButton('addOneEditingNews', {
                    text: false,
                    tooltip: 'Добавить 1(редактируемую) новость',
                    icon: 'um-icon-add_news_list'
                });

                editor.addButton('addBlockquote', {
                    text: false,
                    tooltip: 'Цитата',
                    icon: 'um-icon-blockquote',
                    onclick: addBlockquote
                });

                function getSelectionText() {
                    var rng = editor.selection.getRng();
                    rng.setStartBefore(editor.dom.getParent(editor.selection.getStart(), editor.dom.isBlock));
                    rng.setEndAfter(editor.dom.getParent(editor.selection.getEnd(), editor.dom.isBlock));
                    var tmp = editor.selection.getContent({ format: 'html' });
                    return tmp;
                }

                function showIndent() {
                    var tmp = getSelectionText();
                    if (tmp) {
                        tmp = tmp.replace(/<[\/]*p>/g, '');
                        editor.execCommand('mceReplaceContent', false, '<div class="fact"><p>' + tmp + '</p></div>');
                    }
                }

                function showIndentFact() {
                    var tmp = getSelectionText();
                    if (tmp) {
                        tmp = tmp.replace(/<[\/]*p>/g, '');
                        editor.execCommand('mceReplaceContent', false, '<div class="fact"><h3 class="fact__title">Факт</h3><p>' + tmp + '</p></div>');
                    }
                }

                function addHeader1() {
                    var tmp = getSelectionText();
                    if (tmp) {
                        tmp = tmp.replace(/<[\/]*p>/g, '');
                        editor.execCommand('mceReplaceContent', false, '<h2>' + tmp + '</h2>');
                    }
                }

                function addHeader2() {
                    var tmp = getSelectionText();
                    if (tmp) {
                        tmp = tmp.replace(/<[\/]*p>/g, '');
                        editor.execCommand('mceReplaceContent', false, '<h3>' + tmp + '</h3>');
                    }
                }

                function addBlockquote() {
                    var tmp = getSelectionText();
                    if (tmp) {
                        tmp = tmp.replace(/<[\/]*p>/g, '');
                        editor.execCommand('mceReplaceContent', false, '<div class="quote">' + '<blockquote class="quote__content">' + +tmp + '</blockquote></div>');
                    }
                }
            }
        });
    }
}

window.tinyMceDestroy2 = function (editorElement) {
    if (!$(editorElement).length) return;
    tinymce.remove();
}

// сбрасываем текст по-умолчанию
window.clearDefaultTextEditor = function (editor, textDefault) {
    if ($(editor).text() === textDefault) {
        $(editor).text('');
    }
}

// tinyMce();

window.tinyMce = function () {
    if (!$('.js-tiny-init').length) return;
    tinymce.init({
        selector: 'textarea.js-tiny-init',
        height: 500,
        // menubar: false,
        statusbar: false,
        // branding: false,
        // plugins: [
        //     'advlist autolink lists link image charmap print preview anchor textcolor customem noneditable',
        //     'searchreplace visualblocks code fullscreen noneditable',
        //     'insertdatetime media table contextmenu paste code help noneditable'],
        // toolbar: 'youtubeInsert | quoteInsert | infographicsInsert | galleryInsert | vrezkaInsert | factInsert | insert | undo redo |  styleselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        content_css: ['//fonts.googleapis.com/css?family=Lato:300,300i,400,400i', '//www.tinymce.com/css/codepen.min.css', '/local/templates/um/styles/editor.css'],
        script_url: ['//code.jquery.com/jquery-1.12.4.min.js'],
        relative_urls: true,
        images_upload_url: '/ajax/bloggers/post_photo.php',
        images_upload_base_path: '/',
        images_upload_credentials: true,
        language_url: '/assets/js/tinymce/langs/ru.js',
        setup: function setup(ed) {
            ed.on('init', function (e) {
                $(ed.getBody()).on("click", ".dbe_element .edit_block", function () {
                    var parent = $(this).parents('.dbe_element');
                    TEditor.openPopup(ed, parent.attr('data-dbe'), parent.attr('data-id'));
                });
            });

            ed.on('init', function (e) {
                $(ed.getBody()).on("click", ".dbe_local_element .edit_block", function () {
                    var parent = $(this).parents('.dbe_local_element');
                    var data = {};
                    // alert($(this).find('[data-content]').length);
                    parent.find('[data-content]').each(function () {
                        // alert('olo');
                        data[parent.attr('data-content')] = parent.html();
                    });
                    TEditor.openPopup(ed, parent.attr('data-dbe'), parent.attr('data-id'), data);
                });
            });
            ed.on('init', function (e) {
                $(ed.getBody()).on("mouseover", ".dbe_element", function () {
                    if (!$(this).find(".edit_block").length) {
                        $(this).append($('<div class="edit_block"><span> Изменить</span></div>'));
                    }
                    $(this).find(".edit_block").show();
                });
            });
            ed.on('init', function (e) {
                $(ed.getBody()).on("mouseout", ".dbe_element", function () {
                    $(this).find(".edit_block").hide();
                });

                // $(ed.getBody()).on('click', '.js-gallery-modal', function (e) {
                //     e.preventDefault();
                //     var link = $(this).attr('href');
                //     $.magnificPopup.open({
                //         items: {
                //             src: link
                //         },
                //         type: 'ajax',
                //         callbacks: {
                //             ajaxContentAdded: function () {
                //                 galleryGrid();
                //                 slideShow();
                //             }
                //         }
                //     });
                // });
            });
        }
    });
}

$(window).bind('load', function () {
    setDefaults();
    bindEvents(MQ, true);

    $.fn.checkActiveNews = function () {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight() - $(window).height();
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    scrollNews();

    $(window).on('resize', function () {
        MQ = deviceType();
        bindEvents(MQ, bindToggle);
        if (MQ == 'mobile') {
            bindToggle = true;
            $(window).off('scroll', scrollNews);
            $(window).off('resize', stickySideMenu);
            if ($('.content__side_stacked').length) {
                $('.content__side_stacked').css({
                    'padding-top': $('.content__side_stacked').parent().find('.content__main').position().top,
                    'padding-bottom': $('.content__side_stacked').parent().find('[data-sticky-limit]').outerHeight()
                });
            }
        }
        if (MQ == 'desktop') {
            bindToggle = false;
            $(window).on('scroll', scrollNews);
            $(window).on('resize', stickySideMenu);
            $('.content__side_stacked').css({
                'padding-top': '0',
                'padding-bottom': '0'
            });
        }
    });

    initMobileMenu();
    checkScrollDirection();
    startAgregator();
    toggleShareList();
    toggleNewsView();
    // ajaxComments();
    pinHeader();
    toggleRubricator();
    ajaxPopup();
    toTop();
    slideShow();
    photogallery();
    showVideoModal();
    stickySideMenu();
    stickyTrigger();
    setCardTitleMargin();
    partnerBlocks();
    setProjectsHeaderPosition();
    preloader("canvas-fullscreen", "animation_container-fullscreen", "dom_overlay_container-fullscreen");
    preloader("canvas-inline-1", "animation_container-inline-1", "dom_overlay_container-inline-1");
    preloader("canvas-inline-2", "animation_container-inline-2", "dom_overlay_container-inline-2");
    preloader("canvas-inline-3", "animation_container-inline-3", "dom_overlay_container-inline-3");

    if ('scrollRestoration' in window.history) {
        window.history.scrollRestoration = 'manual';
    }

    if ($("[data-js='dropdown']")) {
        $('body').on('click', '[data-js="dropdown"]', function (e) {
            e.preventDefault();
            if (!$('.author__edit-descr').hasClass('display-none')) {
                $(this).toggleClass('_droped').next().toggleClass('_droped');
            }
        });

        $('body').on('click', '[data-js="close-publisher-params"]', function () {
            $('[data-js="dropdown"]').removeClass('_droped').next().removeClass('_droped');
        });

        $('body').on('click', function (e) {
            if ($('.dropdown').has(e.target).length === 0 && !$('body').hasClass('_freezed _freezed_ui_cal')) {
                $('[data-js="dropdown"]').removeClass('_droped').next().removeClass('_droped');
            }
        });
    }

    var $dragging = null;
    if ($button.length) {
        var mc = new Hammer($button[0]);
    }

    $('body').on('click', '[data-js="droplist"]', function () {
        if (!$dragging) {
            $(this).parent().toggleClass('_droped');
        }
    });

    if ($button.length) {
        $(window).resize(function () {
            if ($(window).width() < $button.offset().left) {
                $button.addClass('_stacked_right');
            }
            checkButtonSide();
        });
    }
    $(document.body).on('touchmove mousemove', function (e) {
        if ($dragging) {
            var pageYstate = e.pageY - 30;
            pageXstate = e.pageX - 30;
            $dragging.offset({
                top: pageYstate,
                left: pageXstate
            });
            checkButtonSide();
            if (is_stacked) {
                $button.removeClass('_stacked_right');
            } else {
                $button.removeClass('_stacked_left');
            }
        }
    });

    if ($button.length) {
        // mc.on('pressup', function () {
        //     alert()
        // });
        mc.on('press', function () {
            $dragging = $button;
            $('body').addClass('_moving');
            $button.removeClass('_droped');
        });

    }
    $(document.body).on('touchend mouseup', $button, function (e) {
        $dragging = null;
        $('body').removeClass('_moving');
        if (is_stacked) {
            $button
                .addClass('_stacked_right')
                .removeClass('_stacked_left');
        } else {
            $button
                .removeClass('_stacked_right')
                .addClass('_stacked_left');
        }
    });

    displayNone('.display-none');

    function displayNone(block) {
        $(block).css('display', 'none');
    }

    contactsMap();

    function contactsMap() {

        if (!$('#map-contacts').length) return;

        ymaps.ready(function () {
            var myMap = new ymaps.Map("map-contacts", {
                    center: [56.842270, 53.206064],
                    zoom: 16,
                    controls: ['searchControl']
                }),
                myGeoObject = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: [56.842288, 53.205310],
                    },
                    properties: {
                        balloonContent: 'Россия, Удмуртская Республика, Ижевск, улица Пастухова, 13'
                    }
                });
            myMap.geoObjects.add(myGeoObject);
        });
    }

    toggleMapContacts();

    function partnerBlocks() {
      if ($('.partners-item__block .partners-mediametrics .mm-body__line a')) {
        var partnerBlock = 160;
        if ($('.partners-item__block .partners-smi2')) {
          partnerBlock = $('.partners-item__block .partners-smi2');
        } else {
          partnerBlock = $('.partners-item__block .partners-lentainform');
        }
        var mediametricsBlock = $('.partners-item__block .partners-mediametrics .mm-body__line a')
        mediametricsBlock.css('height', partnerBlock.height());
        $(window).on('resize', function () {
          mediametricsBlock.css('height', partnerBlock.height());
        });
      } else {
        return false;
      }
    }

    function toggleMapContacts() {
        $('body').on('click', '[data-js="map-toggle"]', function (e) {
            e.preventDefault();
            $(this).parents('.contacts__map').toggleClass('_active');
        });
    }

    // Окрываем поиск новостей Callback openSearchNews()
    $('body').on('click', '[data-js="open-search-archive"]', function (e) {
        e.preventDefault();
        openSearchNews();
        console.log('sdsdsd');
    });
    //END  Callback openSearchNews()

    //Закрываем поиск новостей Callback closeSearchNews()
    $(document).on('click', '[data-js="close-search-archive"]', function (e) {
        e.preventDefault();
        closeSearchNews();

    });
    // END Callback closeSearchNews()

    // Функция сброса введенных данных в input
    function cleanInputValueText(selectorInput) {
        if (!$(selectorInput).length) return;
        $(selectorInput).val('').focus();
    }

    // Позволяет добавить в element вертикальный position:top - равный величене вертикального скролла
    // Функция для результатов поиска в попапе добавляющая величину скролла от начала документа,
    // чтобы вставить результаты под input поиска имеющий position: fixed
    function addVerticalScrollValue(element) {
        if (!$(element).length) return;
        $(element).css('top', scrolled_pixels);
    }

    // Функция открытия попапа поиска новостей
    window.openSearchNews = function () {
        if (!$('#search_form').length) return;

        $('.popup_search_news').addClass('_active');
        $('body').addClass('_freezed');
        datePickerInit();

        //Очиска инпута поиска при клике на 'x'
        $(document).on('click', '[data-js="clear-input"]', function (evt) {
            cleanInputValueText('#search_tag');
        });

        addVerticalScrollValue('.popup_search_news');
        $('body').addClass('_has-popup-search')
    };
    //END Функция открытия попапа поиска новостей

    // Функция закрытия попапа поиска новостей
    window.closeSearchNews = function () {
        if (!$('#search_form').length) return;
        var $form = $('#search_form'),
            $inputFrom = $form.find('#input_date_from'),
            $inputTo = $form.find('#input_date_to'),
            $searchTag = $form.find('#search_tag');

        $inputFrom.val('');
        $inputTo.val('');
        $searchTag.val('');
        $inputFrom.focus();

        $form.find('.ui-datepicker').removeClass('_active');
        $('.popup_search_news').removeClass('_active');
        $('body').removeClass('_freezed');
        $('body').removeClass('_has-popup-search')
    };
    //END Функция закрытия попапа поиска новостей

    // Вешаем обработчик swipe-событий при открытии datepicker
    $(document).on('click', '.calendar__toggle', function (evt) {
        swipeDatePicker('.ui-datepicker', '.ui-datepicker-prev', '.ui-datepicker-next');
    });
    //END Вешаем обработчик swipe-событий при открытии datepicker

    // Функция для обработки событий свайпа для листания калнедаря
    function swipeDatePicker(datePicker, prevDateButton, nextDateButton) {
        if (!document.querySelector(datePicker) && !$(prevDateButton).length && !$(nextDateButton).length && ($(window).width() > 770)) return;

        mc = new Hammer(document.querySelector(datePicker));

        mc.on('swipeleft', function (evt) {
            $(datePicker).find(nextDateButton).click();
        });
        mc.on('swiperight', function (evt) {
            $(datePicker).find(prevDateButton).click();
        });
    }

    //END Функция для обработки событий свайпа

    // Выбор даты в поиске новостей
    function datePickerInit() {
        if (!$('#search_form').length) return;
        var $form = $('#search_form'),
            $inputDate = $form.find('.calendar__input'),
            $monthsNum,
            $regExpInput = /(\d+)\.(\d+)\.(\d+)/,
            $replacePatternInput = '$2/$1/$3',
            inputArrayVal = [],
            $windowWidth = $(window).width();

        // Задаем кол-во календарей в зав-сти от ширины вьюпорта
        if ($windowWidth <= 700) {
            $monthsNum = 1;
        } else if ($windowWidth >= 700 && $windowWidth < 800) {
            $monthsNum = 2;
        } else {
            $monthsNum = 3;
        }

        // Подключаем маску для инпутов даты
        $inputDate.mask('00.00.0000', {placeholder: "ДД.ММ.ГГГГ"});

        // Руссифицируем
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '&#x3C;Пред',
            nextText: 'След&#x3E;',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Нед',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);

        // Сбрасываем введеные даты
        $form.find('#clear_search_filter').on('click', function (e) {
            e.preventDefault();
            // console.log('click reset');

            var $inputFrom = $form.find('#input_date_from'),
                $inputTo = $form.find('#input_date_to'),
                $searchTag = $form.find('#search_tag');

            $inputFrom.val('');
            $inputTo.val('');
            $searchTag.val('');
            $inputFrom.focus();
            inputArrayVal = [];

            $('#date_range').datepicker("refresh");
            datePickerInit();
        });

        // Отображаем на календаре введенные даты:
        $form.find('#input_date_from').on('change', function (e) {
            var inputValue = $(this).val(),
                inputDate = new Date(inputValue.replace($regExpInput, $replacePatternInput));

            inputArrayVal[0] = inputDate;
            $('#date_range').datepicker("setDate", inputArrayVal);
        });
        $form.find('#input_date_to').on('change', function (e) {
            var inputValue = $(this).val(),
                inputDate = new Date(inputValue.replace($regExpInput, $replacePatternInput));
            inputArrayVal[1] = inputDate;
            $('#date_range').datepicker("setDate", inputArrayVal);
        });

        $('#date_range').datepicker({
            dateFormat: "dd.mm.yy",
            range: 'period', // режим - выбор периода
            numberOfMonths: $monthsNum,
            constrainInput: true,
            showOtherMonths: true,
            maxDate: 1,
            defaultDate: null,
            stepMonths: $monthsNum,
            onSelect: function (dateText, inst, extensionRange) {
                // extensionRange - объект расширения
                $('[name=startDate]').val(extensionRange.startDateText);
                $('[name=endDate]').val(extensionRange.endDateText);
            }
        });
    }

    // END  Выбор даты в поиске новостей

    // Закрыть дропдаун c календарем при клике вне его, вне инпутов даты вне кнопок навигации

    $(document).on('click', closeDropDownDatePicker);

    function closeDropDownDatePicker(e) {
        if (!$(e.target).closest('.ui-datepicker').length && !$(e.target).hasClass('calendar__toggle') && !$(e.target).hasClass('calendar__input') && !$(e.target).closest('.calendar__nav-buttons').length && !$(e.target).hasClass('ui-datepicker-prev') && !$(e.target).hasClass('ui-datepicker-next') && !$(e.target).hasClass('form-control_search')) {
            $('.calendar__inner').removeClass('_droped');
            $('.calendar__toggle').removeClass('_droped');
        }
    }

    // END  Закрыть дропдаун c календарем при клике вне его, вне инпутов даты вне кнопок навигации

    // Выпадающий блок с выбором периода публикации новости
    periodNewsDropdown();

    function periodNewsDropdown() {
      if (!$('[data-js="filter-by-popular"]').length) return;

      var $filterButtonByPopular = $('[data-js="filter-by-popular"]');
      var $filterButtonByNew = $('[data-js="filter-by-new"]');
      var $filterTabs = $('[data-js="tabs-filter-by-popular"]');

      $filterButtonByPopular.click(function(e) {
        e.preventDefault();
        $filterTabs.show();
      });
      $filterButtonByNew.click(function(e) {
        e.preventDefault();
        $filterTabs.hide();
      });
    }

    // END  Выпадающий блок с выбором периода публикации новости

    // Выбор даты публикации материала
    datePickerPublish();

    function datePickerPublish() {
        if (!$('[data-js="date-picker-publish"]').length) return;

        var $datePicker = $('[data-js="date-picker-publish"]');

        // Подключаем маску для инпута даты
        $datePicker.mask('00.00.0000', {placeholder: "ДД.ММ.ГГГГ"});

        // Руссифицируем
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '&#x3C;Пред',
            nextText: 'След&#x3E;',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Нед',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);

        // Инициализируем  datepicker
        $datePicker.datepicker({
            dateFormat: "dd.mm.yy",
            numberOfMonths: 1,
            constrainInput: true,
            // selectOtherMonths: true,
            showOtherMonths: true
        });
    }

    // END Выбор даты публикации материала

    cropAvatar('[data-js="popup-inline-photo"]', '[data-js="crop-photo"]', '[data-js="crop-preview"]', '[data-js="save-crop-image"]');
    cropAvatar('[data-js="popup-inline-logo"]', '[data-js="crop-logo"]', '[data-js="crop-preview"]', '[data-js="save-crop-image"]', 2.421052631578947);

    function cropAvatar(openPopupLink, photo$, previewCrop$, btnSave$, aspectRatio$,) {
        if (!$(openPopupLink).length) return;

        uploadImage();
        deleteImage()
        openPopupAvatar();

        function openPopupAvatar() {
            $(openPopupLink).magnificPopup({
                type: 'ajax',
                closeOnContentClick: false,
                closeOnBgClick: false,
                removalDelay: 300,
                mainClass: 'mfp-fadein',
                preloader: true,
                callbacks: {
                    ajaxContentAdded: function () {
                        cropPhoto2(photo$, previewCrop$, btnSave$, aspectRatio$);
                        $('body').on('click', '[data-js="js-close-popup"]', function () {
                            $.magnificPopup.close();
                        });
                    }
                }
            });
        }

        function cropPhoto2(photo, previewCrop, btnSave, aspectRatio) {
            var $image = $(photo);
            var $previews = $(previewCrop);
            var $btnSave = $(btnSave);
            var $aspectRatio = aspectRatio ? aspectRatio : 1;
            var $cropBoxData;
            var $canvasData;
            var $cropImgData;
            var $formData = new FormData();
            var $croppedCanvas = $().cropper('getCroppedCanvas');

            $image.cropper({
                movable: false,
                aspectRatio: $aspectRatio,
                zoomable: false,
                guides: false,
                center: false,
                background: false,
                minCropBoxWidth: 100,
                preview: $previews,
                // ready: function (e) {
                //     aspectRatio =  $aspectRatio ? $aspectRatio : this.naturalWidth / this.naturalHeight;
                //     console.log(aspectRatio);
                //     $().cropper('setAspectRatio', aspectRatio);
                // },

                crop: function (e) {
                    $cropImgData = e; // save returned data to $cropImgData.
                    // console.log($cropImgData);   //$cropImgData is the returned data from croppe
                },

                built: function () {
                    // Strict mode: set crop box data first
                    console.log();
                    $image.cropper('setCropBoxData', $cropBoxData);
                    $image.cropper('setCanvasData', $canvasData);
                    $('body').on('click', '[data-js="save-crop-image"]', function () {
                        $.ajax({
                            type: "POST",
                            url: 'assets/php/crop.php',
                            data: 'data=' + JSON.stringify($cropImgData) + '&cpfl_id=' + "<?=$_GET['id']?>",
                            success: function (res) {
                                console.log(res);
                            }, //END function(res)
                            error: function () {
                                console.log('Upload error');
                            }
                        });
                        $.magnificPopup.close();
                    });//END $btnSave.on
                }//END built

            }); //END $image.cropper
            // https://github.com/fengyuanchen/cropper/issues/325
            // https://github.com/fengyuanchen/cropper#getcroppedcanvasoptions

            $(openPopupLink).on('mfpClose', function () {
                $cropBoxData = $image.cropper('getCropBoxData');
                $canvasData = $image.cropper('getCanvasData');
                $image.cropper('destroy');
            });
        }

        function uploadImage() {
            $("#avatarInput").fileupload({
                url: '/assets/php/index.php',
                dataType: 'html',
                done: function (e, data) {
                    console.log('doneUpload');
                }
            })
        }

        function deleteImage() {
            $("#avatarDelete").on('click', function () {
              console.log('delete');
            });
        }
    }

    openAuthorDataEditor();

    function openAuthorDataEditor() {
        var $btnOpenForm = $('[data-js="edit-author-data"]'),
            $authorDescr = $('.author__descr'),
            $authorForm = $('.author__edit-descr'),
            $btnCancelEdit = $('[data-js="cancel-edit"]'),
            $authorPhoto = $('[data-js="dropdown"]');

        if (!$('[data-js="edit-author-data"]').length) return;

        $('body').on('click','[data-js="edit-author-data"]',function (e) {
            $('.author__descr').addClass('display-none');
            $('[data-js="dropdown"]').addClass('active');
            $('.author__edit-descr').removeClass('display-none').show().find('.form-control')[0].focus();
        });

        $('body').on('click','[data-js="cancel-edit"]',function (e) {
            $('.author__edit-descr').addClass('display-none').hide();
            $('.author__descr').removeClass('display-none').show();
            $('[data-js="dropdown"]').removeClass('active');
        });
    }

    toggleTabs('.articles-list', '.tabs__tab', '.articles-list__inner-news', '.news');

    function toggleTabs(container, tabLink, innerTab, tab) {
        if (!$(tabLink).length) return;
        var $container = $(container),
            $tabLink = $container.find(tabLink),
            $innerTab = $container.find(innerTab),
            $tab = $innerTab.find(tab);

        $tabLink.on('click', function (e) {
            e.preventDefault();

            var tabID = $(this).attr('href');

            $tabLink.removeClass('_active');
            $(this).addClass('_active');

            $tab.addClass('display-none').hide();
            $(tabID).removeClass('display-none').show();

        });
    }

    docsPopup('.view-docs', '[data-js="docs-popup"]');

    function docsPopup(popupInner, popupLink) {
        var $windowHeight = $(window).height() * .8,
            $linkDocs,
            isPdf = false;

        $(popupLink).on('click', function (e) {
            $linkDocs = $(this).data('link-doc');
            ($(this).is('[data-link-doc$=".pdf"]')) ? isPdf = true : isPdf = false;
        });

        $(popupLink).magnificPopup({
            type: 'ajax',
            closeOnContentClick: false,
            removalDelay: 300,
            mainClass: 'mfp-fadein mfp-docs',
            preloader: true,
            fixedBgPos: true,
            fixedContentPos: false,
            overflowY: 'hidden',
            callbacks: {
                ajaxContentAdded: function () {

                    if ($(window).width() < 769 && (isPdf)) {
                        setTimeout(function () {
                            $(popupInner).addClass('_active_pdf');
                            pdfViewer($linkDocs, '#the-canvas');
                        }, 300);
                    } else {
                        $(popupInner).removeClass('_active_pdf').find('#iframe')
                            .attr('src', $linkDocs)
                            .height($windowHeight);
                    }
                }
            },
            beforeClose: function () {
                $(popupInner).removeClass('_active_pdf');
            }
        });
    }

    function pdfViewer(url, canvasId) {

        if (!$(canvasId)) return;

        // If absolute URL from the remote server is provided, configure the CORS
        // header on that server.

        // The workerSrc property shall be specified.
        PDFJS.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

        var pdfDoc = null,
            pageNum = 1,
            pageRendering = false,
            pageNumPending = null,
            scale = 1.3,
            canvas = document.getElementById('the-canvas'),
            ctx = canvas.getContext('2d');

        /**
         * Get page info from document, resize canvas accordingly, and render page.
         * @param num Page number.
         */
        function renderPage(num) {
            pageRendering = true;
            // Using promise to fetch the page
            pdfDoc.getPage(num).then(function (page) {
                var viewport = page.getViewport(scale);
                canvas.height = viewport.height;
                canvas.width = viewport.width;

                // Render PDF page into canvas context
                var renderContext = {
                    canvasContext: ctx,
                    viewport: viewport
                };
                var renderTask = page.render(renderContext);

                // Wait for rendering to finish
                renderTask.promise.then(function () {
                    pageRendering = false;
                    if (pageNumPending !== null) {
                        // New page rendering is pending
                        renderPage(pageNumPending);
                        pageNumPending = null;
                    }
                });
            });

            // Update page counters
            document.getElementById('page_num').textContent = pageNum;
        }

        /**
         * If another page rendering in progress, waits until the rendering is
         * finised. Otherwise, executes rendering immediately.
         */
        function queueRenderPage(num) {
            if (pageRendering) {
                pageNumPending = num;
            } else {
                renderPage(num);
            }
        }

        /**
         * Displays previous page.
         */
        function onPrevPage() {
            if (pageNum <= 1) {
                return;
            }
            pageNum--;
            queueRenderPage(pageNum);
        }

        document.getElementById('prev').addEventListener('click', onPrevPage);

        /**
         * Displays next page.
         */
        function onNextPage() {
            if (pageNum >= pdfDoc.numPages) {
                return;
            }
            pageNum++;
            queueRenderPage(pageNum);
        }

        document.getElementById('next').addEventListener('click', onNextPage);

        /**
         * Asynchronously downloads PDF.
         */
        PDFJS.getDocument(url).then(function (pdfDoc_) {
            pdfDoc = pdfDoc_;
            document.getElementById('page_count').textContent = pdfDoc.numPages;

            // Initial/first page rendering
            renderPage(pageNum);
        });

    }

    function docsPopupIframe(selector, popupInner) {
        var $windowHeight = $(window).height() * .8,
            $linkDocs,
            $linkDocsPdf,
            $isPdf = false,
            $magnificParams = {
                type: 'ajax',
                closeOnContentClick: false,
                removalDelay: 300,
                mainClass: 'mfp-fadein',
                preloader: true,
                fixedBgPos: true,
                fixedContentPos: false,
                overflowY: 'hidden'
            };

        $(selector).on('click', function (e) {
            $linkDocs = $(this).attr('href');
            $linkDocsPdf = $(this).data('link-doc');
            console.log('$linkDocsPdf:' + $linkDocsPdf);
            ($(this).is('[href$=".pdf"]')) ? $isPdf = true : $isPdf = false;
        });

        // if ($isPdf) {
        if ($(window).width() < 769 && ($isPdf)) {
            $(selector).magnificPopup({
                type: 'ajax',
                closeOnContentClick: false,
                removalDelay: 300,
                mainClass: 'mfp-fadein',
                preloader: true,
                fixedBgPos: true,
                fixedContentPos: false,
                overflowY: 'hidden',

                callbacks: {
                    ajaxContentAdded: function () {
                        setTimeout(function () {
                            pdfViewer($linkDocsPdf, '#the-canvas');
                        }, 300);
                    }
                }
            });
        }
        else {
            $(selector).magnificPopup({
                type: 'iframe',
                removalDelay: 300,
                mainClass: 'mfp-fadein',
                preloader: true,
                fixedBgPos: true,
                fixedContentPos: false,
                overflowY: 'hidden',
                showCloseBtn: true,
                closeBtnInside: true,
                iframe: {
                    markup:
                    '<div class="popup popap_docs">' +
                    '<div class="view-docs">' +
                    '<div class="mfp-close"></div>' +
                    '<iframe class="mfp-iframe view-docs__iframe" frameborder="0" allowfullscreen  style="border-style: none;width: 100%; height: 100%"></iframe>' +
                    '</div>' +
                    '</div>',

                },
                callbacks: {
                    open: function () {
                        $(popupInner).height($windowHeight);
                    }
                }
            });
        }
    }

    toggleArticleTopic('[data-js="select-box"]');

    //кастомный селект выбора темы
    function toggleArticleTopic(element) {
      if (!$(element) || !$('[data-js="select-box-options"]')) return;
      var selectBoxOptions = $('[data-js="select-box-options"]');

      selectBoxOptions.width($(element).width());

      $('body').click(function(event) {
        if ($(event.target).data('js') === 'select-box') {
          $(element).hasClass('__toggled') ? $(element).removeClass('__toggled') : $(element).addClass('__toggled');
          selectBoxOptions.slideToggle(100);
        } else {
          $(event.target).data('js') === 'select-box-option' ? $(element).attr('value', $(event.target).text()) : null;
          $(element).removeClass('__toggled');
          selectBoxOptions.slideUp(100);
        }
      });

      $(window).on('resize', function () {
        selectBoxOptions.width($(element).width());
      });
    }

    uploadFiles('[data-js="dropzone"]');

    //Загрузка файлов drop&drug
    function uploadFiles(element) {
        if (!$(element)) return;

        $(element).dropzone({
            url: '/file/post',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5,
            maxFiles: 1,
            addRemoveLinks: true,
            dictDefaultMessage: 'Удаление',
            dictFileTooBig: 'Размер файла слишком большой',
            dictInvalidFileType: 'Данный тип файлов не поддерживается',
            dictResponseError: 'Сервер не отвечает',
            dictCancelUpload: 'Отменить загрузку',
            dictCancelUploadConfirmation: 'Oтменить загрузку?',
            dictRemoveFile: 'Удалить файл',
            createImageThumbnails: false,
            dictMaxFilesExceeded: 'Вы можете загрузить только 1 файл',
            dictFileSizeUnits: 'Максимальный размер файла 5 мегабайт',
            init: function () {
                this.on("maxfilesexceeded", function (file) {
                    if (this.files[1] != null) {
                        this.removeFile(this.files[0]);
                    }
                });
            }
        });

    }

    uploadPhotoCover('[data-js="dropzone-cover-photo"]');

    function uploadPhotoCover(element) {
        if (!$(element)) return;

        $(element).dropzone({
            createImageThumbnails: true,
            // previewsContainer: '.dropzone-previews',
            url: '/file/post',
            thumbnailWidth: 750,
            thumbnailHeight: 500,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5,
            maxFiles: 1,
            addRemoveLinks: true,
            dictDefaultMessage: 'Удаление',
            dictFileTooBig: 'Размер файла слишком большой',
            dictInvalidFileType: 'Данный тип файлов не поддерживается',
            dictResponseError: 'Сервер не отвечает',
            dictCancelUpload: 'Отменить загрузку',
            dictCancelUploadConfirmation: 'Oтменить загрузку?',
            dictRemoveFile: 'Удалить файл',
            dictMaxFilesExceeded: 'Вы можете загрузить только 1 файл',
            dictFileSizeUnits: 'Максимальный размер файла 5 мегабайт',
            init: function () {
                this.on("maxfilesexceeded", function (file) {
                    if (this.files[1] != null) {
                        this.removeFile(this.files[0]);
                    }
                });
            },
        });
    }

    // Навигация для добавления медиафайлов в редакторе
    addMediaNav('.add-media', '[data-js="toggle-media-nav"]');

    function addMediaNav(innerNav, toggleBtn) {
        if (!$(innerNav)) return;

        var $navInner = $(innerNav),
            $toggleButton = $navInner.find(toggleBtn);

        $toggleButton.click(function (e) {
            var $that = $(this);
            $that.closest(innerNav).toggleClass('_active');
            // console.log('click $toggleButton');
        });

        $(document).click(function (event) {
            if ($(event.target).closest(innerNav).length) return;
            $navInner.removeClass('_active');
            event.stopPropagation();
        });

    }

    noJS();

    function noJS() {
        $('body').removeClass('no-js');
    }

    // Всплывающее меню редактора при выделение текста
    selectEditor('[data-js="selectorTextArea"]', '.editor__input-text');

    function selectEditor(selectorInner, selectorTextArea) {
        if (!$(selectorInner)) return;

        var $innerSelector = $(selectorInner),
            $selectorTextarea = $innerSelector.find(selectorTextArea);

        $selectorTextarea.select(function (e) {
            console.log('select textarea');
        })
    }

    editCompanyData('.company.company_about', '[data-js="edit-company-data"]', '[data-js="cancel-edit"]');

    function editCompanyData(companyAboutInner, buttonOpen, cancelEdit) {
        if (!$(buttonOpen).length) return;
        var $companyAboutInner = $(companyAboutInner),
            $btnOpenForm = $companyAboutInner.find(buttonOpen),
            $btnCancelEdit = $companyAboutInner.find(cancelEdit);

        $btnOpenForm.on('click', function () {
            $companyAboutInner.removeClass('_view').addClass('_editor');
        });

        $btnCancelEdit.on('click', function () {
            $companyAboutInner.removeClass('_editor').addClass('_view');
        })
    }

    toggleTabs('.company__inner-about', '.tabs__tab', '.company__inner-tab-about', '.company__tab-content');


    // Окрываем редактор
    $('body').on('click', '[data-js="open-editor"]', function (e) {
        e.preventDefault();
        openEditor('#article-editor', '.js-tiny-init', 2);
    });
    $('body').on('click', '[data-js="open-press-release-editor"]', function (e) {
        e.preventDefault();
        openEditor('#press-release-editor', '.js-tiny-init', 1);
    });
    //END Окрываем редактор

    // Закрываем редактор
    $('body').on('click', '[data-js="close-editor"]', function (e) {
        e.preventDefault();
        closeEditor('.popup_editor', '.js-tiny-init');
    });
    //END Закрываем редактор

    // Закрыть тултип валидации инпута при клике вне его
    $(document).on('click', closeErrorTooltipIn);

    function closeErrorTooltipIn(e) {
        if ($(e.target).closest('.form-control-error').length || (!$('.form .form-control-error').length)) return;
        var $form = $(e.target).closest('.form'),
            $input = $form.find('input'),
            $error = $form.find('.form-control-error'),
            $borderColorDefault = $input.css('border-color');
        // console.log(' borderColorDefault: ' + borderColorDefault);
        $error.hide();
        $input.css('border-color', $borderColorDefault);
    }

    promoVisitorsChart('#promo-visitors-chart');

    function promoVisitorsChart(selector) {
        if (!$(selector).length) return;

        // Парсим данные для построения графика
        var arrViewsData = getNumbersArrayDataChart($(selector).parent().find('[data-js = "chart-views"] .stats-chart__value'));
        var arrVisitorsData = getNumbersArrayDataChart($(selector).parent().find('[data-js = "chart-visitors"] .stats-chart__value'));

        // Переключение табов графика
        toggleTabs('.web-stats__charts', '.web-stats__link-tab', '.stats-chart', '.web-stats__tab-item');

        // Функция парсинга чилсловых данных из верcтки в массив
        function getNumbersArrayDataChart(selector) {
            if (!(selector).length) return;
            var arrData = [];
            $(selector).each(function () {
                var value = $(this).html();
                arrData.push(parseInt(value.replace(/\D/g, ''), 10));
            });
            return arrData;
        }

        // Строим график просмотров при загрузке страницы
        addChart('ПРОСМОТРЫ', arrViewsData);

        // Функция построения чарта
        function addChart(titleSiries, dataArray) {

            var chart = Highcharts.chart('promo-visitors-chart', {
                colors: ['#2b59fc'],
                plotOptions: {
                    series: {
                        pointStart: 0
                    },
                    line: {
                        lineWidth: 8
                    }
                },
                chart: {
                    type: 'line',
                    backgroundColor: 'transparent',
                    borderWidth: 0,
                    borderRadius: 0,
                    plotBackgroundColor: null,
                    plotShadow: false,
                    plotBorderWidth: 0
                },
                title: {
                    style: {
                        color: 'rgba(255, 255, 255, 0)',
                        font: '0 Lucida Grande, Lucida Sans Unicode,' +
                        ' Verdana, Arial, Helvetica, sans-serif'
                    }
                },
                xAxis: {
                    categories: false,
                    gridLineColor: '#c0c0c0',
                    gridLineWidth: 1,
                    gridLineDashStyle: 'Solid',
                    labels: {
                        enabled: false
                    },
                    lineColor: 'transparent',
                    tickColor: '#c0c0c0',
                    title: {
                        enabled: false
                    },
                    showEmpty: false,
                    minorTicks: false,
                    minorTickLength: 0,
                    tickPixelInterval: 200

                },
                yAxis: {
                    alternateGridColor: null,
                    minorTickInterval: null,
                    gridLineColor: 'rgba(255, 255, 255, 0)',
                    minorGridLineColor: 'rgba(255,255,255,0)',
                    lineWidth: 0,
                    tickWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        enabled: false
                    }

                },
                legend: {
                    enabled: false
                },

                tooltip: {
                    // enabled: false
                    useHTML: true,
                    headerFormat: ''
                },
                series: [{
                    name: titleSiries,
                    data: dataArray
                }
                ]
            });

            // Обновляем график при клике по табу
            $(document).on('click', '.web-stats__link-tab', onTabClick);

            function onTabClick(evt) {
                evt.preventDefault();
                var titleChart = $(this).data('chartType');
                var arrChart = [];
                var href = $(this).attr('href');

                if (href === '#chart-views') {
                    arrChart = arrViewsData;
                } else if (href === '#chart-visitors') {
                    arrChart = arrVisitorsData;
                }

                chart.update({
                    series: [{
                        name: titleChart,
                        data: arrChart
                    }
                    ]
                });
            }
        }
    }
});
