$(function () {

    var Search =
        {
            loading: false,
            query: '',
            dateFrom: '',
            dateTo: '',
            calendar: '',

            // load : function(query,isTag){
            //     Search.query = query;
            //     NewPager.prevPage = true;
            //     var aj = new Aj();
            //     if(isTag === true){
            //         aj.get(AllPages.siteLink('/ajax/search/search.php?tags='+Search.query+'&date_form='+Search.dateFrom+'&date_to='+Search.dateTo),false,
            //             function(data){
            //                 $('#res').html(data);
            //             });
            //     }else{
            //         aj.get(AllPages.siteLink('/ajax/search/search.php?q='+Search.query+'&date_form='+Search.dateFrom+'&date_to='+Search.dateTo),false,
            //             function(data){
            //                 $('#res').html(data);
            //             });
            //     }
            // },
            selectDateEvent: function (val) {
                console.log(val);
                if('filter_date_from' == val){
                    Search.calendar.datepicker("setDate", new Date($('#filter_date_from').val()) );
                    $('.ui-datepicker-current-day').click();
                }
                if('filter_date_to' == val){
                    Search.calendar.datepicker("setDate", new Date($('#filter_date_to').val()) );
                    $('.ui-datepicker-current-day').click();
                }
                /*  Search.calendar.datepicker("setDate", new Date("2013-08-06") );
                  console.log('selectDateEvent'+val);*/
            },
            sort: function () {

                $.datepicker.regional['ru'] = {
                    closeText: '�������',
                    prevText: '&#x3C;����',
                    nextText: '����&#x3E;',
                    currentText: '�������',
                    monthNames: ['������', '�������', '�����', '������', '���', '����',
                        '����', '�������', '��������', '�������', '������', '�������'],
                    monthNamesShort: ['���', '���', '���', '���', '���', '���',
                        '���', '���', '���', '���', '���', '���'],
                    dayNames: ['�����������', '�����������', '�������', '�����', '�������', '�������', '�������'],
                    dayNamesShort: ['���', '���', '���', '���', '���', '���', '���'],
                    dayNamesMin: ['��', '��', '��', '��', '��', '��', '��'],
                    weekHeader: '���',
                    dateFormat: 'dd.mm.yy',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: ''
                };
                $.datepicker.setDefaults($.datepicker.regional['ru']);

                // Search.calendar = $('#datepicker'), yearSort = null;
                Search.calendar = $('#datepicker');
                Search.calendarWidget =  Search.calendar.find('.ui-widget');


                var datepickerRange = {

                    startDate: null,
                    endDate: null,
                    currentDate: new Date(),
                    selectCount: 0,

                    checkDays: function (datepicker) {
                        var self = this;
                        if (this.startDate && this.endDate) {
                            setTimeout(function () {
                                //������������ ��� ������� ������
                                datepicker.dpDiv.find('.ui-datepicker-calendar').each(function (monthIndex) {
                                    var calendar = $(this);
                                    var currMonth = datepicker.drawMonth + monthIndex;
                                    var currYear = datepicker.drawYear;

                                    if (currMonth > 11) {
                                        currYear++;
                                        currMonth = datepicker.drawMonth - 12 + monthIndex;
                                    }

                                    calendar.find('td>a.ui-state-default').each(function (dayIndex) {
                                        var day = dayIndex + 1;
                                        self.checkDay(this, day, currMonth, currYear);
                                    });
                                })

                            }, 1);
                        }
                    },

                    checkDay: function (elem, day, month, year) {
                        //console.log(elem, day, month, year);
                        var date = new Date(year, month, day);
                        if (date.getTime() >= this.startDate.getTime() && date.getTime() <= this.endDate.getTime()) {
                            $(elem).addClass('ui-state-active').removeClass('ui-state-highlight');
                        }
                    },

                    getSelectedDate: function (inst) {
                        return new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                    },

                    getInputDate: function () {
                        $(".date_from").val($("#dateText span").eq(0).html())
                        $(".date_to").val($("#dateText span").eq(1).html())
                    }
                };

                var monthsNum;
                var windowWidth = $(window).width();
                if (windowWidth <= 600) {
                    monthsNum = 1;
                } else if (windowWidth >= 600 && windowWidth < 800) {
                    monthsNum = 2;
                } else {
                    monthsNum = 3;
                }

                Search.calendar.datepicker({
                    numberOfMonths: monthsNum,
                    showCurrentAtPos: 2,
                    defaultDate: null,
                    showOtherMonths: true,
                    // selectOtherMonths: true,
                    // defaultDate : yearSort,
                    // showButtonPanel: true,
                    // firstDay : "1",
                    // changeYear: true,
                    dateFormat: 'dd.mm.yy',
                    onSelect: function (dateText, inst) {
                        // Search.calendarWidget.css({'display':'flex!important'});
                        //console.log('onSelect');
                        inst.drawMonth += 2;
                        datepickerRange.selectCount++;
                        datepickerRange.currentDate = datepickerRange.getSelectedDate(inst);
                        if (datepickerRange.selectCount < 2) {
                            datepickerRange.startDate = datepickerRange.getSelectedDate(inst);
                            datepickerRange.endDate = null;
                            $("#dateText").html("<span>" + dateText + "</span>");
                            datepickerRange.getInputDate();
                        } else {
                            datepickerRange.selectCount = 0;
                            datepickerRange.endDate = datepickerRange.getSelectedDate(inst);
                            if (datepickerRange.startDate.getTime() > datepickerRange.endDate.getTime()) {
                                datepickerRange.endDate = datepickerRange.startDate;
                                datepickerRange.startDate = datepickerRange.currentDate;
                                $("#dateText").prepend("<span>" + dateText + "</span>");
                                datepickerRange.getInputDate();
                            } else {
                                $("#dateText").append("<span>" + dateText + "</span>");
                                datepickerRange.getInputDate();
                            }
                            datepickerRange.checkDays(inst);
                        }
                        return false;
                    },

                    onChangeMonthYear: function (year, month, inst) {
                        //console.log('onChangeMonthYear');
                        datepickerRange.currentDate = datepickerRange.getSelectedDate(inst);
                        datepickerRange.checkDays(inst);
                    }

                });

                $('#news_sort').click(function () {
                    Search.dateFrom = $('#filter_date_from').val();
                    Search.dateTo = $('#filter_date_to').val();
                    Search.load($('#search_tag').val());
                });
                $('#show_search_filter, .popap_close, #news_sort').click(function () {

                    $('#calendar-popap').toggle();
                });
            },

            init: function () {
                if (!$('#search_form').length) return;
                var arr = [];
                // $('body').on('click','#search_tag',function(e){
                //     e.preventDefault();
                //
                //     $('.search-link').trigger('click');
                //     $('#search_tag').val($(this).html());
                //     Search.load($('#search_tag').val(), true);
                //
                // });
                // $( "#search_tag" ).autocomplete({
                //     minLength: 3,
                //     source: arr,
                //     response: function( event, ui ) {
                //         Search.load(event.target.value);
                //     }
                // });
                // new More('#loadSearch','#res');
                // $('#search_form').submit(function(e){
                //     e.preventDefault();
                //     Search.load($('#search_tag').val());
                // });
                //console.log('kiad');
                $('.js-search, .footer-contacts--search, .footer-contacts--archive').click(function (e) {
                    e.preventDefault();
                    Search.load($('#search_tag').val());
                    //console.log('click');
                });
                $('#renew_content_search').hide();
                $('.js-search, .fullscreen-search .exit-link').click(function (e) {
                    e.preventDefault();


                    //  NewPager.saveContentPage();

                    var bodyDataState = $('body').data('isopen'),
                        bodyDataDevice = $('body').data('ismobile');
                    if ($('.fullscreen-search').is(':hidden')) {
                        //$('.search-wrap').hide();
                        Search.opened = true;
                        Search.showSearch();
                        history.pushState('', $(this).text(), $(this).attr('href'));
                        // bodyDataState = 1;
                    } else {
                        if (NewPager.prevPage) {
                            window.history.back();
                        } else {
                            window.location = AllPages.siteLink('/');
                        }
                    }
                });
                $('#clear_search_filter').click(function (e) {
                    e.preventDefault();
                    Search.dateFrom = '';
                    Search.dateTo = '';
                    $('#search_tag').val('');
                    $('#filter_date_from').val('');
                    $('#filter_date_to').val('');
                    Search.calendar.datepicker("setDate", '');
                    Search.calendar.datepicker("setDate", '');
                    Search.load('');
                });
                /*$('body').on('click','.search_tag',function(e){
                    e.preventDefault();
                    //console.log($(this).text());

                    if ($('.fullscreen-search').is(':hidden')) {
                        $('.fullscreen-search').addClass('_toggled');
                        $('body').addClass('no-scroll');
                        if (bodyDataDevice == 0) {
                            $('.pusher').addClass('blurred');
                        }
                        // bodyDataState = 1;
                    } else {
                        $('.fullscreen-search').removeClass('_toggled');
                        $('body').removeClass('no-scroll');
                        $('.pusher').removeClass('blurred');
                        if ($('.rubrics').is(':hidden')) {
                            $('body').removeClass('no-scroll');
                        }
                        // bodyDataState = 0;
                    }
                    $('#search_tag').val($(this).text());
                    Search.load($(this).text());
                });*/
                Search.sort();
                Search.events();
            },
            showSearch: function () {
                var bodyDataState = $('body').data('isopen'),
                    bodyDataDevice = $('body').data('ismobile');
                $('.fullscreen-search').addClass('_toggled');
                $('body').addClass('no-scroll');
                if (bodyDataDevice == 0) {
                    $('.pusher').addClass('blurred');
                }
                //  NewPager.saveContentPage();
                //  $('.fullscreen-search').html();

            },
            hideSearch: function () {
                $('.fullscreen-search').removeClass('_toggled');
                $('body').removeClass('no-scroll');
                $('.pusher').removeClass('blurred');
                if ($('.rubrics').is(':hidden')) {
                    $('body').removeClass('no-scroll');
                }
            },
            events: function () {
                window.addEventListener('popstate', function (event) {
                    var location = window.location.href;
                    var pattern = new RegExp('(.*)#search$');
                    if (pattern.test(location)) {
                        Search.showSearch();

                    } else {
                        Search.hideSearch();

                    }
                });
            }
        };

    Search.init();
});
