$(function () {
    // tinyMce();

    function tinyMce() {
        if (!$('.js-tiny-init').length) return;
        tinymce.init({
            selector: 'textarea.js-tiny-init',
            height: 500,
            menubar: false,
            statusbar: false,
            branding: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor customem noneditable',
                'searchreplace visualblocks code fullscreen noneditable',
                'insertdatetime media table contextmenu paste code help noneditable'],
            toolbar: 'youtubeInsert | quoteInsert | infographicsInsert | galleryInsert | vrezkaInsert | factInsert | insert | undo redo |  styleselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css', '/local/templates/um/styles/editor.css'],
            script_url: ['//code.jquery.com/jquery-1.12.4.min.js'],
            relative_urls: true,
            images_upload_url: '/ajax/bloggers/post_photo.php',
            images_upload_base_path: '/',
            images_upload_credentials: true,
            language_url: '/assets/js/tinymce/ru.js',
            setup: function (ed) {
                ed.on('init', function (e) {
                    $(ed.getBody()).on("click", ".dbe_element .edit_block", function () {
                        var parent = $(this).parents('.dbe_element');
                        TEditor.openPopup(ed, parent.attr('data-dbe'), parent.attr('data-id'));
                    });
                });

                ed.on('init', function (e) {
                    $(ed.getBody()).on("click", ".dbe_local_element .edit_block", function () {
                        var parent = $(this).parents('.dbe_local_element');
                        var data = {};
                        // alert($(this).find('[data-content]').length);
                        parent.find('[data-content]').each(function () {
                            // alert('olo');
                            data[parent.attr('data-content')] = parent.html();
                        });
                        TEditor.openPopup(ed, parent.attr('data-dbe'), parent.attr('data-id'), data);
                    });
                });
                ed.on('init', function (e) {
                    $(ed.getBody()).on("mouseover", ".dbe_element", function () {
                        if (!$(this).find(".edit_block").length) {
                            $(this).append($('<div class="edit_block"><span> Изменить</span></div>'));
                        }
                        $(this).find(".edit_block").show();
                    });
                });
                ed.on('init', function (e) {
                    $(ed.getBody()).on("mouseout", ".dbe_element", function () {
                        $(this).find(".edit_block").hide();
                    });

                    // $(ed.getBody()).on('click', '.js-gallery-modal', function (e) {
                    //     e.preventDefault();
                    //     var link = $(this).attr('href');
                    //     $.magnificPopup.open({
                    //         items: {
                    //             src: link
                    //         },
                    //         type: 'ajax',
                    //         callbacks: {
                    //             ajaxContentAdded: function () {
                    //                 galleryGrid();
                    //                 slideShow();
                    //             }
                    //         }
                    //     });
                    // });

                });

            }
        });
    }

    tinyMce2();

    function tinyMce2() {
        if (!$('.js-tiny-init').length) return;
        tinymce.init({
            selector: 'div.js-tiny-init',
            theme: 'inlite',
            inline: true,
            height: 150,
            menubar: false,
            toolbar: false,
            statusbar: false,
            branding: false,
            contextmenu: false,
            plugins: 'lists link',
            selection_toolbar: 'bold bullist link indentText indentFact',
            setup: function (editor) {
                editor.addButton('indentText', {
                    text: 'Отступ',
                    tooltip: 'Отступ',
                    icon: false,
                    onclick: showIndent
                });

                editor.addButton('indentFact', {
                    text: 'Факт',
                    tooltip: 'Факт',
                    icon: false,
                    onclick: showIndentFact
                });

                function getSelectionText() {
                    var rng = editor.selection.getRng();
                    rng.setStartBefore(editor.dom.getParent(editor.selection.getStart(), editor.dom.isBlock));
                    rng.setEndAfter(editor.dom.getParent(editor.selection.getEnd(), editor.dom.isBlock));
                    var tmp=editor.selection.getContent({format : 'html'});
                    return tmp;
                }

                function showIndent() {
                    var tmp = getSelectionText();
                    if(tmp) {
                        tmp=tmp.replace(/<[\/]*p>/g, '');
                        editor.execCommand('mceReplaceContent',false,'<div class="fact"><p>'+tmp+'</p></div>');
                    }
                }

                function showIndentFact() {
                    var tmp = getSelectionText();
                    if (tmp) {
                        tmp=tmp.replace(/<[\/]*p>/g, '');
                        editor.execCommand('mceReplaceContent',false,'<div class="fact"><h3 class="fact__title">Факт</h3><p>'+tmp+'</p></div>');
                    }
                }

                // function showIndent() {
                //     var rng = editor.selection.getRng();
                //     rng.setStartBefore(editor.dom.getParent(editor.selection.getStart(), editor.dom.isBlock));
                //     rng.setEndAfter(editor.dom.getParent(editor.selection.getEnd(), editor.dom.isBlock));
                //     var tmp=editor.selection.getContent({format : 'html'});
                //     if (tmp) {
                //         tmp=tmp.replace(/<[\/]*p>/g, '');
                //         editor.execCommand('mceReplaceContent',false,'<div class="fact"><p>'+tmp+'</p></div>');
                //     }
                // }

                // function showIndentFact() {
                //     var rng = editor.selection.getRng();
                //     rng.setStartBefore(editor.dom.getParent(editor.selection.getStart(), editor.dom.isBlock));
                //     rng.setEndAfter(editor.dom.getParent(editor.selection.getEnd(), editor.dom.isBlock));
                //     var tmp=editor.selection.getContent({format : 'html'});
                //     if (tmp) {
                //         tmp=tmp.replace(/<[\/]*p>/g, '');
                //         editor.execCommand('mceReplaceContent',false,'<div class="fact"><h3 class="fact__title">Факт</h3><p>'+tmp+'</p></div>');
                //     }
                // }
            }
        });

        // tinymce.PluginManager.add('list', function(editor) {
        //     function showDialog() {
        //         var rng = editor.selection.getRng();
        //         rng.setStartBefore(editor.dom.getParent(editor.selection.getStart(), editor.dom.isBlock));
        //         rng.setEndAfter(editor.dom.getParent(editor.selection.getEnd(), editor.dom.isBlock));
        //         var tmp=editor.selection.getContent({format : 'html'});
        //         if (tmp) {
        //             tmp=tmp.replace(/<[\/]*p>/g, '');
        //             editor.execCommand('mceReplaceContent',false,"<ul><li>"+tmp+"</li></ul>");
        //         }
        //     }
        //
        //
        //     editor.addMenuItem('list', {
        //         text: 'Bullet list',
        //         context: 'insert',
        //         onclick: showDialog
        //     });
        // });
    }

});
