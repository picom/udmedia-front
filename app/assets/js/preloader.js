(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.Символ1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 43
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#999999").ss(1.5,1,1).p("AAKAKIgTgT");
	this.shape.setTransform(93,36.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#999999").ss(1.5,1,1).p("AgZgZIA0Az");
	this.shape_1.setTransform(91.4,34.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#999999").ss(1.5,1,1).p("AgngmIBPBN");
	this.shape_2.setTransform(90,33.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#999999").ss(1.5,1,1).p("AgwgvIBhBf");
	this.shape_3.setTransform(89.1,32.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#999999").ss(1.5,1,1).p("Ag2g1IBtBr");
	this.shape_4.setTransform(88.5,31.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#999999").ss(1.5,1,1).p("AA5A3Ihwhu");
	this.shape_5.setTransform(88.4,31.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},48).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},23).to({state:[{t:this.shape_5}]},38).to({state:[]},1).wait(5));

	// Слой 42
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#999999").ss(1.5,1,1).p("AAIgHIgPAP");
	this.shape_6.setTransform(107.1,24);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#999999").ss(1.5,1,1).p("AgTAUIAngn");
	this.shape_7.setTransform(105.9,25.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#999999").ss(1.5,1,1).p("AgfAhIA/hB");
	this.shape_8.setTransform(104.7,26.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#999999").ss(1.5,1,1).p("AgrAtIBXha");
	this.shape_9.setTransform(103.5,27.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#999999").ss(1.5,1,1).p("Ag4A6IBxhz");
	this.shape_10.setTransform(102.3,29);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#999999").ss(1.5,1,1).p("ABFhGIiJCM");
	this.shape_11.setTransform(101,30.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#999999").ss(1.5,1,1).p("AgtAvIBbhd");
	this.shape_12.setTransform(98.8,32.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#999999").ss(1.5,1,1).p("AgbAdIA3g5");
	this.shape_13.setTransform(97,34.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#999999").ss(1.5,1,1).p("AgPAQIAfgf");
	this.shape_14.setTransform(95.7,35.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#999999").ss(1.5,1,1).p("AgHAIIAPgP");
	this.shape_15.setTransform(95,36.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#999999").ss(1.5,1,1).p("AAGgFIgLAL");
	this.shape_16.setTransform(94.7,36.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6}]},45).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},59).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[]},1).to({state:[]},1).wait(4));

	// Слой 41
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#999999").ss(1.5,1,1).p("AgHAAIAPAA");
	this.shape_17.setTransform(104.3,23.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#999999").ss(1.5,1,1).p("AgNAAIAbAA");
	this.shape_18.setTransform(104.8,23.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#999999").ss(1.5,1,1).p("AgRAAIAjAA");
	this.shape_19.setTransform(105.2,23.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#999999").ss(1.5,1,1).p("AgUAAIApAB");
	this.shape_20.setTransform(105.5,23.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#999999").ss(1.5,1,1).p("AgWAAIAtAB");
	this.shape_21.setTransform(105.7,23.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#999999").ss(1.5,1,1).p("AgEAAIAIAA");
	this.shape_22.setTransform(107.7,23.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_17}]},42).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21,p:{x:105.7}}]},1).to({state:[{t:this.shape_21,p:{x:105.8}}]},1).to({state:[{t:this.shape_21,p:{x:105.8}}]},57).to({state:[]},1).to({state:[{t:this.shape_22}]},4).to({state:[]},1).to({state:[]},1).wait(9));

	// Слой 40
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#999999").ss(1.5,1,1).p("AAAgKIABAV");
	this.shape_23.setTransform(101.7,9.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#999999").ss(1.5,1,1).p("AgDggIAHBB");
	this.shape_24.setTransform(102,11.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#999999").ss(1.5,1,1).p("AgFgxIALBj");
	this.shape_25.setTransform(102.2,13.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#999999").ss(1.5,1,1).p("AgHg+IAPB9");
	this.shape_26.setTransform(102.4,14.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#999999").ss(1.5,1,1).p("AgIhFIARCL");
	this.shape_27.setTransform(102.5,15.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#999999").ss(1.5,1,1).p("AgIhHIARCP");
	this.shape_28.setTransform(102.5,15.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#999999").ss(1.5,1,1).p("AgFgwIALBh");
	this.shape_29.setTransform(102.8,18.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#999999").ss(1.5,1,1).p("AgDgdIAHA7");
	this.shape_30.setTransform(103,20);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#999999").ss(1.5,1,1).p("AgBgQIADAh");
	this.shape_31.setTransform(103.2,21.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#999999").ss(1.5,1,1).p("AAAgIIABAR");
	this.shape_32.setTransform(103.3,22.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#999999").ss(1.5,1,1).p("AAAgGIABAN");
	this.shape_33.setTransform(103.3,22.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_23}]},39).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_28}]},13).to({state:[{t:this.shape_28}]},42).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[]},1).to({state:[]},1).wait(14));

	// Слой 39
	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#999999").ss(1.5,1,1).p("AgHAIIAPgP");
	this.shape_34.setTransform(92.4,19.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#999999").ss(1.5,1,1).p("AgWAZIAtgx");
	this.shape_35.setTransform(93.9,17.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#999999").ss(1.5,1,1).p("AgiAnIBFhN");
	this.shape_36.setTransform(95.1,16);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#999999").ss(1.5,1,1).p("AgqAxIBVhh");
	this.shape_37.setTransform(95.9,15);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#999999").ss(1.5,1,1).p("AgvA2IBfhr");
	this.shape_38.setTransform(96.4,14.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#999999").ss(1.5,1,1).p("AgxA4IBjhv");
	this.shape_39.setTransform(96.6,14.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#999999").ss(1.5,1,1).p("AghAmIBDhL");
	this.shape_40.setTransform(98.2,12.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#999999").ss(1.5,1,1).p("AgUAYIApgv");
	this.shape_41.setTransform(99.5,11);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#999999").ss(1.5,1,1).p("AgMAOIAZgb");
	this.shape_42.setTransform(100.4,10);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#999999").ss(1.5,1,1).p("AgGAIIANgP");
	this.shape_43.setTransform(100.9,9.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#999999").ss(1.5,1,1).p("AgEAGIAJgL");
	this.shape_44.setTransform(101.1,9.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_34}]},36).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_39}]},16).to({state:[{t:this.shape_39}]},37).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[]},1).to({state:[]},1).wait(19));

	// Слой 38
	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#999999").ss(1.5,1,1).p("AgHgIIAPAR");
	this.shape_45.setTransform(82.5,9.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#999999").ss(1.5,1,1).p("AgWgZIAtAz");
	this.shape_46.setTransform(84,11.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#999999").ss(1.5,1,1).p("AgigmIBFBN");
	this.shape_47.setTransform(85.2,12.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#999999").ss(1.5,1,1).p("AgqgvIBVBf");
	this.shape_48.setTransform(86,13.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#999999").ss(1.5,1,1).p("Agvg1IBfBr");
	this.shape_49.setTransform(86.5,14.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#999999").ss(1.5,1,1).p("Agxg2IBiBt");
	this.shape_50.setTransform(86.7,14.3);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#999999").ss(1.5,1,1).p("AgDgDIAHAH");
	this.shape_51.setTransform(91.2,19.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_45}]},55).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_50}]},29).to({state:[]},1).to({state:[{t:this.shape_51}]},4).to({state:[]},1).to({state:[]},2).wait(23));

	// Слой 37
	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#999999").ss(1.5,1,1).p("AgBAOIACgb");
	this.shape_52.setTransform(80.4,21.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#999999").ss(1.5,1,1).p("AgCAjIAGhF");
	this.shape_53.setTransform(80.6,19.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#999999").ss(1.5,1,1).p("AgEAzIAJhl");
	this.shape_54.setTransform(80.8,18.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#999999").ss(1.5,1,1).p("AgFA/IALh8");
	this.shape_55.setTransform(80.9,16.9);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#999999").ss(1.5,1,1).p("AgGBGIANiL");
	this.shape_56.setTransform(81,16.2);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#999999").ss(1.5,1,1).p("AgGBIIANiP");
	this.shape_57.setTransform(81,16);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#999999").ss(1.5,1,1).p("AgEAwIAJhf");
	this.shape_58.setTransform(81.2,13.6);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#999999").ss(1.5,1,1).p("AgCAeIAFg7");
	this.shape_59.setTransform(81.4,11.8);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#999999").ss(1.5,1,1).p("AgBARIADgh");
	this.shape_60.setTransform(81.5,10.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#999999").ss(1.5,1,1).p("AAAAJIABgR");
	this.shape_61.setTransform(81.6,9.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#999999").ss(1.5,1,1).p("AAAAHIABgM");
	this.shape_62.setTransform(81.6,9.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_52}]},50).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_57}]},2).to({state:[{t:this.shape_57}]},27).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[]},1).to({state:[]},2).wait(28));

	// Слой 36
	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#999999").ss(1.5,1,1).p("AgLAAIAXAA");
	this.shape_63.setTransform(76.5,23.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#999999").ss(1.5,1,1).p("AgQAAIAhAA");
	this.shape_64.setTransform(76.9,23.1);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#999999").ss(1.5,1,1).p("AgTAAIAnAA");
	this.shape_65.setTransform(77.3,23.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#999999").ss(1.5,1,1).p("AgWAAIAtAA");
	this.shape_66.setTransform(77.6,23.2);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#999999").ss(1.5,1,1).p("AgYAAIAxAA");
	this.shape_67.setTransform(77.7,23.2);
	this.shape_67._off = true;

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#999999").ss(1.5,1,1).p("AgEAAIAJAA");
	this.shape_68.setTransform(79.7,23.2);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#999999").ss(1.5,1,1).p("AgMAAIAZAA");
	this.shape_69.setTransform(79,23.2);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#999999").ss(1.5,1,1).p("AgRAAIAjAA");
	this.shape_70.setTransform(78.5,23.2);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#999999").ss(1.5,1,1).p("AgVAAIArAA");
	this.shape_71.setTransform(78.1,23.2);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#999999").ss(1.5,1,1).p("AgXAAIAvAA");
	this.shape_72.setTransform(77.8,23.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_63}]},45).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_67}]},7).to({state:[{t:this.shape_68}]},22).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_67}]},1).to({state:[]},1).to({state:[]},3).wait(32));
	this.timeline.addTween(cjs.Tween.get(this.shape_67).wait(49).to({_off:false},0).wait(1).to({x:77.8},0).wait(7).to({_off:true},22).wait(5).to({_off:false},0).to({_off:true},1).wait(35));

	// Слой 35
	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#999999").ss(1.5,1,1).p("AAMAMIgXgX");
	this.shape_73.setTransform(92.8,40.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#999999").ss(1.5,1,1).p("AgogoIBRBR");
	this.shape_74.setTransform(89.8,37.8);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#999999").ss(1.5,1,1).p("Ag/g/IB/B/");
	this.shape_75.setTransform(87.6,35.5);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#999999").ss(1.5,1,1).p("AhQhQICgCh");
	this.shape_76.setTransform(85.9,33.8);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#999999").ss(1.5,1,1).p("AhZhaICzC0");
	this.shape_77.setTransform(84.9,32.9);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#999999").ss(1.5,1,1).p("ABeBeIi6i7");
	this.shape_78.setTransform(84.6,32.5);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#999999").ss(1.5,1,1).p("Ag9g9IB7B7");
	this.shape_79.setTransform(81.5,29.4);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#999999").ss(1.5,1,1).p("AglglIBLBL");
	this.shape_80.setTransform(79,26.9);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#999999").ss(1.5,1,1).p("AgTgTIAnAn");
	this.shape_81.setTransform(77.3,25.2);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#999999").ss(1.5,1,1).p("AgJgJIATAT");
	this.shape_82.setTransform(76.3,24.1);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#999999").ss(1.5,1,1).p("AAGAGIgMgL");
	this.shape_83.setTransform(75.9,23.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_73}]},40).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_78}]},10).to({state:[{t:this.shape_78}]},19).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[]},1).to({state:[]},8).wait(32));

	// Слой 15
	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#999999").ss(1.5,1,1).p("AANgMIgZAZ");
	this.shape_84.setTransform(115.3,20.7);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#999999").ss(1.5,1,1).p("AgwAxIBhhg");
	this.shape_85.setTransform(111.7,24.3);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#999999").ss(1.5,1,1).p("AhMBMICZiX");
	this.shape_86.setTransform(108.9,27.1);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#999999").ss(1.5,1,1).p("AhgBgIDBi/");
	this.shape_87.setTransform(106.9,29.1);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#999999").ss(1.5,1,1).p("AhsBsIDZjX");
	this.shape_88.setTransform(105.7,30.3);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#999999").ss(1.5,1,1).p("ABxhvIjhDf");
	this.shape_89.setTransform(105.3,30.7);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#999999").ss(1.5,1,1).p("AhKBKICViU");
	this.shape_90.setTransform(101.5,34.5);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#999999").ss(1.5,1,1).p("AgsAtIBZhZ");
	this.shape_91.setTransform(98.5,37.4);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#999999").ss(1.5,1,1).p("AgYAYIAwgv");
	this.shape_92.setTransform(96.4,39.5);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#999999").ss(1.5,1,1).p("AgLAMIAXgX");
	this.shape_93.setTransform(95.1,40.7);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#999999").ss(1.5,1,1).p("AAIgHIgPAP");
	this.shape_94.setTransform(94.7,41.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_84}]},35).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_89}]},29).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[]},1).to({state:[]},13).wait(32));

	// Слой 34
	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#999999").ss(1.5,1,1).p("AgLAAIAXAA");
	this.shape_95.setTransform(107.9,19.4);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#999999").ss(1.5,1,1).p("AgYAAIAxAA");
	this.shape_96.setTransform(109.2,19.4);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().s("#999999").ss(1.5,1,1).p("AgjAAIBHAA");
	this.shape_97.setTransform(110.3,19.4);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#999999").ss(1.5,1,1).p("AgrAAIBXAB");
	this.shape_98.setTransform(111,19.4);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#999999").ss(1.5,1,1).p("AgvAAIBfAB");
	this.shape_99.setTransform(111.5,19.4);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().s("#999999").ss(1.5,1,1).p("AgxAAIBjAB");
	this.shape_100.setTransform(111.6,19.4);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().s("#999999").ss(1.5,1,1).p("AgiAAIBFAA");
	this.shape_101.setTransform(113.1,19.4);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().s("#999999").ss(1.5,1,1).p("AgWAAIAtAA");
	this.shape_102.setTransform(114.3,19.4);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f().s("#999999").ss(1.5,1,1).p("AgOAAIAdAA");
	this.shape_103.setTransform(115.1,19.5);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#999999").ss(1.5,1,1).p("AgJAAIATAA");
	this.shape_104.setTransform(115.6,19.5);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#999999").ss(1.5,1,1).p("AgIAAIARAA");
	this.shape_105.setTransform(115.7,19.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_95}]},30).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_100}]},28).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[]},1).to({state:[]},18).wait(32));

	// Слой 25
	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f().s("#999999").ss(1.5,1,1).p("AAAgMIACAZ");
	this.shape_106.setTransform(104.4,0.7);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#999999").ss(1.5,1,1).p("AgEgrIAJBX");
	this.shape_107.setTransform(104.7,3.9);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#999999").ss(1.5,1,1).p("AgIhDIAQCH");
	this.shape_108.setTransform(105.1,6.3);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f().s("#999999").ss(1.5,1,1).p("AgJhVIATCr");
	this.shape_109.setTransform(105.3,8);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#999999").ss(1.5,1,1).p("AgLhfIAXC/");
	this.shape_110.setTransform(105.4,9.1);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f().s("#999999").ss(1.5,1,1).p("AgLhjIAXDH");
	this.shape_111.setTransform(105.4,9.4);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f().s("#999999").ss(1.5,1,1).p("AgJhQIATCh");
	this.shape_112.setTransform(105.7,11.3);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f().s("#999999").ss(1.5,1,1).p("AgHg+IAPB9");
	this.shape_113.setTransform(105.9,13.1);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f().s("#999999").ss(1.5,1,1).p("AgEgrIAKBX");
	this.shape_114.setTransform(106.1,14.9);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f().s("#999999").ss(1.5,1,1).p("AgCgZIAFAz");
	this.shape_115.setTransform(106.3,16.8);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f().s("#999999").ss(1.5,1,1).p("AAAgGIABAN");
	this.shape_116.setTransform(106.6,18.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_106}]},25).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_111}]},6).to({state:[{t:this.shape_111}]},23).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[]},1).to({state:[]},23).wait(32));

	// Слой 27
	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f().s("#999999").ss(1.5,1,1).p("AgJAMIATgX");
	this.shape_117.setTransform(92.5,13.2);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f().s("#999999").ss(1.5,1,1).p("AgdAjIA7hE");
	this.shape_118.setTransform(94.5,11);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f().s("#999999").ss(1.5,1,1).p("AgrA0IBXhn");
	this.shape_119.setTransform(95.9,9.2);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f().s("#999999").ss(1.5,1,1).p("Ag2BBIBtiB");
	this.shape_120.setTransform(97,7.9);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f().s("#999999").ss(1.5,1,1).p("Ag8BIIB5iP");
	this.shape_121.setTransform(97.6,7.2);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f().s("#999999").ss(1.5,1,1).p("Ag+BLIB+iV");
	this.shape_122.setTransform(97.9,6.9);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f().s("#999999").ss(1.5,1,1).p("AgpAyIBThj");
	this.shape_123.setTransform(100,4.4);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f().s("#999999").ss(1.5,1,1).p("AgZAfIAzg9");
	this.shape_124.setTransform(101.6,2.5);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f().s("#999999").ss(1.5,1,1).p("AgNARIAbgh");
	this.shape_125.setTransform(102.8,1.1);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f().s("#999999").ss(1.5,1,1).p("AgGAIIANgP");
	this.shape_126.setTransform(103.5,0.3);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f().s("#999999").ss(1.5,1,1).p("AgEAGIAJgL");
	this.shape_127.setTransform(103.7,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_117}]},20).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_122}]},11).to({state:[{t:this.shape_122}]},18).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_126}]},1).to({state:[{t:this.shape_127}]},1).to({state:[]},1).to({state:[]},28).wait(32));

	// Слой 33
	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f().s("#999999").ss(1.5,1,1).p("AgKgMIAUAZ");
	this.shape_128.setTransform(80.2,0.8);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f().s("#999999").ss(1.5,1,1).p("AgcgiIA5BF");
	this.shape_129.setTransform(82,3);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f().s("#999999").ss(1.5,1,1).p("AgqgzIBWBn");
	this.shape_130.setTransform(83.5,4.7);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f().s("#999999").ss(1.5,1,1).p("Ag1hAIBrCB");
	this.shape_131.setTransform(84.5,6);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f().s("#999999").ss(1.5,1,1).p("Ag7hHIB3CP");
	this.shape_132.setTransform(85.1,6.7);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f().s("#999999").ss(1.5,1,1).p("Ag9hKIB7CV");
	this.shape_133.setTransform(85.3,7);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f().s("#999999").ss(1.5,1,1).p("AgogxIBSBj");
	this.shape_134.setTransform(87.4,9.4);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f().s("#999999").ss(1.5,1,1).p("AgYgeIAxA9");
	this.shape_135.setTransform(89,11.3);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f().s("#999999").ss(1.5,1,1).p("AgNgQIAbAh");
	this.shape_136.setTransform(90.1,12.7);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f().s("#999999").ss(1.5,1,1).p("AgGgIIANAR");
	this.shape_137.setTransform(90.8,13.5);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f().s("#999999").ss(1.5,1,1).p("AgEgFIAJAL");
	this.shape_138.setTransform(91,13.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_128}]},15).to({state:[{t:this.shape_129}]},1).to({state:[{t:this.shape_130}]},1).to({state:[{t:this.shape_131}]},1).to({state:[{t:this.shape_132}]},1).to({state:[{t:this.shape_133}]},1).to({state:[{t:this.shape_133}]},16).to({state:[{t:this.shape_133}]},13).to({state:[{t:this.shape_134}]},1).to({state:[{t:this.shape_135}]},1).to({state:[{t:this.shape_136}]},1).to({state:[{t:this.shape_137}]},1).to({state:[{t:this.shape_138}]},1).to({state:[]},1).to({state:[]},33).wait(32));

	// Слой 32
	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f().s("#999999").ss(1.5,1,1).p("AAAANIACgZ");
	this.shape_139.setTransform(76.7,18.2);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f().s("#999999").ss(1.5,1,1).p("AgEAsIAKhX");
	this.shape_140.setTransform(77.1,15.1);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f().s("#999999").ss(1.5,1,1).p("AgIBEIARiH");
	this.shape_141.setTransform(77.4,12.6);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f().s("#999999").ss(1.5,1,1).p("AgKBWIAVir");
	this.shape_142.setTransform(77.7,10.9);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f().s("#999999").ss(1.5,1,1).p("AgLBgIAXi/");
	this.shape_143.setTransform(77.8,9.8);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f().s("#999999").ss(1.5,1,1).p("AgMBkIAZjH");
	this.shape_144.setTransform(77.8,9.5);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f().s("#999999").ss(1.5,1,1).p("AgHBDIAPiF");
	this.shape_145.setTransform(78.3,6.2);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f().s("#999999").ss(1.5,1,1).p("AgEApIAJhR");
	this.shape_146.setTransform(78.6,3.6);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f().s("#999999").ss(1.5,1,1).p("AgCAXIAFgt");
	this.shape_147.setTransform(78.8,1.8);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f().s("#999999").ss(1.5,1,1).p("AAAAMIACgX");
	this.shape_148.setTransform(79,0.7);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f().s("#999999").ss(1.5,1,1).p("AAAAIIABgP");
	this.shape_149.setTransform(79,0.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_139}]},10).to({state:[{t:this.shape_140}]},1).to({state:[{t:this.shape_141}]},1).to({state:[{t:this.shape_142}]},1).to({state:[{t:this.shape_143}]},1).to({state:[{t:this.shape_144}]},1).to({state:[{t:this.shape_144}]},21).to({state:[{t:this.shape_144}]},8).to({state:[{t:this.shape_145}]},1).to({state:[{t:this.shape_146}]},1).to({state:[{t:this.shape_147}]},1).to({state:[{t:this.shape_148}]},1).to({state:[{t:this.shape_149}]},1).to({state:[]},1).to({state:[]},38).wait(32));

	// Слой 31
	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f().s("#999999").ss(1.5,1,1).p("AgLAAIAXAA");
	this.shape_150.setTransform(67.9,19.6);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f().s("#999999").ss(1.5,1,1).p("AgYAAIAxAA");
	this.shape_151.setTransform(69.2,19.5);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f().s("#999999").ss(1.5,1,1).p("AgjAAIBHAA");
	this.shape_152.setTransform(70.3,19.5);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f().s("#999999").ss(1.5,1,1).p("AgqABIBVgB");
	this.shape_153.setTransform(71,19.5);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f().s("#999999").ss(1.5,1,1).p("AgvABIBfgB");
	this.shape_154.setTransform(71.5,19.5);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f().s("#999999").ss(1.5,1,1).p("AgwABIBigB");
	this.shape_155.setTransform(71.6,19.5);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f().s("#999999").ss(1.5,1,1).p("AghAAIBDAA");
	this.shape_156.setTransform(73.1,19.5);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f().s("#999999").ss(1.5,1,1).p("AgVAAIArAA");
	this.shape_157.setTransform(74.3,19.5);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f().s("#999999").ss(1.5,1,1).p("AgNAAIAbAA");
	this.shape_158.setTransform(75.2,19.5);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f().s("#999999").ss(1.5,1,1).p("AgIAAIARAA");
	this.shape_159.setTransform(75.7,19.5);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f().s("#999999").ss(1.5,1,1).p("AgGAAIANAA");
	this.shape_160.setTransform(75.9,19.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_150}]},5).to({state:[{t:this.shape_151}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_153}]},1).to({state:[{t:this.shape_154}]},1).to({state:[{t:this.shape_155}]},1).to({state:[{t:this.shape_155}]},26).to({state:[{t:this.shape_155}]},3).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_157}]},1).to({state:[{t:this.shape_158}]},1).to({state:[{t:this.shape_159}]},1).to({state:[{t:this.shape_160}]},1).to({state:[]},1).to({state:[]},43).wait(32));

	// Слой 30
	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f().s("#999999").ss(1.5,1,1).p("AAMAMIgXgX");
	this.shape_161.setTransform(90.2,43.2);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f().s("#999999").ss(1.5,1,1).p("AgzgzIBnBn");
	this.shape_162.setTransform(86.2,39.1);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f().s("#999999").ss(1.5,1,1).p("AhShTIClCn");
	this.shape_163.setTransform(83.1,36);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f().s("#999999").ss(1.5,1,1).p("AhphpIDTDT");
	this.shape_164.setTransform(80.8,33.8);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f().s("#999999").ss(1.5,1,1).p("Ah2h3IDtDv");
	this.shape_165.setTransform(79.5,32.4);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f().s("#999999").ss(1.5,1,1).p("AB7B8Ij2j3");
	this.shape_166.setTransform(79.1,32);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f().s("#999999").ss(1.5,1,1).p("AhRhRICjCj");
	this.shape_167.setTransform(74.9,27.8);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f().s("#999999").ss(1.5,1,1).p("AgwgwIBhBh");
	this.shape_168.setTransform(71.6,24.5);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f().s("#999999").ss(1.5,1,1).p("AgZgZIAzAz");
	this.shape_169.setTransform(69.3,22.2);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f().s("#999999").ss(1.5,1,1).p("AgLgLIAXAX");
	this.shape_170.setTransform(67.9,20.8);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f().s("#999999").ss(1.5,1,1).p("AAHAHIgNgN");
	this.shape_171.setTransform(67.4,20.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_161}]}).to({state:[{t:this.shape_162}]},1).to({state:[{t:this.shape_163}]},1).to({state:[{t:this.shape_164}]},1).to({state:[{t:this.shape_165}]},1).to({state:[{t:this.shape_166}]},1).to({state:[{t:this.shape_166}]},29).to({state:[{t:this.shape_167}]},1).to({state:[{t:this.shape_168}]},1).to({state:[{t:this.shape_169}]},1).to({state:[{t:this.shape_170}]},1).to({state:[{t:this.shape_171}]},1).to({state:[]},1).to({state:[]},48).wait(32));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(88,41,4.4,4.4);


// stage content:
(lib.preloaderUM = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Символ1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-14.3,61.1,1,1,0,0,0,24.4,22.9);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).wait(119));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;
// library properties:
lib.properties = {
	width: 120,
	height: 120,
	fps: 45,
	color: "#E3E3E3",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;