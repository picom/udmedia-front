// page404
(function (cjs, an) {

    var p; // shortcut to reference prototypes
    var lib={};var ss={};var img={};
    lib.webFontTxtInst = {};
    var loadedTypekitCount = 0;
    var loadedGoogleCount = 0;
    var gFontsUpdateCacheList = [];
    var tFontsUpdateCacheList = [];
    lib.ssMetadata = [];



    lib.updateListCache = function (cacheList) {
        for(var i = 0; i < cacheList.length; i++) {
            if(cacheList[i].cacheCanvas)
                cacheList[i].updateCache();
        }
    };

    lib.addElementsToCache = function (textInst, cacheList) {
        var cur = textInst;
        while(cur != null && cur != exportRoot) {
            if(cacheList.indexOf(cur) != -1)
                break;
            cur = cur.parent;
        }
        if(cur != exportRoot) {
            var cur2 = textInst;
            var index = cacheList.indexOf(cur);
            while(cur2 != null && cur2 != cur) {
                cacheList.splice(index, 0, cur2);
                cur2 = cur2.parent;
                index++;
            }
        }
        else {
            cur = textInst;
            while(cur != null && cur != exportRoot) {
                cacheList.push(cur);
                cur = cur.parent;
            }
        }
    };

    lib.gfontAvailable = function(family, totalGoogleCount) {
        lib.properties.webfonts[family] = true;
        var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
        for(var f = 0; f < txtInst.length; ++f)
            lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);

        loadedGoogleCount++;
        if(loadedGoogleCount == totalGoogleCount) {
            lib.updateListCache(gFontsUpdateCacheList);
        }
    };

    lib.tfontAvailable = function(family, totalTypekitCount) {
        lib.properties.webfonts[family] = true;
        var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
        for(var f = 0; f < txtInst.length; ++f)
            lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);

        loadedTypekitCount++;
        if(loadedTypekitCount == totalTypekitCount) {
            lib.updateListCache(tFontsUpdateCacheList);
        }
    };
// symbols:



    (lib.Анимация16 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('Aj0AVQgjgJAKgjQAKgkAjAJQDYA2DpgsQAjgHAKAkQAKAjgjAHQh1AWhwAAQiFAAh/ggg');
        this.shape.setTransform(-2.5,-97.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#FF0000').s().p('AAXBlQgngdgZgrQgVglgNg2QgJgjAkgKQAkgJAJAjQAJApAMAYQAQAhAaAUQAdAVgTAhQgLATgPAAQgJAAgMgJg');
        this.shape_1.setTransform(-1.2,-238.2);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#FF0000').s().p('Ag4BjQgkgJALgjQAQguAXghQAYgkAkgeQAcgXAaAaQAaAbgbAXQgfAZgSAZQgUAbgLAjQgJAbgXAAQgHAAgIgDg');
        this.shape_2.setTransform(9,-238.3);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#FF0000').s().p('AgpBgQAAh+ALhJQAGgkAjAKQAkAKgGAkQgGAngBAyIgBBaQAAAkgkAAQgmAAAAgkg');
        this.shape_3.setTransform(4.9,-243.4);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('AByA/Qg2g0g2gKQg9gLg4ArQgcAWgbgaQgagaAcgWQBRhABZAKQBUAKBNBJQAaAZgaAbQgOANgOAAQgMAAgNgMg');
        this.shape_4.setTransform(-43,52.5);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('AAeA0QgkAAgSgJQgWgKgPgdQgHgNADgOQAEgPANgIQANgIAQAEQAPAEAGANIAFAJIABACIAAAAIADABIAEAAIAPAAQAPAAALALQALAKAAAPQAAAPgLALQgKALgPAAIgBAAg');
        this.shape_5.setTransform(-115,42.7);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#000000').s().p('ACqBeQgZghgVgQQgfgWggAEQgQACgbAKQgdALgMACQhBAPg4ghQgtgbgtg+QgVgdAggTQAhgTAUAdQAaAlAUARQAfAaAhgDQARgBAagJIApgOQBAgRA4AeQAtAYAuA7QAWAdghATQgNAIgLAAQgRAAgNgSg');
        this.shape_6.setTransform(-79.7,44.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#000000').s().p('AgHAmQgPAAgLgMQgLgKAAgQQAAgOALgMQALgKAPgBIAOAAQAQABALAKQALAMAAAOQAAAQgLAKQgLAMgQAAg');
        this.shape_7.setTransform(81.2,46.7);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#000000').s().p('AieBGQgagbAXgbQBChKBQgSQBSgTBWAuQAgASgTAfQgSAggggRQh5hAhnB2QgMAOgNAAQgMAAgNgNg');
        this.shape_8.setTransform(49.6,52.4);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#000000').s().p('ABTBKQh5gmhHgpQgfgTASggQATggAfASQA+AlBwAjQAjAMgKAkQgHAbgWAAQgHAAgIgDg');
        this.shape_9.setTransform(-95.9,-54.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('ABoBeQh2gch6hhQgcgXAagaQAbgaAcAWQBlBSBqAYQAkAIgKAkQgIAdgZAAIgNgBg');
        this.shape_10.setTransform(-88.6,-73.1);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('Ah0BPQgkgCAAglQAAglAkACQA3ADA6gWQAogRA8goQAegUATAhQASAggeAUQhCArg1AUQg5AXg5AAIgRgBg');
        this.shape_11.setTransform(95.4,-63.4);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#000000').s().p('AiYBRQAAgmAkgEQA/gJA1gnQAlgdA0g/QALgNAOABQAOAAAKALQAbAagXAcQg8BHg2AkQg/AshRAKIgIABQgcAAAAghg');
        this.shape_12.setTransform(91.4,-81.8);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#000000').s().p('AAACWQgQiYg2iKQgNghAkgKQAkgKAMAhQA2CKATCsQAEAkglAAQglAAgEgkg');
        this.shape_13.setTransform(115.5,26.8);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#000000').s().p('AihJ5QgkgKAKgjQBsmGA2jMQBVlMA5kMQAHgkAkAKQAkAKgHAjQg5ENhWFLQg1DPhsGEQgIAbgXAAQgHAAgIgCg');
        this.shape_14.setTransform(-13.8,-169.1);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#000000').s().p('ABlKNQg1jthUmWQhjnWgmisQgHgjAkgKQAkgKAHAjQAmCvBiHUQBVGVA1DtQAIAjgkAKQgHACgHAAQgXAAgHgbg');
        this.shape_15.setTransform(19.2,-168.6);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('AAJCJQgOg5gahIIgvh9QgNgiAkgKQAkgKANAiIAuB/QAaBHAPA4QAJAjgkAKQgIACgHAAQgXAAgHgbg');
        this.shape_16.setTransform(94.3,-26.8);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('AhKCAQgggTAOghQAdhDAXgmQAig7AlgiQAbgYAaAaQAbAagbAYQgjAfgfA3QgSAjgcA/QgJAVgQAAQgJAAgMgHg');
        this.shape_17.setTransform(36.1,-125.8);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('AC0G2QgOgjg6iuQgriAgqhLQgmhEgmgyQgPgSgNgaIgXgwQg4hvhUhmQgYgcAbgaQAKgLAOAAQAOgBALANQBiB1A3BwIAXAvQANAaANATQAxBBAcA1QArBPAtCFQA+CzAPAmQAOAhgkAKQgIACgIAAQgXAAgLgZg');
        this.shape_18.setTransform(63.7,-93);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('AgIC0QgagbAYgaQAug0AYhQQAQg3ACg3QhWAMhXAxQgfASgTggQgTggAggSQB5hFB+gFQAPgBALAMQALALABAPQAEBZgcBZQgeBdg5BAQgMANgNAAQgNAAgMgNg');
        this.shape_19.setTransform(107.4,-11.6);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('Ai4GMQAcjQBPjSQBLjFB0jAQATgfAgATQAgATgSAfQhxC3hHC7QhODKgaDFQgFAjglAAQgmAAAFgjg');
        this.shape_20.setTransform(-103.7,1);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('AjmG6QgggSARggQAjg/BAiMQA9iHAnhDIApg+QAZgnAKgaQAYg/AVgrQAshiA8hWQAVgeAgATQAgATgUAeQhCBfguBnIgbBDQgRAmgQAZQgjA1gcAyQghA/g9CEQg8CDgkBBQgKATgRAAQgKAAgMgHg');
        this.shape_21.setTransform(-63.7,-89.5);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AATBjQgLgQgjg8QgdgvgYgZQgYgaAagaQAbgbAYAaQAaAcAfA0QApBCANARQAVAdggATQgNAHgLAAQgRAAgNgRg');
        this.shape_22.setTransform(-34.2,-124.3);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#E6E7E8').s().p('AgTCRQjqgPj3gqQjcgkiOg0QgggLgNgeQgLgaAIgeQAIgdAWgOQAZgQAgAMQCSA2DZAjQEJAsDUAJQC2AHDogOQBpgHEzgdQAigDAUAVQARATAAAeQAAAegSAXQgUAZghADQlKAfhlAHQiLAJh6AAQhaAAhQgFg');
        this.shape_23.setTransform(-8.7,32);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#E6E7E8').s().p('AEcCHQingLiYgeQhZgRg5gRQhPgXg7ghQgdgQgGgeQgFgbAPgaQAPgaAagKQAegKAeARQAxAcBHATQAtAMBSAQQCKAaCOAKQAiACAUAYQASAWAAAeQAAAfgSAUQgSAUgdAAIgHgBg');
        this.shape_24.setTransform(-56.8,-1.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#E6E7E8').s().p('Al6BaQgigBgUgZQgSgVAAgfQAAgdASgUQAUgXAiACQDTALCegGQDHgHCngiQAigHAZATQAYAQAIAeQAIAdgMAXQgNAagiAHQiyAkjQAJQhGADhNAAQhyAAiAgHg');
        this.shape_25.setTransform(40.9,5.1);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AgKA5QgKgLgBgPIAAgKIAAADIAAgEIAAABIgCgFQgBgBABACIgBgCIgDgEIADADIgQgQQgZgZAagbQAagaAZAZQAXAXAGAMQAMAXAAAcQAAAPgLALQgLALgPAAQgQAAgKgLgAgVAVg');
        this.shape_26.setTransform(145.3,-118.3);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('AACA6QgNgIgEgPQgCgHgHgHQgFgHgGgEIgBgBIACACIgDgDIABABIgCgBQgNgIgEgOQgEgPAIgNQAHgNAQgFQAQgEAMAIQAsAfAMAoQAEAPgHAOQgIANgPAEIgKABQgKAAgIgEg');
        this.shape_27.setTransform(138.8,-92.7);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AAFBCQgPgEgGgNIgeg7QgHgOADgPQAEgPANgHQANgIAQAEQAPAEAGANIAeA7QAHAOgDAOQgEAPgNAIQgIAFgKAAIgLgBg');
        this.shape_28.setTransform(132.3,-67.2);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('AgKAzQgLgLAAgPIgBgEIABADIgBgEIAAABIgBgFIgBgBIgCgDIgHgFQgNgHgEgOQgEgQAIgNQAHgNAPgEQAPgEANAIQAWAMANAWQANAWAAAZQABAPgMALQgLALgPAAQgPAAgKgLgAgaAMIAAAAIgBgBIABABg');
        this.shape_29.setTransform(128.3,-43.7);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#FF0000').s().p('AgKBEQgJgKgCgPIABAFIgBgGIgahAQgHgOADgOQAEgPANgIQANgIAQAEQAPAEAGANIAXAyQAMAdACAWQABAPgMAMQgMALgPAAQgPAAgKgLg');
        this.shape_30.setTransform(169.6,-126.1);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#FF0000').s().p('AhNA4QgbgaAagZQBJhHA9gDQAkgCAAAmQAAAkgkACQgeABgzAzQgNANgNAAQgNAAgNgOg');
        this.shape_31.setTransform(133.4,-149.1);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#FF0000').s().p('AASAoQgOgHgMgCIgdgBQgkAAAAgkQAAgmAkAAQAzAAAqAUQANAHAEAQQAEAOgIANQgIAOgPADIgJACQgJAAgKgFg');
        this.shape_32.setTransform(180.9,-148.2);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#FF0000').s().p('AhFBGQgOgEgJgOQgIgOAFgOQARgrAxgWQAQgHAcgJIASgGIAHgDQAggPATAgQATAgghAQQgMAFgTAGIggALQghALgEAMQgGAPgMAHQgIAFgJAAQgFAAgGgBg');
        this.shape_33.setTransform(177.1,-171.9);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#FF0000').s().p('AgDBPQgFgQgjg4QgagpgBgiQgCgkAlAAQAkAAACAkQABAPALATIAUAeQAXAjAKAcQANAigkAKQgIACgHAAQgYAAgJgag');
        this.shape_34.setTransform(145.4,-178.4);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#000000').s().p('AAKAvQgWgBgMgOIgJgNIgDgDQgKgLAAgOQgBgQALgLQALgKAQAAQAPgBAKALIAKANQAFADAFAFQALAKAAAPQAAAPgLALQgKALgOAAIgCAAg');
        this.shape_35.setTransform(155.9,-150);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#FF0000').s().p('AisKSQgkgJAKgjQCnpMCeqSQAJgjAkAKQAkAKgJAjQifKSimJMQgIAagXAAQgHAAgIgCg');
        this.shape_36.setTransform(135.3,-78);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#FF0000').s().p('AiMBVQgjgGAKgkQAKgkAjAGQA+AMA8gaQA3gZAugyQAYgbAbAbQAaAagYAbQg8BAhKAdQg0AUg3AAQgbAAgcgFg');
        this.shape_37.setTransform(4,-192.2);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#FF0000').s().p('AinBAQgggRASggQATgfAgAQQA5AdBPgcQBGgYAwgzQAZgaAaAbQAbAagZAaQhCBFhfAbQgrANgnAAQg3AAgugYg');
        this.shape_38.setTransform(0.9,-166.5);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#FF0000').s().p('AjRBMQgjgKAKgkQAKgjAjAJQBbAZBmgbQBbgXBWg9QAegUASAgQATAggdAVQhhBEhsAaQg5AOg3AAQg5AAg2gPg');
        this.shape_39.setTransform(0,-146.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#FF0000').s().p('Aj+BhQgjgGAKgkQAKgkAjAGQB/AXB0gbQCFgcBMhXQAYgbAbAaQAaAbgYAbQhZBkiaAiQhHAQhMAAQhCAAhFgMg');
        this.shape_40.setTransform(0.5,-127.4);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#FF0000').s().p('AjHB7QgkgCAAglQAAgmAkACQBvAGBfgnQBlgpBBhUQALgNAOAAQANABALAKQAaAbgWAcQhJBdh1AwQhiAohtAAIgcgBg');
        this.shape_41.setTransform(9,-109.9);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#FFFFFF').s().p('AivW/QkQgUh1gLQjDgRhxgTQkVgwghhuQgSg7AqiUQAZhaA5iUQAdhJBmjEQBji9AFgRQAHgUBwjVQBxjVANgmQB4lPBGAZQAsAQAtBHQAXAkANAgIDrt/QAHhHANhFQAaiLAhAJQAjAKAhCNQARBHAKBEQCWMRAPAyIAhhGQAqhGAoAAQApAABFB4QAiA8AaA8IBHBwQBNCBAmBTQAlBVA1CrQAaBWATBEIBGCMIBegjQBhgiATAJQATAKgyCcQgZBOgdBMIBXEdQAIAZAAAXQAABIhLAZQi5A+klAyQlGA4kZAAQhpAAhjgIg');
        this.shape_42.setTransform(0.4,-83.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('AhIBAQglgOgSgfQgTggANgkQAMgiAkAJQAkAKgMAiQgFAQARAJQANAGATAAQAzADAxgdQAfgTASAgQATAfgfATQgsAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_43.setTransform(86.4,182.9);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQglgnAGg7QAFgyAWgnQAZgtArgQQAigLAKAkQAJAkgiALQgRAGgMAcQgKAYgBAUQgBAeAKAPQAMARAdABQAkABAAAmQABAkgjAAIgCAAg');
        this.shape_44.setTransform(98.6,87);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#000000').s().p('AlOF1QgTggAggSQDpiFClijQBlhiAqhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjuDzQi5C9kACSQgLAGgJAAQgSAAgMgVg');
        this.shape_45.setTransform(112.9,136.6);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgJgkQgKgjAhgOQBJgfBHAEQBGADBIAmQAgAPgSAhQgNAVgSAAQgJAAgLgGg');
        this.shape_46.setTransform(124.3,94);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#000000').s().p('AlZIxQgSggAfgTQDwiPB6huQC/isAti+QAXhggbhWQgchdhOg2QhXg7hogKQhlgJhlAmQgiANgKgkQgJgkAhgNQBxgrB3ALQB9ALBdBFQBUA+ArBaQArBdgNBlQgeDljPDKQiLCIkMCgQgMAGgKAAQgRAAgMgUg');
        this.shape_47.setTransform(135.9,123.9);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('AATBsQgOgQhsiIQgcgkgEg3IACgxQCpAKBDC0QAhBbgBBYQgtAAhHhNg');
        this.shape_48.setTransform(154.4,92.4);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBAgBQAkAAAAAlQAAAkgkABQgrAAglAZQgLAIgKAAQgQAAgLgVg');
        this.shape_49.setTransform(-179.7,200.5);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#FF0000').s().p('AgmAlQglAAAAglQAAgkAlAAIBOAAQAkAAAAAkQAAAlgkAAg');
        this.shape_50.setTransform(-15.3,252.8);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#FF0000').s().p('AhMAfQAAgkAlgBQASgBAKgHQALgHADgSQAKgjAkAKQAkAKgKAjQgLAogfAXQgfAXgpAAIgCAAQgjAAAAgkg');
        this.shape_51.setTransform(114.2,227.8);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#FF0000').s().p('AgQBAQhHgPgvg0QgYgbAagaQAagbAZAbQAjAnAwAKQAvAIAzgRQAjgLAKAjQAJAkgiALQgqAPgrAAQgaAAgZgGg');
        this.shape_52.setTransform(-58.2,233.9);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgZANQgVAMgPAbQgRAggggTQghgTARggQAaguAsgWQAwgYAwAVQAPAHARAOIAdAaQAWAUALgDQAKgDASgdQAUgeAgATQAhATgUAdQgdAtghASQgUALgSAAQgXAAgWgQg');
        this.shape_53.setTransform(-137.2,228.4);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTAUggQASghAfATQAlAXArAQQAjAOA0ANQAiAJgJAkQgIAdgXAAQgHAAgHgDg');
        this.shape_54.setTransform(-62.3,197.3);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#000000').s().p('AjtA1QgkgGAKgkQAKgjAjAGQBnASB2gNQBwgMBpglQAigLAKAkQAKAkgiAKQhyAnh7AMQgwAFgvAAQhLAAhGgMg');
        this.shape_55.setTransform(16.5,202.5);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#000000').s().p('AlKC7QAAglAkgCQBygIBmggQBugjBXg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg1BMQgpA3hJA2QhjBIh8ApQh0AmiDAIIgFAAQggAAABgjg');
        this.shape_56.setTransform(56,205.3);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#000000').s().p('AjkBwQiCgwhGg1QhrhPgDhoQgBgkAlAAQAlAAABAkQADBIBeA+QBcA9CHAnQEnBYFdgUQAkgCAAAlQAAAmgkACQhHAEhDAAQlPAAkDhhg');
        this.shape_57.setTransform(-37.8,208.1);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBkgJBdQgDAjglABQglAAADgkg');
        this.shape_58.setTransform(129.8,57.8);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#000000').s().p('AhkErQgggTAPggQBujsA5kiQAHgjAkAKQAkAKgHAjQg6Ekh1D8QgJAUgRAAQgJAAgMgHg');
        this.shape_59.setTransform(115.9,122.8);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#000000').s().p('ADZLGQj1kDiCmIQhylZgJmQQAAgkAlAAQAlAAABAkQAJGCBsFLQB7F7DsD3QAZAagbAbQgNANgNAAQgMAAgNgNg');
        this.shape_60.setTransform(-110.4,119.5);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#000000').s().p('AqODlQgkgBAAglQAAglAkAAQFnABGKhAQBrgSBGgPQBggVBNgaQAugOAYgKQAmgQAagUQArghgUgfQgPgWg1gYQghgOATggQATghAhAPQB3A1gEBSQgDAqgmAmQgeAcguAWQhNAlhkAbQhJAThwAVQm5BTmaAAIgPAAg');
        this.shape_61.setTransform(64.7,50.7);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#000000').s().p('AI0DiQjlgPiFgMQjHgTiggbQhzgTg7gPQhhgXhFgjQhlgzgBhMQAAg2A9gtQAlgcBGgdQAhgOAKAlQAKAkghANQgsASgVANQgqAZgFAaQgEATAUARQALALAYAMQAtAXBFATQCDAlDEAbQDaAeF5AYQAkADAAAlQAAAkggAAIgEgBg');
        this.shape_62.setTransform(-76.6,50.8);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#FF0000').s().p('AhCD0Qh5gBhNhDIg1hOQgvhoBLhZQBDhQCFgoQCAgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhpA2huAAIgCAAg');
        this.shape_63.setTransform(40.8,146.2);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#FF0000').s().p('ABnEgQipgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBtAKIDQAoQA8AXA1A3QAyAzAYA6QAzB+gxBoQg0BuiKAAIgLgBg');
        this.shape_64.setTransform(-38.8,95.7);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#FF0000').s().p('AgIDbQh2g4hUhdQhbhiAAhaIANg1QAkhXBlgCQBagCBvA/QBsA8BJBXQBNBbgGBMQgIB3hhAWQgVAFgYAAQhHAAhZgqg');
        this.shape_65.setTransform(-51.8,170.8);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#FF0000').s().p('AiREPQg+gagcg+QgzhwA+iCQA4h0Bpg9ICEghQBugkAzAoQAzAngnBaQgYABgIADIgPAJQgLAIgmAGQgyAIgUAGQhbAbgIBUQgGBFBdAbQBQAWBOgSQgkBEhDAtQg+ArhHAMQgXADgWAAQguAAgogQg');
        this.shape_66.setTransform(94.3,84.5);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#FF0000').s().p('AiaCFQgxgCgfgZIgVgaQAeh2CTg8QCAg0BpAYQBKASAUA1QAKAZgDArQg8A4h3AiQhnAfhkAAIgcgBg');
        this.shape_67.setTransform(31.5,199.5);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FF0000').s().p('AkDBwQgVgagEglQgFgoATgeQAWgkAvgMIBRgFQBagGAsgJQArgIA+gSIBFgUQAYgFAbAWQAaAWANAjQAfBYhdAyQg0AKhPANQidAaiIALIgEAAQgaAAgVgZg');
        this.shape_68.setTransform(32.5,211.1);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FF0000').s().p('AioCnQgoghAhhTQAghOBFg6QBghTBYgHQAjgDAXAKQAVAKACASQADAsg2A6QgfAgg+A5QgsAxg0AlQg3AogjAAQgRAAgMgKg');
        this.shape_69.setTransform(127.4,153.8);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FF0000').s().p('AgZD5Igxg6IhWjrQghh6AahEQAXg8A8gCQA6gCA9AzQBBA2AlBYQA+CXgdB8QgbBuhIAQIgLABQglAAgwgwg');
        this.shape_70.setTransform(-114,108.9);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FFFFFF').s().p('AnBJAQhkgmChhpQA0gjBXgtIBtg5QBKgpB1iEQB/iPAxhxQAvhrhGhEQg5g3hPAAQgyAAiUBBIiMBBIgni0QA1g6BZgxQCyhjCzAtQC8AvBHCLQAwBegLB5QgIBYifDDQilDNiIBHQg0AchNA1QhYA6gbAQQhSAwhHAAQgkAAgigNg');
        this.shape_71.setTransform(119.3,128.2);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#FFFFFF').s().p('AhXPdQiygMilgiQidgghXgnQiNg/gehvQgKgjADgkIAFgdQkWkbh+oAQgvi+gPi0QgNicAOhHQAVhoDFg0QBjgaBfgFQChAMEJAFQISAKILghQILgiCfBrQAyAhAIAsQADAVgGAPIgTDXQgZD6gdCvQgdCuinFEQhUChhOB/IgBAWQgGAdgVAgQhGBojQBkQivBUkxAAQhYAAhigHg');
        this.shape_72.setTransform(-2.5,125);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-188.3,-256.5,376.7,513.1);


    (lib.Анимация15 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#FF0000').s().p('AAqA5QgRgggagQQgdgQgjAHQgjAHgKgjQgJgkAjgIQA7gNAzAaQA0AZAdA1QARAgghATQgMAHgJAAQgRAAgLgUg');
        this.shape.setTransform(-25.7,-236.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#FF0000').s().p('Ag+BkQgTggAfgTQAfgTALgiQAJgfgIgmQgIgjAjgKQAkgKAIAjQAOA/gSA2QgUA6g0AgQgMAHgJAAQgRAAgMgVg');
        this.shape_1.setTransform(-15,-241.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#FF0000').s().p('AACBtQgEg4gPgrQgRgwgggoQgWgcAagaQALgLANgBQAPAAAKANQApA1AVA8QAVA3AGBIQAEAkgmAAQglgBgDgjg');
        this.shape_2.setTransform(-22.1,-243.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('ACJBAQhEg4hGgHQhMgHhGAuQgeAUgTggQgTgfAegVQBbg8BlAMQBeAMBYBHQAcAXgaAaQgOAPgPAAQgMAAgNgLg');
        this.shape_3.setTransform(-39.5,54.2);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('AAeA0QgSAAgKgBQgOgCgMgGQgWgKgPgdQgIgNAEgOQAEgPANgIQANgIAQAEQAPAEAGANIAFAJIABACIAAAAIACAAIAFABIAPAAQAPAAALALQALAKAAAPQAAAPgLALQgKALgPAAIgBAAg');
        this.shape_4.setTransform(-122.2,44.5);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('ADVBlQgggkgYgQQgkgZgkACQgWABggAIQghALgSAEQhJARhAgcQg4gZg1g+QgXgbAagbQALgLANAAQAPgBALANQAhAnAXASQAkAcAkAAQAXAAAfgIIAzgOQBJgSA+AYQA6AWA1A6QAYAbgaAaQgNANgNAAQgNAAgMgNg');
        this.shape_5.setTransform(-83.2,46);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#000000').s().p('AgHAlQgPAAgLgLQgLgKAAgQQAAgOALgLQALgLAPAAIAOAAQAQAAALALQALALAAAOQAAAQgLAKQgLALgQAAg');
        this.shape_6.setTransform(84.5,49.5);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#000000').s().p('AieBGQgagbAXgbQBChKBQgSQBSgTBWAuQAgASgTAfQgSAggggRQh5hAhnB2QgMAOgNAAQgMAAgNgNg');
        this.shape_7.setTransform(55.3,51.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#000000').s().p('Ag5A6Qg9gGgggIQgzgOgggdQgbgXAbgaQAagbAbAYQAbAZA3AIQAYADBCACQB5AFBXgOQAkgGAKAjQAKAkgkAGQhZAOhZAAQgxAAgygFg');
        this.shape_8.setTransform(-6.3,-101.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#000000').s().p('AA3BBQgkgIgjgQQgcgNgmgXQgfgSASggQATggAfASIAXAPIAXAOQAbAOAvAKQAjAHgKAkQgIAegZAAIgMgCg');
        this.shape_9.setTransform(-92.9,-51.8);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('ABNBUQg0gMgvgdQgggUg3gsQgcgXAbgaQAagbAcAXQAtAlAZAQQAnAXAsAKQAjAIgKAkQgIAdgZAAIgMgBg');
        this.shape_10.setTransform(-85.7,-70.3);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('Ai2A/QAAgmAkgCQBOgHA/gZQBAgaA6gyQAbgYAaAbQAbAagbAYQhGA7hNAgQhMAfhdAHIgGAAQgeAAAAgig');
        this.shape_11.setTransform(98.6,-63.4);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#000000').s().p('Ai1BnQgKgkAjgIQBbgVA9gmQBDgpAwhIQAVgdAgASQAgATgUAeQg6BThNAzQhJAwhoAYIgMABQgZAAgIgdg');
        this.shape_12.setTransform(94.6,-81.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#000000').s().p('AgWCCIgMh9QgHhIgJgzQgHgjAkgKQAjgKAHAjQAKA4AIBOIANCGQAEAkgmAAQgkgBgEgjg');
        this.shape_13.setTransform(110.5,30.6);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#000000').s().p('AhdJPQAKhqA0nlQAlloAOjmQACgkAlAAQAmAAgDAkQgNDmgnFoQgzHlgJBqQgEAjglABQglAAADgkg');
        this.shape_14.setTransform(-23.4,-166.5);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#000000').s().p('ADAIfQiwmTg1h+QiAkphcjqQgNgiAkgJQAkgKANAhQBZDjB/EpIDiIGQAOAhggATQgMAHgJAAQgRAAgJgVg');
        this.shape_15.setTransform(8.5,-176.8);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('AAJCJQgOg5gahIIgvh9QgNgiAkgKQAkgKANAiIAuB/QAaBGAPA5QAJAjgkAKQgIACgHAAQgXAAgHgbg');
        this.shape_16.setTransform(94.5,-25);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('AhtDEQgkgKAIgjQAZhgA3hYQA3hZBLhBQAbgXAbAaQAaAbgbAXQhFA7gyBPQgyBQgWBXQgHAcgXAAQgGAAgIgDg');
        this.shape_17.setTransform(32.1,-117.5);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('AC1G2QgOgig7ivQgriAgrhLQgkhDgngzQgOgTgNgZIgYgwQg5hyhThjQgXgcAagaQAKgLAOAAQAPgBALANQBjB3A1BuIAXAvQAOAaANATQAxBCAbA0QArBPAtCFQA/C0APAlQANAhgkAKQgIACgIAAQgXAAgKgZg');
        this.shape_18.setTransform(63.9,-91.2);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('AgBC7QgbgaAYgbQAzg5AghRQAYg+AEg4QgxAFg1AQQhFAVg9AiQgfASgTggQgTghAggSQBFgnBQgXQBRgYBNgDQAQAAALALQAKALABAQQAEBYgnBiQgmBfg+BEQgMAOgNAAQgMAAgMgOg');
        this.shape_19.setTransform(111.5,-9.1);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('Ai1GkQgkgKAEgjQAai+BvjYQAyhiCokRQATgfAhATQAgATgTAfQioEOguBdQhsDSgaC6QgDAbgXAAQgGAAgIgCg');
        this.shape_20.setTransform(-107.9,3.9);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('AjmG6QgggSARggQAjg/BAiMQA9iHAnhDIApg+QAZgnAKgaQAYg9AUgtQAuhjA7hVQAVgeAgATQAgATgUAeQhCBfguBnIgbBDQgRAmgQAZQgmA5gZAuQghA+g9CFQg8CDgkBBQgKATgRAAQgKAAgMgHg');
        this.shape_21.setTransform(-63.5,-87.7);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AAjBZQgNgFgKgNQgjgygugtQgZgaAagaQAbgaAZAZQAzA1AqA6QAIAMgEAQQgFAPgNAIQgJAFgJAAIgKgBg');
        this.shape_22.setTransform(-35.1,-125);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#E6E7E8').s().p('AizCdQiZgThhgRQiKgYhtgjQhRgZgsgVQhFgfgpgrQgYgZADgfQACgcAWgVQAVgWAbgCQAfgCAXAZIAMALIADACIAMAJQAUANAbANQA0AYBQAWQDJA3EBAbQELAeDhgEQEJgEDigvQAhgHAaASQAXAQAIAdQAIAdgMAYQgNAbgiAHQkRA6k4AAQjjAAj3gfg');
        this.shape_23.setTransform(-32.1,30.8);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#E6E7E8').s().p('AELClQiGgQhagSQh4gZhfgnQhIgegjgbQg2gpgHg6QgEgdAZgYQAYgWAdAAQAgAAAVAXQAKALAGAPIABACIAFAEIARALQAmAWA1ASQCWA0DIAVQAgAEAUATQAXAUAAAgQAAAdgXAYQgUAWgZAAIgHAAg');
        this.shape_24.setTransform(-76.8,-2.4);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#E6E7E8').s().p('Al6BbQgigCgUgYQgSgWAAgeQAAgeASgUQAUgWAiABQGQAVFPg5QAigFAZATQAYARAIAdQAIAegMAWQgOAaghAFQkUAvkqAAQhjAAhmgFg');
        this.shape_25.setTransform(19.2,6.9);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#FF0000').s().p('Ag7BYQgQgBgLgKQgLgLABgQQAAgOAKgMQAWgbAngdIBCgxQAMgJAQAFQAQAFAHANQAIANgEAPQgEANgNAKIg8AqQggAagUAZQgIALgQAAIgCAAg');
        this.shape_26.setTransform(-12.1,-198.4);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#FF0000').s().p('AiWBzQgagbAbgXQAygqBNg1ICChaQAdgVATAhQATAggdAVIh7BTQhIAzgvAnQgNALgMAAQgPAAgOgOg');
        this.shape_27.setTransform(-9.8,-173.5);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#FF0000').s().p('AjDBlQgKgkAigMQCxg7B9huQAcgYAaAbQAbAagcAYQiOB7jBBBQgJADgHAAQgVAAgHgbg');
        this.shape_28.setTransform(-4.9,-153.3);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#FF0000').s().p('AjXBvQgJgkAigLQBoghBPgpQBcgxBDhCQAagZAbAaQAaAbgaAZQiLCGjsBKQgIADgHAAQgWAAgIgcg');
        this.shape_29.setTransform(-4,-131.4);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#FF0000').s().p('AjwCJQgKglAigLQDkhPClihQAagZAaAaQAbAbgaAZQiyCuj4BVQgIADgHAAQgVAAgIgbg');
        this.shape_30.setTransform(6.6,-113.9);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('AgJA5QgLgLAAgPIgBgIIABABIgBgEIAAADIAAgCIgCgEIgBgCIgDgEIADADIgEgFIgMgLQgZgZAagbQAbgaAYAZQAWAWAHANQALAVABAeQABAPgMALQgLALgPAAQgQAAgJgLgAgVAXg');
        this.shape_31.setTransform(152.3,-116.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#000000').s().p('AACA6QgNgIgEgPQgCgHgHgHQgFgHgGgEIgCgCIAAAAIgBAAQgNgIgEgOQgEgPAIgNQAHgNAPgFQAQgEANAIQAsAgAMAnQAEAPgIAOQgIANgOAEIgKABQgJAAgJgEg');
        this.shape_32.setTransform(145.9,-90.9);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#000000').s().p('AAFBCQgPgEgGgNIgeg7QgHgOADgPQAEgPANgHQANgIAQAEQAPAEAGANIAeA7QAHAOgDAOQgEAPgNAIQgIAFgKAAIgLgBg');
        this.shape_33.setTransform(139.4,-65.4);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#000000').s().p('AgKAzQgLgLAAgPIAAgBIgBgFIgBgCIgBgCIgCgDIgHgFQgNgHgEgOQgEgQAIgNQAHgNAPgEQAPgEANAIQAWAMANAWQAMAWABAZQABAPgMALQgLALgPAAQgPAAgKgLg');
        this.shape_34.setTransform(135.4,-41.9);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#FF0000').s().p('AgSApQgIgIgCgLIAAgCIgBgCIABABIgBgCIAAAAIAAgBIAAACIgCgFIABACIgDgDIAAgBQgLgLAAgOQAAgQALgKQALgLAPAAQAPAAALALQAYAYACAfQAAAPgLALQgMALgPAAQgOAAgLgLg');
        this.shape_35.setTransform(177.5,-121.7);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#FF0000').s().p('Ag9AoQgagbAagYQAcgcAagIQAQgFAeAAQAkAAAAAmQAAAjgkABIgSAAIgCAAQAGgBgGABQgGAAAGAAIgBAAIgFACIgFACIgGAFIgKAJQgNANgNAAQgNAAgOgNg');
        this.shape_36.setTransform(138.8,-145.6);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#FF0000').s().p('AASAoQgPgHgLgCIgdgBQgkAAAAgkQAAgmAkAAQAzAAAqAUQANAHAEAQQADAPgHAMQgIAOgPADIgJACQgJAAgKgFg');
        this.shape_37.setTransform(188,-146.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#FF0000').s().p('Ag3BRQgOgEgIgOQgJgOAFgOQAahFBQgqQAggRATAhQATAgggAQQg7AdgNAmQgFAPgMAHQgIAGgKAAQgFAAgGgCg');
        this.shape_38.setTransform(182.8,-171.2);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#FF0000').s().p('AgVBNQgehWgBg6QgBgkAlAAQAlAAABAkQAAAdAJAjQAFASAOArQAMAigkAKQgIACgHAAQgXAAgJgbg');
        this.shape_39.setTransform(154.3,-176.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AAKAvQgWgBgMgOIgKgPIgBgBQgLgLAAgOQAAgQALgLQAKgKAQAAQAPgBALALIAKAOQAFADAEAEQALAKAAAPQAAAPgLALQgLALgNAAIgCAAg');
        this.shape_40.setTransform(162.9,-148.2);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#FF0000').s().p('AisKSQgkgJAKgjQCnpMCfqSQAIgjAkAKQAkAKgIAjQigKSimJMQgIAagXAAQgHAAgIgCg');
        this.shape_41.setTransform(142.3,-76.2);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#FFFFFF').s().p('At5VmQjogiiQgiQgHghAHg7QANh4BDiFQBCiGB8jmIBujLQCSklAbhDQARgqA4hVIA2hPQAchQAjhOQBGidAhAJQAiAIAyAvQAZAXASAWQBJq2AHhQQALiFAGgsQAPh2AVgFQAVgEDdIHQBvEDBqEEIAmgxQAtguAlAIQAmAIA+B8QAeA9AYA8IBCBpQBKB5AhBXIDcJEIByg1QB7gwAyAVQAyAVhrDGQg1BjhABfIANBlQAKB0gQBLQiIAqjKApQmTBUlJADIgxABQmiAAm3g/g');
        this.shape_42.setTransform(0.7,-81.9);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('AhIBAQglgOgSgfQgTggANgkQAMgiAkAJQAkAKgMAiQgFAQARAJQANAGATAAQAzADAxgdQAfgTASAgQATAfgfATQgsAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_43.setTransform(79.3,184.7);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQglgnAGg7QAFgyAWgnQAZgtArgQQAigLAKAkQAJAkgiALQgRAGgMAcQgKAYgBAUQgBAeAKAPQAMARAdABQAkABAAAmQABAkgjAAIgCAAg');
        this.shape_44.setTransform(91.6,88.8);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#000000').s().p('AlOF1QgTggAggSQDpiFClijQBlhiAqhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjuDzQi5C9kACSQgLAGgJAAQgSAAgMgVg');
        this.shape_45.setTransform(105.9,138.4);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgJgkQgKgjAhgOQBJgfBHAEQBGADBIAmQAgAPgSAhQgNAVgSAAQgJAAgLgGg');
        this.shape_46.setTransform(117.2,95.8);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#000000').s().p('AlZIxQgSggAfgTQDwiPB6huQC/isAti+QAXhggbhWQgchdhOg2QhXg7hogKQhlgJhlAmQgiANgKgkQgJgkAhgNQBxgrB3ALQB9ALBdBFQBUA+ArBaQArBdgNBlQgeDljPDKQiLCIkMCgQgMAGgKAAQgRAAgMgUg');
        this.shape_47.setTransform(128.9,125.7);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('AATBrQgOgPhsiIQgcgjgEg4IACgxQCpAKBDC1QAhBagBBYQgtAAhHhOg');
        this.shape_48.setTransform(147.3,94.2);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBAgBQAkAAAAAlQAAAkgkABQgrAAglAZQgLAIgKAAQgQAAgLgVg');
        this.shape_49.setTransform(-186.7,202.3);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#FF0000').s().p('AgmAlQglAAAAglQAAgkAlAAIBOAAQAkAAAAAkQAAAlgkAAg');
        this.shape_50.setTransform(-22.4,254.6);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#FF0000').s().p('AhMAeQAAgkAlgBQASAAAKgHQALgIADgRQAKgjAkAKQAkAKgKAjQgLAngfAXQgfAXgpABIgBAAQgkAAAAglg');
        this.shape_51.setTransform(107.1,229.7);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#FF0000').s().p('AgQBAQhHgPgvg0QgYgbAagaQAagbAZAbQAjAnAwAKQAvAIAzgRQAjgLAKAjQAJAkgiALQgqAPgrAAQgaAAgZgGg');
        this.shape_52.setTransform(-65.3,235.7);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgZANQgVAMgPAbQgRAggggTQghgTARggQAaguAsgWQAwgYAwAVQAPAHARAOIAdAaQAWAUALgDQAKgDASgdQAUgeAgATQAhATgUAdQgdAtghASQgUALgSAAQgXAAgWgQg');
        this.shape_53.setTransform(-144.3,230.2);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTAUggQASghAfATQAlAXArAQQAjAOA0ANQAiAJgJAkQgIAdgXAAQgHAAgHgDg');
        this.shape_54.setTransform(-69.4,199.1);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#000000').s().p('AjtA1QgkgGAKgkQAKgjAjAGQBnASB2gNQBwgMBpglQAigLAKAkQAKAkgiAKQhyAnh7AMQgwAFgvAAQhLAAhGgMg');
        this.shape_55.setTransform(9.4,204.3);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#000000').s().p('AlKC7QAAglAkgCQBygIBmggQBugjBXg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg1BMQgpA3hJA2QhjBIh8ApQh0AmiDAIIgFAAQggAAABgjg');
        this.shape_56.setTransform(49,207.1);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#000000').s().p('AjkBwQiCgwhGg1QhrhPgDhoQgBgkAlAAQAlAAABAkQADBIBeA+QBcA9CHAnQEnBYFdgUQAkgCAAAlQAAAmgkACQhHAEhDAAQlPAAkDhhg');
        this.shape_57.setTransform(-44.9,209.9);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBkgJBdQgDAjglABQglAAADgkg');
        this.shape_58.setTransform(122.7,59.6);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#000000').s().p('AhkErQgggTAPggQBujsA5kiQAHgjAkAKQAkAKgHAjQg6Ekh1D8QgJAUgRAAQgJAAgMgHg');
        this.shape_59.setTransform(108.9,124.6);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#000000').s().p('ADZLGQj1kDiCmIQhylZgJmQQAAgkAlAAQAlAAABAkQAJGCBsFLQB7F7DsD3QAZAagbAbQgNANgNAAQgMAAgNgNg');
        this.shape_60.setTransform(-117.4,121.3);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#000000').s().p('AqODlQgkgBAAglQAAglAkAAQFnABGKhAQBrgSBGgPQBggVBNgaQAugOAYgKQAmgQAagUQArghgUgfQgPgWg1gYQghgOATggQATghAhAPQB3A1gEBSQgDAqgmAmQgeAcguAWQhNAlhkAbQhJAThwAVQm5BTmaAAIgPAAg');
        this.shape_61.setTransform(57.7,52.5);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#000000').s().p('AI0DiQjlgPiFgMQjHgTiggbQh1gUg5gNQhhgYhFgjQhlgzgBhMQAAg2A9gtQAlgcBGgdQAhgOAKAlQAKAkghANQgsASgVANQgqAZgFAaQgEATAUARQALALAYAMQAtAXBFATQCDAlDEAbQDaAeF5AYQAkADAAAlQAAAkggAAIgEgBg');
        this.shape_62.setTransform(-83.6,52.6);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#FF0000').s().p('AhCD0Qh5gBhNhDIg1hOQgvhoBLhZQBDhQCFgoQCAgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhpA2huAAIgCAAg');
        this.shape_63.setTransform(33.8,148);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#FF0000').s().p('ABnEgQipgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBtAKIDQAoQA8AXA1A3QAyAzAYA6QAzB+gxBoQg0BuiKAAIgLgBg');
        this.shape_64.setTransform(-45.9,97.5);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#FF0000').s().p('AgIDbQh2g4hUhdQhbhiAAhaIANg1QAkhXBlgCQBagCBvA/QBsA8BJBXQBNBcgGBMQgIB2hhAXQgVAEgYAAQhGAAhagqg');
        this.shape_65.setTransform(-58.9,172.6);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#FF0000').s().p('AiREPQg+gagcg+QgzhwA+iCQA4h0Bpg9ICEghQBugkAzAoQAzAngnBaQgYABgIADIgPAJQgLAIgmAGQgyAIgUAGQhbAbgIBUQgGBFBdAbQBQAWBOgSQgkBEhDAtQg+ArhHAMQgXADgWAAQguAAgogQg');
        this.shape_66.setTransform(87.2,86.3);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#FF0000').s().p('AiaCFQgxgCgfgZIgVgaQAeh2CTg8QCAg0BpAYQBKASAUA1QAKAZgDArQg8A4h3AiQhoAfhjAAIgcgBg');
        this.shape_67.setTransform(24.4,201.3);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FF0000').s().p('AkDBwQgVgagEglQgFgoATgeQAWgkAvgMIBRgFQBagGAsgJQArgIA+gSIBFgUQAYgFAbAWQAaAWANAjQAfBYhdAyQg0AKhPANQidAaiIALIgEAAQgaAAgVgZg');
        this.shape_68.setTransform(25.5,212.9);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FF0000').s().p('AioCnQgoghAhhTQAghOBFg6QBghTBYgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg+A5QgsAxg0AlQg3AogjAAQgRAAgMgKg');
        this.shape_69.setTransform(120.3,155.6);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FF0000').s().p('AgZD5Igxg6IhWjrQghh6AahEQAXg8A8gCQA6gCA9AzQBBA2AlBYQA+CXgdB8QgbBuhIAQIgLABQglAAgwgwg');
        this.shape_70.setTransform(-121,110.7);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FFFFFF').s().p('AnBJAQhkgmChhpQA0gjBXgtIBtg5QBKgpB1iEQB/iPAxhxQAvhrhGhEQg5g3hPAAQgyAAiUBBIiMBBIgoi0QA2g6BZgxQCyhjCzAtQC8AvBHCLQAwBegLB5QgIBYifDDQilDNiIBHQg0AchNA1QhYA6gbAQQhSAwhHAAQgkAAgigNg');
        this.shape_71.setTransform(112.2,130);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#FFFFFF').s().p('AhXPdQiygMilgiQidgghXgnQiNg/gehvQgKgjADgkIAFgdQkWkbh+oAQgvi+gPi0QgNicAOhHQAVhoDFg0QBjgaBfgFQChAMEJAFQISAKILghQILgiCfBrQAyAhAIAsQADAVgGAPIgTDXQgZD6gdCvQgdCuinFEQhUChhOB/IgBAWQgGAdgVAgQhGBojQBkQivBUkxAAQhYAAhigHg');
        this.shape_72.setTransform(-9.6,126.8);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-195.3,-258.3,390.8,516.7);


    (lib.Анимация14 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#FF0000').s().p('AAIBkQhOhSgCheQgBgkAmAAQAkAAABAkQACBBA5A6QAZAagaAaQgOAOgNAAQgNAAgMgNg');
        this.shape.setTransform(-4.6,-236.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#FF0000').s().p('Ag5BgQgkgKAIgjQAThSBhg8QAfgSASAgQATAggfATQggAUgTAUQgXAagHAfQgGAbgYAAQgGAAgIgCg');
        this.shape_1.setTransform(4.9,-237.2);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#FF0000').s().p('Ag6BpQABh4AphjQANghAjAKQAkAJgNAiQgmBbgBBsQgBAkgkAAQglAAAAgkg');
        this.shape_2.setTransform(2,-243.6);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AADBGQgkgKALgiIABgGIABgCIAAgFIAAgDIABACIgBgDIAAABIAAgCIgBgDIgDgBQgBgBgBgBQgBAAAAAAQAAgBAAAAQAAAAAAAAQgOgEgHgMQgIgNAEgPQAEgPAOgIQAOgIAOAFQAjANAQAhQAQAfgMAmQgIAagXAAQgHAAgIgCg');
        this.shape_3.setTransform(-115.5,37.3);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('ADLBeQgsg7hKgHQgUgCh4AJQhEAGgzgVQg2gVgmg2QgVgdAggTQAhgTAUAdQAmA2BKACQArABBYgMQCPgIBTBwQAWAdghATQgNAIgKAAQgRAAgNgSg');
        this.shape_4.setTransform(-74.7,47.3);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('ABaAyQgngpgwACQgsACguAlQgbAXgbgbQgagaAcgWQBFg5BJgDQBQgEA8BAQAYAagaAaQgNANgNAAQgNAAgMgNg');
        this.shape_5.setTransform(-37.8,50.5);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#000000').s().p('Ag9BMQgggTAQggQAkhDBDgjQAggRATAhQASAgggAQQgzAagYAyQgLAVgQAAQgKAAgMgIg');
        this.shape_6.setTransform(110.4,39.4);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#000000').s().p('ACKBDQhDhAhHgFQhQgHg6BCQgYAbgbgbQgagaAYgbQBOhZBxAIQBnAIBYBTQAaAZgbAaQgNAOgOAAQgMAAgNgMg');
        this.shape_7.setTransform(10.3,52.8);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#000000').s().p('AgJAmQgQgBgKgKQgMgMAAgPQAAgOAMgLQAKgLAQAAIATAAQAPAAAMALQALALgBAOQABAPgLAMQgMAKgPABg');
        this.shape_8.setTransform(42.8,54.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#000000').s().p('AinBZQgggTARggQAbgyAnggQAqgkA0gLQAzgLA3AXQAvATAtApQAaAYgaAbQgbAagagZQhAg7hEANQhDAMgqBNQgKAUgRAAQgKAAgMgHg');
        this.shape_9.setTransform(87.3,45.7);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('ABRBaQgagCgfgPQgTgKgegVIgvghQgagUgMgVQgSggAhgTQAggSASAfQAKASA1AiQAWAPAUAJIAQAIIAHABIABAAQAhAEAAAkQAAAkggAAIgEgBg');
        this.shape_10.setTransform(-91.3,-59.4);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('AA1BkQgVgLgZgVIgpgmQgcgagKgKQgVgWgLgTQgRggAggTQAggTASAgQAJAQAUAUIAhAgIAgAcQATAPARAJQAgASgTAgQgNAVgSAAQgJAAgLgGg');
        this.shape_11.setTransform(-85.9,-77.4);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#000000').s().p('Ah7DEQgagaAZgaQBChEAyhSQAzhZAUhWQAJgjAkAKQAkAKgJAjQgVBeg7BiQg1BbhJBLQgMAMgNAAQgNAAgOgNg');
        this.shape_12.setTransform(111.6,-5.2);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#000000').s().p('Ai0A5QgTggAhgQQBHghBVgXQBQgVBSgJQAkgEAAAmQAAAlgkAEQhIAHhIATQhLAUg9AdQgLAFgJAAQgTAAgNgVg');
        this.shape_13.setTransform(102.6,-20.8);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#000000').s().p('AAXBAIglgmQgWgTgYgIQgPgGgHgMQgIgNAEgPQAEgOAOgIQAPgJANAFQAvATAbAYIAVAUIAVAVQAZAagaAaQgOANgNAAQgNAAgMgMg');
        this.shape_14.setTransform(-26.1,-135.9);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#000000').s().p('AiFDYQgggSAPghQAshfA/hgQAwhNBNhmQAWgcAgASQAhATgWAdQhNBmgyBMQg+BhgrBfQgKAUgQAAQgKAAgMgHg');
        this.shape_15.setTransform(-42.7,-120.9);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('AijE5QgkgKANghQBHi0A+h2QBUijBfhuQAYgbAaAbQAbAagYAbQgnAugoA/QgcAsgnBIQhTCZhACfQgKAagXAAQgIAAgIgCg');
        this.shape_16.setTransform(-69.8,-75);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('AhWDNQgkgKAFgjQAMhWAxhhQAmhLBAhaQAUgeAhATQAgATgVAdQg+BXgkBGQguBcgLBRQgEAcgXAAQgGAAgIgCg');
        this.shape_17.setTransform(39.3,-123);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('ABWDRQgZhxg6hhQg7hmhXhGQgdgXAbgaQAagaAcAWQBdBLBCBwQBABrAaB6QAIAjgkAKQgIACgHAAQgXAAgGgcg');
        this.shape_18.setTransform(58.9,-120);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('ABGETQhCkhiLjnQgTgfAggTQAhgSASAeQBIB4A3CNQA2CHAgCOQAIAkgkAJQgIADgGAAQgYAAgGgcg');
        this.shape_19.setTransform(81.5,-73.6);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('AhgJzQgkgKAHgjQArjrA0ljIBUpRQAGgjAkAKQAkAKgGAjIhVJQQgzFkgrDrQgFAbgXAAQgHAAgIgCg');
        this.shape_20.setTransform(-12.1,-167.3);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('ACOH1QhFiwhuk7Qh7ldg2iNQgMgiAkgKQAjgJANAhQA2COB6FeQBvE5BFCwQANAhgkAKQgIADgHAAQgYAAgKgag');
        this.shape_21.setTransform(18.9,-181.2);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AiNGHQgkgKAGgkQA9ljDUlrQASgfAgATQAhATgTAfQjNFfg8FdQgFAcgXAAQgGAAgIgCg');
        this.shape_22.setTransform(-104.3,-3.6);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#000000').s().p('AhpA3QhTgIhBgcQghgPATgfQATggAhAOQA2AYBLAFQA4AEBMgHQA/gFAwgKIAygNQAcgXAbAbQAaAagcAWQgiAchEALQgaAFhWAGQgrAEglAAQgmAAghgEg');
        this.shape_23.setTransform(3.9,-102.2);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#E6E7E8').s().p('Ai6CGQjIgfiDgiQiygtiKhFQgfgPgGgdQgFgaAPgaQAPgbAbgKQAegLAeAPQCABACvArQCGAhC/AbQHNBBHVglQAigCAUAVQASAUAAAeQAAAfgSAWQgUAZgiADQinAMimAAQlIAAlFgxg');
        this.shape_24.setTransform(-34.7,31.9);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#E6E7E8').s().p('ADaCDQiugXhFgPQiLgehbg2QgegQgFgeQgFgcAPgaQAPgaAagJQAdgKAeARQBLAsB2AaQA2ALCXAVQAgAEAUARQAXAUAAAgQAAAdgXAYQgUAWgZAAIgHAAg');
        this.shape_25.setTransform(-85.4,-3.6);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#E6E7E8').s().p('AmsAzQgigGgNgaQgMgXAIgdQAIgdAXgRQAagTAhAGQC4AiDXAEQC0ADDdgSQAigDAUAWQASAUAAAeQAAAdgSAXQgUAZgiADQiuAOifAAQkUAAjmgrg');
        this.shape_26.setTransform(7.1,10);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('AgEAyIgrgvQgLgLAAgPQAAgPALgLQAKgKAQgBQAQAAAKALIArAvQALAKAAAPQAAAQgLALQgKAKgQAAIgCABQgPAAgJgLg');
        this.shape_27.setTransform(145.2,-112.3);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AgEAyIgrgvQgLgKAAgPQAAgQALgLQAKgKAQgBQAQAAAKALIArAvQALAKAAAPQAAAQgLALQgKAKgQABQgQAAgKgLg');
        this.shape_28.setTransform(138.2,-87);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('AgDAqIgTgSIgCgDIADABIgEgBIABAAIgCAAQgMgCgJgJQgLgKAAgPQAAgOALgMQAMgMAOABQAVACARAMQANAJATATQAKALAAAPQAAAPgKALQgLALgQAAQgQAAgJgLg');
        this.shape_29.setTransform(132.8,-62.5);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('AgEAyIgrgvQgKgKgBgPQAAgQALgLQALgKAPgBQAQAAAKALIArAvQALAKAAAQQAAAPgLALQgKAKgQABQgQAAgKgLg');
        this.shape_30.setTransform(125.9,-39.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#FF0000').s().p('AgfAjIgBgIIAAgGIgIgsQgDgPAHgNQAHgOAPgEQAOgEANAIQAPAIADAOIAIAoQAEAWgBAQQAAAkglAAQglAAABgkg');
        this.shape_31.setTransform(168.3,-122.7);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#FF0000').s().p('Ag4AjQgOgHgEgPQgEgNAIgOQAHgOAPgEQAngJASACIAZAEIgBAAIAFABIgEgBIAFABQAPABALAKQALALAAAOQAAAPgLALQgLAMgPgBIghgEIgCAAIgLAAIgVAEIgJABQgKAAgJgFg');
        this.shape_32.setTransform(135.7,-143.5);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#FF0000').s().p('AAUAmQgLgGgRAAIgfABQgkgBAAgkQAAglAkAAIAXAAQAOgBAIABQAbADAYAMQAOAHAEAQQADAPgHANQgIANgPAEIgJABQgKAAgJgFg');
        this.shape_33.setTransform(182,-143.7);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#FF0000').s().p('AhhBSQgTghAhgOQAegOAkgyQAlg2AbgOQAggRATAhQATAgggAQQgNAHgSAXIgcAkQgXAcgIAJQgVAUgTAJQgLAFgIAAQgUAAgNgWg');
        this.shape_34.setTransform(179.2,-170.6);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#FF0000').s().p('AANBgQgOgIgCgOQgDgagggrQgdgngBgeQAAgkAlAAQAkAAABAkQAAAIAJALIARAUQAfAmAFApQACAQgGANQgGANgQAEIgJABQgKAAgKgFg');
        this.shape_35.setTransform(147.1,-174.1);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#000000').s().p('AAKAwQgXgCgLgOIgMgRQgKgLAAgNQAAgQALgLQALgKAPgBQAQAAAKALIAKAOQAFADAEAEQALALAAAOQAAAPgLAMQgLALgNAAIgCAAg');
        this.shape_36.setTransform(156.8,-146);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#FF0000').s().p('AixKJQgkgKAKgiQBRkIBdlcQAbhjCGoFQAKgjAkAKQAkAKgKAjIiiJnQhcFdhREHQgIAbgYAAQgHAAgHgCg');
        this.shape_37.setTransform(136.2,-71.2);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('AAABbQgUhBguhVQgSgfAhgTQAggTARAgQAyBdAXBKQALAigkAKQgIACgHAAQgYAAgHgag');
        this.shape_38.setTransform(96.6,-31.3);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AibBPQgkgFAKgkQAKgkAjAFQBBAJBGgQQBPgUAhguQAVgdAhATQAgATgVAdQgyBDhkAdQg5AQg8AAQggAAgggFg');
        this.shape_39.setTransform(104.5,-61.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('Ai3BKQAAglAkgBQBHgCBLgnQBDgkA0g4QAZgaAbAaQAaAbgZAaQg+BBhTAqQhXAthWADIgBAAQgjAAAAglg');
        this.shape_40.setTransform(98,-78);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('AgFCmQgVi0goiLQgKgjAkgKQAkgKAJAjQAqCSAWDBQAEAjgmAAQgkAAgEgjg');
        this.shape_41.setTransform(112,19.3);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#FF0000').s().p('AiGA+QgKgkAjgFQBugRBJhTQAXgbAbAaQAaAbgYAbQgqAwg3AfQg3Aeg/AKIgJABQgcAAgIggg');
        this.shape_42.setTransform(1.8,-195.8);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#FF0000').s().p('AibBnQgkgCAAglQAAgmAkACQCkALB3iBQAYgaAbAaQAaAbgZAaQhDBIhVAkQhLAhhSAAIgagBg');
        this.shape_43.setTransform(3.4,-173.7);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#FF0000').s().p('AjxBhQAAglAlgBQBwgCBigtQBjguBJhVQAYgbAaAaQAaAbgXAbQhSBfhyAzQhwA0h/ACIgBAAQgkAAAAglg');
        this.shape_44.setTransform(5.4,-152.8);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#FF0000').s().p('Aj6BVQAAgmAkgDQBugIBpgoQBognBbhDQAdgVATAgQASAhgdAVQhhBGh0ArQhxAph5AKIgFAAQggAAABgig');
        this.shape_45.setTransform(4.4,-131.3);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#FF0000').s().p('Ai6BwQgkgCAAglQAAglAkABQBjAFBXgmQBMggBRhJQAagYAbAbQAaAagbAYQhcBShaAnQhbAohlAAIgVgBg');
        this.shape_46.setTransform(16.5,-112.6);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#FFFFFF').s().p('AmhWsQqBgMiEh6QAAhCAHhSQAOikAihOIDNnWQBzjwAyhuQAghFBfh5QAvg+ApgvQAthbAyhcQBki2AZAAQAZAAA6AtQAdAWAXAXQAej0Amj1QBLnqAlgGQAkgGAoCHQAWBJAkCAQAjBVDSI6IAcgmQAiglAcAAQAnAABWB8QBdCFARBsIBPCXQBTCpASBeQAeCWAZBaIA8CCIBVgcQBagWAZAZQAZAZhBCHQggBDgmA/IAhCPQAiCXADApQACAYhHApQhHAohuAnQkMBejPgDQgwgBkqAHQjAAEiVAAIiVgBg');
        this.shape_47.setTransform(1.5,-83.4);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#000000').s().p('AhIBAQglgPgSgeQgSggANgkQALgiAlAJQAkAKgMAiQgGAQARAJQANAGATAAQA0ADAvgdQAfgTATAgQATAfgfATQgsAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_48.setTransform(84,184);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQglgnAGg7QAFgyAWgnQAZgtArgQQAigMAKAkQAJAlgiALQgRAGgMAcQgJAYgBAUQgDA9A0ACQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_49.setTransform(96.2,88.1);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#000000').s().p('AlNF1QgSggAfgSQDniEClikQBlhiAqhFQBGhzgjhcQgNgiAkgKQAkgJANAhQBECxjtDzQi5C9j+CSQgMAGgJAAQgSAAgMgVg');
        this.shape_50.setTransform(110.5,137.8);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#000000').s().p('ABsAyQg5geg4gBQg1gBg+AaQghAOgKgkQgJgjAggOQBKgfBGAEQBGADBIAmQAgAPgTAhQgMAVgTAAQgJAAgLgGg');
        this.shape_51.setTransform(121.8,95.2);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#000000').s().p('AlXIvQgTggAfgSQDxiQB5huQC/itAsi/QAXhhgdhYQgfhdhRg1QhWg4hlgJQhigIhjAlQgiANgJgkQgKgkAhgNQBpgnBzAKQBwALBcA3QBfA5AwBcQA0BjgPB5QgcDejSDKQiKCFkJCeQgLAHgKAAQgRAAgMgVg');
        this.shape_52.setTransform(133.4,125.2);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#FF0000').s().p('AAUBrQgNgNhtiKQgcgjgEg4IACgxQCoAKBDC1QAhBagBBYQgtAAhGhOg');
        this.shape_53.setTransform(151.8,93.5);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#FF0000').s().p('AhOAhQgSggAdgTQA2giA/gBQAlAAgBAlQAAAlgkAAQgqAAglAZQgMAIgKAAQgQAAgLgVg');
        this.shape_54.setTransform(-181.2,201.6);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#FF0000').s().p('AgmAlQglAAAAglQAAgkAlAAIBNAAQAlAAAAAkQAAAlglAAg');
        this.shape_55.setTransform(-17.3,254);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#FF0000').s().p('AhMAeQAAgkAkgBQATAAAKgHQAKgIAEgRQAJgjAkAKQAkAKgJAjQgLAnggAXQgeAXgqABIgBAAQgjAAAAglg');
        this.shape_56.setTransform(111.8,229);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#FF0000').s().p('AgQBAQhGgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAuAIAzgRQAigLAKAjQAKAkgiALQgqAPgqAAQgbAAgZgGg');
        this.shape_57.setTransform(-60.1,235);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgZANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAwAVQAPAHARAOIAdAaQAVAUAMgDQAJgDATgdQATgeAhATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgVgQg');
        this.shape_58.setTransform(-138.8,229.6);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#000000').s().p('ABOBHQhwgehHgqQgfgTATggQATghAfATQAkAXAsAQQAhANA0AOQAjAJgKAkQgIAdgXAAQgGAAgIgDg');
        this.shape_59.setTransform(-64.2,198.4);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#000000').s().p('AjtA1QgjgGAKgkQAKgjAjAGQBmASB2gNQBvgMBpglQAigLAKAkQAKAkgiAKQhxAnh7AMQgwAFgvAAQhLAAhGgMg');
        this.shape_60.setTransform(14.4,203.6);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#000000').s().p('AlJC7QAAglAkgCQBwgIBlgfQBsgiBWg8QCehsgRhRQgHgjAkgKQAkgKAHAjQAPBGg0BLQgmA3hIA1QhjBLh9ApQh1AniEAIIgFAAQggAAABgjg');
        this.shape_61.setTransform(53.8,206.5);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#000000').s().p('AjjBwQiBgwhIg1QhqhPgDhoQAAgkAlAAQAlAAABAkQACBIBeA+QBcA8CGAoQEnBYFbgUQAkgCAAAlQAAAmgkACQhGAEhDAAQlOAAkChhg');
        this.shape_62.setTransform(-39.8,209.2);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#000000').s().p('AgqBhQAJhbADhmQABgkAkAAQAlAAgBAkQgCBkgJBdQgDAjglABQglAAADgkg');
        this.shape_63.setTransform(127.3,59);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#000000').s().p('AhjErQghgTAPggQBvjtA3khQAHgjAkAKQAkAKgHAjQg4Eih1D+QgKAUgQAAQgJAAgMgHg');
        this.shape_64.setTransform(113.5,123.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#000000').s().p('ADYLGQj0kDiBmIQhylYgJmRQgBgkAmAAQAlAAABAkQAIGBBtFMQB6F6DrD4QAZAagaAbQgOANgNAAQgNAAgMgNg');
        this.shape_65.setTransform(-112.1,120.6);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#000000').s().p('AqMDlQgkgBAAglQAAglAkAAQFmABGJhAQBqgSBGgPQBggVBNgaQAugOAXgKQAmgQAagUQArghgUgfQgPgWg0gYQghgOATghQATggAgAPQB4A1gFBSQgDAqgmAmQgeAcguAWQhNAlhjAbQhJAThvAVQm5BTmYAAIgPAAg');
        this.shape_66.setTransform(62.4,51.8);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#000000').s().p('AIyDiQjjgPiJgNQjHgTiigcQh1gTg4gOQhhgYhEgkQgtgYgZgeQggglAIgrQAKg0A2goQAjgaBDgcQAhgOAKAlQAKAkghANQgrASgXANQgpAZgFAaQgGAeA6AeQAsAWBGAUQCFAlDFAaQDlAfFmAXQAkADAAAlQABAkghAAIgEgBg');
        this.shape_67.setTransform(-78.4,52);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FF0000').s().p('AhCD0Qh5gBhMhDIg1hOQgvhoBLhZQBChQCFgoQCAgoByATQB9AVAmBTQArBdg1BbQgwBThrA3QhqA2htAAIgCAAg');
        this.shape_68.setTransform(38.6,147.3);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FF0000').s().p('ABmEgQipgFiAiGQh0h4gOiIQgGg5Ajg1QAkg4A6gPIBtAKIDQAoQA7AXA0A3QAzAzAXA6QA0B+gyBoQg0BuiJAAIgLgBg');
        this.shape_69.setTransform(-40.7,96.8);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FF0000').s().p('AgJDbQh1g4hUhdQhbhiAAhaIAOg1QAjhXBlgCQBagCBvA/QBrA8BJBXQBNBcgGBMQgJB2hgAXQgVAEgYAAQhGAAhagqg');
        this.shape_70.setTransform(-53.7,171.9);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FF0000').s().p('AiQEPQg+gagcg+QgzhwA+iCQA4h0Bpg9ICDghQBtgkAzAoQAzAngnBaQgXABgIADIgPAJQgLAIgmAGQgyAIgUAGQhaAbgJBUQgGBFBdAbQBQAWBNgSQgkBEhCAtQg/ArhGAMQgXADgWAAQguAAgngQg');
        this.shape_71.setTransform(91.9,85.7);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#FF0000').s().p('AiaCFQgwgCgfgZIgWgaQAeh2CTg8QCAg0BoAYQBKASAUA1QAKAZgDArQg8A4h2AiQhnAfhjAAIgdgBg');
        this.shape_72.setTransform(29.3,200.6);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FF0000').s().p('AkCBwQgVgagEglQgEgoATgeQAVgkAvgMIBRgFQBagGAsgJQAqgIA9gSIBFgUQAYgFAbAWQAbAWAMAjQAfBYhcAyQg0AKhPANQidAaiHALIgFAAQgaAAgUgZg');
        this.shape_73.setTransform(30.3,212.3);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f('#FF0000').s().p('AinCnQgpghAihTQAfhOBFg6QBghTBYgHQAjgDAWAKQAWAKABASQAEAsg3A6QgeAgg+A5QgsAxg0AlQg3AogiAAQgRAAgMgKg');
        this.shape_74.setTransform(124.9,154.9);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f('#FF0000').s().p('AgZD5Igwg6IhXjrQggh6AahEQAXg8A8gCQA5gCA9AzQBBA2AkBYQA/CXgeB8QgaBuhIAQIgMABQgkAAgwgwg');
        this.shape_75.setTransform(-115.7,110.1);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f('#FFFFFF').s().p('Am/JAQhkgmChhpQA0gjBXgtIBsg5QBKgpB0iEQB/iPAxhxQAuhrhGhEQg4g3hPAAQgxAAiUBBIiLBBIgoi0QA1g6BZgxQCxhjCyAtQC8AvBHCLQAwBegLB5QgIBYieDDQilDNiIBHQg0AchNA1QhWA6gcAQQhSAwhGAAQglAAghgNg');
        this.shape_76.setTransform(116.8,129.4);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f('#FFFFFF').s().p('AhXPdQixgMikgiQidgghXgnQiNg/gehvQgJgjADgkIAFgdQkVkbh+oAQgui+gQi0QgNicAPhHQAUhoDFg0QBjgaBegFQCgAMEJAFQIQAKIJghQIJgiCfBrQAyAhAHAsQAEAVgGAPIgTDXQgZD6gdCvQgdCuinFEQhTChhOB/IgBAWQgGAdgVAgQhGBojPBkQivBUkvAAQhYAAhigHg');
        this.shape_77.setTransform(-4.6,126.2);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-189.7,-257.7,379.5,515.4);


    (lib.Анимация13 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('ADdBHQghghgcgRQglgVgmAAQgZgBgeAEIg1AKQhGAOg2gEQg+gGg4gdQgggRATghQATggAgARQA4AfBIgFQArgEBXgQQCHgSBrBrQAaAagaAaQgOAOgNAAQgNAAgMgNg');
        this.shape.setTransform(-75.1,50.7);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('ACKBDQhDhAhHgFQhQgHg7BCQgXAbgbgbQgagaAYgbQBOhZBwAIQBoAIBXBTQAaAZgaAaQgNAOgOAAQgNAAgMgMg');
        this.shape_1.setTransform(-10.8,54.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AgJAmQgPAAgLgMQgLgKAAgQQAAgPALgKQALgMAPAAIATAAQAPAAALAMQALAKAAAPQAAAQgLAKQgLAMgPAAg');
        this.shape_2.setTransform(28.6,60.2);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AimBZQghgTARggQAbgxAnghQAqgkA0gLQA0gLA3AXQAvATAsApQAaAYgaAbQgaAagbgZQhAg7hDANQhEAMgpBNQgLAUgRAAQgKAAgLgHg');
        this.shape_3.setTransform(83.7,49.5);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('AiCBgQgkgKAGgjQAJgwAdgfQAegiAugLQAWgEA7gEQA0gEAcgJQAigMAKAkQAKAkgiAMQgWAHgrADQg5AEgMACQhNALgMBCQgFAbgXAAQgGAAgIgCg');
        this.shape_4.setTransform(111.1,44.1);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('AALA+QgwgZgYgvQgHgOADgOQAEgPANgIQANgIAQAEQAQAEAFANQAGAMAGAJIACACIAGAGIAGAGIgBgBIACABIgBAAIADACIASAKQANAGAEAQQAEAPgIANQgHAOgPADIgKABQgJAAgKgFg');
        this.shape_5.setTransform(-71.9,-71.4);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#000000').s().p('Ah7DEQgagaAZgaQBChEAxhSQA0haAUhVQAJgjAkAKQAkAKgJAjQgWBeg6BiQg2BbhIBLQgNAMgMAAQgNAAgOgNg');
        this.shape_6.setTransform(103.5,-2.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#000000').s().p('Ai0A5QgTghAhgPQCPhECvgSQAkgEAAAmQgBAlgjAEQhIAHhIATQhLAUg+AdQgKAFgKAAQgSAAgNgVg');
        this.shape_7.setTransform(94.5,-18.5);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#FF0000').s().p('AATBpQg9hBgmhvQgMgiAkgKQAlgKALAiQAfBdAxAyQAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_8.setTransform(17.8,-240.1);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#FF0000').s().p('AhWBwQgkgKAJgjQAShCAxgwQAwgwBCgRQAjgIAKAkQAKAkgjAIQgwAMgjAhQghAigNAwQgHAbgXAAQgHAAgIgCg');
        this.shape_9.setTransform(32.5,-239.6);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#FF0000').s().p('Ag6BhQgBhHAHgmQAKg6AegsQAUgeAgATQAgASgUAeQgZAmgIAtQgEAeAAA9QABAkglAAQglAAAAgkg');
        this.shape_10.setTransform(26.3,-246.6);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('AAhBAQgqgqg9gXQgPgGgHgMQgIgNAEgPQAEgOAOgIQAOgJAOAFQBSAgA1A0QAaAZgaAbQgNANgOAAQgMAAgNgMg');
        this.shape_11.setTransform(-17.2,-133.6);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#000000').s().p('Ah3DcQgkgLANghQBFi3CTjFQAVgcAhATQAgATgWAdQhGBdgwBPQg3BegjBfQgLAagXAAQgHAAgIgCg');
        this.shape_12.setTransform(-33.9,-118.8);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#000000').s().p('AijE5QgkgKANghQBHi0A+h2QBUijBfhuQAYgbAaAbQAbAagYAbQgnAtgoBAQgbArgoBJQhWCgg9CYQgKAagXAAQgIAAgIgCg');
        this.shape_13.setTransform(-59.9,-72.7);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#000000').s().p('AhWDNQgkgKAFgjQAViWCOjGQAUgeAhATQAgATgVAdQg+BXgkBGQguBcgLBRQgEAcgXAAQgGAAgIgCg');
        this.shape_14.setTransform(49.2,-120.7);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#000000').s().p('ABWDRQgZhxg6hhQg7hmhYhGQgcgXAbgaQAagaAcAWQBdBLBCBwQA/BrAbB6QAHAjgkAKQgIACgGAAQgXAAgGgcg');
        this.shape_15.setTransform(68.8,-117.7);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('ABGETQhCkhiLjnQgTgfAggTQAhgSASAeQBIB4A3CNQA1CHAhCOQAIAkgkAJQgIADgGAAQgYAAgGgcg');
        this.shape_16.setTransform(91.4,-71.3);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('AjEJ1QgkgKAMgjQDTp5CfopQAKgjAkAKQAkAKgKAiQigIpjSJ6QgJAbgYAAQgHAAgIgCg');
        this.shape_17.setTransform(1.9,-168.7);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('ABKIIIhxn9QhDksgnjSQgGgjAkgKQAkgKAGAjQAnDTBCEsIByH8QAHAjgkAKQgIACgGAAQgYAAgFgbg');
        this.shape_18.setTransform(35.7,-180.8);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('AiNGHQgkgKAGgjQA+lmDTloQASgfAgASQAgATgSAfQjNFeg8FeQgFAcgXAAQgGAAgIgCg');
        this.shape_19.setTransform(-94.4,-1.3);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('AhpA3QhTgIhBgcQghgPATgfQATggAhAOQA2AYBLAFQA4AEBMgHQA/gFAwgKIAygNQAcgXAbAbQAaAagcAWQgiAchEALQgaAFhWAGQgrAEglAAQgmAAghgEg');
        this.shape_20.setTransform(13.8,-99.9);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#E6E7E8').s().p('AiTB7QixgghxgfQiagqh6g8QgfgPgGgeQgFgaAPgaQAPgaAbgKQAegLAeAPQA3AbAiAOQArASAyAPQCDAnDsApQDFAiC6ALQDCALDAgPQAigDAUAWQASAUAAAeQAAAegSAXQgUAZgiACQhpAIhsAAQkmAAlAg6g');
        this.shape_21.setTransform(-36.8,33.9);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#E6E7E8').s().p('ACeB0QidgVhegbQg2gPgSgIQgqgTgPgcQgQgeALgeQAKgaAagQQAagPAbAFQAWAFAPASIAOAGQAiALAaAGQBdAXBcANQAgAEAUASQAWATAAAgQAAAdgWAYQgUAWgYAAIgIAAg');
        this.shape_22.setTransform(-81.5,-2.8);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#E6E7E8').s().p('AmJArQgigGgNgaQgMgXAIgdQAIgdAXgRQAagTAhAGQE5A6GhgFQAiAAAVAXQASAVAAAdQAAAegSAWQgVAXgiABIg+AAQmIAAk7g7g');
        this.shape_23.setTransform(-3.5,10.1);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_24.setTransform(95.4,186.3);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_25.setTransform(107.7,90.4);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_26.setTransform(122.1,140.1);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgTAAQgJAAgLgGg');
        this.shape_27.setTransform(133.4,97.5);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_28.setTransform(145.1,127.4);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#FF0000').s().p('AAUBsQgOgQhtiIQgcgkgEg3IACgxQCpAKBDC0QAiBbgBBYQgtAAhHhNg');
        this.shape_29.setTransform(163.7,95.8);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAlgkAAQgsAAglAZQgLAIgKAAQgQAAgLgVg');
        this.shape_30.setTransform(-171.8,203.9);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_31.setTransform(-6.8,256.3);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#FF0000').s().p('AhMAfQAAgkAkgBQARAAAMgIIAGgFIACgCIABgDQADgGACgJQAKgjAkAKQAkAKgKAjQgLAogfAXQgfAXgqAAIgCAAQgiAAAAgkg');
        this.shape_32.setTransform(123.3,231.3);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_33.setTransform(-49.8,237.3);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_34.setTransform(-129.2,231.9);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_35.setTransform(-54,200.7);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgvAAQhMAAhGgMg');
        this.shape_36.setTransform(25.2,205.9);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_37.setTransform(64.9,208.8);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_38.setTransform(-29.3,211.5);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_39.setTransform(139,61.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_40.setTransform(125,126.2);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_41.setTransform(-102.2,122.9);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_42.setTransform(73.6,54.1);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('AI2DiQjmgPiFgMQjIgTihgbQh1gUg5gNQhigYhFgjQhmgzAAhMQgBg2A9gtQAmgcBGgdQAhgOAKAlQAKAkghANQgtATgVAMQgqAZgEAaQgEATATARQAMALAYAMQAsAXBGATQCDAlDGAbQDaAeF7AYQAkADAAAlQAAAkggAAIgEgBg');
        this.shape_43.setTransform(-68.2,54.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_44.setTransform(49.6,149.6);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_45.setTransform(-30.3,99.1);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBagCBwA/QBtA8BIBXQBOBbgGBMQgIB3hhAWQgVAFgYAAQhHAAhagqg');
        this.shape_46.setTransform(-43.4,174.2);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQglBEhCAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_47.setTransform(103.3,88);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAZgDArQg8A4h4AiQhoAfhkAAIgcgBg');
        this.shape_48.setTransform(40.3,202.9);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_49.setTransform(41.3,214.6);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_50.setTransform(136.5,157.2);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_51.setTransform(-105.8,112.4);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#FFFFFF').s().p('AnDJAQhlgmCjhpQA0gjBXgtIBtg5QBLgpB1iEQCAiPAxhxQAvhrhGhEQg5g3hQAAQgyAAiVBBIiMBBIgoi0QA2g6BZgxQCzhjCzAtQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQg0AchOA1QhXA6gcAQQhSAwhHAAQglAAgigNg');
        this.shape_52.setTransform(128.4,131.7);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#000000').s().p('AAYA7QgQAAgKgLIgugsQgLgJAAgQQAAgQALgKQAKgLAQAAQAPAAALALIAuArQALAKAAAQQgBAQgKAKQgLALgOAAIgBAAg');
        this.shape_53.setTransform(130.9,-109.3);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#000000').s().p('AAXA7QgPAAgKgLIgvgrQgLgKABgQQAAgQAKgKQALgLAQAAQAPAAAKALIAvArQALAKgBAQQAAAQgKAKQgLALgOAAIgCAAg');
        this.shape_54.setTransform(125.6,-83.6);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#000000').s().p('AgDApIgKgKIgFgEIgGgDIgBgBQgNgCgKgJQgLgLAAgOQAAgPALgLQALgMAPABQAUAAATAMQAMAIAUATQALALAAAPQAAAPgLALQgLALgPAAQgPgBgLgKg');
        this.shape_55.setTransform(121.9,-58.8);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#000000').s().p('AAYA7QgQAAgKgLIgugrQgLgKAAgQQAAgQALgKQAKgLAQAAQAPAAALALIAuArQALAKgBAQQAAAQgKAKQgLALgOAAIgBAAg');
        this.shape_56.setTransform(116.6,-35.3);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#FF0000').s().p('AgGBFQgKgKgBgPIAAAFIgCgIIgCgGIgdg4QgRggAggTQAhgTAQAgIAaAyQAOAdACAXQABAPgMALQgLALgPAAQgQAAgJgLg');
        this.shape_57.setTransform(154.4,-120.4);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#FF0000').s().p('AhWAOQgKgjAigKQAagHAggCIA6gCQAkAAAAAlQAAAkgkABIgxABQgZABgWAGQgHADgHAAQgXAAgHgdg');
        this.shape_58.setTransform(117.8,-139.3);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#FF0000').s().p('AAlArQglgQg4ABQgkAAAAgkQAAglAkgBQBGgBA8AaQAOAGAEAQQADAQgHAMQgIAOgPADIgJABQgJAAgKgEg');
        this.shape_59.setTransform(167.1,-142.3);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('AhKA/QgTggAggRQAUgKAYgiQAYgkASgLQAfgTATAhQATAggfASIgEADIgCADIgHAHIgSAbQgcAlgbAPQgLAGgKAAQgSAAgMgWg');
        this.shape_60.setTransform(159.2,-167.3);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#FF0000').s().p('AAABHQgEgZgigoQgggmgCgeQgDgkAlAAQAmAAABAkQABAGAMAMIATAUQAfAkAIAnQAHAkgkAKQgIACgGAAQgXAAgGgcg');
        this.shape_61.setTransform(128.7,-171.1);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#000000').s().p('AgXAhIgKgNIgDgDQgLgLAAgPQAAgPALgLQALgLAQAAQAOAAALALIAJAMQAHACAFAGQALAKAAAPQAAAPgLALQgLALgQAAQgVgBgMgNg');
        this.shape_62.setTransform(140.2,-143.7);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#FF0000').s().p('AiGKTQgkgJAIgkQA/kNBGlhIB4pxQAHgjAkAKQAkAJgHAkIh5JwQhFFjg+EMQgGAbgYAAQgHAAgIgCg');
        this.shape_63.setTransform(124.7,-67.7);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#000000').s().p('AAABbQgVhAgthVQgSggAhgTQAggTARAgQAzBgAWBGQALAjgkAKQgIACgHAAQgYAAgHgag');
        this.shape_64.setTransform(106.5,-29);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#000000').s().p('AiJBPQgkgFAKgkQAKgkAkAFQA6AIA5gQQA/gUAggsQAUgdAhATQAgATgVAdQgvBBhVAcQgzARg5AAQgaAAgcgEg');
        this.shape_65.setTransform(112.5,-59.1);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#000000').s().p('AikBLQAAgmAkgBQBugEB2iAQAZgbAaAbQAbAagZAaQiKCViPAGIgCAAQgiAAAAgkg');
        this.shape_66.setTransform(106,-75.7);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#000000').s().p('AAPD/QgXjkhPkPQgKgjAkgJQAkgKAKAiQBPERAaD2QAEAkglAAQgmAAgEgkg');
        this.shape_67.setTransform(119.7,12.6);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FF0000').s().p('AhdApQgKgkAigLQAVgHAngWQAmgVAVgIQAigMAKAkQAKAlgiAKQgVAIgnAVQgmAWgVAHQgIADgIAAQgVAAgHgbg');
        this.shape_68.setTransform(15.1,-191.4);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FF0000').s().p('AixA9QgKgkAigLICMgrQBPgbA4gbQAhgPATAgQASAgggAQQg9AbhUAdIiUAwQgIADgHAAQgWAAgHgcg');
        this.shape_69.setTransform(14.5,-170.3);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FF0000').s().p('AjeBJQgKgkAjgIQC8goCxhWQAggPATAgQATAgghAQQi/Bbi/AqIgMACQgZAAgIgeg');
        this.shape_70.setTransform(13,-150.1);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FF0000').s().p('Ak4BlQgKgkAjgGQEoguD5iKQAggRATAgQASAggfASQkFCQkuAvIgJABQgcAAgIgfg');
        this.shape_71.setTransform(15.9,-131.3);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#FF0000').s().p('Aj1BOQAAgmAkgEQDugZCihyQAdgVATAhQATAggeAVQitB5kIAcIgHABQgdAAAAgig');
        this.shape_72.setTransform(21.2,-111.7);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FFFFFF').s().p('AhXPdQiygMimgiQiegghYgnQiOg/gehvQgJgjADgkIAFgcQkXkch/oAQgvi9gPi1QgNibAOhIQAVhoDGgzQBjgaBfgFQCiALELAFQIUAKINghQINghCgBqQAyAiAHArQAEAWgGAPIgTDXQgZD6gdCvQgeCuioFDQhUCihOB+IgBAXQgGAcgWAhQhGBnjQBkQiwBUkyAAQhZAAhigHg');
        this.shape_73.setTransform(6.8,127.6);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f('#FFFFFF').s().p('AtEWYQiEgVh0gaIhZgWQgJhGAGhmQAMjNBHijQA0h4B0jeIBpjGQAlhgA3hyQBujhBbhUQAbhTAnhXQBOitA/gRIB4A2QBDjtBMjuQCZnbAygEQAygEBkIDQAyECAoECIAfhFQAmhFAlAFQAvAFBQCMQBSCOAFBdIAiA4QAqBIAjBKQBxDtAJC0IAfA9QAlBRAiBiQBtE7AdFzQh9AqjRAqQmjBVmkAFIgtAAQmOAAmPhBg');
        this.shape_74.setTransform(9.1,-76.2);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-259.9,360.9,520);


    (lib.Анимация12 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('AAHAzQgBgZgEgMQgCgJgFgFQgGgGgHgBQgMgEgPAAQgkAAAAgmQAAglAkAAQBCABAdAjQAfAnABA+QAAAkglAAQglAAgBgkg');
        this.shape.setTransform(-102.7,39);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('AgOAmQgPgBgLgKQgLgMAAgPQAAgOALgLQALgLAPAAIAdAAQAPAAALALQALALAAAOQAAAPgLAMQgLAKgPABg');
        this.shape_1.setTransform(-14.7,57.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('ABpBMQgMgqgXgcQgegignABQhEAAgqBSQgQAggggTQgggTAQggQAdg6AyghQA2gjA9AJQA6AJArAuQAmApARA9QAKAjgkAJQgIACgGAAQgYAAgIgbg');
        this.shape_2.setTransform(-48,48.5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AiRBYQgggUAOggQAYg1AmgiQAtgoA1gCQA1gCAtAgQAoAdAbAyQARAgggASQggATgRggQgrhPg6AIQg6AHglBUQgJAVgRAAQgJAAgMgGg');
        this.shape_3.setTransform(45.2,50.7);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('AkwBhQgggTARggQAshSBGgmQBHglBdALQAvAFBsAbQBgASA6gbQAggPATAgQATAgghAQQg+AchTgHQgigChwgWQgygLgYgCQgsgFggAJQgoALgfAdQgZAZgXArQgKAUgRAAQgKAAgMgHg');
        this.shape_4.setTransform(88.9,48.6);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#FF0000').s().p('AAIBpQhOhSgChoQAAgkAlAAQAkAAABAkQABBKA6A7QAZAagbAaQgNAOgNAAQgMAAgNgNg');
        this.shape_5.setTransform(7.3,-238.8);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#FF0000').s().p('AhTBgQgggTATgeQA3hYBYg3QAfgSASAgQATAggfATQhJAtgqBHQgMATgRAAQgKAAgNgIg');
        this.shape_6.setTransform(18.2,-237.5);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#FF0000').s().p('AgjCXQgkgKAFgjQAOhXAIgjQAPhBAbgzQAQggAgATQAhATgRAgQgZAugNA9QgEATgPBeQgEAbgWAAQgGAAgIgCg');
        this.shape_7.setTransform(13.8,-242.5);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#000000').s().p('AhpCxQgbgaAcgXQBEg5AlhEQAphKAChTQABglAlAAQAmAAgBAlQgCBiguBYQgsBUhOBBQgNALgMAAQgPAAgOgPg');
        this.shape_8.setTransform(67.5,-6.3);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#000000').s().p('AiRBEQgagaAcgXQA5gsBFgaQBGgcBGgCQAkgBAAAmQAAAlgkABQg4ACg5AUQg4AVgsAjQgNAKgMAAQgPAAgPgOg');
        this.shape_9.setTransform(63.4,-22.4);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('AgmA3QgPgDgJgOQgHgMAEgQQADgPAOgHQALgFATgOQATgOAKgGQAPgHAOAEQAPADAHAOQAIAMgDAQQgFAPgMAHQgMAFgTAOQgTAOgKAGQgKAEgJAAIgJgBg');
        this.shape_10.setTransform(106.5,-122.5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('Ag9A2QgagaAagZQALgKAYgSQAZgTAKgLQAagYAbAaQAaAagaAZQgLAKgZATQgYASgKALQgNAMgNAAQgNAAgOgOg');
        this.shape_11.setTransform(96.4,-90.7);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#000000').s().p('AhQAmQgKgkAjgIQAwgMAUgdQAJgNAOgEQAOgEAOAIQANAHAEAQQAFAQgJAMQgpA5hHARIgMACQgZAAgIgdg');
        this.shape_12.setTransform(89.9,-65.8);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#000000').s().p('AgsBGQgQgEgHgNQgIgNAEgPQAEgPANgIQAagPAIgHIAPgPQAGgMgBAEQAFgPAMgIQANgIAPAFQAOAEAJANQAJAPgGANQgLAcgZAYQgSARghAVQgJAGgJAAIgKgCg');
        this.shape_13.setTransform(81.4,-37.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#FF0000').s().p('AAgA+IgXgRIgVgSQgegWgKABQgkAAAAgkQAAgmAkAAQAfgBAiAYQAUAOAlAdQAeATgTAgQgLAUgQAAQgKAAgMgHg');
        this.shape_14.setTransform(86,-160.8);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#FF0000').s().p('AhAAkQgLgKAAgQQgBgQAMgKQASgPAkgHQAhgHARgBQAkAAAAAlQAAAlgkAAIgIABQgBAAgBAAQAAAAAAAAQAAAAABAAQABAAABAAIgVADIgXAGIgEABIAAAAQAAAAABAAQAAAAAAAAQAAAAAAABQgBAAAAAAQgEACAEgDIgBABQgKAGgMABIgCAAQgOAAgKgLgAgQAnIABAAIACgCIgDACg');
        this.shape_15.setTransform(85.7,-133.9);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#FF0000').s().p('AAQA/QgNgHgEgPIgBgDIgCgCIgDgFIgFgGIgMgJIgUgKQgPgGgHgLQgIgNAEgPQAEgOAOgJQAOgIAOAFQBKAcAUA6QAFAOgIAPQgJANgOAEIgKABQgJAAgJgFg');
        this.shape_16.setTransform(133.2,-134.1);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#FF0000').s().p('AARB6QgkAAgCgkQgCg3gCgcQgBgUgCgNQgCgXgHgFQgcgXAbgaQAagbAbAXQARAPAIAfQAEAQAEAlQACATABAeIABAxQADAkglAAIgBAAg');
        this.shape_17.setTransform(106.9,-178.4);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#FF0000').s().p('Ag3BJQgOgEgIgNQgJgPAFgOQAQgmAggWQAMgIASgKIAdgRQAggRATAgQASAggfASIgqAUQgYAPgHAPQgGAPgMAHQgIAGgJAAIgLgCg');
        this.shape_18.setTransform(130.3,-167.3);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('AgjAkQgMgLAAgPQAAgOAMgLQAEgFAGgDIAKgMQALgLAPAAQAPAAALALQAKALABAPQgBAPgKALIgBABIACgEIgGAIIAEgEIgMAPQgNANgUABIgBAAQgOAAgLgLg');
        this.shape_19.setTransform(110.9,-146.3);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#FF0000').s().p('AigJ+QgkgJAJgjQBGkIBVlSICUpcQAJgjAkAKQAkAKgJAjIiVJbQhTFShHEIQgHAbgXAAQgHAAgIgCg');
        this.shape_20.setTransform(92.8,-76.3);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('AiGA9QgjgKAKgkQAKgjAjAKQA+ASA0gKQA7gMAnguQAYgcAaAbQAbAagYAcQgzA7hRARQgdAGgeAAQguAAgwgOg');
        this.shape_21.setTransform(96.6,-47.5);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AiCBQQgKgkAjgKQBvgeA4hcQATgfAgATQAgATgTAfQgjA6g3AoQg1AohEASQgHACgGAAQgYAAgIgcg');
        this.shape_22.setTransform(91.7,-69);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#000000').s().p('AgVBTQgOgEgJgOQgJgPAGgNIAQgpQAHgXAAgUQABgkAlAAQAlAAgBAkQgBAqgZA+QgGAPgMAHQgHAFgKAAQgFAAgFgBg');
        this.shape_23.setTransform(97.8,-40.1);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#000000').s().p('AgHCGQgkgKAHgjQALg+ABgXQACgvgTgjQgRggAggTQAfgTARAgQAaAxAAA+QAAAngNBLQgFAcgXAAQgHAAgHgDg');
        this.shape_24.setTransform(94.2,-56.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('ACADFQglgIgcguIgmhOQgjhJgggnQgug3g8gUQgigMAKgkQAKgkAiAMQB4AoBGCBQATAgAgBEIAOAdQALATAJACQAkAHgKAkQgIAegaAAIgLgBg');
        this.shape_25.setTransform(78,-87);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AAfBVQgNgEgKgNQgmg0gkgkQgZgaAagaQAbgbAZAaQAjAlAzBCQAJAMgFAQQgFAQgNAHQgIAFgJAAQgFAAgGgBg');
        this.shape_26.setTransform(-21.9,-134.2);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#E6E7E8').s().p('AgMBkQiigOiCgsQgggLgNgdQgLgZAIgdQAIgdAWgPQAZgQAhALQB5ApCWAMQB1AKCggGQAigBAUAWQATAUgBAeQAAAegSAWQgUAYgiABQg/ADg6AAQhgAAhPgHg');
        this.shape_27.setTransform(-30.8,5.1);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('Ag3A7QhFgJgbgHQg3gNghgZQgcgUATghQASggAdAWQAfAXA1ALQAfAHA8AFQB/AMB5gNQAjgEAAAkQAAAmgjAEQhFAHhCAAQhKAAhEgJg');
        this.shape_28.setTransform(1.2,-104.3);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('Ah0DTQgkgKANgiQBGi8CUixQAXgcAbAbQAbAagYAcQiHCghACsQgKAagYAAQgHAAgIgCg');
        this.shape_29.setTransform(-36.5,-121.7);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('AiOEdQghgTAOghQBwkHCPjxQATgfAgATQAhATgTAfQiQDwhvEHQgJAVgQAAQgKAAgLgGg');
        this.shape_30.setTransform(-63.5,-72);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('Ah4DXQgkgJALgjQAfhqA7hgQA7hjBPhLQAbgZAaAbQAaAagaAZQhJBFg2BZQg2BZgcBgQgIAbgYAAQgHAAgIgDg');
        this.shape_31.setTransform(34.4,-120);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#000000').s().p('ABADAQg4h4gdg7Qg0hngyhDQgWgdAhgTQAggSAVAcQAyBDAzBoQAeA6A4B4QAQAgggAUQgMAHgKAAQgQAAgKgVg');
        this.shape_32.setTransform(54.5,-120.3);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#000000').s().p('AA/FoIgKh+QgHhKgJgzQgvj+h4jBQgTgeAggTQAhgTATAeQCADQAuEBQAKA3AHBQIAMCIQADAkglAAQgmgBgDgjg');
        this.shape_33.setTransform(111.2,5.8);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#000000').s().p('AioF4QAol8DjmDQASgfAgASQAhATgTAfQhqC1g9CrQhGC9gTC9QgEAjglABQgmAAAEgkg');
        this.shape_34.setTransform(-95.5,-1.5);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#E6E7E8').s().p('AgfCIQkfgxjTh2QgdgRgGgeQgFgbAPgaQAPgaAbgJQAdgLAeARQDCBuEKArQDPAgEYgEQAiAAAUAXQASAVAAAeQAAAegSAWQgUAXgiABIg3AAQkIAAjOgjg');
        this.shape_35.setTransform(-51.8,31.1);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#E6E7E8').s().p('AA5ByQgogIgTgFQgkgKgVgOQgPgLgUgVIghgjQgZgWABgeQACgbAVgWQAWgVAcgDQAfgCAZAXQAIAHARAUIAPARIACACIANAEIBAAOQAeAHAPAcQAPAbgIAdQgIAegbAPQgSAKgUAAQgJAAgKgCg');
        this.shape_36.setTransform(-88.8,-8.1);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#000000').s().p('AioJ7QgkgKAKgjICmpWQBillA2j0QAIgjAkAKQAkAKgIAjQg2D0hjFkIilJXQgIAbgXAAQgHAAgIgCg');
        this.shape_37.setTransform(-4.3,-173.9);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('ABeH5QhgmPijpOQgKgjAkgKQAkgKAKAjQCiJMBiGSQAIAjgkAKQgIACgGAAQgYAAgHgcg');
        this.shape_38.setTransform(24.8,-184.4);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_39.setTransform(95.4,184.2);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_40.setTransform(107.7,88.3);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_41.setTransform(122.1,138);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgSAAQgKAAgLgGg');
        this.shape_42.setTransform(133.4,95.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_43.setTransform(145.1,125.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#FF0000').s().p('AAUBsQgOgPhtiJQgcgjgEg4IACgxQCpAKBDC1QAiBagBBYQgtAAhHhNg');
        this.shape_44.setTransform(163.7,93.7);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAkgkABQgrAAgmAZQgLAIgKAAQgQAAgLgVg');
        this.shape_45.setTransform(-171.8,201.8);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_46.setTransform(-6.8,254.2);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#FF0000').s().p('AhMAfQAAgkAkgBQARAAAMgIIAGgFIACgCIABgDQADgGACgJQAKgjAkAKQAkAKgKAjQgLAogfAXQgfAXgqAAIgCAAQgiAAAAgkg');
        this.shape_47.setTransform(123.3,229.2);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_48.setTransform(-49.8,235.2);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FF0000').s().p('AAfA9QgLgIgTgSQgTgTgJgHQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_49.setTransform(-129.2,229.8);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_50.setTransform(-54,198.6);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgvAAQhMAAhGgMg');
        this.shape_51.setTransform(25.2,203.8);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_52.setTransform(64.9,206.7);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_53.setTransform(-29.3,209.4);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_54.setTransform(139,59.2);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_55.setTransform(125,124.1);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_56.setTransform(-102.2,120.8);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_57.setTransform(73.6,52);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#000000').s().p('AI2DiQjlgPiGgMQjIgTiggbQh2gUg5gNQhhgYhGgjQhmgzAAhMQAAg2A9gtQAlgcBGgdQAigOAJAlQAKAkghANQgsATgWAMQgqAZgFAaQgDATAUARQALALAYAMQAsAXBGATQCEAlDFAbQDaAeF7AYQAkADAAAlQABAkggAAIgFgBg');
        this.shape_58.setTransform(-68.3,52.2);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_59.setTransform(49.6,147.5);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_60.setTransform(-30.3,97);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBagCBwA/QBtA8BIBXQBOBbgGBMQgIB3hhAWQgVAFgYAAQhHAAhagqg');
        this.shape_61.setTransform(-43.4,172.1);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQglBEhCAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_62.setTransform(103.3,85.9);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAagEAqQg8A4h3AiQhoAfhkAAIgcgBg');
        this.shape_63.setTransform(40.3,200.8);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_64.setTransform(41.3,212.5);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_65.setTransform(136.5,155.1);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_66.setTransform(-105.8,110.3);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#FFFFFF').s().p('AkZPdQiygMimgiQiegghYgnQiNg/gehvQgKgjADgkIAFgcQkXkch/oAQgvi9gPi1QgNibAOhIQAVhoDHgzQBjgaBfgFQChALELAFQIVAKIMghQINghCgBqQAyAiAIArQAEAWgHAPIgFBFIgRCvQA5gBA4AOQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQguAZiLBbQhkBDg9APIgGAKIgBAXQgGAcgVAhQhGBnjRBkQiwBUkxAAQhZAAhjgHgAQ4lKQgOBngLBDQgdCtitFMIAigSQBLgpB2iEQCAiQAxhwQAvhrhGhEQg5g3hQAAg');
        this.shape_67.setTransform(25.5,126.5);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FF0000').s().p('Ah6AWQAAgkAkgBQBDgCBUgkQAggOATAgQATAgghANQg0AXgoALQgxAOgvABIgCAAQgiAAAAglg');
        this.shape_68.setTransform(8.4,-194.8);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FF0000').s().p('AjKBCQAAglAkgBQBXgBBNgdQBNgeA+g6QAbgZAaAbQAaAagaAZQhJBChcAkQhZAkhmACIgBAAQgjAAAAglg');
        this.shape_69.setTransform(9.4,-174.2);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FF0000').s().p('AjhBXQgigMAKgkQAJgkAiAMQBcAhB1gkQBsghBMhIQAagZAbAaQAaAbgaAZQhbBUh/AoQhIAWhAAAQg7AAg0gTg');
        this.shape_70.setTransform(9.4,-154.4);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FF0000').s().p('AkKBxQgjgCgBglQAAgmAkACQCcAJBwgbQCPggBchaQAagZAaAbQAbAagaAZQiqCkk5AAQgkAAglgCg');
        this.shape_71.setTransform(8.4,-135.2);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#FF0000').s().p('Ai6AtQAAglAkgBQCmgBBzhPQAdgVATAgQATAhgeAUQiBBZi9ACIgBAAQgjAAAAglg');
        this.shape_72.setTransform(7.8,-110.4);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FFFFFF').s().p('AnLWaQoogoiKhzQABhFAHhWQAOirAchUQAchVB8jsIB2jbIDwnfQCbkYAjgtQALgPAag2QAQgiARADQATADAjAuQARAXAOAXQCbngAeiCQAfiGAchfQAqiDAXAAQAWAAA9D/QAfB/AaCAIBzGpIAVgXQAcgUAfANQAgAMBKCeQAmBPAfBMIA+AlQBCAuAWAvQAjBLAoBAIAnAbQAoAfAGAWQAGAWgKBqQgEA1gHAxIAfAWQAeAcgGAZQgGAYAWAfIAxBLQAlBEAlBiQAyCAAGBMIAeFFQhHAlh/AqQj/BSkaATQjBAMjIAAQjrAAj2gRg');
        this.shape_73.setTransform(6.5,-80.8);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-257.8,360.9,515.8);


    (lib.Анимация11 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('AhpCxQgagaAbgXQBFg5AlhEQAohKAChTQABglAlAAQAmAAgBAlQgCBiguBYQgsBUhOBBQgNALgMAAQgPAAgOgPg');
        this.shape.setTransform(38.5,-6.8);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('AiRBEQgagaAcgXQA5gsBFgaQBGgcBGgCQAkgBAAAmQAAAlgkABQg4ACg5AUQg4AVgsAjQgNAKgNAAQgOAAgPgOg');
        this.shape_1.setTransform(34.4,-22.9);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AgmA6QgPgFgHgMQgIgOAEgPQAEgOANgIIAJgGIADgCIAAAAIAOgLQATgQAMgIQANgHAPAEQAPADAIAOQAIANgEAPQgEAOgNAIIgJAFQgCACgBABQgBABAAAAQAAAAAAgBQABAAABgCIgPANQgTAQgMAHQgJAGgKAAIgKgBg');
        this.shape_2.setTransform(69.6,-121.1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('Ag7A7QgLgMAAgPQABgPAKgLQALgLAXgUQAXgVAJgLQAKgLAQAAQAQABALAKQALALAAAQQgBAOgKAMQgKAKgYAUQgWAVgKAMQgKAKgQAAQgQAAgLgKg');
        this.shape_3.setTransform(61.7,-88.7);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('AhOArQgKgkAigKQAsgOAUghQATgfAgATQAgATgTAeQgkA6hIAXQgIACgGAAQgXAAgHgbg');
        this.shape_4.setTransform(57,-63.4);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('AgqBIQgQgFgHgMQgIgOAEgOQAEgOANgKQAZgRAJgJQAMgMAEgLQAKgjAkAKQAkAKgLAiQgJAcgZAaQgQAQghAZQgHAGgKAAQgFAAgGgCg');
        this.shape_5.setTransform(50.3,-34.5);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#FF0000').s().p('AAdAqQghgSgtAFQgPACgLgNQgLgMAAgOQAAgQALgKQAKgJAQgCQA5gHA7AeQAgARgTAfQgMAVgTAAQgJAAgLgFg');
        this.shape_6.setTransform(47.1,-156.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#FF0000').s().p('AgJAwQgPAAgLgLQgLgLAAgPQAAgLAIgKIABgDIAIgLQAKgNAGgEQALgFALgBQAOgBANAMQALALAAAQQAAAPgLAKIgHAFIAAAAQgEAIgIAIQgKALgOAAIgCAAg');
        this.shape_7.setTransform(51.1,-131.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#FF0000').s().p('AAbBBQgRgEgFgNQgLgcgngMQgjgJAKgkQAKgkAjAKQBIAWAWAzQAGAPgDANQgDAPgNAIQgJAFgKAAIgKgBg');
        this.shape_8.setTransform(95.5,-134.4);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#FF0000').s().p('AAKBKQgOgIgDgOQgGgpgggSQgfgSATggQATghAfATQAbARATAbQAUAcAFAfQADAQgGAMQgHAOgQAEIgJABQgKAAgJgFg');
        this.shape_9.setTransform(65.6,-172.6);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#FF0000').s().p('Ag0BZQgkgKALgjQAYhIBOg2QAegVASAhQATAggdAUQg2AkgOAuQgJAbgYAAQgGAAgIgCg');
        this.shape_10.setTransform(90.3,-168.8);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('AgjAlQgLgLAAgQQAAgOALgLQADgEAFgCIALgPQAKgLAPAAQAQAAALALQALALAAAPQAAALgIAKIgJAOQgKANgGADQgLAGgMABIgDAAQgMAAgLgLg');
        this.shape_11.setTransform(72.5,-145.1);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#FF0000').s().p('Ah0KHQgkgKAGgjQA0kJA+lZIBrplQAGgkAkAKQAkALgGAjIhsJjQg9Fbg0EJQgFAcgXAAQgHAAgHgDg');
        this.shape_12.setTransform(59.1,-74);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#000000').s().p('AgOAlQgPAAgLgLQgLgLAAgPQAAgOALgLQALgLAPAAIAdAAQAPAAALALQALALAAAOQAAAPgLALQgLALgPAAg');
        this.shape_13.setTransform(-64.2,52.7);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#000000').s().p('ABZBWQgdhpg7gOQgfgIgaAYQgSARgSAjQgQAgghgTQgggSAQggQAcg4AsgeQA0gkA2ATQBlAiAnCJQAJAjgkAKQgIACgGAAQgYAAgHgbg');
        this.shape_14.setTransform(-95.1,40.1);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#000000').s().p('AibBXQgggTAPggQAag3ArgjQAwgoA4ABQA3AAAtAfQApAcAdAyQASAfggATQggATgSggQgthMhDAJQhCAJglBOQgJAVgRAAQgJAAgMgHg');
        this.shape_15.setTransform(5.4,50.2);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('AkfBcQgkgJAHgkQAVhrBHgZQA1gUBlAcQA5APB2AlQBkAZA3gYQAggPATAhQATAfghAOQhFAfhcgPQgvgIhwglQg4gSgQgEQgqgMggACQgjACgSAaQgOAUgIApQgGAbgXAAQgGAAgIgCg');
        this.shape_16.setTransform(49.5,49);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('Ai2BRQgbgaAZgbQBRhXBagZQBogeBZBDQAcAWgSAfQgTAggdgVQh+hfiTCfQgMANgNAAQgNAAgNgNg');
        this.shape_17.setTransform(102.2,43.2);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#FF0000').s().p('AgBB9QhThpAiiCQAJgjAkAKQAjAKgJAjQgYBdA2BGQAWAcgaAaQgLALgNAAIgBAAQgOAAgJgNg');
        this.shape_18.setTransform(20.9,-239.2);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#FF0000').s().p('AhbBxQAChOAhg/QAghAA9gvQAdgVASAgQATAggdAWQgsAhgXAxQgWAvgBA6QgBAkglAAQglAAAAgkg');
        this.shape_19.setTransform(28.8,-242.6);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#FF0000').s().p('AhWBmQgggTAQggQAagxAngnQAngnAxgaQAggQATAgQASAgggARQhTAqgqBTQgLAUgQAAQgKAAgMgGg');
        this.shape_20.setTransform(34.4,-239.3);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('AgEA8QgtgDgMhFQgCgPAGgNQAHgNAPgEQAOgEAPAHIAGAFIAGgFQANgIAPAFQAOAEAJANQAIAOgFAOQgKAfgMAQQgRAZgVAAIgEAAg');
        this.shape_21.setTransform(91.9,-67.1);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AAYChQgsihhHiBQgSggAhgTQAggTARAgQBNCOAuCmQAKAjgkAKQgIACgGAAQgYAAgIgbg');
        this.shape_22.setTransform(107.3,-11.4);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#000000').s().p('AgmBGQgbgaAZgbQAMgNAEgQQADgMAAgVQAAglAlAAQAlAAAAAlQAABIgpArQgMANgMAAQgNAAgNgNg');
        this.shape_23.setTransform(100.8,-33.7);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#000000').s().p('ACEBqQg5gFgbgFQgvgIghgSQgigSghgfQgZgXgggmQgXgbAbgbQAKgLANAAQAPgBALANQAgAnARARQAeAeAdANQAbAMAjAGQAVAEAsADQAjADABAmQAAAigfAAIgFAAg');
        this.shape_24.setTransform(34.7,-74.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('AgGBDQhlgDhGhBQgbgYAbgbQAagbAaAZQA0AxBMgCQBFgBA/goQAfgTATAgQATAggfASQhSA0hbAAIgGAAg');
        this.shape_25.setTransform(36.9,-52.6);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AAIBAQgWgBgJgRQgLgWgIgoQgLghAigLIAFgBQAjgJAIAjQAPAvACAPQACAOgNAMQgMALgOAAIgBAAg');
        this.shape_26.setTransform(63.1,-76.7);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('AgMBBQhSgFghhDQgQggAggTQAhgTAPAhQARAjAsACQApACAdgbQAagYAbAaQAaAagbAYQgyAuhFAAIgNgBg');
        this.shape_27.setTransform(83.9,-41.7);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AADB5QgjgKAFgjQAGgqgDgeQgDgkgQghQgQggAhgTQAfgTAPAhQAXAvAFAyQAEAqgIA6QgEAdgXAAQgGAAgIgDg');
        this.shape_28.setTransform(97.5,-56.9);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('AiiCwQgggTARggQA3hkBKhLQBQhUBegrQAggPATAgQATAgghAPQhWAohKBLQhABFgzBcQgLAUgRAAQgKAAgMgHg');
        this.shape_29.setTransform(-5.1,-118.7);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('AAXCZIg9iNQgmhVgUg6QgLgiAkgKQAkgKAMAiQATA7AlBVIA/CMQANAigkAJQgIACgHAAQgYAAgLgZg');
        this.shape_30.setTransform(20.2,-119.2);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('AgWCCIgMh9QgHhIgJgzQgHgjAkgKQAjgKAHAkQAKA3AIBPIANCFQAEAkgmAAQgkAAgEgkg');
        this.shape_31.setTransform(118.9,27.5);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#000000').s().p('AjPIbQgkgKAOghQAshxBNi9IB5ksQA5iNAWg9QAqhxAahcQAJgjAkAKQAkAKgJAjQgaBdgqBwQgaBHg1CDIh5EsQhODAgrBtQgKAagYAAQgHAAgJgCg');
        this.shape_32.setTransform(3.8,-172.9);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#000000').s().p('ABAIrQgkjtg5k1IhqofQgHgkAkgJQAkgKAHAjIBpIgQA6E0AkDtQAGAkgkAJQgIACgHAAQgXAAgEgbg');
        this.shape_33.setTransform(37.3,-172.8);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#000000').s().p('ACiDLQhfigg1g7Qhihxh5gUQgjgGAKgkQAKgkAjAGQCGAWBvB8QBABGBmCqQATAfggATQgMAHgKAAQgRAAgMgTg');
        this.shape_34.setTransform(68.9,-87.7);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#000000').s().p('AioFoQgkgJAIgkQAwjWBhimQAfg1A4hWQBEhmAXglQATgeAgASQAgATgTAfQgVAhhGBpQg5BVgfA3QhbCjgtDHQgGAcgXAAQgHAAgIgDg');
        this.shape_35.setTransform(-91.6,-0.8);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#000000').s().p('AkDFVQgggTAUgeQAqg+A/h1QBIiEAfgxQCAjPCqhEQAhgOAKAkQAKAkghAOQiZA9h3DJQgYAohGCFQg4BsgoA6QgMASgRAAQgLAAgMgHg');
        this.shape_36.setTransform(-46.1,-76.5);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#000000').s().p('AirAtQAAglAkgEQBGgGBNgXIAygOQAfgLAOgOQAZgaAbAaQAaAagZAaQgSATgkAPQgSAHgrANQhbAchZAIIgHAAQgdAAAAghg');
        this.shape_37.setTransform(39,-104.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#E6E7E8').s().p('AE8CbQmVgPkGiaQgdgRgGgfQgFgbAPgaQAQgbAagIQAdgKAdARQDnCIFpANQAiABAUAYQASAWAAAeQAAAfgSAUQgTAVgfAAIgEAAg');
        this.shape_38.setTransform(-73.2,26.6);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#E6E7E8').s().p('AEmCJQjegHjHgxQhkgYgigPQhNghgWg3QgMggAPgZQAOgWAdgIQAdgIAbALQARAHAKANIAQAJQALAFAWAHQA8AVBMAPQCtAlCnAFQAiABAUAXQASAWAAAeQAAAegSAVQgTAVggAAIgDAAg');
        this.shape_39.setTransform(-61.1,-5.6);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_40.setTransform(95.4,183.8);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_41.setTransform(107.7,87.9);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_42.setTransform(122.1,137.5);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgSAAQgKAAgLgGg');
        this.shape_43.setTransform(133.4,94.9);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_44.setTransform(145.1,124.8);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#FF0000').s().p('AAUBsQgOgPhtiJQgcgkgEg3IACgxQCpAKBDC1QAiBagBBYQgtAAhHhNg');
        this.shape_45.setTransform(163.7,93.3);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAkgkABQgrAAgmAZQgLAIgKAAQgQAAgLgVg');
        this.shape_46.setTransform(-171.8,201.4);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_47.setTransform(-6.8,253.7);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('AhMAeQAAgkAkgBQATAAAMgIQgFADAJgIIACgCIABgCQADgHACgIQAKgjAkAKQAkAKgKAjQgLAngfAXQgfAXgqABIgBAAQgjAAAAglg');
        this.shape_48.setTransform(123.3,228.8);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_49.setTransform(-49.8,234.8);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_50.setTransform(-129.2,229.3);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_51.setTransform(-54,198.2);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgvAAQhMAAhGgMg');
        this.shape_52.setTransform(25.2,203.4);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_53.setTransform(64.9,206.2);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_54.setTransform(-29.3,209);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_55.setTransform(139,58.7);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_56.setTransform(125,123.7);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_57.setTransform(-102.2,120.4);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_58.setTransform(73.6,51.6);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#000000').s().p('AI2DiQjlgPiGgMQjIgTiggbQh2gUg5gNQhigYhFgjQhmgzAAhMQAAg2A9gtQAlgcBGgdQAigOAJAlQAKAkghANQgsATgWAMQgpAZgFAaQgEATAUARQALALAYAMQAsAXBGATQCEAlDFAbQDaAeF7AYQAkADAAAlQABAkggAAIgFgBg');
        this.shape_59.setTransform(-68.3,51.7);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_60.setTransform(49.6,147.1);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_61.setTransform(-30.3,96.6);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBbgCBvA/QBtA8BIBXQBOBcgGBMQgIB2hhAXQgVAEgYAAQhHAAhagqg');
        this.shape_62.setTransform(-43.4,171.7);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQglBEhCAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_63.setTransform(103.3,85.4);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAagEAqQg8A4h3AiQhoAfhkAAIgcgBg');
        this.shape_64.setTransform(40.3,200.4);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_65.setTransform(41.3,212);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAxgzAlQg4AogiAAQgRAAgMgKg');
        this.shape_66.setTransform(136.5,154.7);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_67.setTransform(-105.8,109.8);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FFFFFF').s().p('AnDJAQhlgmCjhpQA0gjBXgtQBXgsAWgNQBLgpB1iEQCAiPAxhxQAvhrhGhEQg5g3hQAAQgyAAiVBBIiMBBIgoi0QA2g6BZgxQCzhjCzAtQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQg0AchOA1QhXA6gcAQQhSAwhHAAQglAAgigNg');
        this.shape_68.setTransform(128.4,129.1);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FF0000').s().p('AhaAtQgKgkAjgJQAsgMAxgwQAagZAaAaQAaAbgaAZQg+A9g/ASQgHABgGAAQgYAAgIgcg');
        this.shape_69.setTransform(26.9,-201.9);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FF0000').s().p('AhzBYQgkgDAAglQAAglAkADQBzAJBYhhQAYgbAaAaQAaAbgXAaQg0A5hAAdQg3AZg9AAIgYgBg');
        this.shape_70.setTransform(22.9,-182.4);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FF0000').s().p('Ai8B2QgkgCAAglQgBglAlACQBgAFBegqQBbgnBEhJQAZgaAaAaQAaAagYAbQhOBShqAuQhhArhhAAIgYgBg');
        this.shape_71.setTransform(20.3,-159.4);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#FF0000').s().p('AjLBhQgKgkAjgFQB0gRA+gcQBagnAthPQASggAhATQAgATgSAgQg1BbhlAxQhNAmh/ATIgIAAQgcAAgJgfg');
        this.shape_72.setTransform(26.2,-142.5);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FF0000').s().p('AiiBKQAAglAkgFQBBgIA5gdQA9ghAhgyQAVgeAgATQAgASgUAfQguBChOAqQhFAmhYALIgHABQgdAAAAgig');
        this.shape_73.setTransform(33.7,-122.4);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f('#FFFFFF').s().p('AhXPdQizgMimgiQidgghYgnQiOg/gehvQgJgjACgkIAFgdQkXkbh+oAQgvi+gPi0QgOicAPhHQAUhoDHg0QBjgaBfgFQCiAMEKAFQIVAKINghQINgiCgBrQAyAhAHAsQAEAVgGAPIgUDXQgZD6gdCvQgdCuioFEQhUChhPB/IgBAWQgFAdgWAgQhGBojQBkQixBUkyAAQhYAAhigHg');
        this.shape_74.setTransform(7,125.5);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f('#FFFFFF').s().p('AsHWMQiMgWiCgaIhmgWQAAg3ALhWQAYirA6ibQA7iaB0i5IBpiaQCGj9A1h/QAkhVCEhzQB6hqAlAAIErsZQAahkAhhkQBBjIAdAAQAdAAAmCyQAUBfAaCaQAUBdB/KuIBTAtQBcA3AmAvQA2BDA0BZQAaAtAPAgIARAGQAWALASANQA4AtAABHIAMAUQANAbAIAcQAZBagmBLIAkA8QArBQAiBfQBsEvgVFWQh5AgjGAfQmLBAl7AAQl6AAnBhGg');
        this.shape_75.setTransform(5.3,-78.3);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-257.4,360.9,514.8);


    (lib.Анимация10 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('AhWC+QgagbAXgbQByiDACiqQAAglAmAAQAlAAgBAlQgCDGiHCcQgMAOgNAAQgMAAgNgNg');
        this.shape.setTransform(0.7,-5.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('AiRBEQgagaAcgXQA5gsBFgaQBGgcBGgCQAkgBAAAmQAAAlgkABQg4ACg5AUQg4AVgsAjQgNAKgNAAQgOAAgPgOg');
        this.shape_1.setTransform(-5.2,-22.6);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AgkA8QgPgFgIgNQgHgNAEgPQAEgOANgIIACgBIABgBIAUgTQAQgPAOgLQANgJAQAFQAPAEAHANQAIAOgEAOQgEAOgNAIIgBACIgCABIgUATQgQAPgOALQgIAFgKAAIgLgBg');
        this.shape_2.setTransform(23,-120.6);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AgeBHQgQAAgKgLQgMgLABgPQAAgOALgNQAJgLAWgWQAVgVAJgMQAJgMARAAQARABAKALQAKAKAAAQQAAANgKANQgKAMgWAVQgVAXgIALQgJAMgQAAIgCgBg');
        this.shape_3.setTransform(17.6,-87.7);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('Ag6BEQgOgIgEgOQgEgQAIgMQAHgMAPgGQApgQARglQARggAgATQAhATgRAgQgeA4hJAfQgFACgGAAQgIAAgJgGg');
        this.shape_4.setTransform(14.8,-62.2);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('Ag4BCQgKgLgBgQQAAgQALgKIAfgbQAQgRAEgRQAIgjAkAKQAkAKgJAjQgGAbgVAZQgOARgcAZQgMAKgPAAIgBAAQgOAAgLgKg');
        this.shape_5.setTransform(10.4,-32.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#FF0000').s().p('AAfAnQgfgNgnAHQgPADgNgHQgOgHgEgPQgEgOAIgOQAIgOAOgDQBBgNA/AbQANAGAEARQADAPgHAMQgIAOgPADIgIAAQgKAAgKgEg');
        this.shape_6.setTransform(-2.3,-154.1);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#FF0000').s().p('AgmBDQgNgHgFgQQgEgQAIgMQAKgNAQgkQAQgeAWgGQAjgKAJAkQAIAegWAMIgNAWIgIARQgFAKgEAGQgKANgMAEQgGABgFAAQgJAAgIgFg');
        this.shape_7.setTransform(4.9,-131.7);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#FF0000').s().p('AAdA9QgQgEgGgNQgNgZgpgIQgPgEgIgNQgHgOAEgOQAEgPANgIQANgHAQAEQAfAHAZAQQAcASAOAZQAHAOgDAPQgEAPgNAIQgJAFgJAAIgLgCg');
        this.shape_8.setTransform(47.7,-135.9);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#FF0000').s().p('AAKBIQgGgZgWggQgYghgUgLQgggQATggQASghAgARQATAJASAVQAMAOARAZQAhAtAIAfQAJAjgkAKQgIADgHAAQgXAAgHgcg');
        this.shape_9.setTransform(13.5,-173.9);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#FF0000').s().p('Ag/BWQgkgKAJgjQALglAqghQAYgSAygiQALgJARAFQAQAFAHANQAIAOgEAOQgFANgNAKQgLAJgTAMIgfAVQgaAUgEAPQgIAbgYAAQgGAAgIgCg');
        this.shape_10.setTransform(41.5,-169.2);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('AgbAsQgNgIgDgOQgFgQAIgMQAFgJAHgEIAGgKQAJgNAMgEQAQgEANAIQANAHADAPQAFARgIALIgJARQgMATgSAEIgJAAQgLAAgJgEg');
        this.shape_11.setTransform(24,-144.8);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#FF0000').s().p('AhjJrQAvmfBOs2QADgjAmgBQAlAAgEAkQhOM2gvGfQgEAjglABQglAAAEgkg');
        this.shape_12.setTransform(16.1,-72.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#FF0000').s().p('AgLBlQgmhwgMhDQgGgkAkgKQAjgKAGAkQANBFAlBuQALAigjAKQgIACgHAAQgXAAgJgag');
        this.shape_13.setTransform(24.7,-238.2);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#FF0000').s().p('Ag3BbQghgTARggQAVgoA6hPQAKgNANgEQAOgEAOAIQANAHAEAQQAFAQgJAMQg6BPgVAoQgKAUgRAAQgJAAgMgHg');
        this.shape_14.setTransform(34.1,-239);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#FF0000').s().p('Ag5BkQAChZAnh4QAMgiAjAKQAjAKgLAiQglBwgCBNQAAAkglAAQglAAABgkg');
        this.shape_15.setTransform(32.2,-244.1);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('AkUBBQgKgkAkgFQBUgMBOgoIA3gfQAhgSAYgJQBBgWBCAbQAyAVA8A3QAbAXgbAbQgaAagbgYQgigggUgOQgjgXgeAAQgWgBgYAJQgSAGgaANQhFAmgrASQg/Abg6AIIgIABQgdAAgJggg');
        this.shape_16.setTransform(44.7,51.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('ADUB3Qg3gCg7giQgbgRhGg2QgggZgMgIQgcgSgXgFQgZgFgXATQgPANgSAbQgUAegggTQgggSATgeQArhBA3gSQA8gVBAAnQAjAWBJA9QBEAzA2ACQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_17.setTransform(-78.6,43.8);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('AA9COQgahAg0hDQgogxhBg/QgagZAagbQAbgaAaAZQBFBFAuA7QA5BKAeBKQANAhgkAKQgIACgIAAQgXAAgKgZg');
        this.shape_18.setTransform(79.9,-94.3);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('AkMFEQgagaAYgcQASgVAUgiIAhg7QBCh0Ang6QBHhrBDhHQBWhYBegvQAhgQASAgQATAgggAQQheAvhUBaQhCBIhEBsIhOCMQgwBWgqAvQgMAOgMAAQgNAAgNgNg');
        this.shape_19.setTransform(-49.2,-78.1);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('AAfCkQgRhZgbhFQgfhMgyhCQgWgcAggTQAggTAWAcQA0BFAhBTQAeBJASBdQAHAkgkAKQgIACgHAAQgXAAgFgcg');
        this.shape_20.setTransform(100.9,-58.5);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('AiPBJQgggSATgfQAegvAdgYQAogjAsACQAcACAuAPQA0ASAXADQAjAFgKAjQgKAjgjgEQgVgDgggLQgngOgMgCQgigJgbAUQgSANgYAmQgMAUgRAAQgKAAgNgIg');
        this.shape_21.setTransform(105.7,44.7);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AA9AqQg9ghg8AhQggASgSggQgTggAfgRQAwgbAyAAQAzAAAwAbQAgARgTAgQgNAVgRAAQgKAAgLgHg');
        this.shape_22.setTransform(-9.8,55);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#000000').s().p('ABIC7QhIiUiFi+QgUgeAggSQAggTAVAdQCFC/BHCTQAQAhggATQgLAHgKAAQgRAAgKgVg');
        this.shape_23.setTransform(2.2,-118.9);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#000000').s().p('AhvDDQgkgKANghQAqhoAshLQA1hbBEhCQAagZAaAaQAaAbgaAZQhtBrhODDQgKAagYAAQgHAAgIgCg');
        this.shape_24.setTransform(-17.9,-120.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('Ag1BMQgggTAQggQAYgtA2gyQAbgZAaAaQAbAbgbAYQgwAtgSAkQgLAUgQAAQgKAAgMgHg');
        this.shape_25.setTransform(46.7,-135.5);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('ABEC4Qg2hygeg3Qg1hcg4g8QgYgaAagbQAagaAZAaQA7BAA3BhQAlA+A2BxQAPAgggATQgMAHgKAAQgQAAgKgUg');
        this.shape_26.setTransform(60.2,-123.6);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_27.setTransform(95.4,184.1);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_28.setTransform(107.7,88.2);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_29.setTransform(122.1,137.8);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgTAAQgJAAgLgGg');
        this.shape_30.setTransform(133.4,95.2);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_31.setTransform(145.1,125.1);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#FF0000').s().p('AAUBrQgOgPhtiIQgcgkgEg3IACgxQCpALBDCzQAiBbgBBYQgtAAhHhOg');
        this.shape_32.setTransform(163.7,93.6);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAkgkABQgsAAglAZQgLAIgKAAQgQAAgLgVg');
        this.shape_33.setTransform(-171.8,201.7);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_34.setTransform(-6.8,254);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#FF0000').s().p('AhMAfQAAgkAkgBQARAAAMgIIAGgFIACgCIABgDQADgGACgJQAKgjAkAKQAkAKgKAjQgLAogfAXQgfAXgqAAIgCAAQgiAAAAgkg');
        this.shape_35.setTransform(123.3,229);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_36.setTransform(-49.8,235.1);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgSIgcgaQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_37.setTransform(-129.2,229.6);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_38.setTransform(-54,198.5);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgvAAQhMAAhGgMg');
        this.shape_39.setTransform(25.2,203.7);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_40.setTransform(64.9,206.5);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_41.setTransform(-29.3,209.3);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_42.setTransform(139,59);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_43.setTransform(125,124);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_44.setTransform(-102.2,120.7);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_45.setTransform(73.6,51.9);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#000000').s().p('AI2DiQjlgPiGgMQjIgTiggbQh2gUg5gNQhhgYhGgjQhmgzAAhMQAAg2A9gtQAlgcBGgdQAigOAJAlQAKAkghANQgsATgWAMQgpAZgFAaQgEATAUARQALALAYAMQAsAXBGATQCEAlDFAbQDaAeF7AYQAkADAAAlQABAkggAAIgFgBg');
        this.shape_46.setTransform(-68.3,52);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#000000').s().p('AAYFzQgEgrgBhZQgBhXgEgsQgWkMhVi7QgPggAggTQAhgTAOAgQBaDHAZELQAFAxACBgQACBiAEAvQADAkgmAAQglgBgDgjg');
        this.shape_47.setTransform(116.8,4.6);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#000000').s().p('AjRHuQgggTAOggQBWjABukJIC7nNQAOghAkAKQAkAKgOAhIjAHVQhvEOhXDEQgJAVgRAAQgJAAgMgHg');
        this.shape_48.setTransform(6,-180.2);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#000000').s().p('AA6JDQgzj0gylDQgPhZhInhQgFgkAkgKQAkgKAFAkIBWI6QAzFBAzD3QAHAjgkAKQgIACgGAAQgYAAgFgcg');
        this.shape_49.setTransform(38.9,-173.6);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#000000').s().p('AiNGHQgkgKAGgjQA9ljDUlrQASgfAgASQAhATgTAfQjNFgg8FcQgFAcgXAAQgGAAgIgCg');
        this.shape_50.setTransform(-94.2,-3.6);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#000000').s().p('Ai2A5QgkgGAKgkQAKgjAkAGQB4AUBigWIAwgNQAdgKAOgPQAYgbAaAaQAbAbgYAaQglAohkATQg8AMg9AAQg9AAg/gMg');
        this.shape_51.setTransform(31.9,-104.1);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#E6E7E8').s().p('ADnCOQhUgFg7gJQhOgMg9gVQhfgfh3hCQgegRgGgeQgFgbAPgaQAPgaAbgKQAdgKAeARQBkA4BmAeQBmAeB1AJQAiACAUAYQASAWAAAfQAAAegSAUQgSAUgdAAIgHgBg');
        this.shape_52.setTransform(-76.5,28.1);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#E6E7E8').s().p('ADlCbQi9gKiNg3QhLgdghgUQg9gjgWguQgOgfAMgeQAKgbAagPQAbgPAZAFQAdAHAOAfQAFAJAIAHQAHAGAQAJQAuAZAwAQQCBAqCFAHQAiACAUAYQASAWAAAeQAAAfgSATQgTAVgeAAIgFAAg');
        this.shape_53.setTransform(-65.3,-4.1);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_54.setTransform(49.6,147.4);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_55.setTransform(-30.3,96.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBagCBwA/QBtA8BIBXQBOBbgGBMQgIB3hhAWQgVAFgYAAQhHAAhagqg');
        this.shape_56.setTransform(-43.4,172);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQgkBEhDAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_57.setTransform(103.3,85.7);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAZgDArQg8A4h4AiQhoAfhkAAIgcgBg');
        this.shape_58.setTransform(40.3,200.7);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_59.setTransform(41.3,212.3);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_60.setTransform(136.5,155);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_61.setTransform(-105.8,110.1);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#FFFFFF').s().p('AnDJAQhlgmCjhpQA0gjBXgtIBtg5QBLgpB1iEQCAiPAxhxQAvhrhGhEQg5g3hQAAQgyAAiVBBIiMBBIgoi0QA2g6BZgxQCzhjCzAtQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQg0AchOA1QhXA6gcAQQhSAwhHAAQglAAgigNg');
        this.shape_62.setTransform(128.4,129.4);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#000000').s().p('AgPA0QgRgDglgMQgigLAKgjQAKgkAiALIAZAJQAPAEAKABIANAAIABABIAAgBIABAAIACgCIAPgVQAKgLARAAQAQABAKAKQALALgBAQQAAAOgKALIgBABQADgEgDAEIgDAEIgMAQQgMANgSAHQgOAFgQAAQgLAAgOgDg');
        this.shape_63.setTransform(75.1,-29.4);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#000000').s().p('AhGAcQgLgJAAgRQABgPAKgLQAMgLAPABQANABANAJQAJAHAHgDQAFgCAHgMQATgfAgATQAgATgTAeQgaArgnAHIgOABQggAAgigag');
        this.shape_64.setTransform(71.8,-66);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#000000').s().p('AghBBQgegMgYgcQgYgaAagaQAbgbAXAbQAMAOAHAFQAOAJAKgGQAQgJAAgTQABglAlAAQAmAAgBAlQAAAhgSAaQgTAbgeAMQgQAGgQAAQgQAAgRgGg');
        this.shape_65.setTransform(81.7,-46.6);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#000000').s().p('Ah5B4QgegUATggQATggAeAUQAgAVAmACQAsACATggQAUghgGgsQgEgggUguQgOghAlgKQAkgKANAhQAcBBAAA6QABBIgsAoQgtApg9ADIgHAAQg4AAgxghg');
        this.shape_66.setTransform(63.9,-53.1);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#000000').s().p('AgOBFQhjgZhLg9QgbgXAagaQAagbAcAXQA7AxBQATQBWATBDgZQAhgNAKAjQAKAkghANQgvASg1AAQgsAAgvgMg');
        this.shape_67.setTransform(3.8,-48.7);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#000000').s().p('Ai9gnQgYgbAbgaQAagbAYAcQA+BIBJAcQBKAeBdgLQAkgEgBAlQAAAmgjAEQgcADgbAAQivAAh9iRg');
        this.shape_68.setTransform(7,-67.4);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#000000').s().p('AhqAVQAAglAkAAQAtAAAPgCQAdgGAWgVQAbgZAaAbQAaAagaAYQgmAkghAIQgUAFgaACIgvAAQgkAAAAglg');
        this.shape_69.setTransform(112.2,-59.2);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#000000').s().p('AhGBSQgOgJgEgOQgFgPAJgNQAHgMAPgGQArgRAPgPQALgMAJgdQAMgiAkAKQAkAKgMAiQgRAzgXAWQgSARgZANQgQAJgfANQgFACgGAAQgIAAgJgFg');
        this.shape_70.setTransform(107.9,-75);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#000000').s().p('AgGBAQgPgHgIgMIgDgFIgNghIgEgXIAAgJQACgSAJgLQAIgJAOgCQANgBAKAGQAIAEAHAKIAGAKQAIAHADALQACAHgBAHQAHAFACAKQAEAPgIANQgFAJgIAEQgFAGgIAEQgIAEgIAAQgEAAgFgCg');
        this.shape_71.setTransform(85,-82.4);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#000000').s().p('AglAzQgKgLgCgOIgCgUQAAgRABgDQALgcAIgIQAKgKAOgCQANgCAMAHQAMAHAHANIABAEIADACQALALAAAQQABAGgDAHIABAEQAEAQgIAMQgHAOgQADQgGACgHgBIgIADIgLABQgRAAgMgMg');
        this.shape_72.setTransform(38.5,-76.5);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FFFFFF').s().p('AhXPdQizgMimgiQidgghYgnQiOg/gehvQgJgjACgkIAFgdQkXkbh+oAQgvi+gPi0QgOicAPhHQAUhoDHg0QBjgaBfgFQCiAMEKAFQIVAKINghQINgiCgBrQAyAhAHAsQAEAVgGAPIgUDXQgZD6gdCvQgdCuioFEQhUChhPB/IgBAWQgFAdgWAgQhGBojQBkQixBUkyAAQhYAAhigHg');
        this.shape_73.setTransform(6.1,126.2);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f('#FF0000').s().p('Ah9BDQgigKAKgkQAJgjAjAKQBtAgBUhcQAYgbAaAaQAbAbgYAbQg2A6hEAVQghALgjAAQglAAgngMg');
        this.shape_74.setTransform(25.7,-197.2);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f('#FF0000').s().p('Ah7BSQg4gNgRgxQgLgiAkgKQAkgJALAhQAGASAxgEQAegCAegIQAtgLAdgNQAqgUAUgcQAVgdAgATQAhASgWAeQgiAtg8AfQg3Acg/AIQgeAFgZAAQgbAAgUgFg');
        this.shape_75.setTransform(20.2,-173.1);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f('#FF0000').s().p('AjnBaQgigLAKgkQAKgkAiALQBnAjBxgeQB2gfA/hWQAVgdAgATQAhATgWAdQhOBqiLAqQhDAUhBAAQhDAAhBgWg');
        this.shape_76.setTransform(17.3,-153.5);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f('#FF0000').s().p('AjNCDQgkgEAAglQAAglAkADQBzANBjgrQBpgtA5hfQATgfAgATQAgATgSAfQhDBvh7A4QheArhoAAQgaAAgbgDg');
        this.shape_77.setTransform(23.7,-134.5);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f('#FF0000').s().p('Ai+B6QgjgIAKgkQAKgkAjAIQBWAUBhgtQBmguAchSQAMgiAkAJQAkAKgMAjQgmBviAA+QhTAphRAAQgmAAglgJg');
        this.shape_78.setTransform(29.2,-117.7);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f('#FFFFFF').s().p('AjRW2QjngLkRgkQlrgvhYg4QABg3AIhLQAQiYAjhnQAlhuBQivQBEiXAegxQAhg3BbiRQBsisArg/QApg7BmhgQA7g3A6gxQAegZCCjmQAqh5AxiGQBikLAeg/IBgjcQBBiTAYgDQAZgDAVBtQALA6ANBgQAGAmAmE9IAkE2IAigeQAogYAfAiQAlApA0BeQAzBcAPA1QAKAkA+A9QBJBIAeAzQAGAMBjCRQBBBfAPA+QAZBpBBDSIAzDjQAyD2gGBhQgHBthlBCQhXA6iCAMQldAiidANQlcAdiTAAIgwgBg');
        this.shape_79.setTransform(6.7,-81.3);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-257.7,360.9,515.5);


    (lib.Анимация9 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('AgxDnQghgTAQggQBijIg2irQgLgiAjgKQAkgKALAjQA/DDhxDoQgKAVgQAAQgKAAgMgHg');
        this.shape.setTransform(-27.2,-10.9);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('AhiBxQgggTAQggQAfg6AxguQAygwA7gYQAigOAJAkQAKAkghAOQgrASgpAoQgkAmgYAtQgLAUgRAAQgJAAgMgGg');
        this.shape_1.setTransform(-39.4,-23.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AgtA8QgKgKgBgQQAAgRALgKIAMgLQgBABgBABQAAAAAAABQAAAAAAgBQAAAAABgCIAFgHIASgpQAGgNAQgEQAPgEANAIQAOAIADAPQADANgGAPIgTAnQgMAWgPANQgLALgOAAIgCAAQgOAAgLgLg');
        this.shape_2.setTransform(-29.1,-117.3);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AgfBFQgQgEgHgNQgIgOAEgOQAEgOANgJIAFgEIAFgGIAAACIACgEIANgWIgBABIAEgLQAKgjAjAKQAkAKgKAjQgSA6grAeQgIAGgJAAQgFAAgGgCg');
        this.shape_3.setTransform(-26.6,-90);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('AgwA9QgLgLAAgQQAAgQALgKQAMgLAJgQIARggQASgfAgATQAgATgSAfIgWAmQgOAWgPAOQgKAKgPAAIgBAAQgPAAgKgKg');
        this.shape_4.setTransform(-25,-61.6);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('AgvBEQgbgbAbgYQANgMAKgPQAKgSACgPQACgQAJgKQAJgLAQAAQAPAAAMALQAMALgBAPQgEAggOAdQgPAegYAWQgMALgNAAQgOAAgNgNg');
        this.shape_5.setTransform(-24,-35.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#FF0000').s().p('AAFA2QgPgKgPgRIgLgMIgEgDIABAAIgDgCQgNgHgEgOQgEgPAIgNQAIgOAPgDQAPgFANAIQAOAJAPARIAMANIADADIgBAAIADABQAOAJAEANQAEAPgIAOQgIAMgPAFQgGABgFAAQgJAAgIgFg');
        this.shape_6.setTransform(-13,-142.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#FF0000').s().p('AgpAoQgLgLAAgPQgBgQAMgKIAggcQALgKAOAAQAQgBALALQAKAMAAAPQABAQgLAKIggAbQgLALgPAAIgBAAQgPAAgKgLg');
        this.shape_7.setTransform(-50.6,-135.3);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#FF0000').s().p('Ag7BbQgbgaAbgZQA1gvAFg9QADgjAlgBQAlAAgDAkQgJBhhGBAQgNAMgMAAQgOAAgOgOg');
        this.shape_8.setTransform(-17.6,-172.5);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#FF0000').s().p('AhQAdQgTggAfgRQA7ggA9AEQAkACAAAlQAAAkgkgCQgugCgkAUQgLAHgKAAQgRAAgMgVg');
        this.shape_9.setTransform(-59.6,-158.9);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#FF0000').s().p('AgIA/QgKgwgigmQgXgbAagbQAbgaAWAbQAzA7AMA9QAHAjgkAKQgIACgHAAQgWAAgFgcg');
        this.shape_10.setTransform(-42.2,-178.5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('AgOAlIgKgHIgGgFQgOgHgDgPQgEgPAHgNQAIgNAPgEQAPgDAMAHIALAIQAHABAHAEQAOAHADAPQAEAOgHANQgIAOgPAEQgHACgHAAQgMAAgKgHg');
        this.shape_11.setTransform(-32.2,-151.2);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#FF0000').s().p('AANJ7QgNkTgeloIg4p6QgDgkAmAAQAlAAADAkQAtH+AKB8QAeFmANEVQACAkglAAQgmAAgBgkg');
        this.shape_12.setTransform(-26.3,-73.8);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#000000').s().p('ABJBaQgRgogMgVQgTgggXgTQgXgVgVASQgMALgRAiQgQAggggTQghgSAQggQAZgyAigYQAqgfAuASQArARAjAxQAYAiAZA4QAOAhggATQgMAHgJAAQgRAAgJgVg');
        this.shape_13.setTransform(-97.2,41.8);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#000000').s().p('AiKBFQgagaAWgcQA8hJBJgSQBRgUBEA+QAbAYgbAaQgaAagbgZQgrgng0AWQgoAQgpAzQgMAPgMAAQgMAAgNgNg');
        this.shape_14.setTransform(111,41.8);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#000000').s().p('Ag+BmQgkgQgpgmIhFhAQgtgkgpApQgVATglBCQgRAggggTQghgTASgfQAohIAkgjQA5g4A8ATQAoAMAuAsQA8A5AQALQAzAiBEgZQAogPBMgrQCJhDBlBsQAYAbgaAaQgbAagYgaQgbgdgWgLQgegPgfAKQgXAHgeAQIg0AcQg5AegrAKQgYAFgVAAQghAAgcgLg');
        this.shape_15.setTransform(22.8,50.4);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('ABKA4QgLgggMgPQgTgWgaAIQgXAHgZAUQgQAOgZAaQgYAbgbgbQgagaAYgaQApgrAigVQAvgeAtADQBNAGAmBvQALAigkAKQgIACgGAAQgYAAgJgag');
        this.shape_16.setTransform(-28.6,49.3);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#FF0000').s().p('AgVBlQgBhbgchjQgJgiAkgKQAjgKAJAjQAfBtAABkQABAkgmAAQgkAAAAgkg');
        this.shape_17.setTransform(26.8,-240.1);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#FF0000').s().p('Ag/CCQgbgbAZgaQBDhHABhvQABglAlAAQAmAAgBAlQgCCOhYBdQgMANgNAAQgNAAgNgNg');
        this.shape_18.setTransform(33.3,-243.4);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#FF0000').s().p('AhcBdQgOgIgDgOQgFgQAIgMQAIgMAPgGQBjgpAbg9QAPghAgATQAgATgOAgQgkBQiHA5QgGACgFAAQgJAAgJgGg');
        this.shape_19.setTransform(38.8,-236.1);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('AA5C7Qguhugfg/Qgwhcg2hDQgXgcAbgaQAagbAXAcQA8BLAzBlQAlBGAyB4QAOAhgkAKQgIACgHAAQgYAAgLgag');
        this.shape_20.setTransform(-6,-118.8);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('Ah4DUQgkgKAMgiQAghdBDhjQAigzBgh7QAWgcAaAaQAbAbgWAcQhbB1gdAoQg9BcgdBTQgJAbgYAAQgHAAgIgCg');
        this.shape_21.setTransform(-26.8,-118.8);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AitEmQgkgJAOgiQBCioBFh2QBYiYBphhQAbgZAaAbQAbAagbAYQhjBbhTCPQhBBwg+CdQgKAagYAAQgHAAgJgDg');
        this.shape_22.setTransform(-56.6,-73.2);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#000000').s().p('Ag0BhQgggTAQggQAUgpAzhYQATgfAgASQAgATgSAfQgyBVgWAsQgKAVgQAAQgKAAgMgHg');
        this.shape_23.setTransform(50,-132.5);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#000000').s().p('ABRDCQhRiriLirQgXgbAagbQALgLANAAQAPgBALAOQCUC2BUCuQAQAhghASQgMAHgJAAQgRAAgKgUg');
        this.shape_24.setTransform(65,-121.6);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('ABpEcQgeiEhSiWQg0hfhvijQgUgeAggSQAggTAUAeQBwClA3BmQBUCaAgCJQAIAjgkAKQgIACgGAAQgXAAgHgcg');
        this.shape_25.setTransform(90.2,-76.5);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AA+GTIgMh5QgHhGgKgzQgvkEh2kmQgOghAkgKQAkgJAOAhQB5EyAuECQAKA1AHBIIANB+QADAkglAAQglAAgEgkg');
        this.shape_26.setTransform(115.1,1.3);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('AiNGHQgkgKAGgjQA9ljDUlrQASgfAgASQAhATgTAfQjNFgg8FcQgFAcgXAAQgGAAgIgCg');
        this.shape_27.setTransform(-92.1,-3.6);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AjWA5QgkgGAKgkQAKgjAkAGQB7AVB/gYIBAgLQAqgKAQgQQAZgaAbAaQAaAagZAaQgZAag3APQgcAIg9AKQhLALhGAAQhEAAg/gLg');
        this.shape_28.setTransform(30.9,-104.2);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#E6E7E8').s().p('ABnB+Qg7gLgngMQg0gPgogXQgUgLgZgUIgrgiQgWgRABgiQABghAUgVQAXgWAeABQAcABAZAUIAeAaQANAJAWALQAdAPArALQAWAGA1AKQAhAGAOAZQAMAXgIAeQgIAdgYARQgTAOgYAAIgQgBg');
        this.shape_29.setTransform(-85,26.6);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#E6E7E8').s().p('AB9CVQhDgIgqgMQg5gRgpgeQgYgSgdgiIgqg0QgUgZgBgcQgCgeAXgWQAUgVAhgBQAigBASAXIAhAsQASAXAQAOQAmAkBcAKQAgAEAUASQAWAVAAAgQAAAdgWAYQgUAWgZAAIgHgBg');
        this.shape_30.setTransform(-81.1,-7.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('Ag6AtQgjgJAKgkQAKgjAjAJQAiAIAhgZQANgIAQAFQAPAEAIANQAIANgFAPQgEANgNAJQgbAUghAGQgNACgOAAQgTAAgTgEg');
        this.shape_31.setTransform(53.2,-32.6);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#000000').s().p('AhFAdQgNgKgEgNQgFgOAIgNQAIgNAPgEQARgFALAIQAgAYAXgZQAZgaAbAaQAaAagZAZQgeAfgpAEIgLAAQgiAAgdgVg');
        this.shape_32.setTransform(51.5,-65.7);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#000000').s().p('AgzBBQgggNgYgbQgYgZAagbQAagaAYAbQAiAjAigLQAQgFAIgPIAOgdQAPghAgATQAgATgPAgQgRAmgUATQgZAYglAFIgTABQgZAAgXgIg');
        this.shape_33.setTransform(59,-50.7);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#000000').s().p('AgWCJQhBgEg1gjQgegUATggQASghAeAUQAjAXAqAGQAuAHAfgVQAigVAAgqQABgfgSgsQgNgiAkgKQAkgJANAhQAZA/gHA3QgIBDg1AhQguAdg6AAIgPAAg');
        this.shape_34.setTransform(40.6,-54.6);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#000000').s().p('AgfA5QgMgGgEgKQgHgRgBgWQgBgQAJgQQAGgLAHgHQAYgXAcAOQAKAFALAQIAGAFQALALAAAQQAAAGgDAHQAFALgDAMQgEAPgOAIQgNAIgOgEQgKAEgJAAIgBAAQgLAAgKgGg');
        this.shape_35.setTransform(67.3,-82.3);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#000000').s().p('AgyApQgHgQgBgWQgBgSAJgOQAHgLAHgHQAXgVAWAIQAQAFANASQAGACAEAFQALAKAAAQQAAAGgDAHQAFALgDANQgEAOgOAIQgOAIgOgEIgBAAQgQAFgGAAQgdgCgKgVgAAMAIIACgCIABgBIgBgCIgBAAg');
        this.shape_36.setTransform(19.5,-77.3);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#000000').s().p('AjWILQgggTAPghQBGieCBlLQB+lGBKikQAPghAgATQAgATgPAhQhJCjh/FGQiAFLhHCfQgJAVgQAAQgKAAgMgHg');
        this.shape_37.setTransform(9.3,-181.1);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('AAxJJIhOpAQgwlYgrjlQgGgkAkgJQAkgKAGAjQArDmAvFZIBPI+QAGAkgkAKQgIACgHAAQgXAAgEgcg');
        this.shape_38.setTransform(37.8,-172.6);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AirAtQAAglAkgEQBGgGBNgXIAygOQAfgLAOgOQAZgaAbAaQAaAagZAaQgSATgkAPQgSAHgrANQhcAchYAIIgHAAQgdAAAAghg');
        this.shape_39.setTransform(39,-104.2);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_40.setTransform(95.4,184);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_41.setTransform(107.7,88.1);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_42.setTransform(122.1,137.8);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgTAAQgJAAgLgGg');
        this.shape_43.setTransform(133.4,95.2);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_44.setTransform(145.1,125.1);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#FF0000').s().p('AAUBsQgOgQhtiIQgcgjgEg4IACgxQCpAKBDC0QAiBbgBBYQgtAAhHhNg');
        this.shape_45.setTransform(163.7,93.5);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAlgkAAQgrAAgmAZQgLAIgKAAQgQAAgLgVg');
        this.shape_46.setTransform(-171.8,201.6);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_47.setTransform(-6.8,254);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('AhMAeQAAgkAkgBQATAAAMgIQgFADAJgIIACgCIABgCQADgHACgIQAKgjAkAKQAkAKgKAjQgLAngfAXQgfAXgqABIgBAAQgjAAAAglg');
        this.shape_48.setTransform(123.3,229);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_49.setTransform(-49.8,235);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_50.setTransform(-129.2,229.6);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_51.setTransform(-54,198.4);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgvAAQhMAAhGgMg');
        this.shape_52.setTransform(25.2,203.6);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_53.setTransform(64.9,206.5);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_54.setTransform(-29.3,209.2);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_55.setTransform(139,59);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_56.setTransform(125,123.9);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_57.setTransform(-102.2,120.6);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_58.setTransform(73.6,51.8);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#000000').s().p('AI2DiQjmgPiFgMQjIgTihgbQh1gUg5gNQhigYhFgjQhmgzAAhMQgBg2A9gtQAmgcBGgdQAhgOAKAlQAKAkghANQgtATgVAMQgqAZgEAaQgEATATARQAMALAYAMQAsAXBGATQCDAlDGAbQDaAeF7AYQAkADAAAlQAAAkggAAIgEgBg');
        this.shape_59.setTransform(-68.2,52);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_60.setTransform(49.6,147.3);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_61.setTransform(-30.3,96.8);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBbgCBvA/QBtA8BIBXQBOBcgGBMQgIB2hhAXQgVAEgYAAQhHAAhagqg');
        this.shape_62.setTransform(-43.4,171.9);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQgkBEhDAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_63.setTransform(103.3,85.7);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAZgDArQg8A4h4AiQhoAfhkAAIgcgBg');
        this.shape_64.setTransform(40.3,200.6);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_65.setTransform(41.3,212.3);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_66.setTransform(136.5,154.9);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_67.setTransform(-105.8,110.1);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FFFFFF').s().p('AnDJAQhlgmCjhpQA0gjBXgtIBtg5QBLgpB1iEQCAiPAxhxQAvhrhGhEQg5g3hQAAQgyAAiVBBIiMBBIgoi0QA2g6BZgxQCzhjCzAtQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQg0AchOA1QhXA6gcAQQhSAwhHAAQglAAgigNg');
        this.shape_68.setTransform(128.4,129.4);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#000000').s().p('AhoA8QgkgDAAgmQAAgkAkADQA2AGAogHQAxgIAjgcQAcgWAaAaQAbAagcAWQhNA9hwAAQgUAAgWgCg');
        this.shape_69.setTransform(113.4,-57);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#000000').s().p('AiDBMQgkgFAAglQAAglAkAEQCFASBjhWQAbgYAbAbQAaAagbAYQhtBdiAAAQgYAAgYgDg');
        this.shape_70.setTransform(108.6,-71.6);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#000000').s().p('Ai1gTQgcgXAbgbQAagaAcAXQA5AxBOATQBDAPBWgEQAkgBAAAlQAAAlgkACIgiABQi6AAh5hmg');
        this.shape_71.setTransform(-23.7,-59.3);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#000000').s().p('ACyB6Qh5gDhegsQhmgwhChcQgVgeAhgTQAggTAVAeQA1BMBXAlQBNAjBlACQAkABAAAlQAAAlgiAAIgCAAg');
        this.shape_72.setTransform(-20.3,-77.6);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FF0000').s().p('Ag/A/QgwgHgQglQgOgfAhgTQAggUAOAiQAIARA0gMQAggIANgIQAGgDAGgFQADgCgBAHQAFghAlAAQAlAAgEAkQgEAjgmAZQgdASgsAJQgdAGgZAAQgOAAgMgCg');
        this.shape_73.setTransform(24.9,-188.1);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f('#FF0000').s().p('AjNAqQgagZAbgaQAagaAaAaQAwAwBugaQBsgYAig9QARgfAhATQAgASgSAgQgZAsg7AgQgwAbg9ANQgvALgoAAQhYAAgxgzg');
        this.shape_74.setTransform(22.3,-166.7);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f('#FF0000').s().p('AhWB2QhXgGhGgrQgegTASggQATgfAfARQA3AiBDAGQBAAFA/gUQCHgrAJhEQAEgkAmAAQAlAAgFAkQgIBBg6AxQgwAqhIAXQhFAWhCAAIgbgBg');
        this.shape_75.setTransform(17.8,-147.7);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f('#FF0000').s().p('AjrCIQghgOAJgkQAKgkAiAOQBuAtCCg9QCGg9Adh0QAIgjAkAJQAkAKgIAjQgjCQijBPQhJAkhNAHIggACQg+AAg1gWg');
        this.shape_76.setTransform(22,-127);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f('#FF0000').s().p('AiBBdQgKgkAjgIQA/gOApgiQAsgkAPg7QAIgjAkAKQAkAKgIAjQgTBMg8A0Qg3AxhRASIgMABQgZAAgIgdg');
        this.shape_77.setTransform(35.9,-109.6);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f('#FFFFFF').s().p('AhXPdQiygMimgiQiegghYgnQiOg/gehvQgJgjADgkIAFgdQkXkbh/oAQgvi9gPi1QgNibAOhIQAVhoDGg0QBjgaBfgFQCiAMELAFQIUAKINghQINgiCgBrQAyAhAHAsQAEAVgGAPIgTDXQgZD6gdCvQgeCuioFEQhUChhOB/IgBAWQgGAdgWAgQhGBojQBkQiwBUkyAAQhZAAhigHg');
        this.shape_78.setTransform(6.5,125.8);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f('#FFFFFF').s().p('AsfV/QiPgUh+gZIhigVQAHhVAPhqQAejTAqhoQAthxBVizQBIiZAmhFQBEiSBCh2QB/jhBwhTQAvhTA2hSQBsijAmAIIAhANIFctgQAhhBAZAdICZPOIAcg8QAfg8AVAAQAYAABKBtQBHBnAmBRQAMAZAxBLQA8BcAhA5QB+DaAhDNIAjBbQApBzAgB1QBlF3gdEGQiFAkjGAkQmNBIlFADIg6AAQmlAAmrg9g');
        this.shape_79.setTransform(7.3,-76.5);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-257.7,360.9,515.4);


    (lib.Анимация8 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('AAtA3QgagfgPgKQgZgPgcAMQghAPgSgfQgTggAggQQA6gaAsAQQAnAOAsAzQAWAcgaAbQgNANgMAAQgMAAgMgPg');
        this.shape.setTransform(-101.1,45.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_1.setTransform(95.4,185.6);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_2.setTransform(107.7,89.7);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_3.setTransform(122.1,139.4);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgSAAQgKAAgLgGg');
        this.shape_4.setTransform(133.4,96.8);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_5.setTransform(145.1,126.7);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#FF0000').s().p('AAUBsQgOgQhtiIQgcgkgEg3IACgxQCpAKBDC0QAiBbgBBYQgtAAhHhNg');
        this.shape_6.setTransform(163.7,95.1);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#000000').s().p('AifBQQgkgHAKgkQAKgkAkAHQBJANBGgZQA3gUBJg0QAegVASAgQATAggdAVQhTA7hJAWQg0ARg3AAQggAAgigGg');
        this.shape_7.setTransform(97.1,-49.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#000000').s().p('AiWBwQgkgBAAglQAAgmAkACQB5AFCWiOQAagZAbAaQAaAbgaAZQhLBGhJAoQhXAwhOAAIgLAAg');
        this.shape_8.setTransform(93.1,-67.7);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#000000').s().p('AgQBIQhqgZhcg+QgegUATggQASggAfAUQCqByDMgbQAkgFAAAlQAAAmgkAEQgoAGgoAAQhFAAhBgQg');
        this.shape_9.setTransform(-61.4,-52.4);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('ADNCBQh9gLhtgvQh4gzhPhTQgZgaAbgaQAagbAZAaQBFBKBqAtQBgApBtAKQAkAEAAAlQAAAjgeAAIgGgBg');
        this.shape_10.setTransform(-60.1,-70.7);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#FF0000').s().p('AgmBPQgThRAdhOQANghAjAKQAkAKgNAhQgXA9ANA6QAIAkgkAKQgHACgGAAQgYAAgGgcg');
        this.shape_11.setTransform(15.3,-244.4);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#FF0000').s().p('AhnBaQgkgKAKgjQAUhEBEgkQA5geBQgCQAkAAAAAlQAAAlgkABQg3ABglAPQgwAVgNAsQgIAbgXAAQgHAAgIgCg');
        this.shape_12.setTransform(31.4,-236);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#FF0000').s().p('Ah1B+QAKhfArhFQAuhLBPgqQAggRATAgQASAgggARQg/AhgjA5QgiA2gIBJQgEAjglABQglAAADgkg');
        this.shape_13.setTransform(30,-243);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA0giBCgBQAkAAAAAlQAAAkgkABQgrAAgmAZQgLAIgKAAQgQAAgLgVg');
        this.shape_14.setTransform(-171.8,203.2);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_15.setTransform(-6.8,255.6);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#FF0000').s().p('AhMAeQAAgkAkgBQATAAAMgIQgFADAJgIIACgCIABgCQADgHACgIQAKgjAkAKQAkAKgKAjQgLAngfAXQgfAXgqABIgBAAQgjAAAAglg');
        this.shape_16.setTransform(123.3,230.6);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_17.setTransform(-49.8,236.6);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_18.setTransform(-129.2,231.2);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('AgtAdQgLgLAAgPQAAgOALgLQALgLAPAAIAQgBIADAAIADgCIABAAQANgFANADQAOADAIAOQAIAMgEAPQgEARgNAGQgZALghAAQgPAAgLgLg');
        this.shape_19.setTransform(100.2,47.9);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('Ai2BVQgggTAVgdQBPhkBngXQBxgYBYBZQAaAYgaAbQgbAagZgZQg+g+hOATQhHAPg3BIQgOARgRAAQgLAAgMgHg');
        this.shape_20.setTransform(53.2,51.8);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('AFcB6QgmgqgfgTQgrgbgtAIIg/APQgnAKgZABQhDAEhBgwIgagTQgRgLgMgEQgSgFgTARQgIAHgSAaQgNATgZgCQgagBgEgZQgHgkgMgXQgPgcgSADQgOADgZAZQgaAagagbQgbgaAagaQAognArgKQA0gNAhArQAQAVAMAYQAIgHAJgGQAsgbAuASQANAFA7ApQArAeAjgCQAXgBA7gPQAzgOAfACQA2AEAvAdQApAYAoAuQAXAbgaAaQgNANgMAAQgNAAgMgOg');
        this.shape_21.setTransform(-37.2,50);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_22.setTransform(-54,200);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgvAAQhMAAhGgMg');
        this.shape_23.setTransform(25.2,205.2);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_24.setTransform(64.9,208.1);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_25.setTransform(-29.3,210.8);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_26.setTransform(139,60.6);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_27.setTransform(125,125.5);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_28.setTransform(-102.2,122.2);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_29.setTransform(73.6,53.4);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('AI2DiQjlgPiGgMQjIgTiggbQh2gUg5gNQhhgYhGgjQhmgzAAhMQAAg2A9gtQAlgcBGgdQAigOAJAlQAKAkghANQgsATgWAMQgqAZgFAaQgDATAUARQALALAYAMQAsAXBGATQCEAlDFAbQDaAeF7AYQAkADAAAlQABAkggAAIgFgBg');
        this.shape_30.setTransform(-68.3,53.6);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#FF0000').s().p('AgkA/QgMgLABgPQAAgSAIgXIAPgpQAFgPANgIQAMgIAPAFQAOAEAIAOQAJAOgFAOIgLAeIgIAYIgCAIQgBAPgKALQgLALgOAAQgPAAgLgLg');
        this.shape_31.setTransform(-152,-122.9);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#FF0000').s().p('AAJAoQgJgJgEgDIgHgEIgDgBIgCgBIgDAAIADAAIgDAAIgTgBQgkAAAAgkQAAglAkAAQAiAAAPAGQAaAJAZAaQAaAYgaAaQgOAOgNAAQgNAAgNgNg');
        this.shape_32.setTransform(-113.7,-144.6);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#FF0000').s().p('AhOAeQgTggAhgPQBAggAygCQAkAAAAAlQAAAkgkABQghABgsAWQgLAGgJAAQgTAAgMgWg');
        this.shape_33.setTransform(-162,-146.1);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#FF0000').s().p('AAOBMQgLgegVgeQgZghgZgOQgggRATggQATggAfARQAlAVAiAtQAdAnARAuQAMAigkAKQgIACgHAAQgYAAgJgag');
        this.shape_34.setTransform(-154.9,-171.2);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#FF0000').s().p('AggBUQgOgEgJgNQgIgOAFgOQAKgcAUgaIALgQQAEgIAAgKQABgkAlAAQAlAAgBAkQgBAmgTAaIgQAVQgIAMgEAKQgGAPgLAIQgIAFgJAAQgGAAgFgCg');
        this.shape_35.setTransform(-128.3,-173.6);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#000000').s().p('AgjAkQgLgLAAgPQAAgPALgKQAEgEAFgDIALgOQAKgLAPABQAQAAAKAKQALALAAAQQAAAOgLALIADgDIgGAHIADgEIgEAHIgIAKQgNANgUABIgBAAQgOAAgLgLg');
        this.shape_36.setTransform(-137.9,-147.3);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#000000').s().p('AgnA7QgagaAZgaIAIgIQAAABAAAAQAAAAAAAAQAAAAABgBQAAAAABgCIAEgHQAFgMAAgOQABgkAkAAQAmAAgBAkQgBA3goApQgMANgMAAQgNAAgOgOg');
        this.shape_37.setTransform(-131.4,-119.6);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('AgTBZQgkgKALgiQASg1ABguQAAgkAlAAQAlAAgBAkQAAA3gVBAQgJAagXAAQgHAAgHgCg');
        this.shape_38.setTransform(-123,-94.8);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AgXBaQgkgKAJgjQAVhIAJglQAJgjAjAKQAkAKgJAjIgeBtQgHAbgWAAQgHAAgIgCg');
        this.shape_39.setTransform(-114.4,-64.8);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AgSBeQgkgKAJgjQAShEABgpQABgkAkAAQAlAAgBAkQAAAvgVBSQgHAcgWAAQgHAAgIgDg');
        this.shape_40.setTransform(-107.9,-40.2);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#FF0000').s().p('AB/J6QiqpdicqBQgIgjAkgKQAkgKAJAjQCYJ7CtJjQAKAjgkAJQgIACgHAAQgXAAgIgag');
        this.shape_41.setTransform(-117.3,-75.3);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('AAADXQhLhOgwhtQg0h1AEhkQABgQALgLQALgLAPAAQBEAEBQAjQA0AXBVAyQAgASgTAgQgTAhgfgTQhSgwgkgRQgxgXgrgIQAGBFAjBOQAqBeBABEQAZAagaAbQgOANgNAAQgMAAgMgNg');
        this.shape_42.setTransform(-87.5,-5.4);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('AhuDKQAFgoAQgyQAIgaAWg+QAziJAyhoQAQggAgATQAgATgQAgQg1BugpBxIgbBOQgQAvgEAhQgFAkglAAIgBAAQglAAAFgkg');
        this.shape_43.setTransform(-101.4,24.3);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#000000').s().p('ABCJdIhjpUQg6lVguj9QgGgjAkgKQAkgKAGAkQAuD8A5FXIBkJSQAGAkgkAKQgIACgHAAQgWAAgFgcg');
        this.shape_44.setTransform(32.8,-165.1);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#000000').s().p('AjBIwQgkgKALgjQA0ihCFlqQB5lSA7i8QALgjAkAKQAkAKgLAiQg7C8h6FRQiDFrg1CiQgJAbgYAAQgGAAgIgCg');
        this.shape_45.setTransform(0.3,-176.1);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#000000').s().p('AgvBvQgkgKANgiIAhhSQATguAUggQAUgfAgATQAhATgUAeQgTAdgSApIgdBJQgJAagYAAQgHAAgIgCg');
        this.shape_46.setTransform(-82.5,-29.3);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#000000').s().p('ABBC1QgXhahLhzIgqg/QgZgjgYgVQgbgYAagaQAbgbAbAYQAdAaAfArQAQAYAhA1QBMB4AXBbQAIAjgkAKQgIACgGAAQgXAAgHgbg');
        this.shape_47.setTransform(-21,-115.7);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#000000').s().p('AjNGxQglgJAOgiIBQjMQAwh6AnhOQAPggAqhGQAlhCARglQAihMBNh5QATgeAhATQAgATgUAeQgvBJgPAaQghA5gTAvQgOAkgiA5QgpBCgMAYQgmBPgwB5IhQDKQgKAZgYAAQgHAAgIgCg');
        this.shape_48.setTransform(-55.1,-87.5);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#000000').s().p('ABnGVQhnl0gJgbQhIjhhZiaQgSggAggTQAggSASAfQBcCeBJDnQAPAtBlFqQAKAjgkAKQgIACgGAAQgYAAgIgbg');
        this.shape_49.setTransform(114.9,3.8);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#000000').s().p('ACyGlQg5jdhyilQgrg+gxg6QgPgSgPgpIgXhAQgxhng4hRQgUgeAggSQAggTAVAdQBGBmAwBxQAUAvAIAQQAQAhATAaQAzBBAbAnQBxCuA4DYQAJAjgkAKQgIADgHAAQgXAAgHgcg');
        this.shape_50.setTransform(74.7,-86.8);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#000000').s().p('AhBBWQgggSAUgeQAthAA1g2QAZgZAbAaQAaAagaAaQg0A0ghAzQgNARgRAAQgLAAgMgHg');
        this.shape_51.setTransform(45.8,-123.7);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#000000').s().p('AgJA6Qh4gJgpgGQgggGgQgFQgbgJgQgRQgagZAbgaQAagbAaAaQAMANAhAFQASACAfACIA9AGQAkADAYAAQBAABArgGQA/gIAogWQAggRATAgQATAgggAQQg1AdhMALQgtAHgtAAQgXAAgWgCg');
        this.shape_52.setTransform(10.6,-97.5);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_53.setTransform(49.6,148.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_54.setTransform(-30.3,98.4);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBbgCBvA/QBtA8BIBXQBOBcgGBMQgIB2hhAXQgVAEgYAAQhHAAhagqg');
        this.shape_55.setTransform(-43.4,173.5);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQglBEhCAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_56.setTransform(103.3,87.3);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAagEAqQg8A4h3AiQhoAfhkAAIgcgBg');
        this.shape_57.setTransform(40.3,202.2);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_58.setTransform(41.3,213.9);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_59.setTransform(136.5,156.5);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_60.setTransform(-105.8,111.7);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#FFFFFF').s().p('AndPRQjHhFhfhEQgrgfgrhmIgihhIigjwQhkiWhkmjQgfiDgbiOIgWh1QgLhyBBhYQAyhEBdgxQLijcNzBiQEVAeECA7QCBAeBJAXQApASAfAcQAPANAHAKQAhClgoD6QgXCOhGEQQgoCciVELQhKCGhDBnIgXBKQgmBThFArQhqBEiTAuQjKA/j/ANQgdACgcAAQjcAAj4hVg');
        this.shape_61.setTransform(5.4,122.6);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#FFFFFF').s().p('AnDJAQhlgmCjhpQA0gjBXgtIBtg5QBLgpB1iEQCAiPAxhxQAvhrhGhEQg5g3hQAAQgyAAiVBBIiMBBIgoi0QA2g6BZgxQCzhjCzAtQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQg0AchOA1QhXA6gcAQQhSAwhHAAQglAAgigNg');
        this.shape_62.setTransform(128.4,131);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#FFFFFF').s().p('AhXPdQizgMimgiQidgghYgnQiOg/gehvQgJgjADgkIAEgdQkXkbh+oAQgvi+gPi0QgOicAPhHQAVhoDGg0QBjgaBfgFQCiAMELAFQIUAKINghQINgiCgBrQAyAhAHAsQAEAVgGAPIgTDXQgZD6geCvQgdCuioFEQhUChhPB/IgBAWQgFAdgWAgQhGBojQBkQixBUkyAAQhYAAhigHg');
        this.shape_63.setTransform(6,127.4);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#000000').s().p('Ag7AtQgjgJAKgkQAKgjAjAJQAkAJAigaQAMgIAQAFQAQAEAHANQAIANgEAPQgEANgNAJQgdAUghAGQgNACgPAAQgTAAgTgEg');
        this.shape_64.setTransform(29.7,-25.7);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#000000').s().p('AhHAdQgNgKgEgNQgEgOAIgNQAIgNAPgFQAQgEAMAIQAhAYAZgZQAZgaAbAaQAaAbgZAYQgfAfgqAEIgLAAQgjAAgegVg');
        this.shape_65.setTransform(27.9,-58.9);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#000000').s().p('AhzAZQgZgZAbgbQAagaAZAaQAfAhAjgJQAhgJAVgoQARggAgATQAgATgRAgQglBGhIAOQgNADgOAAQg5AAgsgwg');
        this.shape_66.setTransform(39,-41.2);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#000000').s().p('AgWCIQhDgDg2gkQgegTASggQATghAfAUQAjAXAsAGQAvAGAhgUQAjgWAAgpQABgfgSgtQgOghAkgKQAlgKANAhQAZA/gHA4QgJBCg1AhQgwAeg7AAIgPgBg');
        this.shape_67.setTransform(16.7,-47.7);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#000000').s().p('AgfA5QgMgGgEgKQgIgTAAgUQgBgQAJgRQAHgLAGgGQAXgXAdAOQAKAFAKAQIAHAFQALALAAAPQAAAHgDAHQAFALgDAMQgFAOgNAJQgOAIgNgEQgLAEgIAAQgMAAgKgGg');
        this.shape_68.setTransform(51.7,-72.6);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#000000').s().p('AgLBAQgdgCgKgVQgHgRgBgVQAAgSAIgOQAJgNAFgFQAWgVAXAIQAQAFAMARIAAABIABAAIABABIAAAAIgBgBQAFACAFAFQALAKAAAQQAAAGgDAHQAFALgDANQgEAOgOAIQgNAIgPgEIgBAAQgQAFgFAAIgBAAgAAMAIIADgCIgBgCIgBgBgAAPAGIAAAAIABAAIAAAAg');
        this.shape_69.setTransform(-7.6,-70.8);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FF0000').s().p('AinA4QAAglAkgBQBHAAA4gYQA2gXA0gzQAagZAaAaQAbAbgaAZQg+A8hFAdQhGAehVABIgBAAQgjAAAAglg');
        this.shape_70.setTransform(17.2,-189.1);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FF0000').s().p('Ai0BaQgkgFAKgkQAKgkAkAFQBPALBSgdQBMgaBBg4QAcgXAaAaQAbAbgcAXQhRBFheAfQhFAXhGAAQgeAAgfgEg');
        this.shape_71.setTransform(11.2,-164.9);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#FF0000').s().p('Aj+CRQgkgGAKgkQAKgkAkAGQA9AKBJgRQBAgOA/gfQBAgeAogfQA2gpAbgwQASgfAhASQAgATgSAgQhBBwitBMQh4A1hrAAQgiAAgggFg');
        this.shape_72.setTransform(11.6,-143.4);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FF0000').s().p('AkDCoQgkgBAAgmQgBglAlABQCaAHCAg4QCUhAA4h/QAPghAgATQAgATgOAhQhACPioBLQiFA8iaAAIgggBg');
        this.shape_73.setTransform(12.1,-124.6);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f('#FF0000').s().p('AifBeQgKgkAjgIQBTgUAxgeQA8glAihBQARggAgATQAhATgRAgQgpBNhJAuQg9AohgAXIgNABQgYAAgIgdg');
        this.shape_74.setTransform(26.6,-106.1);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f('#FFFFFF').s().p('AAcXXQkMgNj5gVQmAggiZgnQiTgmgShIQgGgXAHgXIAJgTQAYhOAfhYQA9iwAig1QgbhCgWhFQgsiKAcgMQAcgNBAANQAgAGAaAJQB4kiAyiCQA4iRCGjqQA0huAhhCQA+h8AhAAQAjAAArA5QAWAdAPAdQClm4AZhLQBXkGAOglQBXjzAfgDQAegDAZC8QAjEEAFAUQAeB4BLH5IAeg3QAmg3AlAAQAmAAA+CHQAgBDAXBEIBSBkQBZBzAiBKQA3B4BaEEIB1ErQB4E4ASBFQAYBWgYA8QgZBBhQAdQhdAimKA8Ql3A4icAAIggAAg');
        this.shape_75.setTransform(8.8,-76.5);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-259.2,360.9,518.6);


    (lib.Анимация7 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('AhJAWQgegTATggQATggAeATQAnAZApgJQAkgHAKAjQAKAkgkAHQgWAFgVAAQgyAAgtgcg');
        this.shape.setTransform(-10,-29.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('AjcBRQgkgCAAglQAAglAkACQBOAFAogBQBDAAAygLQBUgTBlg6QAfgTATAhQATAggfASQh6BGhdARQg/ALhRAAQgvAAg0gEg');
        this.shape_1.setTransform(82.7,-47.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AjcBwQgkgCAAglQAAgmAkACQDdAMC8iWQAcgWAbAaQAbAagdAXQjMCgjhAAIghAAg');
        this.shape_2.setTransform(77.6,-65.5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AC0BoQjZgDiliMQgbgXAagbQAbgaAbAXQCMB3C9ACQAlABAAAlQAAAlgkAAIgBAAg');
        this.shape_3.setTransform(-85.8,-61.8);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('AC0CbQh5gdhlhAQhuhDhChfQgVgeAhgTQAggTAUAeQA6BSBiA7QBbA2BrAaQAjAJgKAkQgIAdgZAAIgMgCg');
        this.shape_4.setTransform(-80.8,-78.4);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#FF0000').s().p('AgmBPQgThRAdhOQANghAjAKQAkAKgNAhQgXA+ANA5QAIAkgkAKQgHACgGAAQgYAAgGgcg');
        this.shape_5.setTransform(19.8,-244.4);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#FF0000').s().p('AhnBaQgkgKAKgjQAUhEBEgkQA5geBQgCQAkAAAAAlQAAAlgkABQg3ABglAPQgwAVgNAsQgIAbgXAAQgHAAgIgCg');
        this.shape_6.setTransform(35.9,-236);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#FF0000').s().p('Ah1B+QAKhfArhFQAthLBPgqQAggRATAgQATAgggARQg/AhgjA5QgjA2gHBJQgEAjgmABQglAAAEgkg');
        this.shape_7.setTransform(34.6,-243);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#000000').s().p('AgtAdQgLgLAAgPQAAgPALgLQALgLAPAAIAPAAIAEgBIADgBIgCABQAPgHAOADQAOAEAIANQAIANgEAPQgEAQgNAGQgaAMggAAQgPAAgLgLg');
        this.shape_8.setTransform(73.9,52.6);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#000000').s().p('AB+BIQghgjgbgSQgmgagigCQglgBgfASQgaAQgbAiQgWAdgagbQgbgaAWgcQAlguAtgXQAxgaA2AHQBWALBYBaQAZAagbAaQgNANgNAAQgNAAgMgMg');
        this.shape_9.setTransform(28,52.4);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('AFbB6QglgqgfgTQgsgbgsAIQgNACgzANQgnAKgZABQhCAEhCgwIgagTQgQgLgMgEQgTgFgSARQgIAHgSAaQgOATgZgCQgZgBgFgZQgGgkgMgXQgQgcgRADQgOADgaAZQgZAagagbQgbgaAagaQAognAqgKQA1gNAhArQARAVALAYQAJgHAIgGQAsgbAuASQANAGA7AoQArAeAigCQAYgBA6gPQA0gOAfACQA1AEAwAdQAoAYAoAuQAYAbgaAaQgNANgMAAQgNAAgNgOg');
        this.shape_10.setTransform(-62.8,48);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#FF0000').s().p('AghApQgLgLAAgPQACgfAYgYQALgLAPAAQAPAAALALQALAKAAAQQAAAOgLALIgCADIADgFIgEAGIAAAAIgBACIgBABIAAAEIABgDQgCANgJAJQgLALgOAAQgPAAgMgLgAAeARIAAgBIAAADIAAgCgAAfAPIABgBIgCACg');
        this.shape_11.setTransform(-168,-120.7);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#FF0000').s().p('AAKAoQgKgJgEgDIgHgEIgCgBIgFgBIgUgBQgkAAAAgkQAAglAkAAQAjAAAPAGQAaAKAZAZQAaAYgbAaQgNAOgOAAQgNAAgMgNg');
        this.shape_12.setTransform(-129.3,-144.6);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#FF0000').s().p('AgtArQgPgDgIgOQgHgNADgOQAEgQANgHQAqgUAzAAQAkAAAAAmQAAAkgkAAIgdABQgMACgOAHQgKAFgJAAIgJgCg');
        this.shape_13.setTransform(-178.5,-145.5);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#FF0000').s().p('AAcBNQgNgHgFgPQgNgmg7gdQgggQASggQATghAgARQBRAqAaBFQAFAOgJAOQgIAOgOAEQgGACgFAAQgJAAgIgGg');
        this.shape_14.setTransform(-173.3,-170.2);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#FF0000').s().p('AgZBmQgkgKAMgiQAOgrAFgSQAJgjAAgdQABgkAkAAQAmAAgBAkQgBAigJAnQgHAbgOAsQgJAbgXAAQgHAAgIgCg');
        this.shape_15.setTransform(-144.8,-175.5);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('AgjAkQgLgLAAgPQAAgPALgKQAEgFAGgCIAKgOQAKgLAPABQAQAAALAKQALALgBAQQAAAOgKALIgEAEIgIANQgOANgUABIgBAAQgOAAgLgLg');
        this.shape_16.setTransform(-153.5,-147.3);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('AgnA7QgagaAZgaQAFgEAEgEIgBABIACgDIADgGQABgBAAgBQABgBAAAAQAAAAAAABQAAAAAAABIAEgNIAAgNQABgkAkAAQAmAAgBAkQgBA3goApQgMANgMAAQgNAAgOgOg');
        this.shape_17.setTransform(-147,-119.6);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('AgTBZQgkgKALgiQASg2AAgtQABgkAkAAQAmAAgBAkQgBA4gUA/QgJAagXAAQgHAAgHgCg');
        this.shape_18.setTransform(-138.5,-94.8);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('AgXBaQgkgKAJgjIAehtQAJgjAjAKQAkAKgJAjIgeBtQgHAbgWAAQgHAAgIgCg');
        this.shape_19.setTransform(-129.9,-64.8);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('AgRBeQgkgKAIgjQAShHABgmQABgkAkAAQAlAAAAAkQgBAwgVBRQgHAcgWAAQgGAAgIgDg');
        this.shape_20.setTransform(-123.5,-40.2);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#FF0000').s().p('AB/J6QimpMifqSQgJgjAkgKQAkgKAJAjQCeKSCnJMQAKAjgkAJQgIACgHAAQgXAAgIgag');
        this.shape_21.setTransform(-132.9,-75.3);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AgzCCIANiGQAIhPAKg3QAHgjAjAKQAkAKgHAjQgJAzgHBIIgMB9QgEAjgkABQgmAAAEgkg');
        this.shape_22.setTransform(-101,31.6);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#000000').s().p('AATH3QgNiHgvlwQgpk7gLi7QgCglAlABQAlAAACAkQALC7ApE7QAxFyAMCFQADAkgmAAQglAAgDgkg');
        this.shape_23.setTransform(32.9,-174.3);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#000000').s().p('AkFJzQghgTAPggQD1ozDzptQANghAlAKQAkAJgOAiQj3J2j5I7QgJAVgQAAQgJAAgMgHg');
        this.shape_24.setTransform(-1.3,-168.7);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('AAMA/QgKgBgMgDIgCgBIAAAAQgPAFgOgIQgNgIgEgPQgDgMAEgLQgDgHAAgHQAAgPALgLQAGgEAGgDQAOgTAQgEQALgEANAFQAMAEAJAJQAKALAFAJQAIANgBARQgCAcgHALQgPAWgUAAIgEgBg');
        this.shape_25.setTransform(-32.4,-78.7);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AAMA/QgHAAgPgEIgCgBIABAAIABAAIgBAAIAAAAIgBAAIgBAAQgOAFgOgIQgNgIgEgPQgDgMAEgLQgDgHAAgGQAAgQALgLQAGgFAGgCQAOgTAQgEQALgDANAEQAMAEAJAJQALANAEAIQAIAMgBARQgCAcgHALQgPAVgUAAIgEAAg');
        this.shape_26.setTransform(22.9,-71.5);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('Ag1CYQgkgKAJgjQANgxAchEQAghOAOgnQANgiAkAKQAkAKgNAiQgPAnggBMQgcBGgMAxQgHAbgXAAQgHAAgIgCg');
        this.shape_27.setTransform(-85,-25);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AAUBZIgshGQgbgogagWQgcgYAbgaQAagaAbAXQAcAZAfAuIAyBNQAUAeggATQgMAHgLAAQgRAAgMgTg');
        this.shape_28.setTransform(-27.3,-125.7);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('AjNGxQgkgKANghQASgtAyiWQAoh5AjhHQAjhFAgguIAigwQAVgdAJgWQAihRBRh8QAUgeAgATQAgASgUAfQgqBCgXAoQgiA8gVA1QgIAUgYAfQgfAngGAJQglA9gTAoQggBCgnB1QgtCKgUAwQgKAZgYAAQgHAAgIgCg');
        this.shape_29.setTransform(-56.8,-87.5);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('AgkC7Qg+hEglhfQgohiAFhYQAAgPALgMQALgLAPAAQBEAEBQAjQA0AWBWAzQAfASgTAgQgTAhgfgTQhTgwgjgRQgygXgqgIQAFA3AXA9QAgBRAzA5QAYAbgaAaQgOAOgLAAQgNAAgMgOg');
        this.shape_30.setTransform(-103.1,-8.1);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('ACNGLQgZi6hsjSQgvhfinkMQgTgfAggTQAggTATAfQCoEQAzBjQBvDYAZC+QAFAjgkAKQgIACgGAAQgXAAgEgbg');
        this.shape_31.setTransform(117.4,4.8);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#000000').s().p('AC1GuQghg+hBiOQg9iFgnhFQgkg+glg3QgNgSgNgmQgSgygEgKQgxhng4hQQgUgeAggTQAggTAVAeQBCBfAtBoIAcBCQAQAmARAZQAiA3AbAwQAjA/A9CFQA8CDAjBAQARAgggASQgMAHgKAAQgRAAgLgTg');
        this.shape_32.setTransform(73,-86.7);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#000000').s().p('AhdCeQgkgKANghQA4iPB0h2QAZgaAbAaQAaAbgZAaQhrBqguB5QgKAagYAAQgHAAgIgCg');
        this.shape_33.setTransform(40.7,-117.1);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#000000').s().p('AkrgIQgYgbAbgaQAagbAYAbQATAVAlAOQAYAJApAIQBFAMBSgDQBZgCA5gJIApgGQAZgFAPgIQAggRATAhQATAfggARQgYAMgjAHIg+AKQhcAMhOABIgEAAQjaAAhNhUg');
        this.shape_34.setTransform(5.4,-99.6);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#E6E7E8').s().p('AiqCAQgagNgJgfQgIgcAQgdQAPgdAdgFIBVgOQAxgKAdgRQARgKASgRIAlgmQAXgZAeABQAbACAWAWQAVAVADAcQACAggXAZQgeAggRAPQgcAagbAOQgqAWg2AOQgoALg+AJIgRACQgVAAgTgKg');
        this.shape_35.setTransform(108.7,29);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#E6E7E8').s().p('AjQB2QgSgTAAgfQAAgeASgWQAUgYAigDQBKgFA+gaQA7gaA5gxQAZgXAgADQAcADAWAWQAVAVABAbQACAegaAWQhPBFhXAkQhVAlhqAIIgHAAQgdAAgSgUg');
        this.shape_36.setTransform(97.9,-3.9);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#000000').s().p('AgEAxQgrgEgigeQgbgYAagaQAbgbAaAZQAdAbAngaQAfgUASAhQATAfgeAUQghAWglAAIgLgBg');
        this.shape_37.setTransform(-7.5,-62.8);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('AgLBLQgpgFgfgVQgagSgcgkQgWgdAhgSQAggTAWAcQAjAtAngFQAmgGAigwQAVgdAhATQAgASgVAeQhGBfhFAAIgLgBg');
        this.shape_38.setTransform(-20,-45.6);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AhfBlQgwgpgIg8QgHg3Aag+QAPghAgATQAgATgOAhQgRAnAHAjQAIAnAjATQA3AdBLguQAfgTATAgQATAhgfATQg6Ajg6ADIgJAAQg8AAgsgmg');
        this.shape_39.setTransform(2.8,-51.6);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#FF0000').s().p('Ai2BPQgKgkAjgFQCTgXCBhxQAbgYAbAaQAaAbgbAXQiVCDigAZIgJABQgcAAgIggg');
        this.shape_40.setTransform(13.3,-103.3);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#FF0000').s().p('AiQBFQgKgkAjgIQBHgSApgTQA2gbAlguQAKgNAPAAQANABALALQAaAagXAcQgpAzhBAhQgxAZhQAUIgMACQgZAAgIgeg');
        this.shape_41.setTransform(18.9,-194.7);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#FF0000').s().p('AitBdQgjgJAKgkQAKgkAjAJQBEARBYgeQBcgfAjg6QATgfAgATQAgATgSAfQgwBPh0ApQhEAZg/AAQgmAAgjgJg');
        this.shape_42.setTransform(13,-171.6);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#FF0000').s().p('Aj1CHQgkgFAKgkQAKgkAjAFQA9AKBIgOQA/gNA/gdQA+gcAkgZQA1gmAVgtQAPggAgATQAgATgPAgQgaA4hAAuQgwAihJAgQh7A1hvAAQgjAAgigFg');
        this.shape_43.setTransform(7.5,-149.2);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#FF0000').s().p('AlRB+QAAgmAkgBQCpgHCMgyQCtg8BVhyQAVgcAgATQAgASgVAdQhiCCi7BHQicA7i+AIIgDAAQghAAAAgkg');
        this.shape_44.setTransform(8.8,-126.7);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#FFFFFF').s().p('AxQUpQgFgxAAhAQgBiAAZhHQgthLgrhbQhVi2ANhPIAUgCQAagBAdAFQBdAQBcBLQAth+AziLQBlkUAdg7QAeg6BBh0IA7hpQAahJAnhJQBMiTA7AAIBqBxQBmkMBskMQDYoZAhAAQAhAAAJBeQAFA3AHB8QANByBdK8IAkgxQAqgxAdAEQAdAFA7CNQAdBHAXBGIBnC9QBsDLAdBDQAdBDAsBHIBeCVQBZCRBDCAQBmDDAIBOQAJBLgBA1QgBAbgCALQhCAXh3AdQjvA7kKAgQkbAikVAAQovAAociJg');
        this.shape_45.setTransform(8.4,-81.7);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#000000').s().p('AhIBAQglgOgSgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQARAJQANAGAUAAQAzADAxgdQAfgTASAgQATAfgfATQgtAcgxAIQgRADgPAAQgjAAgfgNg');
        this.shape_46.setTransform(101,185.6);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQglgnAGg7QAEgyAWgnQAagtArgQQAigLAKAkQAJAkgiALQgRAGgMAcQgKAYgBAUQgDA9A1ACQAkABAAAmQABAkgjAAIgCAAg');
        this.shape_47.setTransform(113.3,89.7);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#000000').s().p('AlOF1QgTggAfgSQDqiFClijQBlhiArhFQBGhzgkhcQgNgiAkgKQAkgJANAhQBFCxjvDzQi4C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_48.setTransform(127.6,139.4);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgJgjAggOQBKgfBHAEQBGADBIAmQAgAPgSAhQgNAVgSAAQgJAAgLgGg');
        this.shape_49.setTransform(139,96.8);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#000000').s().p('AlZIxQgTggAfgTQDxiPB6htQDAitAti+QAWhggbhWQgchdhOg2QhXg7hogKQhlgJhlAmQgiAMgKgjQgKgkAigNQBxgrB3ALQB9ALBeBFQBUA+AqBZQAsBegOBlQgeDljPDKQiLCIkNCgQgLAGgKAAQgRAAgMgUg');
        this.shape_50.setTransform(150.6,126.7);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#FF0000').s().p('AAUBsQgMgOhviKQgcgkgEg3IACgxQCqAKBCC0QAiBbgBBYQgtAAhHhNg');
        this.shape_51.setTransform(169.1,95.1);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAkgkABQgsAAgkAZQgMAIgKAAQgQAAgLgVg');
        this.shape_52.setTransform(-165.5,203.2);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_53.setTransform(-0.8,255.6);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#FF0000').s().p('AhMAeQAAgkAkgBQASAAALgHIAGgGIACgCIABgCQACgFADgKQAKgjAkAKQAkAKgKAjQgLAngfAXQgfAXgqABIgBAAQgjAAAAglg');
        this.shape_54.setTransform(128.9,230.6);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#FF0000').s().p('AgQBAQhGgPgwg0QgYgbAagaQAagbAZAbQAjAnAwAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_55.setTransform(-43.8,236.6);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgZANQgVAMgPAbQgRAggggTQghgTARggQAaguAsgWQAwgYAwAVQAQAHARAOIAcAaQAWAUALgDQAKgDATgdQATgeAhATQAgATgUAdQgdAtghASQgUALgTAAQgWAAgWgQg');
        this.shape_56.setTransform(-122.9,231.2);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgegTASggQATghAgATQAkAXAsAQQAiAOAzANQAkAJgLAkQgHAdgXAAQgGAAgIgDg');
        this.shape_57.setTransform(-47.9,200);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#000000').s().p('AjuA1QgjgGAKgkQAJgjAkAGQBmASB3gNQBwgMBpglQAigLAKAkQAKAkgiAKQhyAnh7AMQgwAFgvAAQhMAAhGgMg');
        this.shape_58.setTransform(31,205.2);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#000000').s().p('AlLC7QAAglAkgCQBzgIBmggQBugjBXg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg1BMQgoA3hKA2QhjBIh8ApQh1AmiDAIIgEAAQggAAAAgjg');
        this.shape_59.setTransform(70.6,208.1);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#000000').s().p('AjlBwQiAgwhIg1QhrhPgDhoQgBgkAlAAQAmAAABAkQACBIBeA+QBdA9CHAnQEoBYFcgUQAlgCgBAlQAAAmgkACQhGAEhEAAQlOAAkFhhg');
        this.shape_60.setTransform(-23.4,210.8);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_61.setTransform(144.5,60.6);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#000000').s().p('AhkErQghgTAQggQBvjuA4kgQAHgjAkAKQAkAKgHAjQg7Ekh0D8QgJAUgRAAQgJAAgMgHg');
        this.shape_62.setTransform(130.6,125.5);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#000000').s().p('ADZLGQj1kCiCmIQhylZgJmRQgBgkAlAAQAmAAABAkQAIGBBtFMQB7F6DsD4QAZAagaAbQgOANgMAAQgNAAgNgNg');
        this.shape_63.setTransform(-96,122.2);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#000000').s().p('AqPDlQgkgBAAglQAAglAkAAQFoABGKhAQBrgSBGgPQBhgVBNgaQAtgOAYgKQAngQAZgUQAsghgVgfQgPgWg0gYQghgOATggQATggAhAOQB3A1gEBSQgDAqgmAmQgeAcguAWQhNAlhlAbQhJAThvAVQm7BTmaAAIgPAAg');
        this.shape_64.setTransform(79.3,53.4);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#000000').s().p('AI1DiQjlgPiGgMQjHgTiggbQh1gUg5gNQhhgYhGgjQhlgygBhNQAAg2A9gtQAlgcBGgdQAigOAJAlQAKAkghANQgsATgVAMQgqAZgFAaQgEATAUARQAMALAYAMQArAWBGAUQCDAlDFAbQDbAeF5AYQAjADABAlQAAAkggAAIgEgBg');
        this.shape_65.setTransform(-62.2,53.6);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#FF0000').s().p('AhDD0Qh5gBhNhDIg1hOQguhoBLhZQBChQCFgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_66.setTransform(55.4,148.9);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#FF0000').s().p('ABmEgQipgFiBiGQh1h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDQAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg0BuiLAAIgLgBg');
        this.shape_67.setTransform(-24.3,98.4);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhbhiAAhaIAOg1QAjhXBmgCQBagCBvA/QBtA8BIBXQBNBcgFBMQgJB2hgAXQgWAEgYAAQhGAAhagqg');
        this.shape_68.setTransform(-37.4,173.5);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICEghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgUAGQhbAbgIBUQgHBFBeAbQBQAWBOgSQgkBEhDAtQg/ArhHAMQgXADgVAAQgvAAgngQg');
        this.shape_69.setTransform(108.9,87.3);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FF0000').s().p('AiaCFQgxgCgfgZIgWgaQAeh2CUg8QCAg0BpAYQBKASAVA1QAKAZgEArQg8A4h2AiQhoAfhkAAIgcgBg');
        this.shape_70.setTransform(46,202.2);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgEgoATgeQAWgkAvgMIBRgFQBagGAtgJQAqgIA+gSIBFgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQidAaiIALIgFAAQgaAAgVgZg');
        this.shape_71.setTransform(47.1,213.9);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#FF0000').s().p('AioCnQgpghAihTQAfhOBFg6QBhhTBYgHQAjgDAXAKQAVAKACASQADAsg2A6QgfAgg+A5QguAygyAkQg4AogiAAQgRAAgMgKg');
        this.shape_72.setTransform(142.1,156.5);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQggh6AahEQAXg8A8gCQA6gCA9AzQBBA2AlBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_73.setTransform(-99.6,111.7);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f('#FFFFFF').s().p('AnCJAQhkgmChhpQA1gjBXgtIBtg5QBJgpB1iEQCAiPAyhxQAuhrhGhEQg5g3hPAAQgyAAiUBBIiMBBIgoi0QA2g6BZgxQCxhjC0AtQC9AvBHCLQAwBegLB5QgIBYifDDQimDNiIBHQg0AchNA1QhYA6gcAQQhSAwhHAAQgkAAgigNg');
        this.shape_74.setTransform(134,131);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f('#FFFFFF').s().p('AhXPdQiygMilgiQiegghXgnQiNg/gehvQgKgjADgkIAFgdQkXkbh+oAQgvi+gPi0QgNibAOhIQAVhoDGg0QBjgaBfgFQChAMEKAFQITAKILghQIMgiCfBrQAyAhAIAsQADAVgGAPIgTDXQgZD6gdCvQgdCuioFEQhUChhOB/IgBAWQgGAdgVAgQhGBojQBkQivBUkyAAQhYAAhigHg');
        this.shape_75.setTransform(12,127.8);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-185.9,-259.2,371.9,518.6);


    (lib.Анимация6 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#FF0000').s().p('AgMA+QgBg5gjgkQgZgaAagaQAbgbAYAaQA4A8ABBWQABAkglAAQglAAAAgkg');
        this.shape.setTransform(8.7,-237.6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#FF0000').s().p('AhcA8QgKgkAjgHQBDgNAVhDQALgiAkAKQAkAKgLAiQgRAzgjAjQgmAkgyAKIgLABQgaAAgIgeg');
        this.shape_1.setTransform(18.5,-236.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#FF0000').s().p('AgiCLQgkgKAIgjIAVhtQAOg/AWgrQAQggAgATQAhATgRAgQgUAmgMA6IgSBlQgGAcgWAAQgHAAgIgDg');
        this.shape_2.setTransform(15.8,-243.2);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AhSBSQgOgJgEgOQgEgPAIgNQAHgLAPgGQBBgaAwhAQAJgLARAAQARABAKAKQALALgBAPQAAAOgKANQg4BHhaAmQgFACgFAAQgJAAgJgGg');
        this.shape_3.setTransform(117.8,40.7);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('AClBCQgXglgTgOQgbgVgiAJIgvARQgbAKgTACQg0AHgzgLQg6gOggggQgZgaAagbQAagbAZAbQAmAoBDgJIBtgcQA9gPAxAdQApAZAlA6QATAfggASQgMAHgKAAQgSAAgMgTg');
        this.shape_4.setTransform(-68.5,48.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('AhaAjQgTggAfgSIAlgWQAWgMASgEQAlgGArAZQAfASgTAfQgTAggfgSQgMgHgFgBQgLgEgIAFIgtAbQgMAGgJAAQgRAAgMgUg');
        this.shape_5.setTransform(52.5,53.9);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#000000').s().p('ACHBJQg5hJhJgKQhPgMgzBNQgUAegggSQghgTAUgeQBHhrB1AFQBuAEBQBkQAWAcgaAbQgLALgNAAIgBAAQgOAAgKgNg');
        this.shape_6.setTransform(1.1,53);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#000000').s().p('AgeA+QgNgIgEgPQgDgPAHgNIAKgaQAFgSAIgOQAHgOAPgDQAPgEANAHQAOAIADAPQAEAPgHANIgKAaQgGARgIAPQgHAOgOAEIgKABQgKAAgJgFg');
        this.shape_7.setTransform(-133.6,-122.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#000000').s().p('Ag5AzQADg6Apg9QASgeAhATQAgASgUAfQgVAegGASQgGAQgBARQgCAkgkAAIgBAAQgkAAACgkg');
        this.shape_8.setTransform(-127.9,-95.2);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#000000').s().p('Ag5A2QAHhYArgqQAZgaAaAbQAbAagaAZQgMAMgIAYQgGAWgCAUQgCAkglAAQglAAACgkg');
        this.shape_9.setTransform(-123.5,-65.5);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('Ag4A7QAFg+AnhHQARggAgATQAgATgRAgQggA5gCAmQgDAjglABQglAAADgkg');
        this.shape_10.setTransform(-118.7,-42.7);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#FF0000').s().p('AguBFQgMgMACgPQACgXAPgdIAbgxQARgfAgATQAgATgRAfIgSAgIgOAcIgCAEIgBAGIAAgFQgCAPgJAKQgJALgQAAQgPAAgMgLg');
        this.shape_11.setTransform(-154.6,-132.5);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#FF0000').s().p('AAbA1QghgLgTgPIgKgHIAAAAIgBAAQgPgBgLgKQgLgJAAgQQAAgPALgLQAMgMAOABQAdABAPANQASAOAVAHQAPAFAHANQAIAMgEAPQgEAOgOAIQgJAFgJAAQgFAAgFgBg');
        this.shape_12.setTransform(-115.1,-150.7);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#FF0000').s().p('AguAqQgPgDgIgOQgHgMADgPQAEgRAOgGQAtgTAwADQAQABAKAKQALALAAAPQAAAOgLALQgLAMgPgBIgdAAQgNABgOAFQgKAFgKAAIgIgBg');
        this.shape_13.setTransform(-167,-154);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#FF0000').s().p('AAbBCQgTgKgbgXQgfgagOgHQgfgSATggQATghAfATQAPAJAdAZQAcAXASAIQAgARgTAgQgMAWgSAAQgJAAgLgGg');
        this.shape_14.setTransform(-157.1,-179.8);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#FF0000').s().p('AgeBXQgkgKAHgjQAKgqAXgeIAKgMIACgCIAAgBIABgCIAAgEQABgQAKgKQAKgLAPAAQAPAAAMALQAMALgBAPQgDAkgXAbQgPASgIAgQgFAbgXAAQgGAAgIgCg');
        this.shape_15.setTransform(-129.2,-183.6);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('AgkAkQgLgLAAgQQAAgOALgLQAFgFAGgDIAKgLQALgKAPgBQAPAAALALQAKALABAQQAAAOgLALIgDADIgEAFQgDAGgEADQgLAIgIADQgFACgJAAQgPAAgLgLg');
        this.shape_16.setTransform(-138.9,-154.5);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#FF0000').s().p('ABNJ8Qg5kLg9lmIhrpzQgGgjAkgKQAkgKAGAkIBqJzQA+FlA5ELQAHAkgkAKQgIACgGAAQgYAAgFgcg');
        this.shape_17.setTransform(-125.1,-78.1);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('Ah0C2QgggTARgfQBfipBuiKQALgNAPAAQANABAKALQAbAagXAcQhjB6heCpQgLAUgRAAQgKAAgMgHg');
        this.shape_18.setTransform(24.2,-121.4);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('ABTCyQhDh0gggxQg8hZhAg5QgbgYAbgaQAagbAbAYQBDA8A/BeQAkA1BFB4QASAfggATQgMAHgKAAQgRAAgMgUg');
        this.shape_19.setTransform(45.7,-120.3);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('ABnECQh3ktiWisQgYgbAagbQAbgaAYAbQBaBnBLCKQBAByA7CXQANAhgkAKQgIACgHAAQgYAAgKgZg');
        this.shape_20.setTransform(77.4,-72.1);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('AAUBJQgOgagdgdQgOgOgmggQgcgXAbgbQAagaAcAXQAmAhAUAVQAgAfAQAfQARAgggATQgMAHgKAAQgRAAgKgUg');
        this.shape_21.setTransform(-35.7,-134.8);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AiHDpQgggTAPghQBBiHAshMQBAhyBIhRQAYgbAbAaQAaAbgYAaQhEBNg/BuQglBEhBCJQgKAVgRAAQgJAAgMgHg');
        this.shape_22.setTransform(-52.7,-120.4);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#000000').s().p('AAICYQgihPgTg+QgXhLgIhIQgDgkAlAAQAmABADAjQAGA+AUBDQARA0AfBFQAOAhggATQgMAHgJAAQgRAAgJgVg');
        this.shape_23.setTransform(-112.8,-10.7);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#000000').s().p('AAzA9QhBgshBgMQgjgGAKgkQAKgkAjAHQBJANBLAyQAeATgTAgQgLAUgQAAQgKAAgMgHg');
        this.shape_24.setTransform(-105.7,-23.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('AhxEsQgkgJAIgkQAhiOA1iHQA3iNBIh4QATgeAgASQAgATgTAfQhGB0g1CHQgyCDggCKQgGAcgXAAQgHAAgIgDg');
        this.shape_25.setTransform(-75.1,-73.9);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AgzCqQgjgJAJgjIAmiIQAXhSAUg2QANghAkAKQAkAKgNAhQgVA2gXBQIglCKQgHAbgYAAQgGAAgJgDg');
        this.shape_26.setTransform(-92.5,-24.8);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('AhBC+QAXjuAjiVQAIgjAkAKQAkAKgJAjQgiCTgVDcQgEAjgkABQglAAADgkg');
        this.shape_27.setTransform(-103.5,22.3);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AAUHYQgVimgkkyQgmlFgTiRQgEgkAmAAQAlAAAEAkQASCQAmFGQAkExAVCnQAFAjgmAAQglAAgEgjg');
        this.shape_28.setTransform(21.4,-181.5);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('Aj0JjQgkgKAOghQC8m+ERrEQANghAkAKQAkAKgNAhQkRLFi8G8QgLAagYAAQgHAAgIgCg');
        this.shape_29.setTransform(-11.7,-174.8);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('ABkFtQg8lejNleQgSgfAggTQAggSASAfQDTFoA+FmQAGAjgkAKQgIACgGAAQgXAAgFgcg');
        this.shape_30.setTransform(110.7,-3.9);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('AhLA2Qg9gMgVgGQgygPgXgXQgZgaAagbQAbgaAZAaQARASAvALIBIALQB+ATBugSQAkgHAKAjQAKAkgkAHQhEAMhEAAQhNAAhNgPg');
        this.shape_31.setTransform(-15.2,-104.4);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#E6E7E8').s().p('AkpBwQgSgUAAgeQAAgfASgWQAUgXAigDQEJgTC0hWQAfgPAeAMQAbAKAPAaQAPAagFAaQgGAdgfAOQh1A4iKAfQh4AciSAKIgHAAQgdAAgSgTg');
        this.shape_32.setTransform(91.9,26.9);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#E6E7E8').s().p('Ak8B9QgSgUAAgeQAAgfASgVQAUgXAigBQCkgGCRgsQBIgVAigRQAcgOANgJIABgBQAMgSAVgJQAbgLAdAIQAdAIAOAWQAPAZgMAgQgTA1hBAjQgmAWhPAZQi6A9jNAHIgEAAQggAAgSgWg');
        this.shape_33.setTransform(80.8,-4.7);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#000000').s().p('AhEAVQgNgKgEgMQgEgPAIgOQAIgNAPgEQAQgEAMAIQAjAZAjgJQAjgIAKAjQAKAkgjAIQgUAFgUAAQguAAgqgcg');
        this.shape_34.setTransform(-32.2,-32.8);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#000000').s().p('AgEAxQgqgEgfgeQgZgZAagaQAbgaAZAZQAZAaAhgYQAMgJAQAFQAPAEAIANQAIAOgEANQgEAOgNAJQgeAWgjAAIgLgBg');
        this.shape_35.setTransform(-30.5,-66);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#000000').s().p('AgMBGQhIgOglhGQgRggAggTQAggTARAgQAVAoAhAJQAjAJAfghQAZgaAaAaQAbAbgZAZQgsAwg5AAQgOAAgNgDg');
        this.shape_36.setTransform(-41.5,-48.3);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#000000').s().p('AhjBrQg0gngJg9QgIg2AZhAQANghAlAKQAkAJgOAiQgrBrA9AfQAkATAtgFQAqgFAkgYQAfgTATAgQASAggeAUQg1AihEAGIgUABQg8AAgqgfg');
        this.shape_37.setTransform(-19.2,-54.8);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('AjGBEQgjgFAAglQAAgkAjAEQBHAIAiACQA7ADAsgJQBLgPBcg0QAfgTATAhQATAgggASQg1AegpARQg2AXgwAHQglAFgtAAQg8AAhKgJg');
        this.shape_38.setTransform(55.5,-50.3);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AjHBkQgjgEgBglQAAgmAkAEQDCAWCtiLQAcgWAbAaQAaAbgcAWQhiBOhjAjQhRAdhVAAQgcAAgdgDg');
        this.shape_39.setTransform(52,-68.4);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('ACkBTQhbgBhXgXQhhgahGgsQgegTATggQATghAeAUQA+AoBVAWQBQAUBQACQAkABAAAlQAAAkgiAAIgCAAg');
        this.shape_40.setTransform(-94.4,-61);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('ACOBwQhegChRgsQhTgtg1hMQgVgeAhgTQAggSAUAdQApA8BCAiQBCAjBKACQAkABAAAlQAAAkgiAAIgCAAg');
        this.shape_41.setTransform(-88.7,-76.9);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('AAJA/IgTgEQgNAEgNgIQgOgIgEgPQgDgMAFgLQgDgHAAgGQAAgQALgKIAHgGQALgTASgGQALgDANAEQAMAEAIAKQAJAKAFAKQAHAQgBAPQgBAcgIAJQgPAVgTAAIgEAAg');
        this.shape_42.setTransform(-50.4,-82.6);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('AALA/QgGgBgQgEQgOAFgOgJQgOgIgEgOQgDgMAFgLQgDgHAAgHQAAgPALgLQAFgEAGgDQAOgUAOgDQAdgFARAUQAJAJAFAKQAIAOgBAQQgBAbgIALQgPAWgTAAIgFAAgAgRADIACACIAAgBIgCgBIAAAAg');
        this.shape_43.setTransform(2.1,-77.6);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#FF0000').s().p('AhbAuQgTggAggPIB3g8QAggQATAgQATAgggAPIh3A8QgLAGgJAAQgTAAgMgWg');
        this.shape_44.setTransform(3.5,-194.5);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#FF0000').s().p('AjFBmQgkgCAAgmQAAglAkACQBhAFBgggQBcgeBOg9QAcgWAbAaQAaAbgcAWQhcBGhoAlQhfAhhhAAIgcAAg');
        this.shape_45.setTransform(6.2,-174.9);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#FF0000').s().p('AjlBTQgjgHAKgkQAKgkAjAHQBhAUB0gdQBugbBYg7QAegUASAgQATAggeAUQhjBCh+AeQhKARhFAAQg0AAgwgKg');
        this.shape_46.setTransform(1.4,-153.2);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#FF0000').s().p('AkGBoQgjgFAAgmQAAglAjAFQCCARCCghQCFghBkhPQAcgWAbAaQAaAagcAXQhxBYiRApQhiAbhiAAQguAAgugGg');
        this.shape_47.setTransform(0.7,-135.1);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('AjUAzQAAglAkgBQBWgBBWgZQBLgXBUgtQAfgRATAgQATAgggARQi+BmiyADIAAAAQgkAAAAglg');
        this.shape_48.setTransform(-1.9,-111.5);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FFFFFF').s().p('AhgWyQmcgBisgZQixgZh1ggQhugdgQgYQACg4AFhKQAKiTAShYQgvhMgphTQhTimAfgjQAggiBfAuQAvAXApAeQBpk7AZhaQAThDA1h8QA0h6AphMQBNiWAwhXQBZihAfADQAgADAvAlQAYASARARQDSoIAZhBQAyiIAohgQBKi5AZgDQAagDAQBzQAIA/AKB4QAKBaBaKKIAWgoQAagnAWAJQAUAJBtCoQCDDKAUAaQAnAzBnC2IB2DTQCiD+AmBlQAjBbA3C2QA8DGAAAiQAAATg9AjQg9AjhhAjQjuBXjaAOQj3AQlZAAIgrAAg');
        this.shape_49.setTransform(3.7,-86.2);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_50.setTransform(95.4,183.7);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_51.setTransform(107.7,87.8);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_52.setTransform(122.1,137.5);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgTAAQgJAAgLgGg');
        this.shape_53.setTransform(133.4,94.9);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_54.setTransform(145.1,124.8);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#FF0000').s().p('AAUBsQgOgQhtiIQgcgjgEg4IACgxQCpALBDC0QAiBagBBYQgtAAhHhNg');
        this.shape_55.setTransform(163.7,93.2);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAlgkAAQgsAAglAZQgLAIgKAAQgQAAgLgVg');
        this.shape_56.setTransform(-171.8,201.3);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_57.setTransform(-6.8,253.7);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#FF0000').s().p('AhMAfQAAgkAkgBQARAAAMgIIAGgFIACgCIABgDQADgGACgJQAKgjAkAKQAkAKgKAjQgLAogfAXQgfAXgqAAIgCAAQgiAAAAgkg');
        this.shape_58.setTransform(123.3,228.7);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_59.setTransform(-49.8,234.7);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_60.setTransform(-129.2,229.3);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_61.setTransform(-54,198.1);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgvAAQhMAAhGgMg');
        this.shape_62.setTransform(25.2,203.3);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_63.setTransform(64.9,206.2);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_64.setTransform(-29.3,208.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_65.setTransform(139,58.7);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_66.setTransform(125,123.6);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_67.setTransform(-102.2,120.3);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_68.setTransform(73.6,51.5);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#000000').s().p('AI2DiQjlgPiGgMQjIgTiggbQh2gUg5gNQhhgYhGgjQhmgzAAhMQAAg2A9gtQAlgcBGgdQAigOAJAlQAKAkghANQgsATgWAMQgpAZgFAaQgEATAUARQALALAYAMQAsAXBGATQCEAlDFAbQDaAeF7AYQAkADAAAlQABAkggAAIgFgBg');
        this.shape_69.setTransform(-68.3,51.7);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_70.setTransform(49.6,147);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_71.setTransform(-30.3,96.5);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBagCBwA/QBtA8BIBXQBOBbgGBMQgIB3hhAWQgVAFgYAAQhHAAhagqg');
        this.shape_72.setTransform(-43.4,171.6);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQglBEhCAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_73.setTransform(103.3,85.4);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAZgDArQg8A4h4AiQhoAfhkAAIgcgBg');
        this.shape_74.setTransform(40.3,200.3);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_75.setTransform(41.3,212);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_76.setTransform(136.5,154.6);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_77.setTransform(-105.8,109.8);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f('#FFFFFF').s().p('AnDJAQhlgmCjhpQA0gjBXgtQBXgsAWgNQBLgpB1iEQCAiPAxhxQAvhrhGhEQg5g3hQAAQgyAAiVBBIiMBBIgoi0QA2g6BZgxQCzhjCzAtQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQg0AchOA1QhXA6gcAQQhSAwhHAAQglAAgigNg');
        this.shape_78.setTransform(128.4,129.1);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f('#FFFFFF').s().p('AhXPdQizgMimgiQidgghYgnQiOg/gehvQgJgjACgkIAFgdQkXkbh+oAQgvi+gPi0QgOicAPhHQAUhoDHg0QBjgaBfgFQChAMELAFQIUAKINghQINgiCgBrQAyAhAIAsQAEAVgHAPIgTDXQgZD6gdCvQgdCuioFEQhUChhPB/IgBAWQgFAdgWAgQhGBojRBkQiwBUkxAAQhZAAhigHg');
        this.shape_79.setTransform(6.1,125.9);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-257.3,360.9,514.8);


    (lib.Анимация5 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('AADAsQAAgVgDgNIgCgGIABABIgBgCIAAABIgBgCIgDgEIAAgBIgBgBIgDAAIAAAAIgFAAIgOAEQgOAFgOgIQgOgIgEgPQgEgPAIgNQAHgMAPgFQAZgKAYADQAbAEARARQAhAhABBEQAAAkglAAQgmAAAAgkg');
        this.shape.setTransform(120.3,34.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('AhSBSQgOgIgEgOQgEgQAIgNQAHgLAPgGQBBgaAwhAQAJgLARAAQARABAKAKQALALgBAPQAAAOgKANQg4BHhaAmQgFACgFAAQgJAAgJgGg');
        this.shape_1.setTransform(101.8,43.8);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AClBCQgXglgTgOQgbgVgiAJIgvARQgbAKgTACQg0AHgzgLQg6gOggggQgZgaAagbQAagbAZAbQAmAoBDgJIBtgcQA9gPAxAdQApAZAlA6QATAfggASQgMAHgKAAQgSAAgMgTg');
        this.shape_2.setTransform(-86.5,46);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AA9AqQg9ghg8AhQggASgTggQgSggAfgRQAvgbAzAAQAzAAAwAbQAgARgUAgQgMAVgRAAQgKAAgLgHg');
        this.shape_3.setTransform(36.3,53.8);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('ACHBJQg5hJhJgKQhPgMgzBNQgUAegggSQghgTAUgeQBHhrB1AFQBuAEBQBkQAWAcgaAbQgLALgNAAIgBAAQgOAAgKgNg');
        this.shape_4.setTransform(-18.9,52.1);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('AgcA+QgNgIgEgOQgDgOAHgPIAHgYQAFgTAIgQQAGgNAPgEQAQgEANAIQANAIAEAPQADAOgHAOIgHAYQgEAPgJAUQgGAOgQADIgKABQgJAAgJgFg');
        this.shape_5.setTransform(-135,-121.6);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#000000').s().p('Ag2A0QABg4AmhBQARgfAgATQAhATgTAfQgSAfgGASQgEAPAAATQAAAlgkAAQgmAAAAglg');
        this.shape_6.setTransform(-128,-94.1);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#000000').s().p('Ag1A3QABgmAKggQALgmAWgYQAYgbAaAbQAaAagYAbQgIAJgIAdQgGAXAAASQgBAkgkAAQgmAAABgkg');
        this.shape_7.setTransform(-122,-64.7);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#000000').s().p('Ag1A8QACg6AkhMQAPghAgATQAhATgQAgQgbA4gBApQgBAkglAAQglAAABgkg');
        this.shape_8.setTransform(-116,-42.2);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#FF0000').s().p('AgrBFQgLgMAAgOQABgPAHgTIAOgfIASgmQAHgNAPgEQAQgEANAHQANAIAEAPQADAPgHANIgXAwIgDAHIgDALIgBADQgCANgJAKQgKALgPAAQgPAAgMgLg');
        this.shape_9.setTransform(-155.5,-129);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#FF0000').s().p('AAbAzQghgKgTgNIgKgGIgCAAQgkgBAAgkQAAglAkAAQAbABARAMQARAMAXAHQAjAKgKAjQgIAcgXAAQgGAAgIgCgAgjAWIABAAIgCgBg');
        this.shape_10.setTransform(-117.9,-150.2);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#FF0000').s().p('AgtArQgPgDgIgOQgIgMAEgPQAEgQANgHQAqgUAzAAQAkAAAAAmQAAAkgkAAQgUAAgJABQgLACgPAHQgJAFgKAAIgJgCg');
        this.shape_11.setTransform(-167,-151);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#FF0000').s().p('AAbBOQgdgOgdgnIgRgYIgJgJIgBAAIgEgDQgggSATggQATggAgARQATALAYAjQAZAhAVAKQANAHAEAQQADAQgHAMQgIAOgPAEIgIABQgKAAgKgFg');
        this.shape_12.setTransform(-161.8,-175.8);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#FF0000').s().p('AgYBnQgQgEgGgNQgHgNADgQQACgQAOgtQAMgnAAgYQABgkAlAAQAlAAgBAkQgBAcgMAsQgPA2gDASQgCAPgQAIQgIAEgKAAIgJgBg');
        this.shape_13.setTransform(-133.2,-180.9);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#000000').s().p('AgjAkQgLgLAAgPQAAgPALgKQAEgEAFgDIALgNQAKgLAPAAQAQAAALALQALALgBAPQAAAOgKALIgCACIAEgFIgGAHIACgCIgKAPQgOANgUABIgCAAQgOAAgKgLg');
        this.shape_14.setTransform(-142,-152.8);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#FF0000').s().p('ABtJ2QhHkMhPlfIiLpsQgIgjAkgKQAkgKAIAjICKJuQBRFgBGEJQAJAjgkAKQgHACgHAAQgYAAgHgbg');
        this.shape_15.setTransform(-124.2,-77.2);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('Ah0C2QgggTARgfQBfipBuiKQALgNAPAAQANABAKALQAbAagXAcQhjB6heCpQgLAUgRAAQgKAAgMgHg');
        this.shape_16.setTransform(24.2,-121.3);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('ABTCyQhDh0gggxQg8hZhAg5QgbgYAbgaQAagbAbAYQBDA8A/BeQAkA1BFB4QASAfggATQgMAHgKAAQgRAAgMgUg');
        this.shape_17.setTransform(45.7,-120.2);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('ABnECQh3ktiWisQgYgbAagbQAbgaAYAbQBaBnBLCKQBAByA7CXQANAhgkAKQgIACgHAAQgYAAgKgZg');
        this.shape_18.setTransform(77.4,-72);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('AAcBZQgRgggigmIg+g+QgagZAbgaQAagbAaAaQA3A4AMANQAmApATAkQASAgghATQgLAHgKAAQgRAAgLgUg');
        this.shape_19.setTransform(-33.7,-133.2);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('AiNDnQgggSAPghQArhYBMhzQBYiBAqhDQAUgeAgATQAhASgUAfQgrBChYCBQhMB0gqBYQgKAUgQAAQgKAAgMgHg');
        this.shape_20.setTransform(-52.1,-120.1);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('AAICYQgihPgTg+QgXhLgIhIQgDgkAlAAQAmABADAjQAGA+AUBDQARA0AfBFQAOAhggATQgMAHgJAAQgRAAgJgVg');
        this.shape_21.setTransform(-104.8,-10.5);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AAzA9QhBgshBgMQgjgGAKgkQAKgkAjAHQBJANBLAyQAeATgTAgQgLAUgQAAQgKAAgMgHg');
        this.shape_22.setTransform(-97.7,-23.1);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#000000').s().p('AhxEsQgkgJAIgkQAhiOA1iHQA3iNBIh4QATgeAgASQAgATgTAfQhGB0g1CHQgyCDggCKQgGAcgXAAQgHAAgIgDg');
        this.shape_23.setTransform(-75.1,-73.7);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#FF0000').s().p('AgDBLQADgkgPgfQgOgagdgbQgagZAagaQAbgaAaAYQBUBRgIBcQgDAjglABQglAAADgkg');
        this.shape_24.setTransform(6.1,-236.3);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#FF0000').s().p('AgSCbQgkgKALgjQAghpghhyQgJgjAkgKQAjgKAJAjQAmCGglB9QgIAbgXAAQgGAAgJgCg');
        this.shape_25.setTransform(11.2,-241.9);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#FF0000').s().p('AhGBlQgTggAdgWQBBgxgChRQAAgkAlAAQAlAAABAkQACB0hmBPQgMAIgKAAQgPAAgLgTg');
        this.shape_26.setTransform(17.7,-236.9);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_27.setTransform(95.4,183.9);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_28.setTransform(107.7,88);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_29.setTransform(122.1,137.6);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgSAAQgKAAgLgGg');
        this.shape_30.setTransform(133.4,95);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_31.setTransform(145.1,124.9);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#FF0000').s().p('AAUBsQgOgPhtiJQgcgjgEg4IACgxQCpALBDC0QAiBagBBYQgtAAhHhNg');
        this.shape_32.setTransform(163.7,93.4);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAlgkAAQgsAAglAZQgLAIgKAAQgQAAgLgVg');
        this.shape_33.setTransform(-171.8,201.5);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_34.setTransform(-6.8,253.8);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#FF0000').s().p('AhMAeQAAgkAkgBQARAAAMgHIAGgGIACgCIABgCQADgHACgIQAKgjAkAKQAkAKgKAjQgLAngfAXQgfAXgqABIgBAAQgjAAAAglg');
        this.shape_35.setTransform(123.3,228.9);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_36.setTransform(-49.8,234.9);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_37.setTransform(-129.2,229.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_38.setTransform(-54,198.3);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgvAAQhMAAhGgMg');
        this.shape_39.setTransform(25.2,203.5);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_40.setTransform(64.9,206.3);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_41.setTransform(-29.3,209.1);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_42.setTransform(139,58.8);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_43.setTransform(125,123.8);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_44.setTransform(-102.2,120.5);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_45.setTransform(73.6,51.7);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#000000').s().p('AI2DiQjlgPiGgMQjIgTiggbQh2gUg5gNQhhgYhGgjQhmgzAAhMQAAg2A9gtQAlgcBGgdQAigOAJAlQAKAkghANQgsATgWAMQgpAZgFAaQgEATAUARQALALAYAMQAsAXBGATQCEAlDFAbQDaAeF7AYQAkADAAAlQABAkggAAIgFgBg');
        this.shape_46.setTransform(-68.3,51.8);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#000000').s().p('AiIGTIANh+QAHhIAKg1QAwkJB3krQANghAlAJQAkAKgOAhQh4EsgtD+QgKA0gHBFIgMB5QgEAkglAAQglAAADgkg');
        this.shape_47.setTransform(-96.4,1.1);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#000000').s().p('AApHQQhPmshKnfQgFgkAkgJQAkgKAFAjQBJHcBQGvQAHAkgkAKQgIACgGAAQgXAAgGgcg');
        this.shape_48.setTransform(22.3,-179.6);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#000000').s().p('AjgJRQgkgKAOghQBUjHCClnQCPmPBAiiQANghAlAKQAkAKgOAhQhAChiQGPQiBFnhUDIQgLAZgYAAQgHAAgIgCg');
        this.shape_49.setTransform(-10.7,-172.9);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#000000').s().p('ABkFtQg8lejNleQgSgfAggTQAggSASAfQDTFoA+FmQAGAjgkAKQgIACgGAAQgXAAgFgcg');
        this.shape_50.setTransform(110.7,-3.8);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#000000').s().p('AhLA2Qg9gMgVgGQgygPgXgXQgZgaAagbQAbgaAZAaQARASAvALIBIALQB+ATBugSQAkgHAKAjQAKAkgkAHQhEAMhEAAQhNAAhNgPg');
        this.shape_51.setTransform(-12.2,-104.3);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#E6E7E8').s().p('AkpBwQgSgUAAgeQAAgfASgWQAUgXAigDQEJgTC0hWQAfgPAeAMQAbAKAPAaQAPAagFAaQgGAdgfAOQh1A4iKAfQh4AciSAKIgHAAQgdAAgSgTg');
        this.shape_52.setTransform(91.9,27);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#E6E7E8').s().p('Ak8B9QgSgUAAgeQAAgfASgVQAUgXAigBQCkgGCRgsQBIgVAigRQAcgOANgJIABgBQAMgSAVgJQAagLAeAIQAdAIAOAWQAPAZgMAgQgTA1hBAjQgmAWhPAZQi6A9jNAHIgEAAQggAAgSgWg');
        this.shape_53.setTransform(80.8,-4.5);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_54.setTransform(49.6,147.2);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_55.setTransform(-30.3,96.7);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBbgCBvA/QBtA8BIBXQBOBcgGBMQgIB2hhAXQgVAEgYAAQhHAAhagqg');
        this.shape_56.setTransform(-43.4,171.8);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQglBEhCAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_57.setTransform(103.3,85.5);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAagEAqQg8A4h3AiQhoAfhkAAIgcgBg');
        this.shape_58.setTransform(40.3,200.5);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_59.setTransform(41.3,212.1);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_60.setTransform(136.5,154.8);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_61.setTransform(-105.8,109.9);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#FFFFFF').s().p('AnDJAQhlgmCjhpQA0gjBXgtQBXgsAWgNQBLgpB1iEQCAiPAxhxQAvhrhGhEQg5g3hQAAQgyAAiVBBIiMBBIgoi0QA2g6BZgxQCzhjCzAtQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQg0AchOA1QhXA6gcAQQhSAwhHAAQglAAgigNg');
        this.shape_62.setTransform(128.4,129.2);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#000000').s().p('AgGAuQgggGgcgTQgNgKgEgMQgFgPAIgOQAIgMAPgFQAQgEAMAIQAjAZAhgJQAjgIAKAjQAKAkgjAIQgTAFgTAAQgOAAgNgDg');
        this.shape_63.setTransform(-39.2,-32.7);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#000000').s().p('AgEAyQgpgEgegfQgZgZAagaQAbgaAZAaQAXAZAfgYQAMgIARAFQAPAEAIANQAIANgFAOQgEANgNAKQgdAVgiAAIgLAAg');
        this.shape_64.setTransform(-37.5,-65.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#000000').s().p('AgXBGQgjgIgWgWQgRgTgRgjQgPghAggTQAggTAPAhIAQAhQALAPATADQAgAEAcgeQAYgbAaAaQAbAbgYAaQgaAcgjANQgWAIgWAAQgNAAgOgEg');
        this.shape_65.setTransform(-47.7,-48.1);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#000000').s().p('AhgBrQgzgngJg+QgIg1AZhAQANghAkAJQAkAKgNAiQgrBrA7AfQAkATAqgFQApgFAjgYQAfgUASAhQATAggeAUQg0AihCAGIgUABQg7AAgogfg');
        this.shape_66.setTransform(-26.6,-54.7);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#000000').s().p('AjGBEQgjgFAAglQAAgkAjAEQBHAIAiACQA7ADAsgJQBMgPBbg0QAfgTATAhQATAgggASQg1AegpARQg2AXgwAHQglAFgtAAQg8AAhKgJg');
        this.shape_67.setTransform(37.8,-49.2);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#000000').s().p('AjHBkQgjgEgBglQAAgmAkAEQDCAWCtiLQAcgWAbAaQAaAbgcAWQhiBOhjAjQhRAdhVAAQgcAAgdgDg');
        this.shape_68.setTransform(34.3,-67.3);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#000000').s().p('ACCBUQhJgChIgYQhMgag5gqQgcgVATghQASggAdAWQAyAmA/AWQBAAWA/ACQAkABAAAlQAAAkgiAAIgCAAg');
        this.shape_69.setTransform(-92.7,-60.9);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#000000').s().p('ABvBxQhQgChDgwQg9gsgrhKQgSgfAggTQAggSASAfQAiA6ArAhQAzAmA7ABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_70.setTransform(-87.8,-76.8);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#000000').s().p('AAJA/IgSgEQgNAEgOgIQgOgIgEgPQgDgMAFgLQgDgHAAgGQAAgQALgKIAHgGQALgTASgGQALgDANAEQAMAEAIAKQAKANAEAHQAHAQgBAPQgBAbgIAKQgPAVgTAAIgEAAg');
        this.shape_71.setTransform(-57.1,-82.4);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#000000').s().p('AALA/IgWgFQgOAFgOgJQgOgIgEgOQgDgMAFgMQgDgGAAgHQAAgPALgLQAEgEAGgDQAQgVANgCQAdgFARAUQAJAJAFAKQAIAOgBAQQgBAbgIALQgPAWgVAAIgDAAgAgPAFIABAAIAAAAIAAAAgAgRADIACACIAAgBIAAAAIgBgBIgBAAg');
        this.shape_72.setTransform(-5.5,-77.4);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FF0000').s().p('AiHBUQgSggAfgSQCHhNBKg0QAdgVATAgQATAhgdAUQhKAziHBOQgMAHgJAAQgSAAgMgVg');
        this.shape_73.setTransform(7.7,-197.8);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f('#FF0000').s().p('AipBGQgKgkAjgIQCTgiBxhSQAdgVATAgQATAhgdAVQh3BVifAmIgMABQgYAAgJgdg');
        this.shape_74.setTransform(1.8,-175.4);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f('#FF0000').s().p('AjYBqQgjgEAAgmQgBglAkAEQBrAOBtgjQBughBNhKQAagZAaAbQAbAagaAZQhXBTh9ApQhbAehYAAQghAAgggEg');
        this.shape_75.setTransform(1,-157.1);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.f('#FF0000').s().p('AlPB+QAAgmAkgEQCXgRCZg/QCVg8B/heQAdgVATAgQASAhgcAVQiGBhigBAQihBAijATIgHAAQgdAAAAghg');
        this.shape_76.setTransform(1.3,-137.4);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.f('#FF0000').s().p('AjdA7QABglAjgDQDSgSCMhWQAfgTATAgQATAggfATQhUAzhnAdQhZAZhwAJIgFAAQgfAAAAgig');
        this.shape_77.setTransform(-6.1,-111.8);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.f('#FFFFFF').s().p('AhXPdQiygMimgiQiegghYgnQiOg/gehvQgJgjADgkIAFgdQkXkbh/oAQgvi9gPi1QgNibAOhIQAVhoDGg0QBjgaBfgFQCiAMELAFQIUAKINghQINgiCgBrQAyAhAHAsQAEAVgGAPIgTDXQgZD6gdCvQgeCuioFEQhUChhOB/IgBAWQgGAdgWAgQhGBojQBkQiwBUkyAAQhZAAhigHg');
        this.shape_78.setTransform(6.1,126);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.f('#FFFFFF').s().p('ABlXWQl8gFmPgyQlUgrh+gqQABhDAHhmQANjLAZivQgZgtgTg1QgmhrAdgqQAdgpA3AUQAcAKAVASIBQjVQAMhKAZhfQAyi9BDhsIC0kzQBpiyAZgIQAZgJAtA8QAWAeASAgQDBnnAbhJQAQguBAjAIA8i2IAbgSQAegLAIAdQAJAdBQHAIBOG7IAdABQAhAGASAUQAdAhBGBmQBQB1A2BEQA8BLB1DbQB1DcAUAZQApAzBFCKQBICNAYBTQA1CyARBPQArDAgNB8Qh9AtjDAsQl1BVlQAAIgfAAg');
        this.shape_79.setTransform(7.5,-78.2);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-257.5,360.9,515.1);


    (lib.Анимация4 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('ADnBgQg8gIhCgdQgcgNhYguQg5gcgxAPQgpAMg2AwQgaAYgagaQgbgbAbgXQA9g5A1gUQBEgaBDAZQAXAJAiAUIA5AfQBKAkBOALQAkAFgKAkQgJAggdAAIgIgBg');
        this.shape.setTransform(-28.2,50.5);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('Aj3BSQAAglAkgCQA4gCBFg2QBghLARgJQBAghA4AVQA0ATApA/QAUAdggATQghATgTgeQgUgegRgNQgZgTgbAKQgYAIgbASQgNAJgfAZQhAAxgeASQg4Afg1ACIgCAAQgiAAAAgkg');
        this.shape_1.setTransform(95.1,42.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AhtClQgkgKAOghQAdhKA5hKQAtg7BHhFQAagZAaAaQAaAbgaAZQhCBAgmAwQg1BDgZBAQgLAZgYAAQgHAAgIgCg');
        this.shape_2.setTransform(-63.4,-95.6);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('ACzEsQgsgygvhNIhPiJQgzhXgzg/Qg+hLhGgzQgdgUASghQATggAdAVQCPBnBoCxQAcAtA1BeQAwBSAsAyQAYAbgaAbQgNANgMAAQgNAAgNgOg');
        this.shape_3.setTransform(69.7,-77.2);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('AhIC+QgkgKAGgkQAki9BhiBQAWgcAgATQAgATgWAcQgyBCgfBMQgcBGgQBYQgFAcgXAAQgHAAgHgCg');
        this.shape_4.setTransform(-84.4,-59.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#FF0000').s().p('AAeBcQgPgIgCgPQgFgggQgXQgTgYgdgMQgPgGgIgLQgIgNAEgQQAEgOAOgIQAPgKANAGQAxAUAfAkQAjAmAHAxQACARgGAMQgHANgPAEIgKACQgKAAgJgFg');
        this.shape_5.setTransform(4.5,-237.3);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#FF0000').s().p('AhPBoQgTggAdgVQA9gqAWhpQAIgjAkAKQAkAKgIAjQgbCAhaBAQgMAIgKAAQgPAAgLgUg');
        this.shape_6.setTransform(15.2,-237.5);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#FF0000').s().p('AgGCWQgggTAOghQAVgwgEg1QgGg8gmgeQgcgWAagbQAbgaAbAWQA5AtAHBZQAGBLgfBJQgJAVgQAAQgKAAgLgHg');
        this.shape_7.setTransform(8.4,-240.7);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#000000').s().p('ABcA9QgYglgSgOQgcgVghAKIgzARQgfAKgWADQgjAFgKgjQgKgkAjgFQAUgDA2gTQAvgRAbABQAvACAlAfQAdAYAeAvQAUAfggASQgNAIgKAAQgRAAgMgUg');
        this.shape_8.setTransform(-89.1,43.4);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#000000').s().p('AA9AqQg9ghg8AhQggASgTggQgSggAfgRQAwgbAyAAQAzAAAwAbQAfARgSAgQgMAVgSAAQgKAAgLgHg');
        this.shape_9.setTransform(26.3,53.6);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('Ah3DJQgggTAPghQBLiXCCi7QAVgdAgATQAgASgUAeQiEC6hJCYQgKAVgRAAQgJAAgMgHg');
        this.shape_10.setTransform(14.4,-120.3);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('AA/CrQhNjChvhsQgagZAbgbQAagaAaAZQBEBCA1BbQAsBLAqBoQANAhgkAKQgIACgHAAQgYAAgKgag');
        this.shape_11.setTransform(34.4,-121.5);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#000000').s().p('AAFA/QgSgkgwgtQgbgYAbgbQAagaAaAZQA2AyAYAtQARAgggATQgMAHgKAAQgQAAgLgUg');
        this.shape_12.setTransform(-30.1,-136.9);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#000000').s().p('AhzDFQgggTAPggQA2hxAlg+QA3hhA7hAQAZgaAaAaQAbAbgZAaQg4A8g1BcQgfA4g1BxQgKAUgRAAQgJAAgMgHg');
        this.shape_13.setTransform(-43.7,-124.9);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_14.setTransform(95.4,182.7);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_15.setTransform(107.7,86.8);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_16.setTransform(122.1,136.5);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgSAAQgKAAgLgGg');
        this.shape_17.setTransform(133.4,93.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_18.setTransform(145.1,123.8);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#FF0000').s().p('AAUBsQgOgQhtiIQgcgjgEg4IACgxQCpAKBDC0QAiBbgBBYQgtAAhHhNg');
        this.shape_19.setTransform(163.7,92.2);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAlgkAAQgsAAglAZQgLAIgKAAQgQAAgLgVg');
        this.shape_20.setTransform(-171.8,200.3);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_21.setTransform(-6.8,252.7);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#FF0000').s().p('AhMAfQAAgkAkgBQARAAAMgIIAGgFIACgCIABgDQADgGACgJQAKgjAkAKQAkAKgKAjQgLAogfAXQgfAXgqAAIgCAAQgiAAAAgkg');
        this.shape_22.setTransform(123.3,227.7);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_23.setTransform(-49.8,233.7);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_24.setTransform(-129.2,228.3);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_25.setTransform(-54,197.1);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgvAAQhMAAhGgMg');
        this.shape_26.setTransform(25.2,202.3);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_27.setTransform(64.9,205.2);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_28.setTransform(-29.3,207.9);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_29.setTransform(139,57.7);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_30.setTransform(125,122.6);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_31.setTransform(-102.2,119.3);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_32.setTransform(73.6,50.5);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#000000').s().p('AI2DiQjlgPiGgMQjIgTiggbQh2gUg5gNQhhgYhGgjQhmgzAAhMQAAg2A9gtQAlgcBGgdQAigOAJAlQAKAkghANQgsATgWAMQgpAZgFAaQgEATAUARQALALAYAMQAsAXBGATQCEAlDFAbQDaAeF7AYQAkADAAAlQABAkggAAIgFgBg');
        this.shape_33.setTransform(-68.3,50.7);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#000000').s().p('AhhFzQAEgvAChiQABhfAFgyQAbkPBYjDQAPggAgATQAgATgPAgQhWC+gVEJQgDAqgBBZQgCBXgEAtQgDAjglABQgmAAAEgkg');
        this.shape_34.setTransform(-100.3,3.2);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#000000').s().p('AAzHQQgMg+hOmHQg4kTgcizQgFgkAkgJQAkgKAFAjQAcCzA3EUIBbHEQAHAkgkAJQgIACgGAAQgYAAgFgbg');
        this.shape_35.setTransform(21.4,-180.8);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#000000').s().p('AjVJRQgkgJANgiQAsh6Cbm2QB5laBRjVQANghAkAKQAjAKgMAhQhRDVh6FZIjGIwQgKAbgXAAQgHAAgJgDg');
        this.shape_36.setTransform(-11.6,-174);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#000000').s().p('ABkFtQg8lejNleQgSgfAggTQAggSASAfQDTFoA+FmQAGAjgkAKQgIACgGAAQgXAAgFgcg');
        this.shape_37.setTransform(110.7,-4.9);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('AhKA2QgrgKgXgKQgmgPgUgVQgYgaAagbQAagaAZAbQAPAQAhALQATAGAkAHQBiASBsgSQAjgGAKAjQAKAkgkAGQhEAMg9AAQhEAAg8gPg');
        this.shape_38.setTransform(-15.4,-105.5);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#E6E7E8').s().p('AkrB7QgSgUAAgeQAAgeASgXQAUgXAigDQB/gJBqgcQB0ggBmg4QAegRAdALQAaAJAQAaQAPAbgFAaQgGAegeARQh3BBiIAkQh7AhiUAKIgHAAQgdAAgSgTg');
        this.shape_39.setTransform(91.5,26.8);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#E6E7E8').s().p('AkpCGQgSgTAAgfQAAgeASgWQAUgYAigCQCSgICDgqQBDgUAlgUIAcgRQAJgGADgIQAPgfAcgGQAagGAbAPQAaAQAKAbQAMAdgPAfQgWAvhDAkQgpAWhLAZQijA3i2AKIgFAAQgeAAgTgVg');
        this.shape_40.setTransform(80.3,-5.5);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_41.setTransform(49.6,146);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_42.setTransform(-30.3,95.5);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBagCBwA/QBtA8BIBXQBOBbgGBMQgIB3hhAWQgVAFgYAAQhHAAhagqg');
        this.shape_43.setTransform(-43.4,170.6);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQglBEhCAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_44.setTransform(103.3,84.4);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAZgDArQg8A4h4AiQhoAfhkAAIgcgBg');
        this.shape_45.setTransform(40.3,199.3);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_46.setTransform(41.3,211);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_47.setTransform(136.5,153.6);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_48.setTransform(-105.8,108.8);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FFFFFF').s().p('AnDJAQhlgmCjhpQA0gjBXgtQBXgsAWgNQBLgpB1iEQCAiPAxhxQAvhrhGhEQg5g3hQAAQgyAAiVBBIiMBBIgoi0QA2g6BZgxQCzhjCzAtQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQg0AchOA1QhXA6gcAQQhSAwhHAAQglAAgigNg');
        this.shape_49.setTransform(128.4,128.1);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#000000').s().p('AgmAyQgWgIgLgOIgLgQIgDgDQgKgLAAgOQgBgQALgLQALgKAQgBQARAAAJALIARAXIgCgDIADADIgBAAIABABIABABIgBgBIAAgBIAAABIABAAIANgBQAVgEAdgKQAigLAKAkQAKAjgiALQgkAMgSADQgOADgKAAQgRAAgNgFg');
        this.shape_50.setTransform(-58.6,-30.7);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#000000').s().p('AgNA0QgmgKgXgnQgTgeAggTQAggTATAfQAHAMAFACQAHADAJgHQANgJANgBQAQgBAKALQALALABAPQAAARgMAJQggAaggAAQgJAAgKgCg');
        this.shape_51.setTransform(-55.2,-67.4);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#000000').s().p('AgZBEQgdgJgTgcQgRgYgHggQgIgjAkgKQAkgKAIAjQAEARADAIQAHAOAKAFQAKAEAagfQAYgcAaAbQAbAagYAbQgYAcgbAMQgSAJgTAAQgMAAgNgFg');
        this.shape_52.setTransform(-65.2,-47.8);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#000000').s().p('AAJCZQhCgBgngrQgqgugBhDQgBg4AchDQAOggAkAJQAkAKgOAhQgUAwgEAfQgIAvAWAeQAXAeApgBQAkgBAigWQAegUATAgQASAhgeAUQgxAhg9AAIgCAAg');
        this.shape_53.setTransform(-47.3,-54.4);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#000000').s().p('AiwA/QghgNAKgkQAKgjAhANQBDAZBWgTQBQgTA7gxQAbgXAbAbQAaAagcAXQhLA9hiAZQgvAMgsAAQg1AAgvgSg');
        this.shape_54.setTransform(12.7,-50);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#000000').s().p('AilBnQgjgEgBgmQAAglAkAEQBdALBKgeQBJgcA+hIQAYgcAaAbQAbAagYAbQh+CRiuAAQgbAAgcgDg');
        this.shape_55.setTransform(9.5,-68.7);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#000000').s().p('AAXA6QgagBgTgGQgjgKgkgiQgagYAagaQAagbAbAZQAWAVAdAGQAPACAtAAQAkAAAAAlQAAAlgkAAIgwAAg');
        this.shape_56.setTransform(-95.6,-60.5);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#000000').s().p('AArBVQgggNgPgIQgagOgRgRQgZgagPgvQgMgiAkgKQAkgKAMAiQAKAeAKALQARARApAPQAPAGAHAMQAIANgEAPQgEAOgNAJQgJAFgJAAQgFAAgGgCg');
        this.shape_57.setTransform(-91.4,-76.4);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#000000').s().p('AgRA+QgJgDgGgGQgHgFgFgIQgIgNAEgQQADgJAGgGQgBgHACgHQADgLAIgGIADgFQALgSAPgFQAJgDALADQALACAGAIQALAOAAAPIAAAKIgEAWIgNAiQgKASgQAEIgJABQgIAAgHgDgAgbgjIABgBIABgBg');
        this.shape_58.setTransform(-69.5,-83.8);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#000000').s().p('AgDA+IgHgDQgHABgHgCQgPgDgIgOQgHgNAEgPIABgFQgDgGAAgGQAAgQALgLIADgCIACgFQAIgPALgEQARgIANAEQANAFAGAHQALANAIAWQACAGgBAQIgCATQgEAQgJAJQgOALgPAAQgGAAgFgBg');
        this.shape_59.setTransform(-21.9,-77.8);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FFFFFF').s().p('AhXPdQizgMimgiQidgghYgnQiOg/gehvQgJgjACgkIAFgdQkXkbh+oAQgvi+gPi0QgOicAPhHQAUhoDHg0QBjgaBfgFQChAMELAFQIUAKINghQINgiCgBrQAyAhAIAsQAEAVgHAPIgTDXQgZD6gdCvQgdCuioFEQhUChhPB/IgBAWQgFAdgWAgQhGBojRBkQiwBUkxAAQhZAAhigHg');
        this.shape_60.setTransform(6.1,124.9);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#000000').s().p('AgcA+QgNgIgEgPQgDgOAHgOIAHgYQAEgPAJgUQAGgNAPgEQAQgEANAIQANAIAEAOQADAOgHAPIgHAYQgFATgIAQQgGANgQAEIgJABQgKAAgJgFg');
        this.shape_61.setTransform(-107.1,-121.8);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#000000').s().p('Ag2A1QAAg5AmhBQASgfAgATQAgATgSAfQgUAhgEAQQgEAPAAAUQgBAkgkAAQglAAAAgkg');
        this.shape_62.setTransform(-100,-94.4);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#FF0000').s().p('AgrBFQgMgMABgOQABgPAHgTIAOgfIASgmQAHgNAPgEQAQgEAMAHQAOAIADAPQAEAOgHAPIgXAvIgDAHIgEALIAAADIAAAAIgBADQgCALgIAJQgKALgQAAQgPAAgLgLg');
        this.shape_63.setTransform(-127.5,-129.2);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#FF0000').s().p('AAbAzQgigLgSgMIgIgGIAAAAIACABIgBgBIgCAAIgBAAIABAAIABAAIgEAAQgkAAAAglQAAglAkAAQAcABAPAMQARAMAYAHQAjAKgKAjQgIAcgXAAQgHAAgHgCg');
        this.shape_64.setTransform(-89.9,-150.4);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#FF0000').s().p('AguAsQgOgEgIgNQgIgNAEgPQAEgQANgHQApgUA0AAQAkAAAAAmQAAAkgkAAIgdABQgMACgOAHQgKAFgKAAIgJgBg');
        this.shape_65.setTransform(-139,-151.2);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#FF0000').s().p('AAbBOQgdgOgdgnIgRgYIgLgKIgDgCQgggRATghQATggAfARQAUALAYAjQAZAiAUAJQAOAHAEAQQADAQgHANQgIANgPAEIgIABQgKAAgKgFg');
        this.shape_66.setTransform(-133.8,-176);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#FF0000').s().p('AgYBnQgPgFgHgNQgHgMADgQQACgQAOguQAMgnABgXQABgkAkAAQAlAAgBAkQAAAcgNAsQgPA2gDASQgCAOgQAIQgIAFgKAAIgJgBg');
        this.shape_67.setTransform(-105.2,-181.1);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#000000').s().p('AgjAkQgLgLAAgPQAAgPALgKIAIgHIAMgNQAKgLAPAAQAQAAAKALQALAKAAAQQAAAOgLALIgMARQgNANgUABIgCAAQgOAAgKgLg');
        this.shape_68.setTransform(-114,-153);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FF0000').s().p('ABDHQQgxi5g6kMQhBkugiiZQgIgiAkgLQAkgJAIAiQAiCYBAEwQA7EKAxC6QAJAjgkAKQgIADgGAAQgYAAgHgcg');
        this.shape_69.setTransform(-100.4,-94);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FF0000').s().p('AiGAqQgKgkAjgFQBygRBMgxQAfgTASAgQATAhgeATQhVA2h7ATIgJAAQgcAAgIgfg');
        this.shape_70.setTransform(3.9,-199.1);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FF0000').s().p('AiqBYQgkgCAAglQAAgmAkACQCmALCahpQAegUATAgQATAhgeAUQhSA3hZAbQhMAXhOAAIghgBg');
        this.shape_71.setTransform(2,-177.5);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.f('#FF0000').s().p('AjnBbQgkgCAAglQgBgmAlACQCJAIBagQQB5gVBUhFQAcgWAaAaQAbAagcAXQhhBPiFAbQhNAQhlAAQglAAgogCg');
        this.shape_72.setTransform(2.2,-156.8);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.f('#FF0000').s().p('AkFBcQgkgCAAglQAAgmAkACQB9AGCEgcQB6gbB5g3QAhgPATAgQATAgghAPQj9B0jwAAIgtgBg');
        this.shape_73.setTransform(-2.9,-134.9);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.f('#FF0000').s().p('Aj0BCQgkgHAKgkQAKgjAjAHQB1ATBtgOQB0gRBjg3QAfgRATAgQATAhggARQhvA9iDASQg0AHg2AAQhJAAhMgNg');
        this.shape_74.setTransform(-9.5,-115.4);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.f('#FFFFFF').s().p('AC4W2QnDgElrgnQmuguhahMQAEhaAIhuQAPjeAShnQAThnAwiIIArh0QAShLAhhcQBCi3BOhUQAYgpAjgxQBGhjA4gpQAihSAphTQBTilAlAAQAmAAAZAeQAMAPAFAPIEdslIAfhGQAjhGAcAAQAcAAAOCMQAHBGABBGICMKyIA6B4QBDB7AoAWQAsAYBnB7QBYBoA3BTICJDNQBeCNAJATQAmBLAwBwQBPC4AZBoQAhCJAEBCQADBSgjAjQgVAWhIAfQhUAjhwAeQkhBOlIAAIgdgBg');
        this.shape_75.setTransform(8.1,-83.2);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-256.4,360.9,512.8);


    (lib.Анимация3 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('AjJBhQghgPATggQATggAhAOQA2AYA6gxQAggbA3g9QA5gvAzAZQAsAVAfBFQAPAfggATQghATgPghQgPgigGgJQgRgagQAKQgSALgVAXIgkAmQg0A0g0ARQgZAIgZAAQgkAAgkgQg');
        this.shape.setTransform(107.6,40);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('Ah1AfQAAgkAkgBQBTgBAygwQAagYAbAaQAaAagaAZQhJBEhxACIgCAAQgiAAAAglg');
        this.shape_1.setTransform(59.5,53.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AAGA+QgLgIgFgOIgCgEIgBgCIgDgHIgFgIIgCgCIgHgHIgCgBIACABIgDgCIABABIgLgHQgNgGgEgQQgEgPAHgNQAIgOAPgDQAPgEAOAHQAuAZAUA5QAFAOgJAOQgIAOgOAEIgKABQgKAAgJgFg');
        this.shape_2.setTransform(-101.2,43.5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AAQAmIABAAIgCAAIABAAIgDgBIgIgCIgLgBIgdgBQgkAAAAgkQAAgmAkAAQA3AAAhAPQAOAGADARQAEAPgIAMQgIAOgOADIgJABQgJAAgKgEg');
        this.shape_3.setTransform(-23.1,55.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('ABXBHQgjgNgugeQgwgggdgCQgkgBgnAnQgaAagagaQgagaAZgZQBdhfBgAtQAfAPA5AnQA0AcAugEQAkgEAAAmQAAAlgkADIgSABQgjAAgkgNg');
        this.shape_4.setTransform(-66.3,46.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('AAEA8QgZgCgRgXQgLgQgLgfQgFgOAJgOQAIgNAOgEQAPgFAOAIIAFAFIAHgFQAOgHAOAEQAPAEAHANQAHANgDAPQgMBIgpAAIgEAAg');
        this.shape_5.setTransform(-75.2,-66.3);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#000000').s().p('AhFC6QgkgKAKgjQAvioBLiMQASggAgATQAgATgRAgQhICCgrCgQgHAbgYAAQgHAAgIgCg');
        this.shape_6.setTransform(-90.7,-10.6);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#000000').s().p('AgPAuQgegdgBgkQgBgPAMgLQALgLAPAAQAPAAALALQAKALABAPIAAADIgBgCIABADIAAAAIAAAAIABACIABABIgCgCQABACABAAQABABAAABQAAAAAAgBQAAAAgBgBIABABIAGAGQAKAKAAAPQABAQgLAKQgLALgQAAQgOAAgLgLg');
        this.shape_7.setTransform(-84.7,-35.5);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#000000').s().p('AhNBdQgjgEgBglQAAgmAkAFQAnAFAkgeQAbgZATgqQAPggAhATQAgATgPAgQgaA4gxAlQguAkgwAAIgRgBg');
        this.shape_8.setTransform(-24.5,-67.1);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#000000').s().p('Ah5AkQgdgVATgfQASggAeAUQAkAZAtgKQAlgJAlgfQAcgWAbAaQAaAagcAXQg7Avg8ANQgSAEgTAAQgxAAgpgcg');
        this.shape_9.setTransform(-25.7,-45.9);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('AgiA1QgNgMACgOQACgTAPgrQAIgjAjAJIAAAAIAEABQAkAKgMAiQgIAogLAWQgJARgWABIgBAAIgBAAQgOAAgLgLg');
        this.shape_10.setTransform(-46.4,-75.9);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('Ah3AUQgbgYAbgaQAagaAbAYQAdAbAogCQAsgCARgjQAQghAgATQAgATgQAgQghBDhSAFIgNABQhFAAgygug');
        this.shape_11.setTransform(-67.3,-40.9);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#000000').s().p('AgrBfQgIg6AEgqQAFgyAXgvQAPghAgATQAgATgQAgQgPAhgEAkQgCAfAFApQAFAjgjAKQgIADgGAAQgXAAgEgdg');
        this.shape_12.setTransform(-80.9,-56.1);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#FF0000').s().p('AAhBQIgsg6QgdgjgegGQgjgHAKgkQAKgkAjAHQAqAJAiAnQAVAXAnAwQAZAagbAaQgNANgNAAQgNAAgMgNg');
        this.shape_13.setTransform(9.4,-234.9);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#FF0000').s().p('AAPB+QgRhNgNgmQgWhAgmgiQgagYAagbQAbgaAaAYQAsApAbBKQAEAKAiB5QAJAjgjAJQgIADgHAAQgXAAgIgbg');
        this.shape_14.setTransform(8.2,-241.1);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#FF0000').s().p('AgjCcQgggSARggQAyhggYh8QgHgkAjgKQAkgKAHAkQAfCchBB5QgLAUgPAAQgKAAgMgHg');
        this.shape_15.setTransform(18.2,-242);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('ABxCjQgyhchBhFQhKhLhWgoQghgPATggQATggAhAPQBeArBQBUQBJBLA3BkQARAgggATQgMAHgKAAQgRAAgLgUg');
        this.shape_16.setTransform(21.7,-117.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('AhPCxQghgSAQghQAHgOAziKQAjhdAqgzQALgNAOAAQAOABAKAKQAbAbgXAbQgoAvgfBZQgsB/gJATQgKAUgQAAQgKAAgLgHg');
        this.shape_17.setTransform(0.8,-118);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_18.setTransform(95.4,184.6);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_19.setTransform(107.7,88.7);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_20.setTransform(122.1,138.3);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgTAAQgJAAgLgGg');
        this.shape_21.setTransform(133.4,95.7);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_22.setTransform(145.1,125.6);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#FF0000').s().p('AAUBrQgOgPhtiIQgcgkgEg3IACgxQCpALBDCzQAiBbgBBYQgtAAhHhOg');
        this.shape_23.setTransform(163.7,94.1);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAlgkAAQgsAAglAZQgLAIgKAAQgQAAgLgVg');
        this.shape_24.setTransform(-171.8,202.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_25.setTransform(-6.8,254.5);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#FF0000').s().p('AhMAeQAAgkAkgBQARAAAMgHIAGgGIACgCIABgCQADgHACgIQAKgjAkAKQAkAKgKAjQgLAngfAXQgfAXgqABIgBAAQgjAAAAglg');
        this.shape_26.setTransform(123.3,229.6);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_27.setTransform(-49.8,235.6);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_28.setTransform(-129.2,230.1);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_29.setTransform(-54,199);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgwAAQhLAAhGgMg');
        this.shape_30.setTransform(25.2,204.2);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_31.setTransform(64.9,207);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_32.setTransform(-29.3,209.8);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_33.setTransform(139,59.5);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_34.setTransform(125,124.5);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_35.setTransform(-102.2,121.2);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_36.setTransform(73.6,52.4);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#000000').s().p('AI2DiQjmgPiFgMQjIgTihgbQh1gUg5gNQhigYhFgjQhmgzAAhMQgBg2A9gtQAmgcBGgdQAhgOAKAlQAKAkghANQgtATgVAMQgqAZgEAaQgEATATARQAMALAYAMQAsAXBGATQCDAlDGAbQDaAeF7AYQAkADAAAlQAAAkggAAIgEgBg');
        this.shape_37.setTransform(-68.2,52.5);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('AgzCCIANiFQAIhPAKg3QAHgkAjAKQAkAKgHAkQgJAygHBIIgMB9QgEAkgkAAQgmAAAEgkg');
        this.shape_38.setTransform(-102.2,28.3);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AAsIXQgkjVgtk4IhOoMQgFgjAkgKQAkgKAFAjIBNINQAuE4AkDUQAGAkgkAJQgIACgGAAQgXAAgFgbg');
        this.shape_39.setTransform(24.3,-174);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AjjJWQgkgJANgiQBOjECKlwQCQmEBFixQANghAkAKQAkAKgNAhQhFCwiRGEQiJFwhODFQgKAagYAAQgHAAgIgDg');
        this.shape_40.setTransform(-9.2,-174);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('AjVDXQgggTAUgeIBQh6QAxhHAogtQBth+CFgWQAkgGAKAkQAKAkgkAGQh3AThiB0QgjAogrBCIhIBuQgMATgRAAQgLAAgMgHg');
        this.shape_41.setTransform(-52.3,-87);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('AB9FPQgvjShgijQgeg0g3hSQhDhlgUggQgTgeAggTQAggTATAfIBfCPQA5BXAhA7QBdCnAtDKQAIAjgkAKQgIACgGAAQgYAAgGgcg');
        this.shape_42.setTransform(108.2,-0.1);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('ADQFKQgng5g5htQhFiEgZgpQh3jJiYg9QghgOAJgkQAKgkAiAOQCqBECADPQAeAxBICEQA/B1AqA+QAVAeghATQgMAHgLAAQgRAAgMgSg');
        this.shape_43.setTransform(62.7,-75.7);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#000000').s().p('ACIBOQhhgJhTgbQgvgOgOgGQgjgPgTgTQgZgaAagaQAbgaAZAaQANAOAgALQARAGAhAIQBSAXBBAGQAkAEAAAlQAAAhgdAAIgHAAg');
        this.shape_44.setTransform(-22.3,-103.6);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#E6E7E8').s().p('AmSCPQgSgUAAgeQAAgfASgWQAUgZAigCQC+gLCVgiQCygpCMhPQAegQAdAKQAaAJAQAbQAPAagFAbQgGAegeAQQidBXjGAuQimAnjTAOIgGAAQgeAAgSgUg');
        this.shape_45.setTransform(81.2,30.7);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#E6E7E8').s().p('AmpCGQgSgUAAgeQAAgfASgVQAUgYAigBQCxgGDzg2QBxgZBMgZIApgOIAPgGQANgGAGgEIABgBQAJgJANgFQAbgMAdAIQAeAIAOAXQAPAYgMAgQgOAkgjAXQgXAPgwARQhKAch3AcQkeBDjTAHIgEAAQgfAAgTgWg');
        this.shape_46.setTransform(70,-2.9);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_47.setTransform(49.6,147.9);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_48.setTransform(-30.3,97.4);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBbgCBvA/QBtA8BIBXQBOBcgGBMQgIB2hhAXQgVAEgYAAQhHAAhagqg');
        this.shape_49.setTransform(-43.4,172.5);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQgkBEhDAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_50.setTransform(103.3,86.2);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAZgDArQg8A4h4AiQhoAfhkAAIgcgBg');
        this.shape_51.setTransform(40.3,201.2);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_52.setTransform(41.3,212.8);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_53.setTransform(136.5,155.5);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_54.setTransform(-105.8,110.6);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#FFFFFF').s().p('AnDJAQhlgmCjhpQA0gjBXgtIBtg5QBLgpB1iEQCAiPAxhxQAvhrhGhEQg5g3hQAAQgyAAiVBBIiMBBIgoi0QA2g6BZgxQCzhjCzAtQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQg0AchOA1QhXA6gcAQQhSAwhHAAQglAAgigNg');
        this.shape_55.setTransform(128.4,129.9);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#FF0000').s().p('AghAqQgLgMAAgPQABgeAZgZQALgMAPAAQAPAAALAMQALAKAAAQQAAAOgLAMIgBABIACgEIgEAGIAAgBIgBACIABgBIgBABIAAAAIAAABIAAABQgCAOgJAKQgLALgOgBQgPABgMgLg');
        this.shape_56.setTransform(-103.1,-124.7);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#FF0000').s().p('AgRAnIgSgPQAAABAAAAQABAAgBAAQAAAAgBAAQgBgBgBgBIgCgBQgNgBgKgKQgLgLAAgOQAAgPALgMQAMgLAPAAQAcACAXAVIAGAFIgBAAIADACIgCgCIADADIACAAIABAAQAGAAgFABQAQgDAMAHQANAGAFAPQADAOgHAOQgIAQgPACQgLACgKAAQgaAAgRgOgAglAWIAAAAIABAAQAIAAgHgBIgCAAQgBAAAAAAQAAAAAAABQAAAAAAAAQAAAAABAAgAAYgVIAAAAIgBgBIABABg');
        this.shape_57.setTransform(-64.6,-148.7);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#FF0000').s().p('Ag3AkQgfgTATgfQATggAeATIABAAIAIgEQAVgPAagBQAOgBAMAMQALALAAAPQAAAPgLALQgKAKgPABQAGgBgHACQgBAAAAABQgBAAAAgBQgBAAABAAQAAAAAAAAIgGADIgRAKQgPAHgPAAQgTAAgTgMg');
        this.shape_58.setTransform(-113.6,-149.2);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#FF0000').s().p('AAkBQQg0gQgkgkQgsgsAVgwQAGgNAQgEQAQgDANAHQAOAIADAPQACAJgCAJIAAAAIgBgBQgDgEADAFIAAABIAAAAQABABAAAAQAAAAAAAAQAAAAAAgBQAAAAAAgBIAAABQAFAJAIAIQAVASAdAIQAiALgJAkQgIAcgWAAQgHAAgIgDgAgHgkIAAAAg');
        this.shape_59.setTransform(-108.4,-174.2);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('AgYBnQgkgKALgjQAOgrAQhbQAGgjAjAKQAjAKgFAjQgRBcgNAqQgJAbgXAAQgGAAgIgCg');
        this.shape_60.setTransform(-79.9,-179.6);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#000000').s().p('AgfAlQgNgMgFgOQgDgKADgLQADgLAHgHQALgKAVgDIABAAIACAAIABAAQAHgFAKAAQAQAAALALQAKAKAAAQQABAPgLALQgGAFgHADQgCAHgFAFQgLAKgPAAQgPAAgLgKgAgCgpIABAAIABgBIgCABg');
        this.shape_61.setTransform(-89,-151.3);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#000000').s().p('AgOBIQgQAAgLgKQgLgLABgQQAAgOAJgLIAAgBIACgQQAEgYAZgdQALgMAPABQAQAAAKALQALALAAAPQAAAPgLAMQgFAFgCAEIgCADIAAACIgBAGQgBAcgTAVQgKAKgOAAIgBAAg');
        this.shape_62.setTransform(-82,-123.8);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#000000').s().p('AgRBbQgkgJAKgjQABgEgBgGIgCgHQgDgbAAgFQADgoAfglQAXgbAaAaQAbAbgYAbQgOAQADAcQAEAkgDANQgIAbgWAAQgHAAgIgDg');
        this.shape_63.setTransform(-73.7,-99);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#FF0000').s().p('AAnEnIhJkdIhMkcQgJgjAkgKQAkgKAJAjIBLEdIBKEcQAJAjgkAKQgIACgHAAQgXAAgHgbg');
        this.shape_64.setTransform(-76.8,-113.1);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#FF0000').s().p('AiLA7QgKgkAkgHQAzgKA8gcIArgVQAagOAMgQQAWgdAbAbQAaAagWAcQgPAUgeAQQgRAKgjARQhRAlgvAJIgLABQgbAAgIgeg');
        this.shape_65.setTransform(7.7,-203.7);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#FF0000').s().p('AjEA0QAAgmAkAAQCggDCMhbQAfgUASAhQATAggeATQicBli2ADIgCAAQgiAAAAgkg');
        this.shape_66.setTransform(8.4,-184.6);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#FF0000').s().p('Aj2BaQgKgkAjgFQD5gnCah5QAcgWAaAaQAbAbgcAWQhaBGh1AvQhqArh7ATIgJABQgbAAgJggg');
        this.shape_67.setTransform(4.5,-164);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FF0000').s().p('AkeBhQAAglAkgBQDZgEEHi0QAegUATAgQATAggeAVQkYC+juAEIgBAAQgjAAAAglg');
        this.shape_68.setTransform(0.4,-141.2);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FF0000').s().p('AirA0QgKgkAjgHICEgaQBMgTAtgjQAcgWAaAaQAbAagcAXQg2AqhTAXIiVAiIgLABQgaAAgIgeg');
        this.shape_69.setTransform(-16,-111);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FFFFFF').s().p('AhXPdQizgMimgiQidgghYgnQiOg/gehvQgJgjACgkIAFgdQkXkbh+oAQgvi+gPi0QgOicAPhHQAUhoDHg0QBjgaBfgFQCiAMEKAFQIVAKINghQINgiCgBrQAyAhAHAsQAEAVgGAPIgUDXQgZD6gdCvQgdCuioFEQhUChhPB/IgBAWQgFAdgWAgQhGBojQBkQixBUkyAAQhYAAhigHg');
        this.shape_70.setTransform(3.8,126.3);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.f('#FFFFFF').s().p('Am7XfQmcgbkghmQgGiHASitQAllZB9i7IAUg2QgJhQAFhVQAIiqA+gZIA2gNQAehEA7hRQB2ijCShDIGxxMQAJgjANgjQAahGAZAAQAZAABJH0QAlD6AfD6IAoD3IASADQAWAFAZALQBQAjBLBPQBLBPCHD3QBGCABQCeQAUAlCDDQQB/DeAdCZIARBaQANBhgRAoQgQAmjxAyQkDA2lJAaQjgASjQAAQibAAiTgKg');
        this.shape_71.setTransform(9.7,-79.2);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-258.2,360.9,516.5);


    (lib.Анимация2 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('AgSBFQhRgRgzg7QgYgcAbgaQAagbAYAcQAnAuA6AMQA1AKA+gSQAjgKAKAjQAKAkgjAKQgwAOguAAQgeAAgdgGg');
        this.shape.setTransform(-84.1,-48);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('ABWBqQhEgSg1goQg3gogkg6QgSgfAggTQAggTATAfQA3BcBwAeQAjAKgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_1.setTransform(-79.1,-69.5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AgFBPQgMgHgGgPQgZg+gBgqQgBgkAlAAQAlAAAAAkQABAUAHAXIAQApQAGANgJAPQgJAOgOAEIgKABQgKAAgHgFg');
        this.shape_2.setTransform(-85.3,-40.6);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('AghBtQgOhKAAgoQAAg+AagxQARggAfATQAhATgRAgQgTAjACAvQABAZALA8QAGAjgkAKQgIADgFAAQgXAAgFgcg');
        this.shape_3.setTransform(-81.7,-56.7);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('Ai2CoQgKgkAjgHQALgCAMgTIARgdIAcgzQAQgdAOgUQBTiCBzgnQAigMAKAkQAKAkgiAMQg9AVgxA2QglApgmBHIgrBOQgfAuglAIIgLABQgZAAgJgeg');
        this.shape_4.setTransform(-64.4,-87.5);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('AADAtQgEgVgNgLQgKgKgQAAQgkgBAAglQAAglAkABQAqABAfAcQAfAaALApQAKAjgkAKQgIACgHAAQgYAAgHgbg');
        this.shape_5.setTransform(-103.5,44);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#FF0000').s().p('AgvBgQgkgKAIgkQAShPBOg+QAMgJAQAFQAQAFAHAMQAIAOgEAPQgEANgNAJQgyAmgMA7QgGAcgXAAQgHAAgIgCg');
        this.shape_6.setTransform(12.4,-235.5);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#FF0000').s().p('AAKBRQgjgUgVgjQgUgigBgqQgBglAlABQAlAAAAAkQACAuAoAUQAgASgTAgQgMAVgSAAQgJAAgMgGg');
        this.shape_7.setTransform(0.5,-235.3);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#FF0000').s().p('AgtB1QgSg1AMhEQALg5Acg8QAPghAgATQAgATgPAhQgWAtgIArQgKA1AOAnQALAigkAKQgHACgHAAQgXAAgJgag');
        this.shape_8.setTransform(8.5,-243.1);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#000000').s().p('Ag7BRQgMgHgFgQQgFgQAJgMQA1hEAhgjQAZgaAbAbQAaAagZAaQgiAigpA2QgJANgNAEQgGABgFAAQgJAAgJgFg');
        this.shape_9.setTransform(34.4,-134.7);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('AgiAlQgkAAAAglQAAglAkAAIBFAAQAkAAAAAlQAAAlgkAAg');
        this.shape_10.setTransform(-80.1,47.6);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('AhzAmQgigNAKgjQAKgkAiANQAuATAugFQAqgFAugZQAggRASAgQATAggfARQhMAphHAAQgvAAgsgSg');
        this.shape_11.setTransform(-27,53.9);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#000000').s().p('Aj0BLQgigNAKgkQAKgjAiAMQA+AWBGgcQApgRBRguQCIg+BfByQAXAagbAbQgaAagXgcQgYgdgUgLQgcgSgeAHQgYAGgeANIgyAcQhDAmg0AMQgaAGgZAAQgnAAglgOg');
        this.shape_12.setTransform(50.1,50.6);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#000000').s().p('AgcA5QgNgIgEgPQgDgOAHgOIACgFIAGgOQAFgQAHgOQAGgNAQgEQAQgEAMAIQAOAIADAOQADAOgHAPIgEAIIgDALQgGARgHANQgGANgQAEIgJABQgKAAgJgFg');
        this.shape_13.setTransform(118.3,38.7);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#000000').s().p('AiYBNQgggTAVgdQBAhVBSgVQBYgYBUA+QAcAVgSAgQgTAggdgVQg8grg5ANQg1ALgtA9QgNASgRAAQgLAAgNgIg');
        this.shape_14.setTransform(89.6,45.7);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#E6E7E8').s().p('AlJBbQgigBgUgYQgSgWAAgeQAAgeASgUQAUgWAiABQDuAJBUgCQC3gECGgoQAhgKAZARQAXAPAIAdQAIAdgMAZQgNAcghAKQiOArjDAGIg4ABQhmAAi3gIg');
        this.shape_15.setTransform(25.5,5.9);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('AiuBDQgjgEgBgmQAAgkAkAEQBdAJBPgIQBkgNA6grQAdgWATAgQASAhgcAUQhBAxhxAQQguAGgyAAQguAAgwgFg');
        this.shape_16.setTransform(15.9,-104.4);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('ABEC7QhAisiHigQgYgcAbgaQAagbAYAcQCTCvBHC+QANAigkAKQgIACgHAAQgYAAgKgag');
        this.shape_17.setTransform(49,-122.2);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#000000').s().p('ABhEOQhvkIiQjvQgTgfAhgTQAggTASAfQCPDwBxEIQAOAhghATQgLAGgKAAQgQAAgJgVg');
        this.shape_18.setTransform(76,-72.5);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#000000').s().p('AA0C6QgbhegohSQgshag4hCQgXgcAbgaQAKgLAOAAQAOgBALANQB5CTBBDaQAKAjgkAJQgIACgHAAQgXAAgIgag');
        this.shape_19.setTransform(-16.4,-118);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('AiEDGQgggSAVgdQAqg3BGhyQBHh0Ang1QAWgdAgATQAgASgVAdQgoA2hHBzQhGBzgpA3QgNARgRAAQgLAAgNgIg');
        this.shape_20.setTransform(-36.4,-118.4);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('AiJFoIAMiIQAHhPAKg4QAxkHB9jKQATgeAhATQAgATgTAeQh8DHgrD4QgJA0gHBJIgKB+QgDAjgmABQglAAADgkg');
        this.shape_21.setTransform(-98.7,5.3);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('ABeF4QgTi9hGi9Qg9irhqi1QgTgfAhgTQAggSASAfQBtC7BCC3QBHDFAVDIQAEAkglAAQgmgBgEgjg');
        this.shape_22.setTransform(108.1,-2);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#E6E7E8').s().p('ApeCqQgiAAgUgYQgSgVAAgeQAAgfASgUQAUgYAiABQGEAFFDg3QCTgaBZgXQCCgiBggwQAegQAeALQAaAKAQAbQAPAagGAaQgGAdgeAQQhtA2iOAnQhuAdiYAZQlKA2lfAAIg2AAg');
        this.shape_23.setTransform(53.1,30.7);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#E6E7E8').s().p('AioBwQgYgRgIgdQgIgdAMgYQAOgZAhgHQBUgQAjgMQA+gUAsgkQAZgVAcgBQAfgBAWAXQAVAUABAhQABAigXARQg/AzhLAeQg8AYhdASQgJACgIAAQgYAAgSgOg');
        this.shape_24.setTransform(95.4,-7.7);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('ABbJfIh8pWQhLlqg1jpQgIgjAkgKQAkgKAIAjQA1DpBKFsIB9JUQAHAjgkAKQgIACgGAAQgYAAgFgbg');
        this.shape_25.setTransform(20.1,-174.1);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AigIOQgkgKALgiQA3irBglAQBjlSAxiaQALgiAkAKQAkAJgLAjQgxCZhkFSQhfFBg3CrQgJAagYAAQgGAAgIgCg');
        this.shape_26.setTransform(-7.5,-184.6);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_27.setTransform(95.4,183.7);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_28.setTransform(107.7,87.8);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBECxjvDzQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_29.setTransform(122.1,137.5);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgTAAQgJAAgLgGg');
        this.shape_30.setTransform(133.4,94.9);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_31.setTransform(145.1,124.8);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#FF0000').s().p('AAUBrQgOgPhtiIQgcgkgEg3IACgxQCpALBDCzQAiBbgBBYQgtAAhHhOg');
        this.shape_32.setTransform(163.7,93.2);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAlgkAAQgsAAglAZQgLAIgKAAQgQAAgLgVg');
        this.shape_33.setTransform(-171.8,201.3);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_34.setTransform(-6.8,253.7);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#FF0000').s().p('AhMAeQAAgkAkgBQARAAAMgHIAGgGIACgCIABgCQADgHACgIQAKgjAkAKQAkAKgKAjQgLAngfAXQgfAXgqABIgBAAQgjAAAAglg');
        this.shape_35.setTransform(123.3,228.7);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_36.setTransform(-49.8,234.7);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_37.setTransform(-129.2,229.3);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_38.setTransform(-54,198.1);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgwAAQhLAAhGgMg');
        this.shape_39.setTransform(25.2,203.3);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_40.setTransform(64.9,206.2);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_41.setTransform(-29.3,208.9);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_42.setTransform(139,58.7);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_43.setTransform(125,123.6);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_44.setTransform(-102.2,120.3);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_45.setTransform(73.6,51.5);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#000000').s().p('AI2DiQjlgPiGgMQjIgTiggbQh2gUg5gNQhhgYhGgjQhmgzAAhMQAAg2A9gtQAlgcBGgdQAigOAJAlQAKAkghANQgsATgWAMQgpAZgFAaQgEATAUARQALALAYAMQAsAXBGATQCEAlDFAbQDaAeF7AYQAkADAAAlQABAkggAAIgFgBg');
        this.shape_46.setTransform(-68.3,51.7);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_47.setTransform(49.6,147);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_48.setTransform(-30.3,96.5);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBbgCBvA/QBtA8BIBXQBOBcgGBMQgIB2hhAXQgVAEgYAAQhHAAhagqg');
        this.shape_49.setTransform(-43.4,171.6);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQglBEhCAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_50.setTransform(103.3,85.4);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAZgDArQg8A4h4AiQhoAfhkAAIgcgBg');
        this.shape_51.setTransform(40.3,200.3);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_52.setTransform(41.3,212);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_53.setTransform(136.5,154.6);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_54.setTransform(-105.8,109.8);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#FFFFFF').s().p('AkZPdQiygMimgiQiegghYgnQiNg/gehvQgKgjADgkIAFgcQkXkch/oAQgvi9gPi1QgNibAOhIQAVhoDHgzQBjgaBfgFQChALELAFQIVAKIMghQINghCgBqQAyAiAIArQAEAWgHAPIgFBFIgRCvQA5gBA4AOQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQguAZiLBbQhkBDg9APIgGAKIgBAXQgGAcgVAhQhGBnjRBkQiwBUkxAAQhZAAhjgHgAQ4lKQgOBngLBDQgdCtitFMIAigSQBLgpB2iEQCAiQAxhwQAvhrhGhEQg5g3hQAAg');
        this.shape_55.setTransform(25.5,126);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#000000').s().p('AAWBGQgOgBgMgKQgPgMgNgWIgVgmQgHgOAEgOQADgPAOgIQANgHAPADQAPAEAHANIAUApIAEAGIANAKQALAKAAARQgBAQgKAKQgLALgOAAIgBAAg');
        this.shape_56.setTransform(81.8,-121.7);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#000000').s().p('AAFBAQgrgbgVg6QgFgOAJgOQAIgOAOgEQAPgFAOAIQALAIAFAOQAEANAKAOQAHAKAKAFQANAIAEAPQAEAPgIAOQgHAMgPAFIgKABQgKAAgJgGg');
        this.shape_57.setTransform(79.5,-98.7);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#FF0000').s().p('Ag4AxQgNgHgEgQQgFgQAJgLQAVgdAfgOQAhgPAhAKQAiAKgKAkQgKAjgigKQgJgEgJAGIgIAEIgCACIgGAGQgJANgNAEQgFABgGAAQgJAAgJgFg');
        this.shape_58.setTransform(65.3,-141.1);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#FF0000').s().p('AAIBGQgOgIgCgPIgFgSIgCgHIgBgCIgEgGIgBgCIgFgFIgSgKQgNgKgEgNQgFgPAIgNQAIgNAPgFQARgFAMAJIARAMQALAGAGAHQAaAZAJAtQACAQgGANQgIANgPAEIgJACQgJAAgKgFg');
        this.shape_59.setTransform(106.3,-135.2);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('AAVBIIgggRQgSgLgLgLQgWgVgNgpQgFgPAIgOQAIgNAPgEQAPgEANAHQANAIAFAPQACAKACAFIAEAHIADAFIgCgDIACADIAGAFIAsAYQAgARgSAgQgNAWgSAAQgJAAgLgGgAAIgWIAAAAg');
        this.shape_60.setTransform(67,-170.4);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#FF0000').s().p('AAdAvQgRgVgMgFQgPgIgUAJQgPAHgOgDQgOgDgIgOQgIgMAEgPQAEgRANgGQAxgWAhAIQAmAJAiApQAKALABAOQAAAQgLALQgKAKgQABIgBAAQgQAAgJgLg');
        this.shape_61.setTransform(110.4,-163.5);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#FF0000').s().p('Ag+A/QgDghAWgpQAcgtAKgXQAFgOAQgDQAQgEANAIQAOAIADAOQADAOgHAPIgeAyQgSAfACAXQADAkgkAAQglgBgEgjg');
        this.shape_62.setTransform(91.5,-182.7);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#000000').s().p('AgVArQgPgDgIgOQgIgPAFgNQAEgPAOgHQAFgDAFgBIACgCIALgIQAMgIAQAEQAPAEAHANQAJANgEAOQgFAPgNAIIgCABIAEgDIgHAGIADgDQgIAIgFAEQgLAHgNAAIgNgCg');
        this.shape_63.setTransform(86.2,-152.5);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#FF0000').s().p('AgtE7QgkgKAGgjQAmjeAnlJQAEgjAmAAQAlAAgEAjQgsFngkDUQgFAbgXAAQgHAAgHgCg');
        this.shape_64.setTransform(81.9,-110.4);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#FF0000').s().p('AhiAzQghgPATggQATgfAgAOQAgAOAdgLQAbgKAUgdQAVgeAgATQAhATgVAeQgoA3g2ARQgVAHgXAAQgjAAglgRg');
        this.shape_65.setTransform(4.5,-195.3);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#FF0000').s().p('AiMBCQgjgKAKgkQAKgjAjAKQA8ARA7gQQA4gQAvgtQAbgZAaAaQAaAbgaAZQg+A5hMAWQgmALgnAAQgoAAgogMg');
        this.shape_66.setTransform(1.6,-172.5);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#FF0000').s().p('Ai+BXQgigLAJgkQAKgkAjALQBcAfBVgaQBZgaAyhRQATgfAhATQAgATgTAfQg/BlhxAlQg1ASg3AAQg6AAg7gUg');
        this.shape_67.setTransform(0.6,-151.6);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FF0000').s().p('AjDCcQgkgCAAgmQAAglAkACQCDAIBbguQBug1AWh2QAHgjAkAKQAkAJgHAkQgaCNiEBGQhjA2iEAAIglgBg');
        this.shape_68.setTransform(7,-136.4);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FF0000').s().p('AiQBwQgKgkAjgHQBJgPAxgxQAxguAWhJQAKgjAkAKQAkAKgKAjQgbBbg/A9QhABAhbATIgLABQgaAAgIgeg');
        this.shape_69.setTransform(19.5,-117.9);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.f('#FFFFFF').s().p('ArJWJQlug2grgzQgkgqAQiXQALhvAYhWQAghwAthuQBHiwA5gqIgUhfIA3gZQgGgkgDguQgGhdAPgyQAOgqA0hGQA4hIANgjQAVg1BJguQBEgsAhAIICCiqQASglAWgmQAthLAZAAQAwiHAyiWQBmkuAPhOQAVhqAahHQAdhUAXgEQAUgDAXCKQAeC5AMAoQAjBzBfIDIAcgtQAigtAcAAQAdAAB6DRQASAeCID2QASAgCFEQQB3D1AUAZQAZAgBfD/QBcD3AIAuQAPBPgFApQgLBThKAgQhsAvkeAwQldA7lkAGIg2ABQkXAAlXgzg');
        this.shape_70.setTransform(5.8,-81.7);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-257.4,360.9,514.8);


    (lib.Анимация1 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f('#000000').s().p('Ag8BdQgQgBgLgKQgLgLABgPQAAgOAKgNQAUgaAmgeQA1gtAIgIQAbgZAaAbQAbAagbAZIg+A0QglAfgUAaQgIALgRAAIgBAAg');
        this.shape.setTransform(36.5,-134.2);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f('#000000').s().p('AgiAlQgkAAAAglQAAgkAkAAIBFAAQAkAAAAAkQAAAlgkAAg');
        this.shape_1.setTransform(-95.1,48.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f('#000000').s().p('AhzAmQgigNAKgjQAKgkAiANQAuATAugFQAqgFAugZQAggRASAgQATAggfARQhMAphHAAQgvAAgsgSg');
        this.shape_2.setTransform(-44,53.1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f('#000000').s().p('Aj9A8QghgPATggQATgfAhAOQA6AZBNgdQArgQBUgqQCCgvBaB6QAVAdggASQggATgWgdQgpg4g7AJQgaADhWAoQhDAfg1AGIgbABQgxAAgvgUg');
        this.shape_3.setTransform(19.9,53.6);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f('#000000').s().p('AgcA5QgNgIgEgPQgDgOAHgOIACgFIAGgOQAFgQAHgOQAGgNAQgEQAQgEAMAIQAOAHADAPQADAOgHAPIgEAIIgDALQgGARgHANQgGANgQAEIgJABQgKAAgJgFg');
        this.shape_4.setTransform(118.3,39.9);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f('#000000').s().p('AifBFQgagbAXgcQBFhQBVgNQBXgNBRBDQAbAXgaAaQgaAagcgXQg4gug7APQg0ANgxA6QgMAPgNAAQgMAAgNgNg');
        this.shape_5.setTransform(60,51.3);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f('#FF0000').s().p('AAFBWQgdgsgOgZQgXgngMgjQgMgiAkgKQAkgKAMAiQAKAeAVAjIAnA8QAUAfggASQgMAHgLAAQgRAAgMgSg');
        this.shape_6.setTransform(-3.4,-237.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f('#FF0000').s().p('AhLBmQgChBAWg4QAXg7ArguQAZgaAbAaQAaAbgZAaQhDBGACBnQABAkglAAQglAAgBgkg');
        this.shape_7.setTransform(5.6,-244.8);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f('#FF0000').s().p('AhBBZQgkgKALgiQARgxAjgiQAjgiAygPQAigMAKAlQAKAjgjAMQhBATgTA9QgIAagYAAQgHAAgIgCg');
        this.shape_8.setTransform(9.9,-237.5);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f('#E6E7E8').s().p('AmUBNQgigDgUgZQgRgWgBgeQAAgeASgTQAUgWAiADQC7AODZgFQCvgFDmgTQAigDAUAVQASAUAAAeQAAAdgSAXQgUAZgiADQjlATiwAFQg9ACg8AAQiVAAiGgLg');
        this.shape_9.setTransform(-0.3,8);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f('#000000').s().p('AjJA6QgkgEAAgmQAAgkAkAEQBoAMBggKQBrgKBXgkQAhgNAKAkQAKAkghAMQhbAmhyALQguAEgwAAQg5AAg6gGg');
        this.shape_10.setTransform(12.5,-103.3);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f('#000000').s().p('AA7DLQgwh6ghhHQgyhog2hNQgUgfAggSQAggSAVAdQA4BRAyBuQAhBFA1CEQANAhgkAKQgIACgHAAQgYAAgKgZg');
        this.shape_11.setTransform(51.8,-120.5);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f('#000000').s().p('ABhEOQhvkIiQjvQgTgfAhgTQAggTASAfQCPDwBxEIQAOAhghATQgLAGgKAAQgQAAgJgVg');
        this.shape_12.setTransform(76,-71.3);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f('#000000').s().p('AA1DDQg8jOhviaQgVgdAggTQAhgTAVAeQBxCfBBDaQAKAjgkAJQgIACgHAAQgXAAgIgag');
        this.shape_13.setTransform(-28.6,-119.7);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f('#000000').s().p('AiADPQghgSAQghQBUiuCUi2QALgOAPABQANAAALALQAaAbgXAbQiMCrhQCrQgKAUgRAAQgJAAgMgHg');
        this.shape_14.setTransform(-48.7,-120.7);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f('#000000').s().p('AhyEkQgkgKAIgjQAhiWAxh5QA2iFBPh2QAUgeAgATQAgASgUAeQhqCfgrB+QgbBNgfCPQgHAbgXAAQgGAAgIgCg');
        this.shape_15.setTransform(-70.5,-77.4);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f('#000000').s().p('AigGbIAJiZQAFhdAKg8QA2kuCrjoQAVgdAgATQAhATgWAdQinDkgyEgQgJA4gEBXIgICPQgEAkglAAQglAAADgkg');
        this.shape_16.setTransform(-96.3,1.4);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f('#000000').s().p('ABeF4QgTi9hGi9Qg9irhqi1QgTgfAhgTQAggSASAfQBtC7BCC3QBHDFAVDIQAEAkglAAQgmgBgEgjg');
        this.shape_17.setTransform(108.1,-0.8);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f('#E6E7E8').s().p('Ar9CZQghgDgUgZQgSgXAAgeQAAgfARgTQAUgWAiADQHPArG3g5QC4gYB0gaQCngnB5g9QAegPAeALQAaAKAQAaQAPAagGAbQgGAdgeAPQiHBEi1ArQiEAfjHAaQkKAjkMAAQi/AAjBgSg');
        this.shape_18.setTransform(37.3,31.9);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f('#E6E7E8').s().p('Aj0CCQgXgRgJgdQgIgdAMgYQAOgaAhgGQCXgdA2gQQByghBKg1QAYgSAgAKQAfAJAPAaQAQAbgJAdQgIAbgaATQhQA6h+AlQhEAUiaAeQgIACgJAAQgXAAgTgOg');
        this.shape_19.setTransform(87.8,-4.8);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f('#000000').s().p('ABwJaQgvjXhil6QhtmggoivQgIgjAkgKQAkgKAIAjQAoCwBsGgQBjF6AvDWQAHAkgkAKQgIACgGAAQgYAAgFgcg');
        this.shape_20.setTransform(18,-172.5);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f('#000000').s().p('AicIvQgkgJAMgjQBAi4BVlTQBblzAxibQALgiAkAKQAkAKgLAiQgxCbhcFyQhUFUhAC4QgJAagXAAQgIAAgIgCg');
        this.shape_21.setTransform(-11.3,-179.3);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f('#000000').s().p('AiTASQgcgVATggQASggAdAVQAsAiBBgEQA7gDAxggQAegTATAgQATAhgeASQhBAqhRADIgOABQhPAAg2gpg');
        this.shape_22.setTransform(-93.9,-49.8);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f('#000000').s().p('AB0BbQhJgJg/geQg9gdg5gyQgbgYAbgaQAagaAbAXQBeBUBrANQAkAEAAAlQAAAigdAAIgHgBg');
        this.shape_23.setTransform(-90.7,-69.2);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f('#000000').s().p('AhIBAQgmgOgRgfQgTggANgkQAMgiAkAJQAkAKgMAiQgGAQASAJQANAGATAAQA0ADAwgdQAfgTATAgQATAfgfATQgtAcgyAIQgRADgPAAQgjAAgfgNg');
        this.shape_24.setTransform(95.4,184.9);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f('#000000').s().p('AAtCSQg5gCgkgmQgmgnAGg7QAFgyAWgnQAagtAqgQQAigLAKAkQAKAkgiALQgRAGgNAcQgJAYgBAUQgCAeALAPQAMARAdABQAkABAAAmQAAAkgiAAIgCAAg');
        this.shape_25.setTransform(107.7,89);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f('#000000').s().p('AlPF1QgTggAfgSQDqiFCmijQBlhiArhFQBHhzgkhcQgNgiAkgKQAkgJANAhQBFCyjwDyQi5C8kBCTQgLAGgKAAQgRAAgMgVg');
        this.shape_26.setTransform(122.1,138.7);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f('#000000').s().p('ABtAyQg6geg4gBQg2gBg+AaQghAOgKgkQgKgjAhgOQBKgfBHAEQBHADBIAmQAgAPgTAhQgMAVgSAAQgKAAgLgGg');
        this.shape_27.setTransform(133.4,96.1);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f('#000000').s().p('AlaIxQgSghAfgSQDxiOB6huQDBitAti+QAWhggbhWQgchdhOg2QhXg7hpgKQhlgJhmAmQgiAMgJgjQgKgkAhgNQBygrB3ALQB+ALBdBFQBYBAApBeQApBegOBsQgeDejTDJQiKCEkLCeQgLAGgJAAQgRAAgNgUg');
        this.shape_28.setTransform(145.1,126);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f('#FF0000').s().p('AAUBrQgOgPhtiIQgcgkgEg3IACgxQCpALBDCzQAiBbgBBYQgtAAhHhOg');
        this.shape_29.setTransform(163.7,94.4);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f('#FF0000').s().p('AhOAhQgTggAegTQA1giBBgBQAkAAAAAlQAAAlgkAAQgsAAglAZQgLAIgKAAQgQAAgLgVg');
        this.shape_30.setTransform(-171.8,202.5);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f('#FF0000').s().p('AgnAlQgkAAAAglQAAgkAkAAIBPAAQAkAAAAAkQAAAlgkAAg');
        this.shape_31.setTransform(-6.8,254.9);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f('#FF0000').s().p('AhMAeQAAgkAkgBQATAAAMgIQgFADAJgIIACgCIABgCQADgHACgIQAKgjAkAKQAkAKgKAjQgLAngfAXQgfAXgqABIgBAAQgjAAAAglg');
        this.shape_32.setTransform(123.3,229.9);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f('#FF0000').s().p('AgQBAQhHgPgwg0QgYgbAbgaQAagbAYAbQAjAnAxAKQAvAIA0gRQAigLAKAjQAKAkgiALQgrAPgqAAQgbAAgZgGg');
        this.shape_33.setTransform(-49.8,235.9);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f('#FF0000').s().p('AAfA9QgMgIgSgRIgcgbQgYgSgaANQgVAMgPAbQgRAggggTQgggTARggQAZguAsgWQAwgYAxAVQAPAHARAOIAdAaQAWAUALgDQAKgDATgdQAUgeAgATQAgATgUAdQgdAtghASQgTALgTAAQgXAAgWgQg');
        this.shape_34.setTransform(-129.2,230.5);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f('#000000').s().p('ABPBHQhwgehJgqQgfgTATggQATggAfASQAlAXAsAQQAiAOA0ANQAjAJgKAkQgIAcgXAAQgHAAgHgCg');
        this.shape_35.setTransform(-54,199.3);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f('#000000').s().p('AjuA1QgkgGAKgkQAKgjAkAGQBmASB3gNQBwgMBqglQAigLAKAkQAKAkgiAKQhyAnh8AMQgwAFgvAAQhMAAhGgMg');
        this.shape_36.setTransform(25.2,204.5);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f('#000000').s().p('AlMC7QAAglAkgCQBzgIBnggQBugjBYg9QCahrgRhPQgHgkAkgJQAkgKAHAjQAPBIg2BMQgoA3hKA2QhjBIh9ApQh0AmiEAIIgEAAQggAAAAgjg');
        this.shape_37.setTransform(64.9,207.4);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f('#000000').s().p('AjlBwQiCgwhHg1QhshPgDhoQgBgkAmAAQAlAAABAkQACBIBfA+QBeA9CGAnQEpBYFegUQAkgCAAAlQAAAmgkACQhHAEhEAAQlQAAkEhhg');
        this.shape_38.setTransform(-29.3,210.1);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f('#000000').s().p('AgqBhQAKhkAChdQABgkAkAAQAlAAgBAkQgCBmgJBbQgEAjgkABQglAAADgkg');
        this.shape_39.setTransform(139,59.9);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f('#000000').s().p('AhkErQgggTAPggQBwjuA3kgQAHgjAkAKQAkAKgHAjQg5Eih2D+QgJAUgRAAQgJAAgMgHg');
        this.shape_40.setTransform(125,124.8);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f('#000000').s().p('ADaLGQj2kCiCmIQhzlZgJmRQgBgkAlAAQAmAAABAkQAIGBBuFMQB7F7DtD3QAZAagbAbQgNANgNAAQgNAAgMgNg');
        this.shape_41.setTransform(-102.2,121.5);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.f('#000000').s().p('AqRDlQgkgBAAglQAAglAkAAQFpABGLhAQBrgSBHgPQBhgVBNgaQAugOAYgKQAngQAZgUQAsghgUgfQgPgWg1gYQghgOATggQATghAgAPQB4A1gEBSQgDAqgmAmQgeAcguAWQhOAlhkAbQhKAThwAVQm7BTmcAAIgPAAg');
        this.shape_42.setTransform(73.6,52.7);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.f('#000000').s().p('AI2DiQjlgPiGgMQjIgTiggbQh2gUg5gNQhhgYhGgjQhmgzAAhMQAAg2A9gtQAlgcBGgdQAigOAJAlQAKAkghANQgsATgWAMQgpAZgFAaQgEATAUARQALALAYAMQAsAXBGATQCEAlDFAbQDaAeF7AYQAkADAAAlQABAkggAAIgFgBg');
        this.shape_43.setTransform(-68.3,52.9);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.f('#FF0000').s().p('AhCD0Qh6gBhNhDIg1hOQgvhoBLhZQBDhQCGgoQCBgoBzATQB9AVAmBTQArBdg1BbQgwBThsA3QhqA2huAAIgCAAg');
        this.shape_44.setTransform(49.6,148.2);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.f('#FF0000').s().p('ABnEgQiqgFiCiGQh0h4gOiIQgGg5Ajg1QAlg4A6gPIBuAKIDRAoQA7AXA1A3QAzAzAYA6QA0B+gyBoQg1BuiKAAIgLgBg');
        this.shape_45.setTransform(-30.3,97.7);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.f('#FF0000').s().p('AgIDbQh2g4hVhdQhchjAAhZIAOg1QAjhXBmgCQBbgCBvA/QBtA8BIBXQBOBcgGBMQgIB2hhAXQgVAEgYAAQhHAAhagqg');
        this.shape_46.setTransform(-43.4,172.8);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.f('#FF0000').s().p('AiREPQg/gagcg+QgzhwA/iCQA4h0Bpg9ICFghQBugkAzAoQA0AngoBaQgYABgIADIgPAJQgLAIgmAGQgyAIgVAGQhaAbgJBUQgGBFBeAbQBQAWBOgSQglBEhCAtQg/ArhHAMQgYADgVAAQgvAAgngQg');
        this.shape_47.setTransform(103.3,86.6);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.f('#FF0000').s().p('AibCFQgxgCgfgZIgWgaQAeh2CUg8QCBg0BpAYQBKASAVA1QAKAZgDArQg8A4h4AiQhoAfhkAAIgcgBg');
        this.shape_48.setTransform(40.3,201.5);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.f('#FF0000').s().p('AkEBwQgVgagEglQgFgoATgeQAWgkAwgMIBRgFQBagGAtgJQAqgIA+gSIBGgUQAYgFAbAWQAbAWAMAjQAgBYhdAyQg0AKhQANQieAaiJALIgFAAQgaAAgUgZg');
        this.shape_49.setTransform(41.3,213.2);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.f('#FF0000').s().p('AioCnQgpghAhhTQAghOBGg6QBghTBZgHQAjgDAXAKQAVAKACASQADAsg3A6QgeAgg/A5QgtAygzAkQg3AogjAAQgRAAgMgKg');
        this.shape_50.setTransform(136.5,155.8);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.f('#FF0000').s().p('AgZD5Igxg6IhXjrQghh6AbhEQAXg8A8gCQA6gCA9AzQBCA2AkBYQA/CXgeB8QgaBuhJAQIgLABQglAAgwgwg');
        this.shape_51.setTransform(-105.8,111);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.f('#FFFFFF').s().p('AnDJAQhlgmCjhpQA0gjBXgtIBtg5QBLgpB1iEQCAiPAxhxQAvhrhGhEQg5g3hQAAQgyAAiVBBIiMBBIgoi0QA2g6BZgxQCzhjCzAtQC+AvBHCLQAwBegLB5QgIBYifDDQinDNiIBHQg0AchOA1QhXA6gcAQQhSAwhHAAQglAAgigNg');
        this.shape_52.setTransform(128.4,130.3);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.f('#000000').s().p('AgGA8QgSgPgJgRIgSgqQgHgPADgNQAEgPANgIQANgIAQAEQAPAEAGANIANAdIABAEIAEAIIAGAKIAJAJQALAKAAAQQAAAQgLAKQgKALgQAAQgPAAgKgLg');
        this.shape_53.setTransform(102.8,-114.4);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.f('#000000').s().p('AADBCQgsgggQg5QgJgjAkgKQAjgKAJAjQAFAQAIANIAFAHIACACQALAJgEgDQANAJAEAOQAEAPgIANQgHANgQAFIgKABQgKAAgIgFg');
        this.shape_54.setTransform(100,-87.2);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.f('#000000').s().p('AgDA9QgOgOgOgWIgWgmQgRggAggSQAggTARAfIARAgQAJARAMAKQALAKAAAQQgBAQgKALQgLALgPAAQgQgBgKgKg');
        this.shape_55.setTransform(97.7,-66.1);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.f('#FF0000').s().p('AghA5QgPgEgHgNQgIgNAEgPQAEgOANgIIAEgDIABAAIADgDIALgNQAPgPANgJQANgIAPAFQAPAEAIANQAIANgEAPQgFAOgNAIIgEADIgPAPQgMAPgPAJQgJAGgKAAIgKgCg');
        this.shape_56.setTransform(82.3,-135.3);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.f('#FF0000').s().p('AAQAzQgPAAgKgLIgggcQgMgKABgPQAAgPALgMQALgLAPABQAPAAALAKIAfAdQALAKAAAPQAAAQgLAKQgKALgPAAIgBAAg');
        this.shape_57.setTransform(124.5,-132.2);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.f('#FF0000').s().p('AAbBcQhUg2gYhZQgKgjAkgKQAkgKAKAjQAQA/A6AjQAfATgTAhQgMAUgQAAQgKAAgMgHg');
        this.shape_58.setTransform(91.8,-169.8);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.f('#FF0000').s().p('AhFAmQgOgIgEgOQgEgQAIgLQAHgNAPgFQA5gWBCANQAjAHgKAjQgJAkgkgHQgogIgrARQgFACgGAAQgIAAgJgGg');
        this.shape_59.setTransform(128.4,-159.5);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.f('#FF0000').s().p('AgHBeQgmAAgCgkQgFg8AhhIQAGgOAQgDQAQgEAMAHQAOAIADAPQADAOgGAPQgWAsAEAyQACAkgjAAIgBAAg');
        this.shape_60.setTransform(115.1,-178.5);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.f('#000000').s().p('AgWAqQgOgEgIgOQgIgNAEgOQAEgPAOgIQAHgDAHgBIALgIQAMgHAPADQAPAEAIANQAHANgDAPQgEAPgOAHIgBACIABgCIgIAFIAHgDQgKAIgGADQgNAGgKAAQgHAAgGgCg');
        this.shape_61.setTransform(106.3,-148.3);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.f('#FF0000').s().p('AhHG4QAfklANiSQAXj7ACi9QABgjAkgBQAmABgBAjQgCC9gXD7QgNCSgfElQgDAjglABQglAAADgkg');
        this.shape_62.setTransform(101.7,-90.5);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.f('#FF0000').s().p('AiEBMQAAgmAkAAQA4gBAvgXQA+gdgMgxQgJgjAkgKQAkgKAJAjQAXBZhUA5QhFAwhfACIgCAAQgiAAAAgkg');
        this.shape_63.setTransform(1.1,-193.1);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.f('#FF0000').s().p('AizB2QgjgJAKgkQAKgkAjAJQBQAVBcgxQBagvAjhMQAPghAgATQAgATgPAgQgsBhh0A6QhPAohKAAQgjAAghgJg');
        this.shape_64.setTransform(0.3,-169.9);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.f('#FF0000').s().p('AjZByQgggQATggQASggAhAPQAsAWA/gOQAwgLA1geQA1gcAbgWQAugkAKgmQAJgiAkAKQAkAJgJAjQgPA3g6AxQgtAnhAAfQhhAxhPAAQg0AAgsgVg');
        this.shape_65.setTransform(0.3,-148.8);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.f('#FF0000').s().p('AjvCXQgjgJAKgkQAKgkAjAJQB2AdCBg+QCJhBAdhzQAIgjAkAKQAkAKgIAjQgkCQilBQQhqA0hmAAQgxAAgvgLg');
        this.shape_66.setTransform(2.4,-129.1);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.f('#FF0000').s().p('AicBmQABglAjgDQBPgIA1gmQA9grAJhJQAEgkAlAAQAmAAgEAkQgNBphQA/QhIA6hwAKIgGABQgeAAAAgjg');
        this.shape_67.setTransform(17.4,-113.5);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.f('#FFFFFF').s().p('Ax+UzQAAhQAHhuQANjcAiiWQAhiWBdi7IBXidQAXhlAph3QBSjuBXhXQAshXAzhWQBoiuApAAQAqAAAXBQQAMAoADAoQA8j/BFkAQCLn/AugFQAsgEBwHVQA4DqAvDrIApghQAwgiAlAAQAmAABYDMQAsBlAlBmIClEvQCqE4AZAlQAhAyBWDzQBaD8AEBLQAEBDgIBqQgEA2gFAoQkbBVmsAqQj0AZjoAAQpIAAoDiYg');
        this.shape_68.setTransform(5.4,-75.4);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.f('#FFFFFF').s().p('AhXPdQizgMimgiQidgghYgnQiOg/gehvQgJgjADgkIAEgcQkXkch+oAQgvi9gPi1QgOibAPhIQAVhoDGgzQBjgaBfgFQCiALELAFQIUAKINghQINghCgBqQAyAiAHArQAEAWgGAPIgTDXQgZD6geCvQgdCuioFDQhUCihPB+IgBAXQgFAcgWAhQhGBnjQBkQixBUkyAAQhYAAhigHg');
        this.shape_69.setTransform(6.2,127.2);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-180.4,-258.6,360.9,517.2);


// stage content:
    (lib.Безымянный1 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Слой 1
        this.instance = new lib.Анимация1('synched',0);
        this.instance.parent = this;
        this.instance.setTransform(67.9,88.9,0.337,0.337,0,0,0,0.1,0.1);

        this.instance_1 = new lib.Анимация2('synched',0);
        this.instance_1.parent = this;
        this.instance_1.setTransform(67.9,89.4,0.337,0.337,0,0,0,0.1,0.5);
        this.instance_1._off = true;

        this.instance_2 = new lib.Анимация3('synched',0);
        this.instance_2.parent = this;
        this.instance_2.setTransform(67.9,89,0.337,0.337,0,0,0,0.1,0.1);
        this.instance_2._off = true;

        this.instance_3 = new lib.Анимация4('synched',0);
        this.instance_3.parent = this;
        this.instance_3.setTransform(67.9,89.7,0.337,0.337,0,0,0,0.1,0.3);
        this.instance_3._off = true;

        this.instance_4 = new lib.Анимация5('synched',0);
        this.instance_4.parent = this;
        this.instance_4.setTransform(67.9,89.2,0.337,0.337,0,0,0,0.1,0.1);
        this.instance_4._off = true;

        this.instance_5 = new lib.Анимация6('synched',0);
        this.instance_5.parent = this;
        this.instance_5.setTransform(67.9,89.5,0.337,0.337,0,0,0,0.1,0.1);
        this.instance_5._off = true;

        this.instance_6 = new lib.Анимация7('synched',0);
        this.instance_6.parent = this;
        this.instance_6.setTransform(66.1,88.8,0.337,0.337,0,0,0,0.3,0.3);
        this.instance_6._off = true;

        this.instance_7 = new lib.Анимация8('synched',0);
        this.instance_7.parent = this;
        this.instance_7.setTransform(67.9,88.8,0.337,0.337,0,0,0,0.1,0.3);
        this.instance_7._off = true;

        this.instance_8 = new lib.Анимация9('synched',0);
        this.instance_8.parent = this;
        this.instance_8.setTransform(67.9,89.3,0.337,0.337,0,0,0,0.1,0.3);
        this.instance_8._off = true;

        this.instance_9 = new lib.Анимация10('synched',0);
        this.instance_9.parent = this;
        this.instance_9.setTransform(67.9,89.3,0.337,0.337,0,0,0,0.1,0.3);
        this.instance_9._off = true;

        this.instance_10 = new lib.Анимация11('synched',0);
        this.instance_10.parent = this;
        this.instance_10.setTransform(67.9,89.5,0.337,0.337,0,0,0,0.1,0.1);
        this.instance_10._off = true;

        this.instance_11 = new lib.Анимация12('synched',0);
        this.instance_11.parent = this;
        this.instance_11.setTransform(67.9,89.3,0.337,0.337,0,0,0,0.1,0.3);
        this.instance_11._off = true;

        this.instance_12 = new lib.Анимация13('synched',0);
        this.instance_12.parent = this;
        this.instance_12.setTransform(67.9,88.7,0.337,0.337,0,0,0,0.1,0.3);
        this.instance_12._off = true;

        this.instance_13 = new lib.Анимация14('synched',0);
        this.instance_13.parent = this;
        this.instance_13.setTransform(71.8,89.3,0.34,0.337,0,0,0,0.5,0.3);
        this.instance_13._off = true;

        this.instance_14 = new lib.Анимация15('synched',0);
        this.instance_14.parent = this;
        this.instance_14.setTransform(73.3,89.2,0.337,0.337,0,0,0,0.3,0.1);
        this.instance_14._off = true;

        this.instance_15 = new lib.Анимация16('synched',0);
        this.instance_15.parent = this;
        this.instance_15.setTransform(70.9,89.6,0.337,0.337,0,0,0,0.3,0.1);

        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).wait(1));
        this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,regY:0.5,y:89.4},1).wait(15));
        this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:false},1).to({_off:true,regY:0.1,y:89},1).wait(14));
        this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({_off:false},1).to({_off:true,regY:0.3,y:89.7},1).wait(13));
        this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off:false},1).to({_off:true,regY:0.1,y:89.2},1).wait(12));
        this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off:false},1).to({_off:true,y:89.5},1).wait(11));
        this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off:false},1).to({_off:true,regX:0.3,regY:0.3,x:66.1,y:88.8},1).wait(10));
        this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(5).to({_off:false},1).to({_off:true,regX:0.1,x:67.9},1).wait(9));
        this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},1).to({_off:true,y:89.3},1).wait(8));
        this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(7).to({_off:false},1).to({_off:true},1).wait(7));
        this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(8).to({_off:false},1).to({_off:true,regY:0.1,y:89.5},1).wait(6));
        this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(9).to({_off:false},1).to({_off:true,regY:0.3,y:89.3},1).wait(5));
        this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(10).to({_off:false},1).to({_off:true,y:88.7},1).wait(4));
        this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(11).to({_off:false},1).to({_off:true,regX:0.5,scaleX:0.34,x:71.8,y:89.3},1).wait(3));
        this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(12).to({_off:false},1).to({_off:true,regX:0.3,regY:0.1,scaleX:0.34,x:73.3,y:89.2},1).wait(2));
        this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(13).to({_off:false},1).to({_off:true,x:70.9,y:89.6},1).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(77,91.1,121.7,174.5);
// library properties:
    lib.properties = {
        id: '97D47E1CED9A50418335DDF43CFA5FA2',
        width: 140,
        height: 179,
        fps: 13,
        color: '#FFFFFF',
        opacity: 1.00,
        webfonts: {},
        manifest: [],
        preloads: []
    };



// bootstrap callback support:

    (lib.Stage = function(canvas) {
        createjs.Stage.call(this, canvas);
    }).prototype = p = new createjs.Stage();

    p.setAutoPlay = function(autoPlay) {
        this.tickEnabled = autoPlay;
    }
    p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
    p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
    p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
    p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

    p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

    an.bootcompsLoaded = an.bootcompsLoaded || [];
    if(!an.bootstrapListeners) {
        an.bootstrapListeners=[];
    }

    an.bootstrapCallback=function(fnCallback) {
        an.bootstrapListeners.push(fnCallback);
        if(an.bootcompsLoaded.length > 0) {
            for(var i=0; i<an.bootcompsLoaded.length; ++i) {
                fnCallback(an.bootcompsLoaded[i]);
            }
        }
    };

    an.compositions = an.compositions || {};
    an.compositions['97D47E1CED9A50418335DDF43CFA5FA2'] = {
        getStage: function() { return exportRoot.getStage(); },
        getLibrary: function() { return lib; },
        getSpriteSheet: function() { return ss; },
        getImages: function() { return img; }
    };

    an.compositionLoaded = function(id) {
        an.bootcompsLoaded.push(id);
        for(var j=0; j<an.bootstrapListeners.length; j++) {
            an.bootstrapListeners[j](id);
        }
    }

    an.getComposition = function(id) {
        return an.compositions[id];
    }

})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function init() {
    if(!$('#canvas').length) return;
    canvas = document.getElementById('canvas');
    anim_container = document.getElementById('animation_container');
    dom_overlay_container = document.getElementById('dom_overlay_container');
    var comp=AdobeAn.getComposition('97D47E1CED9A50418335DDF43CFA5FA2');
    var lib=comp.getLibrary();
    handleComplete({},comp);
}
function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    exportRoot = new lib.Безымянный1();
    stage = new lib.Stage(canvas);
    stage.addChild(exportRoot);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener('tick', stage);
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
        var lastW, lastH, lastS=1;
        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();
        function resizeCanvas() {
            var w = lib.properties.width, h = lib.properties.height;
            var iw = window.innerWidth, ih=window.innerHeight;
            var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
            if(isResp) {
                if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                    sRatio = lastS;
                }
                else if(!isScale) {
                    if(iw<w || ih<h)
                        sRatio = Math.min(xRatio, yRatio);
                }
                else if(scaleType==1) {
                    sRatio = Math.min(xRatio, yRatio);
                }
                else if(scaleType==2) {
                    sRatio = Math.max(xRatio, yRatio);
                }
            }
            canvas.width = w*pRatio*sRatio;
            canvas.height = h*pRatio*sRatio;
            canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
            canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
            stage.scaleX = pRatio*sRatio;
            stage.scaleY = pRatio*sRatio;
            lastW = iw; lastH = ih; lastS = sRatio;
        }
    }
    makeResponsive(false,'both',false,1);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
}

$(function(){
    init();
});
// END page404
